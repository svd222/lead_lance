<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 03.07.17
 * @time: 14:57
 */
namespace frontend\components;

use common\components\StateEvent;
use common\interfaces\fms\ConfigFMSLoaderInterface;
use common\models\Batch;
use common\models\Image;
use common\models\Notification;
use common\models\Order;
use common\models\OrderReview;
use common\models\OrderTrace;
use common\models\Project;
use common\models\User;
use common\models\WsTransferObject;
use Yii;
use yii\base\Event;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use ZMQ;
use ZMQContext;
use common\models\Message;
use yii\base\Component;
use yii\db\ActiveRecord;
use common\models\ProjectMessage;
use common\models\ProjectUserRefuse;
use common\models\OrderMessage;

/**
 * Class Bootstrap contains methods for bootstrapping app
 * @package frontend\components
 */
class Bootstrap extends Component
{
    public function init()
    {
        $this->bootstrapWsMessageEventHandlers();
        $this->bootstrapWsProjectMessageEventHandlers();
        $this->bootstrapWsActionEventHandlers();
        $this->bootstrapWsOrderActionEventHandlers();
        $this->bootstrapWsOrderMessageEventHandlers();
        parent::init();
    }

    /**
     * Get dsn string
     *
     * @return string
     */
    protected function getDsn()
    {
        return Yii::$app->params['mq']['protocol']."://".Yii::$app->params['mq']['serverIp'].":".Yii::$app->params['mq']['port'];
    }

    /**
     * Get push socket
     *
     * @return \ZMQSocket
     */
    protected function getPushSocket()
    {
        $context = new ZMQContext();
        $socket = $context->getSocket(ZMQ::SOCKET_PUSH, 'pusher');
        $socket->connect($this->getDsn());
        return $socket;
    }

    /**
     * Bootstrap Ws message event handlers
     */
    protected function bootstrapWsMessageEventHandlers()
    {
        Event::on(Message::className(), ActiveRecord::EVENT_AFTER_INSERT, function($event) {
            /**
             * @var Message $message
             */
            $message = $event->sender;

            // create notification
            $notification = new Notification($message->provideNotifyInfo());

            if ($notification->save()) {
                if ($message->batch_id) {
                    /**
                     * @var Batch $batch
                     */
                    $batch = Batch::findOne($message->batch_id);
                    /**
                     * @var []Image $images
                     */
                    $images = $batch->images;
                }

                $socket = $this->getPushSocket();
                $methods = get_class_methods(get_class($message));
                if (in_array('wsScenarios', $methods)) {
                    $wsScenarios = $message->wsScenarios();
                    foreach ($wsScenarios as $k => $commands) {
                        if ($k == ActiveRecord::EVENT_AFTER_INSERT) {
                            foreach ($commands as $command) {
                                if (is_callable($command)) {
                                    $command = call_user_func($command);
                                }
                                $data = WsTransferObject::pack($message, $command, Yii::$app->user->id, $notification->recipient_id);
                                if (isset($images)) {
                                    $data->attributes['images'] = [];
                                    foreach ($images as $image) {
                                        $data->attributes['images'][] = WsTransferObject::pack($image, '', Yii::$app->user->id);
                                    }
                                }
                                $socket->send(Json::encode($data));
                            }
                            break;
                        }
                    }
                }
            }
        });
    }

    protected function bootstrapWsProjectMessageEventHandlers()
    {
        Event::on(ProjectMessage::className(), ActiveRecord::EVENT_AFTER_INSERT, function($event) {
            /**
             * @var ProjectMessage $message
             */
            $message = $event->sender;

            // create notification
            $notification = new Notification($message->provideNotifyInfo());

            if ($notification->save()) {
                if ($message->batch_id) {
                    /**
                     * @var Batch $batch
                     */
                    $batch = Batch::findOne($message->batch_id);
                    /**
                     * @var []Image $images
                     */
                    $images = $batch->images;
                }

                $project = $message->project;
                $project->updateActivity();

                $socket = $this->getPushSocket();
                $methods = get_class_methods(get_class($message));
                if (in_array('wsScenarios', $methods)) {
                    $wsScenarios = $message->wsScenarios();
                    foreach ($wsScenarios as $k => $commands) {
                        if ($k == ActiveRecord::EVENT_AFTER_INSERT) {
                            foreach ($commands as $command) {
                                if (is_callable($command)) {
                                    $command = call_user_func($command);
                                }
                                $data = WsTransferObject::pack($message, $command, Yii::$app->user->id, $notification->recipient_id);
                                if (isset($images)) {
                                    $data->attributes['images'] = [];
                                    foreach ($images as $image) {
                                        $image->image = Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $image->image;
                                        $data->attributes['images'][] = WsTransferObject::pack($image, '', Yii::$app->user->id);
                                    }
                                }
                                $data->attributes['project'] = WsTransferObject::pack($project, '', Yii::$app->user->id);

                                if ($message->fromUser) {
                                    $data->attributes['fromUser'] = WsTransferObject::pack($message->fromUser, '', Yii::$app->user->id, ['password_hash', 'auth_key', 'email']);
                                    if ($message->fromUser->proAccount) {
                                        $data->attributes['fromUserProAccount'] = WsTransferObject::pack($message->fromUser->proAccount, '', Yii::$app->user->id);
                                    }
                                    if ($message->fromUser->profilePrivate) {
                                        $data->attributes['fromUserProfilePrivate'] = WsTransferObject::pack($message->fromUser->profilePrivate, '', Yii::$app->user->id);
                                    }
                                }
                                $socket->send(Json::encode($data));
                            }
                            break;
                        }
                    }
                }
            }
        });
    }

    protected function bootstrapWsActionEventHandlers()
    {
        Event::on(ProjectUserRefuse::className(), ActiveRecord::EVENT_AFTER_INSERT, function($event) {
            /**
             * @var ProjectUserRefuse $projectUserRefuse
             */
            $projectUserRefuse = $event->sender;
            /**
             * @var Project $project
             */
            $project = $projectUserRefuse->project;
            $project->updateActivity();

            // create notification
            $notification = new Notification($projectUserRefuse->provideNotifyInfo());

            if ($notification->save()) {
                $socket = $this->getPushSocket();
                $methods = get_class_methods(get_class($projectUserRefuse));
                if (in_array('wsScenarios', $methods)) {
                    $wsScenarios = $projectUserRefuse->wsScenarios();
                    foreach ($wsScenarios as $k => $commands) {
                        if ($k == ActiveRecord::EVENT_AFTER_INSERT) {
                            foreach ($commands as $command) {
                                if (is_callable($command)) {
                                    $command = call_user_func($command);
                                }
                                $data = WsTransferObject::pack($projectUserRefuse, $command, Yii::$app->user->id, $notification->recipient_id);
                                $socket->send(Json::encode($data));
                            }
                            break;
                        }
                    }
                }
            }
        });
    }

    protected function bootstrapWsOrderActionEventHandlers()
    {
        Event::on(Order::className(), Order::EVENT_CHANGED_STATE, function (StateEvent $event) {
            /**
             * @var Order $order
             */
            $order = $event->sender;

            if (!Yii::$app->user->isGuest) {
                /**
                 * @var ConfigFMSLoaderInterface  $fmsConfig
                 */
                $fmsConfig = Yii::$container->get('common\interfaces\fms\ConfigFMSLoaderInterface', [], [
                    'entityName' => 'Order',
                ]);

                $notifyTable = $fmsConfig->get('notifyTable');
                $statusStateTable = $fmsConfig->get('statusStateTable');

                $userId = Yii::$app->user->id;
                $role = User::isCustomer($userId) ? 'customer' : (User::isExecutor($userId) ? 'executor' : '' );
                $description = (isset($notifyTable[$event->toState]) && isset($notifyTable[$event->toState]['admin'])) ? $notifyTable[$event->toState]['admin'] : '';
                /**
                 * @var OrderTrace $orderTrace
                 */
                $orderTrace = Yii::createObject([
                    'class' => OrderTrace::className(),
                    'initiator_id' => $userId,
                    'initiator_role' => $role,
                    'from_state' => isset($statusStateTable[$event->fromState]) ? $statusStateTable[$event->fromState] : null,
                    'to_state' => isset($statusStateTable[$event->toState]) ? $statusStateTable[$event->toState] : null,
                    'description' => $description,
                    'order_id' => $order->id,
                ]);

                $order->updateActivity();

                if ($userId == $order->user_id) {
                    $recipientId = $order->executor_id;
                } else {
                    if ($userId == $order->executor_id) {
                        $recipientId = $order->user_id;
                    }
                }

                $notification = Yii::createObject([
                    'class' => Notification::className(),
                    'initiator_id' => $userId,
                    'recipient_id' => $recipientId,
                    'entity_type' => Notification::ENTITY_TYPE_ORDER_STATE_CHANGED,
                    'entity_id' => $order->id,
                ]);

                if ($notification->save() && $orderTrace->save()) {
                    $socket = $this->getPushSocket();
                    $methods = get_class_methods(get_class($order));
                    if (in_array('wsScenarios', $methods)) {
                        $wsScenarios = $order->wsScenarios();
                        foreach ($wsScenarios as $k => $commands) {
                            if ($k == Order::EVENT_CHANGED_STATE) {
                                foreach ($commands as $command) {
                                    if (is_callable($command)) {
                                        $command = call_user_func($command);
                                    }
                                    $data = WsTransferObject::pack($order, $command, Yii::$app->user->id, $recipientId); //, $recipientId
                                    $socket->send(Json::encode($data));
                                }
                                break;
                            }
                        }
                    }
                }
            }
        });

        Event::on(OrderReview::className(), ActiveRecord::EVENT_AFTER_INSERT, function ($event) {
            /**
             * @var OrderReview $orderReview
             */
            $orderReview = $event->sender;

            /**
             * @var Order $order
             */
            $order = $orderReview->order;

            $userId = Yii::$app->user->id;

            if ($userId == $order->user_id) {
                $recipientId = $order->executor_id;
            } else {
                if ($userId == $order->executor_id) {
                    $recipientId = $order->user_id;
                }
            }

            $notification = Yii::createObject([
                'class' => Notification::className(),
                'initiator_id' => $userId,
                'recipient_id' => $recipientId,
                'entity_type' => Notification::ENTITY_TYPE_ORDER_REVIEW_ADDED,
                'entity_id' => $order->id,
            ]);

            if ($notification->save()) {
                $socket = $this->getPushSocket();
                $methods = get_class_methods(get_class($orderReview));
                if (in_array('wsScenarios', $methods)) {
                    $wsScenarios = $orderReview->wsScenarios();
                    foreach ($wsScenarios as $k => $commands) {
                        if ($k == ActiveRecord::EVENT_AFTER_INSERT) {
                            foreach ($commands as $command) {
                                if (is_callable($command)) {
                                    $command = call_user_func($command);
                                }
                                $data = WsTransferObject::pack($orderReview, $command, Yii::$app->user->id, $recipientId); //, $recipientId
                                $socket->send(Json::encode($data));
                            }
                            break;
                        }
                    }
                }
            }
        });
    }

    protected function bootstrapWsOrderMessageEventHandlers()
    {
        Event::on(OrderMessage::className(), ActiveRecord::EVENT_AFTER_INSERT, function($event) {
            /**
             * @var OrderMessage $message
             */
            $message = $event->sender;

            // create notification

            $notification = Yii::createObject([
                'class' => Notification::className(),
                'initiator_id' => $message->from_user_id,
                'recipient_id' => $message->to_user_id,
                'entity_type' => Notification::ENTITY_TYPE_ORDER_MESSAGE,
                'entity_id' => $message->id,
            ]);

            if ($notification->save()) {
                if ($message->batch_id) {
                    /**
                     * @var Batch $batch
                     */
                    $batch = Batch::findOne($message->batch_id);
                    /**
                     * @var []Image $images
                     */
                    $images = $batch->images;
                }

                $order = $message->order;
                $order->updateActivity();

                $socket = $this->getPushSocket();
                $methods = get_class_methods(get_class($message));
                if (in_array('wsScenarios', $methods)) {
                    $wsScenarios = $message->wsScenarios();
                    foreach ($wsScenarios as $k => $commands) {
                        if ($k == ActiveRecord::EVENT_AFTER_INSERT) {
                            foreach ($commands as $command) {
                                if (is_callable($command)) {
                                    $command = call_user_func($command);
                                }
                                $data = WsTransferObject::pack($message, $command, Yii::$app->user->id, $notification->recipient_id);
                                if (!empty($images)) {
                                    $data->attributes['images'] = [];
                                    foreach ($images as $image) {
                                        $data->attributes['images'][] = $image;//WsTransferObject::pack($image, '', Yii::$app->user->id);
                                    }
                                }
                                $data->attributes['order'] = WsTransferObject::pack($order, '', Yii::$app->user->id);
                                if ($message->fromUser) {
                                    $data->attributes['fromUser'] = WsTransferObject::pack($message->fromUser, '', Yii::$app->user->id, 0, ['password_hash', 'auth_key', 'email']);
                                    if ($message->fromUser->proAccount) {
                                        $data->attributes['fromUserProAccount'] = WsTransferObject::pack($message->fromUser->proAccount, '', Yii::$app->user->id);
                                    }
                                    if ($message->fromUser->profilePrivate) {
                                        $data->attributes['fromUserProfilePrivate'] = WsTransferObject::pack($message->fromUser->profilePrivate, '', Yii::$app->user->id);
                                    }
                                }
                                $socket->send(Json::encode($data));
                            }
                            break;
                        }
                    }
                }
            }
        });
    }
}