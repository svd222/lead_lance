<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 09.08.17
 * @time: 19:01
 */
namespace frontend\components;

use Yii;
use LogicException;
use yii\base\Widget;

class FooterWidgetLoader
{
    /**
     * if you want to print here some additional content,
     * you must implement your own widget inherited from [[yii\base\Widget]] and
     * declare $footerWidget var of type array
     * with value of 'class' key = full class name of widget (including namespace) in appropriate controller
     *
     * for example
     * ```php
     * class MyController extends yii\web\Controller
     * {
     *  public $footerWidget = [
     *      'class' => 'app\widgets\MyFooterWidget',
     *      'publicProp' => 'some prop',
     *      ....
     *  ];
     *  ....
     * }
     *
     * in app\widgets namespace:
     * class MyFooterWidget extends yii\base\Widget
     * {
     *  public $publicProp;
     *
     *  public function run() {
     *  ...
     *  }
     *  ...
     * }
     * ```
     *
     *
     */
    public function load($config = [])
    {
        $footerWidget = !empty($config) ? $config : (isset(Yii::$app->controller->footerWidget) ? Yii::$app->controller->footerWidget : '');
        if (is_array($footerWidget)) {
            /**
             * @var Widget $footerWidgetClassName footer widget class name
             */
            $footerWidgetClassName = isset($footerWidget['class']) ? $footerWidget['class'] : null;
            if ($footerWidgetClassName && class_exists($footerWidgetClassName)) {
                if (is_subclass_of($footerWidgetClassName, 'yii\base\Widget')) {
                    unset($footerWidget['class']);
                    $config = [];
                    if (!empty($footerWidget['template'])) {
                        $config['template'] = $footerWidget['template'];
                        if (!empty($footerWidget['data'])) {
                            $config['data'] = $footerWidget['data'];
                        }
                        echo $footerWidgetClassName::widget($config);
                    }
                } else {
                    throw new LogicException(Yii::t('app', 'Widget must be inherited from yii\base\Widget'));
                }
            } else {
                foreach ($footerWidget as $footerWidgetConfig) {
                    $this->load($footerWidgetConfig);
                }
            }
        }
    }
}