<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 05.08.17
 * @time: 1:30
 */
namespace frontend\components;

use common\interfaces\fms\FMSInterface;
use common\models\Order;
use frontend\controllers\OrderController;
use yii\base\Action;
use Yii;
use yii\base\InvalidCallException;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class OrderAction extends Action
{
    const BEFORE_ORDER_ACTION = 'before_order_action';

    const AFTER_ORDER_ACTION = 'after_order_action';

    public function run($id, $type = '')
    {
        var_dump($id, $this->id, $type);
        die;
        $this->trigger(self::BEFORE_ORDER_ACTION);
        $order = $this->findModel($id);
        $fms = Yii::$container->get(FMSInterface::class, [
            'Order', $order, []
        ]);
        if (!$this->controller instanceof OrderController) {
            throw new InvalidCallException();
        }
        if ($fms->checkAccess($type)) {
            /**
             * @var Order $order
             */
            $post = Yii::$app->request->post();
            $orderMessages = [];//$order->loadOrderMessages();
            $dependencies = $fms->loadDependencies($type);
            $fms->switchState($type);



            if (!empty($post[$processModel->formName()])) {
                if ($processModel->load($post) && $processModel->save())
                {
                    $this->trigger(self::AFTER_ORDER_ACTION);
                    return $this->controller->redirect('/order/' . $order->id);
                }
            }
            $viewParams = $this->getAdditionalViewParams($order, $currentState, $type);
            $viewParams = ArrayHelper::merge($viewParams, [
                'order' => $order,
                'orderMessages' => $orderMessages,
            ]);
            $this->trigger(self::AFTER_ORDER_ACTION);
            return $this->controller->render('view', $viewParams);
        } else {
            throw new ForbiddenHttpException();
        }
    }

    public function getProcessModel(&$order, $currentState, $type)
    {

    }

    protected function getAdditionalViewParams(&$order, $currentState, $type)
    {

    }
}