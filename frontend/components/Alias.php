<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 17.06.17
 * @time: 4:55
 */
namespace frontend\components;

use Yii;
use yii\base\Component;

class Alias extends Component
{
    public function init()
    {
        Yii::setAlias('@currentTheme', 'leadlance');
        Yii::setAlias('@currentThemeBasePath', '@frontend/themes/'.Yii::getAlias('@currentTheme'));
        Yii::setAlias('@currentThemeViewBasePath', '@currentThemeBasePath/views');
        Yii::setAlias('@webTheme', Yii::getAlias('@web') . '/themes/' . Yii::getAlias('@currentTheme'));
        parent::init();
    }
}