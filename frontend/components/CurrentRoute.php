<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 13.09.17
 * Time: 14:49
 */
namespace frontend\components;

use Yii;
use yii\base\Component;

/**
 * Class CurrentRoute calculate current actionId, controllerId and moduleId
 * @package frontend\components
 */
class CurrentRoute extends Component
{
    /**
     * @var string $moduleId
     */
    private $moduleId;

    /**
     * @var string $controllerId
     */
    private $controllerId;

    /**
     * @var string $actionId
     */
    private $actionId;

    /**
     * init
     */
    public function init()
    {
        $controller = Yii::$app->controller;
        $this->moduleId = $controller->module->id;
        $this->controllerId = $controller->id;
        $this->actionId = $controller->action->id;
        parent::init();
    }

    /**
     * Getter
     * Returns current module id
     *
     * @return mixed
     */
    public function getModuleId()
    {
        return $this->moduleId;
    }

    /**
     * Getter
     * Returns current controller id
     *
     * @return mixed
     */
    public function getControllerId()
    {
        return $this->controllerId;
    }

    /**
     * Getter
     * Returns current action id
     *
     * @return mixed
     */
    public function getActionId()
    {
        return $this->actionId;
    }
}