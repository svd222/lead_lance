<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 08.08.17
 * @time: 17:34
 */
namespace frontend\themes\leadlance\widgets;

use frontend\controllers\OrderController;
use yii\base\InvalidCallException;
use yii\base\Widget;
use yii\web\Controller;
use Yii;


class OrderFooterWidget extends Widget
{
    /**
     * @var [] $data The additional data to be send to the template
     */
    public $data = [];

    /**
     * @var string $template View template
     */
    public $template;

    /**
     * @var Controller
     */
    public $controller;

    public function init()
    {
        $this->controller = Yii::$app->controller;
        if (!is_a($this->controller, OrderController::className())) {
            throw new InvalidCallException(get_class($this->controller) . ' ' . Yii::t('app', 'must be an instance of ' . OrderController::className()));
        }
    }

    public function run()
    {
        return $this->controller->renderPartial($this->template, $this->data);
    }
}