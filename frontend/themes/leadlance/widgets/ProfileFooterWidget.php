<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 02.02.17
 * @time: 18:52
 */
namespace frontend\themes\leadlance\widgets;

use Yii;
use common\models\Profile;
use yii\base\InvalidCallException;
use yii\web\Controller;
use yii\base\Widget;
use frontend\controllers\ProfileController;

class ProfileFooterWidget extends Widget
{
    /**
     * @var Controller
     */
    public $controller;

    /**
     * @var [] $data The additional data to be send to the template
     */
    public $data = [];

    /**
     * @var string $template View template
     */
    public $template;

    /**
     * @var Profile
     */

    public function init()
    {
        $this->controller = Yii::$app->controller;
        if (!is_a($this->controller, ProfileController::className())) {
            throw new InvalidCallException(get_class($this->controller) . ' ' . Yii::t('app', 'must be an instance of ' . ProfileController::className()));
        }
    }

    public function run()
    {
        return $this->controller->renderPartial($this->template, $this->data);
    }
}