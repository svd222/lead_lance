<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 11.07.17
 * @time: 17:06
 */

namespace frontend\themes\leadlance\widgets;

use Yii;
use yii\base\Widget;
use frontend\controllers\ProjectController;
use yii\web\Controller;
use yii\base\InvalidCallException;

class ProjectFooterWidget extends Widget
{
    /**
     * @var [] $data The additional data to be send to the template
     */
    public $data = [];

    /**
     * @var string $template View template
     */
    public $template;

    /**
     * @var Controller
     */
    public $controller;

    public function init()
    {
        $this->controller = Yii::$app->controller;
        if (!is_a($this->controller, ProjectController::className())) {
            throw new InvalidCallException(get_class($this->controller) . ' ' . Yii::t('app', 'must be an instance of ' . ProjectController::className()));
        }
    }

    public function run()
    {
        return $this->controller->renderPartial($this->template, $this->data);
    }
}