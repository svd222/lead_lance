<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 07.04.17
 * @time: 17:02
 *
 * @var \common\models\User     $user
 * @var []                      $model
 * @var mixed                   $key
 * @var int                     $index
 * @var \yii\widgets\ListView   $widget
 */

use common\models\User;
use yii\helpers\Url;

$me = Yii::$app->user->identity;
$isMy = $model['from_user_id'] == Yii::$app->user->id;
$collocutorId = $isMy ? $model['to_user_id'] : $model['from_user_id'];
$collocutor = User::findOne($collocutorId);

$messageOwnerId = $isMy ? $model['from_user_id'] : $model['to_user_id'];
$ownerHasPro = User::hasPro($messageOwnerId);
$meHasPro = User::hasPro($me->id);
$commentHot = $isMy ? '' : 'comment-hot ';
?>
<div class="container blockPAddBorder order-executive">
    <div class="row">
        <div class="col-lg-12 comment <?= $commentHot; ?>comment-message">
            <div class="agree-order_name div-inline">
                <p>
                    <?php
                    if (!$isMy) {
                        if ($collocutor->profilePrivate->fullName) {
                            ?>
                    <span class="name"><a href="#" class="hover-text-decoration"><?= $collocutor->profilePrivate->fullName; ?></a></span>
                            <?php
                        }
                        ?>
                    <span class="nickname"><a href="<?= Url::to(['/profile/index', 'id' => $collocutorId]); ?>" class="hover-text-decoration"><?= $collocutor->username; ?></a></span>
                        <?php
                        if ($ownerHasPro) {
                            //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
                            ?>
                            <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                            <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
                            <?php
                        }
                        ?>
                        <?php
                    } else {
                        if ($me->profilePrivate->fullName) {
                            ?>
                        <span class="name"><a href="#" class="hover-text-decoration"><?= $me->profilePrivate->fullName; ?></a></span>
                            <?php
                        }
                        ?>
                    <span class="nickname"><a href="<?= Url::to(['/profile/index']); ?>" class="hover-text-decoration"><?= $me->username; ?></a></span>
                        <?php
                        if ($meHasPro) {
                            //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
                            ?>
                            <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                            <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
                            <?php
                        }
                    }
                    ?>
                    <span class="date"><?= $model['created_at']; ?></span>
                </p>
            </div>
            <p class="comment-text"><?= $model['message']; ?></p>
            <?php
                if ($model->batch && $model->batch->images) {
                    foreach ($model->batch->images as $k => $image) {
                        /**
                         * @var \common\models\Image $image
                         */
                        ?>
                        <p class="downloaded-files">
                            <a href="#" class="hover-text-decoration">
                                <img src="/themes/leadlance/img/order/downloaded-files.png" alt="">
                                <span class="text"><?= $image->original_image; ?></span>
                            </a>
                        </p>
                        <?php
                    }
                }
            ?>
            <!--<p class="downloaded-files">
                <a href="#" class="hover-text-decoration">
                    <img src="/themes/leadlance/img/order/downloaded-files.png" alt="">
                    <span class="text">добавленный файл.doc</span>
                </a>
            </p>
            <p class="downloaded-files">
                <a href="#" class="hover-text-decoration">
                    <img src="/themes/leadlance/img/order/downloaded-files.png" alt="">
                    <span class="text">изображение.jpg</span>
                </a>
            </p>-->
        </div>
    </div>
</div>