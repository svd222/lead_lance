<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 06.04.17
 * @time: 8:37
 *
 * @var yii\web\View                        $this
 * @var yii\data\ArrayDataProvider          $dataProvider
 */
use yii\widgets\ListView;

?>


<div class="row ">
    <div class="col-md-7 col-sm-5">
        <h2 class="projectTitles page-titles message-title">Сообщения</h2>
    </div>
    <div class="col-md-5 col-sm-7">
        <span class="search-wrap">
            <input type="text" class="search-input" placeholder="Поиск">
            <label class="submit-wrap">
                <input type="submit" class="start-search" value="">
            </label>
        </span>
    </div>
</div>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_item',
    'summary' => '',
]);
?>
