<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 07.04.17
 * @time: 15:08
 *
 * @var \common\models\User         $user
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \common\models\Message      $model
 * @var \common\models\Image        $imageModel
 * @var int                         $uid     id of recipient
 */

use yii\widgets\ListView;
use yii\widgets\ActiveForm;
use dosamigos\fileupload\FileUpload;
use common\models\Batch;
use yii\bootstrap\Html;
use yii\web\View;
?>
            <div class="row">
                <div class="col-md-12">
                    <div class="dialog-wrap">
                        <div class="logo"></div>
                        <div class="agree-order_name">
                            <p>
                                <?php
                                if ($user->profile->name) {
                                ?>
                                <span class="name"><a href="#" class="hover-text-decoration"><?= $user->profile->name; ?></a></span>
                                <?php
                                }
                                ?>
                                <span class="nickname"><?= $user->username; ?></span>
                                <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                                <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
                            </p>
                        </div>
                        <div class="counter-mess">
                            <img src="/themes/leadlance/img/message/message.png" alt="">
                            <span><?= $dataProvider->count; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row order-executive message-write">
                <?php
                    $form = ActiveForm::begin([
                        'id' => $model->formName(),
                        'options' => [
                            'class' => 'write-comment'
                        ],
                        'enableAjaxValidation' => false,
                        'enableClientValidation' => false,
                        'fieldConfig' => [
                            'template' => '{input}',
                            'options' => [
                                'tag' => false,
                            ],
                        ]
                    ]);
                    echo $form->field($model, 'from_user_id')->hiddenInput([
                        'value' => Yii::$app->user->id,
                    ]);
                    echo $form->field($model, 'to_user_id')->hiddenInput([
                        'value' => $uid,
                    ]);
                ?>
                        <div class="col-md-8 wrap-textarea">
                            <?php
                                echo $form->field($model, 'message')->textarea([
                                    'class' => 'form-comment-area',
                                ]);
                                echo Html::submitButton(Yii::t('app/message', 'Send message'), [
                                    'class' => 'buttonFilters',
                                ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <div class="dropbox">
                                <label>
                                    <?= FileUpload::widget([
                                        'model' => $imageModel,
                                        'attribute' => 'image',
                                        'url' => ['/image/upload/' . Batch::ENTITY_TYPE_MESSAGE],
                                        'useDefaultButton' => false,
                                        'clientOptions' => [
                                            'maxFileSize' => 2000000,
                                            'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                                        ],
                                        'clientEvents' => [
                                            'fileuploaddone' => 'function(e, data) {
                                                
                                            }',
                                            'fileuploadfail' => 'function(e, data) {
                                            
                                            }',
                                        ],
                                    ]); ?>
                                    <?php
                                    // @todo move below js code to separate file
                                    $this->registerJs("
                                        collection.get('action').set('formName', '".$model->formName()."');
                                        
                                        collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                                        collection.get('action').set('deleteUrl', '/image/delete/');
                                        collection.get('action').set('imageFormName', '".$imageModel->formName()."');
                            
                                    ", View::POS_BEGIN);
                                    ?>
                                    <span class="download-text">загрузите файл<br> или перетащите его сюда</span>
                                    <span class="download-size">(Max: 10 mb)</span>
                                </label>
                            </div>
                            <!--<p class="downloaded-files">
                                <a href="#" class="hover-text-decoration">
                                    <img src="/themes/leadlance/img/order/downloaded-files.png" alt="">
                                    <span class="text">загруженый файл.pdf,doc и тд.</span>
                                </a>
                            </p>-->
                        </div>
                <?php
                    ActiveForm::end();
                ?>
            </div>
        </div>
    <?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_item2',
        'summary' => '',
    ]);
