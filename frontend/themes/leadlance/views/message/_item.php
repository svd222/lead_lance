<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 06.04.17
 * @time: 9:34
 */

use common\models\User;
use yii\helpers\Url;
use common\models\Message;
use yii\web\View;
use common\models\Notification;

/**
 * @var View $this
 * @var []                      $model
 * @var mixed                   $key
 * @var int                     $index
 * @var \yii\widgets\ListView   $widget
 */

$uid = $model['from_user_id'] == Yii::$app->user->id ? $model['to_user_id'] : $model['from_user_id'];
$user = User::findOne($uid);
$userHasPro = User::hasPro($uid);

?>
<div class="row">
    <div class="col-lg-7 col-md-10 col-sm-12">
        <div class="dialog-wrap ">
            <div class="logo"></div>
            <div class="agree-order_name">
                <?php if ($model['is_system'] || $model['is_from_admin']) {
                    ?>
                    <p>
                        <span class="name admin"><a href="#" class="hover-text-decoration">Администрация сайта</a></span>
                    </p>
                    <?php
                } else {
                    ?>
                <p>
                    <?php
                    if ($user->profilePrivate->fullName) {
                        ?>
                        <span class="name"><a href="#" class="hover-text-decoration"><?= $user->profilePrivate->fullName; ?></a></span>
                        <?php
                    }
                    ?>
                    <span class="nickname"><?= $user->username; ?></span>
                    <?php
                    if ($userHasPro) {
                        //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
                        ?>
                        <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                        <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
                        <?php
                    }
                    ?>
                </p>
                    <?php
                }
                ?>
            </div>
            <div class="counter-mess">
                <a href="#">
                    <img src="/themes/leadlance/img/message/message.png" alt="">
                    <?php // @todo rewrite below query (Eager loading) ?>
                    <span><?= Message::find()->meToOrToMeFrom($model['from_user_id'], $model['to_user_id'])->count(); ?></span>
                </a>
            </div>
        </div>
        <?php
            /**
             * @var Notification[] $notificationsByInitiators
             */
            $notificationsByInitiators = $this->params['notificationsByInitiators'];
            $newMessageNotificationsHtml = '';
            if (!empty($notificationsByInitiators)) {
                if (!empty($notificationsByInitiators[$model['from_user_id']])) {
                    $newMessageNotificationsHtml = '<span class="counter">'.count($notificationsByInitiators[$model['from_user_id']]).'</span>';
                }
            }
        ?>
        <div class="open-dialog">
            <a href="<?php echo Url::to(['/message/create/' . $uid]); ?>">Открыть диалог<!--<span class="all-read">(Ваше сообщение прочитано)</span>--><?= $newMessageNotificationsHtml; ?></a>
        </div>
    </div>
</div>
