<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 17.07.17
 * @time: 17:52
 */

use common\widgets\WsEventHandlerRegistrator;

// register private message event handler
echo WsEventHandlerRegistrator::widget([
    'exceptActions' => [
        'frontend_message_create',
    ],
    'assetBundles' => [
        '\frontend\assets\WsMessageHandlerLeftMenuAsset',
    ]
]);

echo WsEventHandlerRegistrator::widget([
    'includeActions' => [
        'frontend_message_index',
    ],
    'assetBundles' => [
        '\frontend\assets\WsMessageHandlerMessageIndexPageAsset',
    ]
]);

// register project message event handler
echo WsEventHandlerRegistrator::widget([
    'assetBundles' => [
        '\frontend\assets\WsProjectMessageHandlerAsset',
    ]
]);

echo WsEventHandlerRegistrator::widget([
    'exceptActions' => [
        'frontend_project_view-offers',
        'frontend_project_respond-project',
    ],
    'assetBundles' => [
        '\frontend\assets\WsProjectUserRefuseAsset',
    ]
]);

echo WsEventHandlerRegistrator::widget([
    'exceptActions' => [
        'frontend_order_view',
    ],
    'assetBundles' => [
        '\frontend\assets\WsOrderActionChangedStateAsset'
    ]
]);

echo WsEventHandlerRegistrator::widget([
    'assetBundles' => [
        '\frontend\assets\WsOrderMessageHandlerAsset'
    ]
]);

echo WsEventHandlerRegistrator::widget([
    'assetBundles' => [
        '\frontend\assets\WsOrderReviewAsset'
    ]
]);