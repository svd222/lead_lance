<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 09.01.17
 * @time: 14:24
 *
 * @var \yii\web\View   $this
 * @var string          $content
 */
use yii\helpers\Html;
use frontend\assets\LandingAsset;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use frontend\models\RegistrationForm;

LandingAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= Html::encode($this->title); ?></title>
    <!-- Bootstrap -->

    <link href="/themes/leadlance/landing/css/bootstrap.min.css" rel="stylesheet">
    <?php $this->head(); ?>

    <!--<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />-->

    <!--<link href="/themes/leadlance/landing/css/main.css" rel="stylesheet">
    <link href="/themes/leadlance/landing/css/landing-style.css" rel="stylesheet">
    <link href="/themes/leadlance/landing/css/media-landing.css" rel="stylesheet">-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<?php
/**
 * @var \frontend\models\RegistrationForm  $registrationForm
 */
$registrationForm = $this->params['registrationForm'];
/**
 * @var \dektrium\user\models\LoginForm         $loginForm
 */
$loginForm = $this->params['loginForm'];
/**
 * @var \dektrium\user\models\RecoveryForm $recoveryForm
 */
$recoveryForm = $this->params['recoveryForm'];
/**
 * @var string $checkedRole
 */
$checkedRole = $this->params['checkedRole'];

/**
 * @var string $code
 */
$code = !empty($this->params['code'])
    ? $this->params['code']
    : '';

/**
 * @var integer $resetUserId
 */
$resetUserId = !empty($this->params['resetUserId'])
    ? $this->params['resetUserId']
    : 0;

$page = 'common';
if (isset($this->params['isCustomerPage']) && $this->params['isCustomerPage']) {
    $page = 'customer';
} elseif (isset($this->params['isExecutorPage']) && $this->params['isExecutorPage']) {
    $page = 'executor';
}
?>
<body>
<?php $this->beginBody() ?>
<header class="landing-header">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xs-6 landing-logo">
                    <h1><a href="#">Leadlance</a></h1>
                </div>
                <div class="col-sm-6 col-xs-6 landing-login">
                    <!--  data-target="#login-account" data-toggle="modal" -->
                    <a href="#" class="div-inline" id="login-link"><img src="/themes/leadlance/landing/img/landing/login-ico.png" alt="">
                        <?php
                            if ($page == 'common') {
                                echo 'Вход';
                            } else {
                                if ($page == 'customer' || $page == 'executor') {
                                    echo 'Регистрация';
                                }
                            }
                        ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<main>
    <?= $content; ?>
</main>
<?php
    if ($page == 'common') {
?>
        <div class="modal fade project-2-modal" id="sigin-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog create-order-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="group-checkeds-modal">
                            <?php
                                $formName = $registrationForm->formName() . '-' . 1;
                                $form = ActiveForm::begin([
                                    'id'                     => $formName,
                                    'enableAjaxValidation'   => true,
                                    'enableClientValidation' => false,
                                    'action' => '/registration/register',
                                    'validationUrl' => '/registration/validate-form',
                                    'fieldConfig' => [
                                        'template' => '{input}',
                                        'options' => [
                                            'tag' => false,
                                        ],
                                    ]
                                ]);
                            ?>
                                <h3 class="title-modal-block-all">Выберите тип аккаунта</h3>
                                <div class="checkeds-block-modal">
                                    <div class="left-block-chekeds-modal">
                                        <p class="exceptionText active ">
                                            <!--<input type="radio" class="checkboxStyle" id="check1_1" name="modal-checks">-->
                                            <?php
                                                $checkBoxId = $formName.'-role-'.RegistrationForm::ROLE_CUSTOMER;
                                                $checkBoxOptions = [
                                                    'id' => $checkBoxId,
                                                    'class' => 'checkboxStyle',
                                                    'value' => RegistrationForm::ROLE_CUSTOMER,
                                                    'uncheck' => RegistrationForm::ROLE_NONE,
                                                ];
                                                if ($checkedRole == RegistrationForm::ROLE_CUSTOMER) {
                                                    $checkBoxOptions['checked'] = true;
                                                }
                                                echo $form->field($registrationForm, 'role', [
                                                    'selectors' => [
                                                        'input' => $checkBoxId,
                                                    ],
                                                ])->checkbox($checkBoxOptions, false);
                                            ?>
                                            <label for="<?= $checkBoxId; ?>" class="div-inline"></label><span class="div-inline">рекламодатель</span>
                                        </p>
                                    </div>
                                    <div class="right-block-chekeds-modal">
                                        <p class="exceptionText active">
                                            <!--<input type="radio" class="checkboxStyle" id="check1_2" name="modal-checks">-->
                                            <?php
                                                $checkBoxId = $formName.'-role-'.RegistrationForm::ROLE_EXECUTOR;
                                                $checkBoxOptions = [
                                                    'id' => $checkBoxId,
                                                    'class' => 'checkboxStyle',
                                                    'value' => RegistrationForm::ROLE_EXECUTOR,
                                                    'uncheck' => RegistrationForm::ROLE_NONE,
                                                ];
                                                if ($checkedRole == RegistrationForm::ROLE_EXECUTOR) {
                                                    $checkBoxOptions['checked'] = true;
                                                }
                                                echo $form->field($registrationForm, 'role', [
                                                    'selectors' => [
                                                        'input' => $checkBoxId,
                                                    ],
                                                ])->checkbox($checkBoxOptions, false);
                                            ?>
                                            <label for="<?= $checkBoxId; ?>" class="div-inline"></label><span class="div-inline">маркетолог</span>
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Имя</h3>
                                <div class="checkeds-block-modal">
                                    <?php
                                    echo $form->field($registrationForm, 'first_name')->textInput([
                                        'class' => 'input-modal-login modal-sign-all-style'
                                    ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Логин</h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($registrationForm, 'username')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">E-mail <span class="gray-text">(пароль придет на вашу почту)</span></h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($registrationForm, 'email')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <p class="prava-this-sign gray-text">Нажимая на кнопку «Зарегистрироваться»,</br>
                                    я соглашаюсь с <a href="#">публичной офертой ООО «Ваан»</a> и <a href="#">правилами сайта.</a></p>
                                <div class="g-recaptcha" data-sitekey="6Lflo0QUAAAAAOrqTst7PoT1DSFvNDE9MM9PHGr4"></div>
                                <?= $form->field($registrationForm, 'recaptcha')->hiddenInput(); ?>
                                <br/>
                                <?php
                                    echo Html::submitButton('Зарегистрироваться', [
                                        'class' => 'modal-sign-button',
                                    ]);
                                ?>
                            <?php
                                ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade project-2-modal" id="login-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog create-order-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="group-checkeds-modal">
                            <?php
                                $form = ActiveForm::begin([
                                    'id'                     => $loginForm->formName(),
                                    'enableAjaxValidation'   => true,
                                    'enableClientValidation' => false,
                                    'action' => '/security/login',
                                    'validationUrl' => '/security/login',
                                    'fieldConfig' => [
                                        'template' => '{input}',
                                        'options' => [
                                            'tag' => false,
                                        ],
                                    ]
                                ]);
                            ?>
                                <h3 class="title-modal-block-all">Логин</h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($loginForm, 'login')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Пароль</h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($loginForm, 'password')->passwordInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                    <?php
                                        echo Html::submitButton('Войти', [
                                            'class' => 'modal-sign-button',
                                        ]);
                                    ?>
                                <!--<button class="modal-sign-button">Войти!</button>-->
                                <div class="block-link-sign clearfix">
                                    <a href="#" class="bottom-modal-sign-link-left gray-text">Забыли пароль?</a>
                                    <a href="#" class="bottom-modal-sign-link-right gray-text">Зарегистрироваться на сайте</a>
                                    <!--<a href="#" class="bottom-modal-sign-link-right gray-text"  data-dismiss="modal" aria-label="Close"  data-target="#sigin-account" data-toggle="modal">Зарегистрироваться на сайте</a>-->
                                </div>
                            <?php
                                ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade project-2-modal" id="reset-account-complete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog create-order-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="group-checkeds-modal">
                            <?php
                                $form = ActiveForm::begin([
                                    'id'                     => 'reset-form',
                                    'enableAjaxValidation'   => true,
                                    'enableClientValidation' => false,
                                    'action' => '/recovery/reset?id=' . $resetUserId . '&code=' . $code,
                                    'validationUrl' => '/recovery/reset?id=' . $resetUserId . '&code=' . $code,
                                    'fieldConfig' => [
                                        'template' => '{input}',
                                        'options' => [
                                            'tag' => false,
                                        ],
                                    ]
                                ]);
                            ?>
                            <h3 class="title-modal-block-all">Новый пароль</h3>
                            <div class="checkeds-block-modal password-first">
                                <?= $form->field($recoveryForm, 'password')->passwordInput() ?>
                                <div class="clearfix"></div>
                            </div>
                            <h3 class="title-modal-block-all">Повторите новый пароль</h3>
                            <div class="checkeds-block-modal password-second">
                                <?= $form->field($recoveryForm, 'password')->passwordInput() ?>
                                <div class="clearfix"></div>
                            </div>
                            <br />
                            <?= Html::submitButton(Yii::t('user', 'Finish'), [
                                'class' => 'modal-sign-button',
                            ]); ?>
                            <?php
                                ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade project-2-modal" id="reset-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog create-order-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="group-checkeds-modal">
                            <div class="panel-body">
                                <?php $form = ActiveForm::begin([
                                    'id'                     => $recoveryForm->formName(),
                                    'enableAjaxValidation'   => true,
                                    'enableClientValidation' => false,
                                    'action'                 => '/recovery/request',
                                    'validationUrl'          => '/recovery/request',
                                    'fieldConfig'            => [
                                        'template' => '{input}',
                                        'options'  => [
                                            'tag' => false,
                                        ],
                                    ],
                                ]); ?>
                                <h3 class="title-modal-block-all">Ваш email</h3>
                                <?= $form->field($recoveryForm, 'email')->textInput(['autofocus' => true]) ?><br/>
                                <?= Html::submitButton(
                                    Yii::t('user', 'Continue'),
                                    [
                                        'class' => 'modal-sign-button',
                                    ]
                                ) ?>
                                <br>
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
    if ($page == 'customer') {
?>
        <footer>
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="footer-all clearfix">
                            <div class="col-md-4 col-sm-4 col-xs-4 footer-logo">
                                <a href="#">Leadlance</a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 footer-social-block">
                        <span class="facebook-block">
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </span>
                                <span class="vk-block">
                            <a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                        </span>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 block-design-all">
                                <p class="block-design">
                                    Design by M.D.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade project-2-modal" id="sigin-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog create-order-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="group-checkeds-modal">
                            <?php
                                $formName = $registrationForm->formName() . '-' . 2;
                                $form = ActiveForm::begin([
                                    'id'                     => $formName,
                                    'enableAjaxValidation'   => true,
                                    'enableClientValidation' => false,
                                    'action' => '/registration/register',
                                    'validationUrl' => '/registration/validate-form',
                                    'fieldConfig' => [
                                        'template' => '{input}',
                                        'options' => [
                                            'tag' => false,
                                        ],
                                    ]
                                ]);
                            ?>
                                <h3 class="title-modal-block-all">Выберите тип аккаунта</h3>
                                <div class="checkeds-block-modal">
                                    <div class="left-block-chekeds-modal">
                                        <p class="exceptionText active ">
                                            <?php
                                                $checkBoxId = $formName.'-role-'.RegistrationForm::ROLE_CUSTOMER;
                                                $checkBoxOptions = [
                                                    'id' => $checkBoxId,
                                                    'class' => 'checkboxStyle',
                                                    'value' => RegistrationForm::ROLE_CUSTOMER,
                                                    'uncheck' => RegistrationForm::ROLE_NONE,
                                                ];
                                                if ($checkedRole == RegistrationForm::ROLE_CUSTOMER) {
                                                    $checkBoxOptions['checked'] = true;
                                                }
                                                echo $form->field($registrationForm, 'role', [
                                                    'selectors' => [
                                                        'input' => $checkBoxId,
                                                    ],
                                                ])->checkbox($checkBoxOptions, false);
                                            ?>
                                            <label for="<?= $checkBoxId; ?>" class="div-inline"></label><span class="div-inline">рекламодатель</span>
                                        </p>
                                    </div>
                                    <div class="right-block-chekeds-modal">
                                        <p class="exceptionText ">
                                            <?php
                                                $checkBoxId = $formName.'-role-'.RegistrationForm::ROLE_EXECUTOR;
                                                $checkBoxOptions = [
                                                    'id' => $checkBoxId,
                                                    'class' => 'checkboxStyle',
                                                    'value' => RegistrationForm::ROLE_EXECUTOR,
                                                    'uncheck' => RegistrationForm::ROLE_NONE,
                                                ];
                                                if ($checkedRole == RegistrationForm::ROLE_EXECUTOR) {
                                                    $checkBoxOptions['checked'] = true;
                                                }
                                                echo $form->field($registrationForm, 'role', [
                                                    'selectors' => [
                                                        'input' => $checkBoxId,
                                                    ],
                                                ])->checkbox($checkBoxOptions, false);
                                            ?>
                                            <label for="<?= $checkBoxId; ?>" class="div-inline"></label><span class="div-inline">маркетолог</span>
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Имя</h3>
                                <div class="checkeds-block-modal">
                                    <?php
                                    echo $form->field($registrationForm, 'first_name')->textInput([
                                        'class' => 'input-modal-login modal-sign-all-style'
                                    ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Логин</h3>
                                <div class="checkeds-block-modal">
                                    <?php
                                        echo $form->field($registrationForm, 'username')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">E-mail <span class="gray-text">(пароль придет на вашу почту)</span></h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($registrationForm, 'email')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <p class="prava-this-sign gray-text">Нажимая на кнопку «Зарегистрироваться»,</br>
                                    я соглашаюсь с <a href="#">публичной офертой ООО «Ваан»</a> и <a href="#">правилами сайта.</a></p>
                                <div class="g-recaptcha" data-sitekey="6Lflo0QUAAAAAOrqTst7PoT1DSFvNDE9MM9PHGr4"></div>
                                <?= $form->field($registrationForm, 'recaptcha')->hiddenInput(); ?>
                                <br/>
                                <?php
                                    echo Html::submitButton('Зарегистрироваться', [
                                        'class' => 'modal-sign-button',
                                    ]);
                                ?>
                            <?php
                                ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade project-2-modal" id="login-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog create-order-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="group-checkeds-modal">
                            <?php
                            $form = ActiveForm::begin([
                                'id'                     => 'registration-form',
                                'enableAjaxValidation'   => false,
                                'enableClientValidation' => false,
                                'action' => '/security/login',
                                'fieldConfig' => [
                                    'template' => '{input}',
                                    'options' => [
                                        'tag' => false,
                                    ],
                                ]
                            ]);
                            ?>
                                <h3 class="title-modal-block-all">Логин</h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($loginForm, 'login')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Пароль</h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($loginForm, 'password')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <!--<button class="modal-sign-button">Войти!</button>-->
                                <?php
                                    echo Html::submitButton('Войти', [
                                        'class' => 'modal-sign-button',
                                    ]);
                                ?>
                                <div class="block-link-sign clearfix">
                                    <a href="#" class="bottom-modal-sign-link-left gray-text">Забыли пароль?</a>
                                    <a href="#" class="bottom-modal-sign-link-right gray-text">Зарегистрироваться на сайте</a>
                                    <!--<a href="#" class="bottom-modal-sign-link-right gray-text"  data-dismiss="modal" aria-label="Close"  data-target="#sigin-account" data-toggle="modal">Зарегистрироваться на сайте</a>-->
                                </div>
                            <?php
                                ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
    if ($page == 'executor') {
?>
        <footer>
            <div class="container-fluid">
                <div class="container">
                    <div class="row">
                        <div class="footer-all clearfix">
                            <div class="col-md-4 footer-logo">
                                <a href="#">Leadlance</a>
                            </div>
                            <div class="col-md-4 footer-social-block">
                        <span class="facebook-block">
                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        </span>
                                <span class="vk-block">
                            <a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                        </span>
                            </div>
                            <div class="col-md-4">
                                <p class="block-design">
                                    Design by M.D.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="modal fade project-2-modal" id="sigin-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog create-order-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="group-checkeds-modal">
                            <?php
                                $formName = $registrationForm->formName() . '-' . 3;
                                $form = ActiveForm::begin([
                                    'id'                     => $formName,
                                    'enableAjaxValidation'   => true,
                                    'enableClientValidation' => false,
                                    'action' => '/registration/register',
                                    'validationUrl' => '/registration/validate-form',
                                    'fieldConfig' => [
                                        'template' => '{input}',
                                        'options' => [
                                            'tag' => false,
                                        ],
                                    ]
                                ]);
                            ?>
                                <h3 class="title-modal-block-all">Выберите тип аккаунта</h3>
                                <div class="checkeds-block-modal">
                                    <div class="left-block-chekeds-modal">
                                        <p class="exceptionText active ">
                                            <?php
                                                $checkBoxId = $formName.'-role-'.RegistrationForm::ROLE_CUSTOMER;
                                                $checkBoxOptions = [
                                                    'id' => $checkBoxId,
                                                    'class' => 'checkboxStyle',
                                                    'value' => RegistrationForm::ROLE_CUSTOMER,
                                                    'uncheck' => RegistrationForm::ROLE_NONE,
                                                ];
                                                if ($checkedRole == RegistrationForm::ROLE_CUSTOMER) {
                                                    $checkBoxOptions['checked'] = true;
                                                }
                                                echo $form->field($registrationForm, 'role', [
                                                    'selectors' => [
                                                        'input' => $checkBoxId,
                                                    ],
                                                ])->checkbox($checkBoxOptions, false);
                                            ?>
                                            <label for="<?= $checkBoxId; ?>" class="div-inline"></label><span class="div-inline">рекламодатель</span>
                                        </p>
                                    </div>
                                    <div class="right-block-chekeds-modal">
                                        <p class="exceptionText ">
                                            <?php
                                                $checkBoxId = $formName.'-role-'.RegistrationForm::ROLE_EXECUTOR;
                                                $checkBoxOptions = [
                                                    'id' => $checkBoxId,
                                                    'class' => 'checkboxStyle',
                                                    'value' => RegistrationForm::ROLE_EXECUTOR,
                                                    'uncheck' => RegistrationForm::ROLE_NONE,
                                                ];
                                                if ($checkedRole == RegistrationForm::ROLE_EXECUTOR) {
                                                    $checkBoxOptions['checked'] = true;
                                                }
                                                echo $form->field($registrationForm, 'role', [
                                                    'selectors' => [
                                                        'input' => $checkBoxId,
                                                    ],
                                                ])->checkbox($checkBoxOptions, false);
                                            ?>
                                            <label for="<?= $checkBoxId; ?>" class="div-inline"></label><span class="div-inline">маркетолог</span>
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Имя</h3>
                                <div class="checkeds-block-modal">
                                    <?php
                                    echo $form->field($registrationForm, 'first_name')->textInput([
                                        'class' => 'input-modal-login modal-sign-all-style'
                                    ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Логин</h3>
                                <div class="checkeds-block-modal">
                                    <?php
                                        echo $form->field($registrationForm, 'username')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">E-mail <span class="gray-text">(пароль придет на вашу почту)</span></h3>
                                <div class="checkeds-block-modal">
                                    <?php
                                        echo $form->field($registrationForm, 'email')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <div class="clearfix"></div>
                                </div>
                                <p class="prava-this-sign gray-text">Нажимая на кнопку «Зарегистрироваться»,</br>
                                    я соглашаюсь с <a href="#">публичной офертой ООО «Ваан»</a> и <a href="#">правилами сайта.</a></p>
                                <div class="g-recaptcha" data-sitekey="6Lflo0QUAAAAAOrqTst7PoT1DSFvNDE9MM9PHGr4"></div>
                                <?= $form->field($registrationForm, 'recaptcha')->hiddenInput(); ?>
                                <br/>
                                <?php
                                    echo Html::submitButton('Зарегистрироваться', [
                                        'class' => 'modal-sign-button',
                                    ]);
                                ?>
                            <?php
                                ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade project-2-modal" id="login-account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog create-order-modal" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="group-checkeds-modal">
                            <?php
                                $formName = $registrationForm->formName() . '-' . 4;
                                $form = ActiveForm::begin([
                                    'id'                     => $formName,
                                    'enableAjaxValidation'   => true,
                                    'enableClientValidation' => false,
                                    'action' => '/registration/register',
                                    'validationUrl' => '/registration/validate-form',
                                    'fieldConfig' => [
                                        'template' => '{input}',
                                        'options' => [
                                            'tag' => false,
                                        ],
                                    ]
                                ]);
                            ?>
                                <h3 class="title-modal-block-all">Выберите тип аккаунта</h3>
                                <div class="checkeds-block-modal">
                                    <div class="left-block-chekeds-modal">
                                        <p class="exceptionText active ">
                                            <?php
                                                $checkBoxId = $formName.'-role-'.RegistrationForm::ROLE_CUSTOMER;
                                                $checkBoxOptions = [
                                                    'id' => $checkBoxId,
                                                    'class' => 'checkboxStyle',
                                                    'value' => RegistrationForm::ROLE_CUSTOMER,
                                                    'uncheck' => RegistrationForm::ROLE_NONE,
                                                ];
                                                if ($checkedRole == RegistrationForm::ROLE_CUSTOMER) {
                                                    $checkBoxOptions['checked'] = true;
                                                }
                                                echo $form->field($registrationForm, 'role', [
                                                    'selectors' => [
                                                        'input' => $checkBoxId,
                                                    ],
                                                ])->checkbox($checkBoxOptions, false);
                                            ?>
                                            <label for="<?= $checkBoxId; ?>" class="div-inline"></label><span class="div-inline">рекламодатель</span>
                                        </p>
                                    </div>
                                    <div class="right-block-chekeds-modal">
                                        <p class="exceptionText ">
                                            <?php
                                                $checkBoxId = $formName.'-role-'.RegistrationForm::ROLE_EXECUTOR;
                                                $checkBoxOptions = [
                                                    'id' => $checkBoxId,
                                                    'class' => 'checkboxStyle',
                                                    'value' => RegistrationForm::ROLE_EXECUTOR,
                                                    'uncheck' => RegistrationForm::ROLE_NONE,
                                                ];
                                                if ($checkedRole == RegistrationForm::ROLE_EXECUTOR) {
                                                    $checkBoxOptions['checked'] = true;
                                                }
                                                echo $form->field($registrationForm, 'role', [
                                                    'selectors' => [
                                                        'input' => $checkBoxId,
                                                    ],
                                                ])->checkbox($checkBoxOptions, false);
                                            ?>
                                            <label for="<?= $checkBoxId; ?>" class="div-inline"></label><span class="div-inline">маркетолог</span>
                                        </p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Имя</h3>
                                <div class="checkeds-block-modal">
                                    <?php
                                    echo $form->field($registrationForm, 'first_name')->textInput([
                                        'class' => 'input-modal-login modal-sign-all-style'
                                    ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">Логин</h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($registrationForm, 'username')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <h3 class="title-modal-block-all">E-mail <span class="gray-text">(пароль придет на вашу почту)</span></h3>
                                <div class="checkeds-block-modal">
                                    <!--<input type="text" class="input-modal-login modal-sign-all-style">-->
                                    <?php
                                        echo $form->field($registrationForm, 'email')->textInput([
                                            'class' => 'input-modal-login modal-sign-all-style'
                                        ]);
                                    ?>
                                    <div class="clearfix"></div>
                                </div>
                                <p class="prava-this-sign gray-text">Нажимая на кнопку «Зарегистрироваться»,</br>
                                    я соглашаюсь с <a href="#">публичной офертой ООО «Ваан»</a> и <a href="#">правилами сайта.</a></p>
                                <div class="g-recaptcha" data-sitekey="6Lflo0QUAAAAAOrqTst7PoT1DSFvNDE9MM9PHGr4"></div>
                                <?= $form->field($registrationForm, 'recaptcha')->hiddenInput(); ?>
                                <br/>
                                <?php
                                    echo Html::submitButton('Зарегистрироваться', [
                                        'class' => 'modal-sign-button',
                                    ]);
                                ?>
                            <?php
                                ActiveForm::end();
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
?>

<?php $this->endBody() ?>
</body>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<script src="/themes/leadlance/landing/js/bootstrap.min.js"></script>
<?php
    $script = "'use strict';
    (function () {
        $(document).on('ready', function () {
            $('#sigin-account').find('.checkboxStyle').on('click', function () {
                $('.exceptionText').removeClass('active');
                $(this).parent().addClass('active');
            });
        })
    })();";
    $this->registerJs($script, View::POS_END);

    if(isset($this->params['isCustomerPage']) && $this->params['isCustomerPage']) {
        //show the slider
        $script = "'use strict';
    (function () {
        $(document).on('ready', function () {
            $('.slider-problem').slick({
                dots: false,
                speed: 500,
                fade: false,
                autoplay: true,
                autoplaySpeed: 5000,
                adaptiveHeight: true,
                swipe: false,
                cssEase: 'linear',
                prevArrow: $('.prev-arrow-slider'),
                nextArrow: $('.next-arrow-slider')
            });

            $('.prev-arrow-slider').on('click',function () {
                countNumber();
            });

            $('.next-arrow-slider').on('click',function () {
                countNumber();
            });
            
            function countNumber() {
                $('.count-slide-block').children('.active').text($('.slick-active').index()+1);
            }

            $('#sigin-account').find('.checkboxStyle').on('click', function () {
                $('.exceptionText').removeClass('active');
                $(this).parent().addClass('active');
            });

            /*$('.bottom-modal-sign-link-right').on('click', function () {
                $('#login-account').modal('hide');
                $('#sigin-account').modal('show');
                setTimeout(function () {
                    $('body').addClass('modal-open');
                },600);

            })*/
        })
    })();";
        $this->registerJs($script, View::POS_END);
    }

    if($page == 'customer' || $page == 'executor') {
        $script = "'use strict';
            $('.section-1-button-position, .buton-section-2-position, .section-10-button-position, .section-3__guarantee-button, .buton-section-6-position').on('click', function() {
                $('#sigin-account').modal('show');
                $('#login-account').modal('hide');
            });";
        $this->registerJs($script, View::POS_END);
    }

    $script = "
        $('#login-link').on('click', function() {
            ";
    if ($page == 'common') {
        $script .= "
            $('#login-account').modal('show');
            $('#sigin-account').modal('hide');
        ";
    } else {
        $script .= "
            $('#sigin-account').modal('show');
            $('#login-account').modal('hide');
        ";
    }

    $script .= "    
        });
    ";
    if (!empty($code)) {
        $script .= "    
        $('#reset-account-complete').modal('show');
    ";
    }

    $this->registerJs($script, View::POS_LOAD);

?>
</html>
<?php $this->endPage() ?>
