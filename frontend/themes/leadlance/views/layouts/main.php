<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use common\widgets\GritterAlert;
use common\models\User;
use common\models\UserAvatar;
use common\models\Notification;
use common\models\Project;
use common\models\ProjectMessage;
use common\models\ProjectUserRefuse;
use yii\helpers\ArrayHelper;
use common\models\Order;
use common\models\OrderMessage;
use yii\helpers\Json;

AppAsset::register($this);

$colorsCssClasses = Yii::$app->params['themes']['leadlance']['colorsCssClasses'];
$this->registerJs("
    var action = collection.get('controller').get('action');
    action.set('colorsCssClasses', ".Json::encode($colorsCssClasses).");
");

$this->renderFile(Yii::getAlias('@currentThemeViewBasePath') . DIRECTORY_SEPARATOR . 'layouts' . DIRECTORY_SEPARATOR . 'ws_event_handler_register.php');

$isGuest = Yii::$app->user->isGuest;

$leftMenuProjects = !empty($this->params['left_menu_projects']) ? $this->params['left_menu_projects'] : null;
$leftMenuOrders = !empty($this->params['left_menu_orders']) ? $this->params['left_menu_orders'] : null;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <script>
        var frontendDomain = '<?= Yii::$app->params['frontendDomain']; ?>';
        <?php
        if (Yii::$app->user->id) {
            ?>
            var userId = <?= Yii::$app->user->id; ?>
            <?php
        }
        ?>
    </script>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title><?= Html::encode($this->title) ?></title>
    <!-- Bootstrap -->

    <!--<link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/jquery.custom-scrollbar.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/style-mark.css" rel="stylesheet">
    <link href="css/style-pavel.css" rel="stylesheet">
    <link href="css/media.css" rel="stylesheet">-->
    <?php $this->head() ?>
    <script>
        var controller = new Controller('<?= Yii::$app->controller->id; ?>');
        var action = new Action('<?= Yii::$app->controller->action->id; ?>');
        collection.set('controller', controller);
        collection.set('action', action);
        <?php
            if (!Yii::$app->user->isGuest) {
        ?>
        collection.set('userId', <?= Yii::$app->user->id; ?>);
        collection.set('ws', {
            serverIp: '<?= Yii::$app->params['ws']['serverIp']; ?>',
            port: <?= Yii::$app->params['ws']['port']; ?>,
            isSSL: <?= (Yii::$app->request->getIsSecureConnection()) ? 1 : 0; ?>,
            location: '<?= Yii::$app->params['ws']['location']; ?>'
        });
        collection.set('maxPossibleSelectedCasesCount', <?= Yii::$app->params['maxPossibleSelectedCasesCount']; ?>);
        collection.set('conversationUpdateStatusInterval', <?= Yii::$app->params['project']['conversation']['update_status']; ?>);
        <?php
            }
        ?>
    </script>
<?php
    /**
     * @var \yii\web\User $user
     */
    $user = Yii::$app->user;
    $userId = 0;
    if (!$user->isGuest) {
        $userId = $user->id;
    }
    $userHasPro = User::hasPro($userId);

    /**
     * @var UserAvatar $userAvatar
     */
    $userAvatar = isset($this->params['userAvatar']) ? $this->params['userAvatar'] : null;
    $notifications = isset($this->params['notifications']) ? $this->params['notifications'] : [];
    $messageNotifications = Notification::selectNotificationsByEntityType($notifications, Notification::ENTITY_TYPE_MESSAGE);
    $countmessageNotifications = count($messageNotifications);

    // @todo all future action notifications must sum here
    $entityTypes = [];

    $currentActionId = Yii::$app->currentRoute->actionId;
    $mcId = Yii::$app->currentRoute->moduleId.'_'.Yii::$app->currentRoute->controllerId;

    if (!($mcId == 'frontend_project' && in_array($currentActionId, ['view-offers', 'respond-project']))) {
        $entityTypes = [
            Notification::ENTITY_TYPE_PROJECT_USER_REFUSE,
        ];
    }

    $allNotificationsActionsByProjectCount = 0;
    $projectUserRefuseIds = [];
    if (!empty($entityTypes)) {
        $allNotificationsActionsByProject = Notification::selectNotificationsByEntityType($notifications, $entityTypes);
        foreach ($allNotificationsActionsByProject as $anp) {
            /**
             * @var Notification $anp
             */
            if ($anp->entity_type == Notification::ENTITY_TYPE_PROJECT_USER_REFUSE) {
                $projectUserRefuseIds[] = $anp->entity_id;
            }
            $refusedProjects = ProjectUserRefuse::find()->where(['id' => $projectUserRefuseIds])->all();
            $refusedProjects = ArrayHelper::getColumn($refusedProjects, 'project_id');
        }

        $allNotificationsActionsByProjectCount += count($allNotificationsActionsByProject);
    }

    $allNewOrderReviewsCount = 0;
    if ($notifications) {
        $allNewOrderReviews = Notification::selectNotificationsByEntityType($notifications, Notification::ENTITY_TYPE_ORDER_REVIEW_ADDED);
        $allNewOrderReviewsCount = count($allNewOrderReviews);
    }



    $isCustomer = User::isCustomer(Yii::$app->user->id);
    $isExecutor = User::isExecutor(Yii::$app->user->id);
?>
</head>
<?php
    $bodyClass = 'wrapper';
    if (!empty($this->params['bodyClass'])) {
        $bodyClass .= ' ' . $this->params['bodyClass'];
    }
?>
<body class="<?= $bodyClass; ?>">
<?php $this->beginBody() ?>
<?= GritterAlert::widget() ?>
<div class="wrapper">
    <div class="wrapperNav">
        <div class="blockTitleName">
            <h2 class="titleName">L<span class="fullLogo">eadLance</span></h2>
        </div>
        <div class="person">
            <div class="person-photo-wrap div-inline">
                <?php
                    if (!empty($userAvatar) && $userAvatar->image) {
                        ?>
                            <img src="<?= UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $userAvatar->image; ?>" alt="" class="person-photo" style="width:55px; height:55px;">
                        <?php
                    } else {
                        ?>
                            <img src="/themes/leadlance/img/index/person-photo.jpg" alt="" class="person-photo" style="width:55px; height:55px;">
                        <?php
                    }
                ?>
                <?php
                    if ($userHasPro) {
                        //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
                ?>
                    <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                    <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
                <?php
                    }
                ?>
            </div>
            <div class="person-info div-inline">
                <?php
                    if (!$user->isGuest) {
                        /**
                         * @var \common\models\ProfilePrivate $profilePrivate
                         */
                        $profilePrivate = $user->identity->profilePrivate;
                        if ($profilePrivate && $profilePrivate->first_name) {
                            ?>
                            <p class="person-name"><a href="<?= Url::to(['/profile/index']); ?>"><?= $profilePrivate->first_name; ?></a></p>
                            <?php
                        }
                    ?>
                    <ul class="person-active">
                        <li class="div-inline"><a href="#" class="person-rait">325</a></li>
                        <?php if ($allNewOrderReviewsCount) { ?>
                            <li class="div-inline" id="person-order-review-count"><a href="<?= Url::to('/profile/index'); ?>" class="person-message"><?= $allNewOrderReviewsCount; ?></a></li>
                        <?php } ?>
                    </ul>
                <?php
                    }
                ?>
            </div>

                <?php
             if (!Yii::$app->user->isGuest) {
                ?>
            <div class="logout-wrap">
                <?php

                        echo Html::beginForm(['/security/logout'], 'post');
                        echo Html::submitButton('Выйти', [
                            'class' => 'logout lout-text logout-button',
                        ]);
                        echo Html::endForm();
                ?>
                <!--<a href="#" class="logout"><span class="logout-text">выйти</span></a>-->
            </div>
                <?php
            }
                ?>

            <ul class="left-menu-toogle">
                <li>
                    <a href="<?php echo Url::to(['/profile/index']); ?>" class="menu-profile" data-element-name="profile">
                        <span class="left-menu-icon"></span>
                        <span class="menu-text">Профиль</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Url::to(['/profile/index', '#' => 'my_projects']); ?>" class="menu-projects active">
                        <span class="left-menu-icon"></span>
                        <span class="menu-text">Проекты</span>
                        <?php if (!$isGuest && ($allNotificationsActionsByProjectCount)) { ?>
                            <span class="menu-counter-red" id="menu-counter-red-actions"><?= $allNotificationsActionsByProjectCount; ?></span>
                        <?php } ?>
                        <?php if (!$isGuest && !empty($this->params['projectsNewMessageNotifications'])): ?>
                            <span class="menu-counter" id="menu-counter-projects-messages">
                                <?= $this->params['projectsNewMessageNotifications']; ?>
                            </span>
                        <?php endif; ?>
                    </a>
                    <a href="#" class="submenu-dropdown">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </a>
                    <?php if (!$isGuest && $leftMenuProjects) { ?>
                    <ul class="menu-dropdown" id="person-project-list">
                        <?php
                            foreach ($leftMenuProjects as $lmP) {
                                /**
                                 * @var Project $lmP
                                 */
                                $link = $isCustomer ? '/project/view-offers/'.$lmP->id : ($isExecutor ? '/project/respond-project/'.$lmP->id : '');
                                $projectMessages = $lmP->projectMessages;

                                $newNotificationsCount = 0;
                                if (!empty($projectMessages)) {
                                    foreach ($projectMessages as $pM) {
                                        /**
                                         * @var ProjectMessage $pM
                                         */
                                        if (!empty($pM->newNotifications)) {
                                            $newNotificationsCount += count($pM->newNotifications);
                                        }
                                    }
                                }
                        ?>
                            <li>
                                <a href="<?= Url::to($link); ?>" data-project-id="<?= $lmP->id; ?>">
                                    <span class="submenu-text"><?= $lmP->title; ?></span>
                                    <?php
                                    if (!empty($refusedProjects) && in_array($lmP->id, $refusedProjects)) {
                                        ?>
                                            <span class="submenu-counter-red">1</span>
                                        <?php
                                    }
                                    if ($newNotificationsCount) {
                                        ?>
                                        <span class="submenu-counter"><?= $newNotificationsCount; ?></span>
                                        <?php
                                    }
                                    ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <?php } ?>
                </li>
                <li>
                    <a href="<?php echo Url::to(['/profile/index',  '#' => 'my_orders']); ?>" class="menu-orders">
                        <span class="left-menu-icon"></span>
                        <span class="menu-text">Заказы</span>
                        <?php if (!$isGuest): ?>
                            <?php if (!empty($this->params['ordersNewActionNotifications'])): ?>
                                <span class="menu-counter-red" id="menu-counter-orders">
                                    <?= $this->params['ordersNewActionNotifications']; ?>
                                </span>
                            <?php endif; ?>
                            <?php if (!empty($this->params['ordersNewMessageNotifications'])): ?>
                                <span class="menu-counter" id="menu-counter-orders-messages">
                                    <?= $this->params['ordersNewMessageNotifications'];; ?>
                                </span>
                            <?php endif;?>
                        <?php endif;?>
                    </a>
                    <a href="#" class="submenu-dropdown">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                    </a>
                    <?php if (!$isGuest && $leftMenuOrders) { ?>
                    <ul class="menu-dropdown" id="order-project-list">
                        <?php
                        foreach ($leftMenuOrders as $lmO) {
                            /**
                             * @var Order $lmO
                             */
                            $link = '/order/'.$lmO->id;
                            $orderMessages = $lmO->orderMessages;

                            $newOrderNotificationsCount = 0;
                            if (!empty($orderMessages)) {
                                foreach ($orderMessages as $oM) {
                                    /**
                                     * @var ProjectMessage $pM
                                     */
                                    if (!empty($oM->newNotifications)) {
                                        $newOrderNotificationsCount += count($oM->newNotifications);
                                    }
                                }
                            }
                        ?>
                        <li>
                            <a href="<?= Url::to($link); ?>" data-order-id="<?= $lmO->id; ?>">
                                <span class="submenu-text"><?= $lmO->title; ?></span>
                                <?php
                                    if ($lmO->newNotifications) {
                                        ?>
                                        <span class="submenu-counter-red"><?= count($lmO->newNotifications); ?></span>
                                        <?php
                                    }
                                ?>
                                <?php
                                if ($newOrderNotificationsCount) {
                                    ?>
                                    <span class="submenu-counter"><?= $newOrderNotificationsCount; ?></span>
                                    <?php
                                }
                                ?>
                            </a>
                        </li>
                        <?php } ?>
                        <!--<li><a href="#"><span class="submenu-text">Длинное название проекта</span><span class="submenu-counter">19</span></a></li>
                        <li><a href="#"><span class="submenu-text">Длинное название проекта в одну строчку</span><span class="submenu-counter">9</span></a></li>-->
                    </ul>
                    <?php } ?>
                </li>
                <li>
                    <a href="<?php echo Url::to(['/message/index']); ?>" class="menu-messages">
                        <span class="left-menu-icon"></span>
                        <span class="menu-text">Сообщения</span>
                        <?php if (!empty($countmessageNotifications)) { ?>
                        <span class="menu-counter" id="menu-counter-messages"><?= $countmessageNotifications; ?></span>
                        <?php } ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Url::to(['/profile/index', '#' => 'balance']); ?>" class="menu-invoice">
                        <span class="left-menu-icon"></span>
                        <span class="menu-text">Счет</span>
                        <span class="menu-text money"> - 1000 руб.</span>
                    </a>
                </li>
                <li>
                    <a href="<?php echo Url::to(['/profile/index', '#' => 'settings']); ?>" class="menu-settings">
                        <span class="left-menu-icon"></span>
                        <span class="menu-text">Настройки</span>
                    </a>
                </li>
            </ul>
            <a href="#" class="toogle-menu-btn"><i class="toogle-menu-img" aria-hidden="true"></i><span class="toggle-menu-text">Свернуть меню</span></a>
        </div>
    </div>
    <div class="wrapperContent">
        <div class="container headerNavAll blockPAddBorder">
            <div class="row">
                <i class="fa fa-bars navBurgers" aria-hidden="true"></i>
                <div class="col-lg-9 col-md-8 col-sm-12 navAll">
                    <ul class="navList">
                        <li>
                            <a href="<?php echo Url::to(['/project/index']); ?>" class="navLinkStyle active">Проекты</a>
                        </li>
                        <li>
                            <a href="#" class="navLinkStyle">Вакансии</a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/user-list/index']) ?>" class="navLinkStyle">Пользователи</a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['/case']) ?>" class="navLinkStyle">
                                Кейсы
                            </a>
                        </li>
                        <li>
                            <a href="<?= Url::to('/news'); ?>" class="navLinkStyle">
                                <?= Yii::t('app/news', 'News'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="navLinkStyle">Помощь</a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-12 projectAddedBlock">
                    <p class="projectAddedText">
                        <a href="<?php echo Url::to(['/project/create']); ?>">+ Опубликовать проект</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="container headerNavAll blockPAddBorder">
            <?php echo $content; ?>
<!--            <div class="row">-->
<!--                <div class="col-md-2 navAll">-->
<!--                    <h2 class="projectTitles index-title">Проекты</h2>-->
<!--                </div>-->
<!--                <div class="col-md-5 col-md-offset-5 projectFIlterText">-->
<!--                    <p>Фильтр по проектам<a href="#"> свернуть</a></p>-->
<!--                </div>-->
<!--                <div class="clearfix"></div>-->
<!--                <form class="filtersForm open">-->
<!--                    <div class="col-md-4 paddingRightNone">-->
<!--                        <div class="wrapper-custom-select">-->
<!--                            <select name="custom-select" class="custom-select" >-->
<!--                                <option value="#">Ниша</option>-->
<!--                                <option value="#">Ниша2</option>-->
<!--                                <option value="#">Ниша3</option>-->
<!--                                <option value="#">Ниша4</option>-->
<!--                            </select>-->
<!--                            <input type="text" class="inputFiltersStyle" value="Ниша" >-->
<!--                            <a href="#" class="select-next"><i class="fa fa-angle-up" aria-hidden="true"></i></a>-->
<!--                            <a href="#" class="select-prev"><i class="fa fa-angle-down" aria-hidden="true"></i></a>-->
<!--                            <div class="wrapper-dropbox-input">-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"   class="checkboxStyle" id="check1_3">-->
<!--                                    <label for="check1_3"></label>интернет-магазин </p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"   class="checkboxStyle" id="check1_4">-->
<!--                                    <label for="check1_4"></label>социальные сети</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_5">-->
<!--                                    <label for="check1_5"></label>товары почтой</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_6">-->
<!--                                    <label for="check1_6"></label>мобильное приложение</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_7">-->
<!--                                    <label for="check1_7"></label>онлайн игры</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_8">-->
<!--                                    <label for="check1_8"></label>знакомства</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_9">-->
<!--                                    <label for="check1_9"></label>финансы и кредитование</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_10">-->
<!--                                    <label for="check1_10"></label>софт</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_11">-->
<!--                                    <label for="check1_11"></label>инфобизнес</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_12">-->
<!--                                    <label for="check1_12"></label>услуги</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_13">-->
<!--                                    <label for="check1_13"></label>веб-сервис</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check1_14">-->
<!--                                    <label for="check1_14"></label>эротика</p>-->
<!--                                <ul class="checkbox-control">-->
<!--                                    <li><a href="#">выбрать все</a></li>-->
<!--                                    <li><a href="#">снять все галочки</a></li>-->
<!--                                </ul>-->
<!--                                <a href="#" class="change-filter">Применить</a>-->
<!--                            </div>-->
<!--                            <ul class="listTags">-->
<!--                                <li>-->
<!--                                    <a href="#" class="blueTags tagsFontStyle">интернет-магазин </a><span class="tagsClose blueTags">x</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#" class="greenTags tagsFontStyle">социальные сети </a><span class="tagsClose greenTags">x</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#" class="carryTags tagsFontStyle">товары почтой </a><span class="tagsClose carryTags">x</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#" class="radTags tagsFontStyle">мобильное приложение </a><span class="tagsClose radTags">x</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#" class="pincTags tagsFontStyle">онлайн игры </a><span class="tagsClose pincTags">x</span>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!---->
<!--                    </div>-->
<!--                    <div class="col-md-4 paddingRightNone">-->
<!--                        <div class="wrapper-custom-select">-->
<!--                            <select name="custom-select" class="custom-select" >-->
<!--                                <option value="#">Ниша</option>-->
<!--                                <option value="#">Ниша2</option>-->
<!--                                <option value="#">Ниша3</option>-->
<!--                                <option value="#">Ниша4</option>-->
<!--                            </select>-->
<!--                            <input type="text" class="inputFiltersStyle" value="Допустимый тип трафика" >-->
<!--                            <a href="#" class="select-next"><i class="fa fa-angle-up" aria-hidden="true"></i></a>-->
<!--                            <a href="#" class="select-prev"><i class="fa fa-angle-down" aria-hidden="true"></i></a>-->
<!--                            <div class="wrapper-dropbox-input">-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"   class="checkboxStyle" id="check_3">-->
<!--                                    <label for="check_3"></label>интернет-магазин </p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"   class="checkboxStyle" id="check_4">-->
<!--                                    <label for="check_4"></label>социальные сети</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_5">-->
<!--                                    <label for="check_5"></label>товары почтой</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_6">-->
<!--                                    <label for="check_6"></label>мобильное приложение</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_7">-->
<!--                                    <label for="check_7"></label>онлайн игры</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_8">-->
<!--                                    <label for="check_8"></label>знакомства</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_9">-->
<!--                                    <label for="check_9"></label>финансы и кредитование</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_10">-->
<!--                                    <label for="check_10"></label>софт</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_11">-->
<!--                                    <label for="check_11"></label>инфобизнес</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_12">-->
<!--                                    <label for="check_12"></label>услуги</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_13">-->
<!--                                    <label for="check_13"></label>веб-сервис</p>-->
<!--                                <p class="exceptionText ">-->
<!--                                    <input type="radio"  class="checkboxStyle" id="check_14">-->
<!--                                    <label for="check_14"></label>эротика</p>-->
<!--                                <ul class="checkbox-control">-->
<!--                                    <li><a href="#">выбрать все</a></li>-->
<!--                                    <li><a href="#">снять все галочки</a></li>-->
<!--                                </ul>-->
<!--                                <a href="#" class="change-filter">Применить</a>-->
<!--                            </div>-->
<!--                            <ul class="listTags">-->
<!--                                <li>-->
<!--                                    <a href="#" class="blueTags tagsFontStyle">интернет-магазин </a><span class="tagsClose blueTags">x</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#" class="greenTags tagsFontStyle">социальные сети </a><span class="tagsClose greenTags">x</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#" class="carryTags tagsFontStyle">товары почтой </a><span class="tagsClose carryTags">x</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#" class="radTags tagsFontStyle">мобильное приложение </a><span class="tagsClose radTags">x</span>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    <a href="#" class="pincTags tagsFontStyle">онлайн игры </a><span class="tagsClose pincTags">x</span>-->
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-4 paddingRightNone">-->
<!--                        <input type="text" class="inputFiltersStyle inputFiltersNonBg" placeholder="Поиск по ключевым словам">-->
<!--                        <ul class="listException">-->
<!--                            <li>-->
<!--                                <p class="exceptionText beforeIcon1">-->
<!--                                    <input type="radio" name="filter" class="checkboxStyle" id="check_1">-->
<!--                                    <label for="check_1"></label>исключить срочные проекты</p>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <p class="exceptionText beforeIcon2">-->
<!--                                    <input type="radio"  name="filter" class="checkboxStyle" id="check_2">-->
<!--                                    <label for="check_2"></label>исключить мультипроекты</p>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </div>-->
<!--                    <div class="clearfix"></div>-->
<!--                    <div class="col-md-4 col-md-offset-8 buttonsFilterBLock paddingRightNone">-->
<!--                        <button class="buttonFilters">Применить</button>-->
<!--                        <p class="clearFilters"><a href="#">Очистить фильтр</a></p>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div class="container blockPAddBorder">-->
<!--            <div class="col-lg-12 col-md-12 listProjectAll paddingRight">-->
<!--                <div class="projectDistributor">-->
<!--                    <div class="col-md-10 paddingRight dopZINdex">-->
<!--                        <div class="col-md-9 col-xs-8 paddingNone">-->
<!--                            <a href="#" class="distribLink-left activeDistribLink">Все</a>-->
<!--                            <a href="#" class="distribLink-left">Мои</a>-->
<!--                            <a href="#" class="distribLink-left">На модерации</a>-->
<!--                        </div>-->
<!--                        <div class="col-md-3 col-xs-4 paddingNone textAlignRight">-->
<!--                            <a href="#" class="distribLink arrowOutline active open-all">-->
<!--                                <i class="fa fa-angle-up" aria-hidden="true"></i>-->
<!--                                <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                            </a>-->
<!--                            <a href="#" class="distribLink arrowOutline close-all">-->
<!--                                <i class="fa fa-angle-down" aria-hidden="true"></i>-->
<!--                                <i class="fa fa-angle-up" aria-hidden="true"></i>-->
<!--                            </a>-->
<!--                        </div>-->
<!--                        <div class="clearfix"></div>-->
<!--                        <div class="accordionProjectFullBlock author">-->
<!--                            <div class="titlesAccordionBlock open hot clearfix">-->
<!--                                <div class="col-md-8 paddingNone">-->
<!--                                    <h3 class="titlesAccordion div-inline"><a href="#">Продажа уникальных футболок с авторским дизайном!</a></h3>-->
<!--                                    <span class="iconProj_1 iconStyleSize"></span>-->
<!--                                    <span class="iconProj_2 iconStyleSize"></span>-->
<!--                                    <span class="iconProj_3 iconStyleSize"></span>-->
<!--                                </div>-->
<!--                                <div class="col-md-4 paddingNone">-->
<!--                                    <ul class="crambsTitleAccord">-->
<!--                                        <li>-->
<!--                                            <a href="#" class="colorCrambs">Аукцион </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">380 лид. </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">23 дня</a>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="accordionContent open clearfix">-->
<!--                                <p class="textContentAccord">-->
<!--Нужно решить вопрос с возможностью выбора цвета и размера для виртумарта-->
<!--(версия 2.6.6). Исходные характеристики товара: артикул, цвет, размер.-->
<!--Артикул-общая характеристика товара (модель, узор,отделка). Цвет и-->
<!--                                    размер-зависимые опции. Покупатель должен видеть реальный цвет-->
<!--                                    товара (либо это вывод картинкию. Жду ваших предложений!-->
<!--                                </p>-->
<!--                                <ul class="tagsList col-md-9 paddingNone">-->
<!--                                    <li>-->
<!--                                        <span>Ниша: </span>-->
<!--                                        <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>-->
<!--                                        <a href="#" class="greenTags tagsFontStyle">социальные сети </a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <span>Допустимый тип трафика: </span>-->
<!--                                        <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>-->
<!--                                        <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>-->
<!--                                        <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>-->
<!--                                    </li>-->
<!--                                    <li class="lastProjPublic">-->
<!--                                        <span class="blackTextTags">Опубликован:</span> 38 минут назад-->
<!--<span class="lastProjPublic-link">-->
<!--                                            <a href="#"><span class="eyeIcon">432</span></a>-->
<!--                                            <a href="#"><span class="listIcon">23</span></a>-->
<!--                                        </span>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                                <a href="#" class="author-edit">редактировать</a>-->
<!--                                <a href="#" class="author-remove">снять с публикации</a>-->
<!--                                <a href="#" class="author-secure">закрепить</a>-->
<!--                            </div>-->
<!--                            <div class="moderator-edit">-->
<!--                                <form>-->
<!--                                    <input type="submit" value="Принять" class="agree">-->
<!--                                    <div class="refuse-wrap">-->
<!--                                        <input type="text" placeholder="Комментарий" class="refuse-text">-->
<!--                                        <input type="submit" value="Отказать" class="refuse">-->
<!--                                    </div>-->
<!--                                </form>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="accordionProjectFullBlock">-->
<!--                            <div class="titlesAccordionBlock open clearfix">-->
<!--                                <div class="col-md-8 paddingNone">-->
<!--                                    <h3 class="titlesAccordion div-inline"><a href="#">Сделать 100 холодных звонков</a></h3>-->
<!--                                    <span class="iconProj_1 iconStyleSize"></span>-->
<!--                                </div>-->
<!--                                <div class="col-md-4 paddingNone">-->
<!--                                    <ul class="crambsTitleAccord">-->
<!--                                        <li>-->
<!--                                            <a href="#" class="">145p. </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">380 лид. </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">23 дня</a>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="accordionContent open clearfix">-->
<!--                                <p class="textContentAccord">-->
<!--Необходимо сделать 100 холодных звонков с целью офоррмления заявки.-->
<!--Предложение для клиента бесплатное, так что конверсия высокая. Звонки физическим лицам.-->
<!--База, связь, скрипты предоставляются.-->
<!--Оплата по договоренности за каждый звонок + премии за оформленные заявки-->
<!--</p>-->
<!--                                <ul class="tagsList col-md-9 paddingNone">-->
<!--                                    <li>-->
<!--                                        <span>Ниша: </span>-->
<!--                                        <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>-->
<!--                                        <a href="#" class="greenTags tagsFontStyle">социальные сети </a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <span>Допустимый тип трафика: </span>-->
<!--                                        <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>-->
<!--                                        <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>-->
<!--                                        <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>-->
<!--                                    </li>-->
<!--                                    <li class="lastProjPublic">-->
<!--                                        <span class="blackTextTags">Опубликован:</span> 38 минут назад-->
<!--<span class="lastProjPublic-link">-->
<!--                                            <a href="#"><span class="eyeIcon">432</span></a>-->
<!--                                            <a href="#"><span class="listIcon">23</span></a>-->
<!--                                        </span>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                                <a href="#" class="author-edit">редактировать</a>-->
<!--                                <a href="#" class="author-remove">снять с публикации</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="accordionProjectFullBlock">-->
<!--                            <div class="titlesAccordionBlock clearfix">-->
<!--                                <div class="col-md-8 paddingNone">-->
<!--                                    <h3 class="titlesAccordion div-inline"><a href="#">Отправка коммерческих прделожений по нашей базе</a></h3>-->
<!--                                </div>-->
<!--                                <div class="col-md-4 paddingNone">-->
<!--                                    <ul class="crambsTitleAccord">-->
<!--                                        <li>-->
<!--                                            <a href="#" class="colorCrambs">Аукцион </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">380 лид. </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">23 дня</a>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="accordionContent open clearfix">-->
<!--                                <p class="textContentAccord">-->
<!--По базе 50 контактов обзвонить, узнать лицо принимающее-->
<!--                                    решение и отправить презентацию. Уточнить получение.-->
<!--                                </p>-->
<!--                                <ul class="tagsList col-md-9 paddingNone">-->
<!--                                    <li>-->
<!--                                        <span>Ниша: </span>-->
<!--                                        <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>-->
<!--                                        <a href="#" class="greenTags tagsFontStyle">социальные сети </a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <span>Допустимый тип трафика: </span>-->
<!--                                        <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>-->
<!--                                        <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>-->
<!--                                        <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>-->
<!--                                    </li>-->
<!--                                    <li class="lastProjPublic">-->
<!--                                        <span class="blackTextTags">Опубликован:</span> 38 минут назад-->
<!--<span class="lastProjPublic-link">-->
<!--                                            <a href="#"><span class="eyeIcon">432</span></a>-->
<!--                                            <a href="#"><span class="listIcon">23</span></a>-->
<!--                                        </span>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                                <a href="#" class="author-edit">редактировать</a>-->
<!--                                <a href="#" class="author-remove">снять с публикации</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="accordionProjectFullBlock">-->
<!--                            <div class="titlesAccordionBlock open clearfix">-->
<!--                                <div class="col-md-8 paddingNone">-->
<!--                                    <h3 class="titlesAccordion div-inline"><a href="#">Поиск клиентов на электронный документооборот</a></h3>-->
<!--                                </div>-->
<!--                                <div class="col-md-4 paddingNone">-->
<!--                                    <ul class="crambsTitleAccord">-->
<!--                                        <li>-->
<!--                                            <a href="#" class="">345p. </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">380 лид. </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">23 дня</a>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="accordionContent open clearfix">-->
<!--                                <p class="textContentAccord">-->
<!--Занимаюсь организацией продвижения электронного-->
<!--                                    документооборота собственной разработки АМАС.-->
<!--Отличный фунционал, простота и удобство, очень-->
<!--                                    низкая стоимость владения. Оплата -10-20 % от-->
<!--                                    суммы заказа. Ищу партнеров для поиска клиентов.-->
<!--                                </p>-->
<!--                                <ul class="tagsList col-md-9 paddingNone">-->
<!--                                    <li>-->
<!--                                        <span>Ниша: </span>-->
<!--                                        <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>-->
<!--                                        <a href="#" class="greenTags tagsFontStyle">социальные сети </a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <span>Допустимый тип трафика: </span>-->
<!--                                        <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>-->
<!--                                        <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>-->
<!--                                        <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>-->
<!--                                    </li>-->
<!--                                    <li class="lastProjPublic">-->
<!--                                        <span class="blackTextTags">Опубликован:</span> 38 минут назад-->
<!--<span class="lastProjPublic-link">-->
<!--                                            <a href="#"><span class="eyeIcon">432</span></a>-->
<!--                                            <a href="#"><span class="listIcon">23</span></a>-->
<!--                                        </span>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                                <a href="#" class="author-edit">редактировать</a>-->
<!--                                <a href="#" class="author-remove">снять с публикации</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="accordionProjectFullBlock">-->
<!--                            <div class="titlesAccordionBlock open clearfix">-->
<!--                                <div class="col-md-8 paddingNone">-->
<!--                                    <h3 class="titlesAccordion div-inline"><a href="#">Привлечение лидов на автокредит</a></h3>-->
<!--                                    <span class="iconProj_1 iconStyleSize"></span>-->
<!--                                    <span class="iconProj_2 iconStyleSize"></span>-->
<!--                                </div>-->
<!--                                <div class="col-md-4 paddingNone">-->
<!--                                    <ul class="crambsTitleAccord">-->
<!--                                        <li>-->
<!--                                            <a href="#" class="">145p. </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">380 лид. </a><span> |</span>-->
<!--                                        </li>-->
<!--                                        <li>-->
<!--                                            <a href="#">23 дня</a>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="accordionContent open clearfix">-->
<!--                                <p class="textContentAccord">-->
<!--Требуется привлечение клиентов на заключение договора на автокредит.-->
<!--Одобрение 99%, взнос клиента 15 % от стоимости автомобиля (от 120 000 т)-->
<!--                                    за каждый подписанный договор 10 000 тысяч. Всю необходимую информацию-->
<!--                                    можем предоставить.-->
<!--Наши клиенты это люди с плохой банковской кредитной истории-->
<!--                                    и люди класса ниже среднего. Условия для клиента не самые гуманные,-->
<!--                                    первоначальный взнос 15 % 25 % годовых.-->
<!--                                </p>-->
<!--                                <ul class="tagsList col-md-9 paddingNone">-->
<!--                                    <li>-->
<!--                                        <span>Ниша: </span>-->
<!--                                        <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>-->
<!--                                        <a href="#" class="greenTags tagsFontStyle">социальные сети </a>-->
<!--                                    </li>-->
<!--                                    <li>-->
<!--                                        <span>Допустимый тип трафика: </span>-->
<!--                                        <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>-->
<!--                                        <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>-->
<!--                                        <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>-->
<!--                                    </li>-->
<!--                                    <li class="lastProjPublic">-->
<!--                                        <span class="blackTextTags">Опубликован:</span> 38 минут назад-->
<!--<span class="lastProjPublic-link">-->
<!--                                            <a href="#"><span class="eyeIcon">432</span></a>-->
<!--                                            <a href="#"><span class="listIcon">23</span></a>-->
<!--                                        </span>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                                <a href="#" class="author-edit">редактировать</a>-->
<!--                                <a href="#" class="author-remove">снять с публикации</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-12 pagination-wrap">-->
<!--                            <nav aria-label="Page navigation">-->
<!--                                <ul class="pagination only-next">-->
<!--                                    <li>-->
<!--                                        <a href="#" aria-label="Previous">-->
<!--                                            <span aria-hidden="true"><span class="next-mob">&laquo;</span><span class="next-desc"><img src="img/index/pagin-arrow.png" alt=""> предыдущая</span></span>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                    <li class="numberPagination active"><a href="#">1</a></li>-->
<!--                                    <li class="numberPagination"><a href="#">2</a></li>-->
<!--                                    <li class="numberPagination"><a href="#">3</a></li>-->
<!--                                    <li>-->
<!--                                        <a href="#" aria-label="Next">-->
<!--                                            <span aria-hidden="true"><span class="next-mob">&raquo;</span><span class="next-desc">следующая <img src="img/index/pagin-arrow.png" alt=""></span></span>-->
<!--                                        </a>-->
<!--                                    </li>-->
<!--                                </ul>-->
<!--                            </nav>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
        </div>
        <footer>
            <div class="container footerAll">
                <div class="row">
                    <div class="col-md-10 col-sm-12 col-xs-12 paddingNone">
                        <div class="col-md-3 col-sm-3 col-xs-6 footerListMediaCenter paddingNone">
                            <ul class="footerList">
                                <li class="titleFooterList">Полезное</li>
                                <li>
                                    <a href="#">Проекты</a>
                                </li>
                                <li>
                                    <a href="#">Вакансии</a>
                                </li>
                                <li>
                                    <a href="#">Пользователи</a>
                                </li>
                                <li>
                                    <a href="#">Новости</a>
                                </li>
                                <li>
                                    <a href="#">Помощь</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 footerListMediaCenter paddingNone">
                            <ul class="footerList">
                                <li class="titleFooterList">Мой профиль</li>
                                <li>
                                    <a href="#">Профиль</a>
                                </li>
                                <li>
                                    <a href="#">Сообщения</a>
                                </li>
                                <li>
                                    <a href="#">Счет</a>
                                </li>
                                <li>
                                    <a href="#">Настройки</a>
                                </li>
                                <li>
                                    <a href="#">Выйти</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 footerListMediaCenter paddingNone">
                            <ul class="footerList">
                                <li class="titleFooterList">Платные услуги</li>
                                <li>
                                    <a href="#">Профиль</a>
                                </li>
                                <li>
                                    <a href="#">Сообщения</a>
                                </li>
                                <li>
                                    <a href="#">Аккаунт PRO</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6 footerListMediaCenter paddingNone">
                            <ul class="footerList">
                                <li class="titleFooterList">Найти работу</li>
                                <li>
                                    <a href="#">Поиск</a>
                                </li>
                                <li>
                                    <a href="#">Вакансии</a>
                                </li>
                                <li>
                                    <a href="#">Проекты</a>
                                </li>
                                <li>
                                    <a href="#">Настройки</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-12 col-xs-12 paddingNone">
                        <ul class="footerList socialList">
                            <li class="titleFooterList">Мы в соц. сетях</li>
                            <li class="listSocialLink twitterLink">
                                <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                            </li>
                            <li class="listSocialLink linkedinLink">
                                <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                            </li>
                            <li class="listSocialLink googlePlusLink">
                                <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                            </li>
                            <li class="clearfix"></li>
                            <li class="listSocialLink faceBookLink">
                                <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            </li>
                            <li class="listSocialLink vkLink">
                                <a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                            </li>
                            <li class="clearfix"></li>
                            <li class="companyFooter">LeadLance © 2016</li>
                        </ul>
                    </div>
                </div>
                <div class="linkToTop" id="linkToTop">
                    <a href="#"><span class="iconLincTop"><img src="/themes/leadlance/img/index/linkTop.png" alt=""></span> наверх</a>
                </div>
            </div>
        </footer>
    </div>
</div>
<?php
    Yii::$app->footerWidgetLoader->load();
?>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.custom-scrollbar.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script src="js/common.js"></script>-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
