<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 20.06.17
 * @time: 16:23
 */

use yii\widgets\ListView;
use yii\web\View;

/**
 * @var View $this
 *
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var int $currentUserId
 */

//@todo move this css in a separate .css file (file or asset)
$this->registerCss(
    '
        .title-news__date, .title-news, title-case, title-case__date {
            display:inline-block;
            width: 30%;
        }
 
        .title-case {
            padding-top: 0px !important
        }
        
        .title-news__date, .title-case__date {
            vertical-align: top;
            padding-top: 2px;
            float: right;
        }
        
        .text-wrap-news {
            position: relative;
        }
        
        .text-news {
            min-height:190px;
        }
        
        .footer-news__tags {
            float: right;
        }
    '
);
?>
<div class="row ">
    <div class="col-md-7">
        <h2 class="projectTitles page-titles message-title">Кейсы</h2>
    </div>
</div>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_case_item',
    'itemOptions' => [
        'currentUserId' => $currentUserId,
        'tag'           => false,
    ],
    'summary' => false,
]);
