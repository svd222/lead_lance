<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 20.06.17
 * @time: 16:23
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use yii\widgets\ListView;
use yii\web\View;
use common\models\ProfileCase;
use common\models\Comment;

/**
 * @var View $this
 * @var ProfileCase $model
 * @var ListView $widget
 */


/**
 * @var User $user
 */
$user = $model->user;

?>

<div class="content-project-all clearfix">
    <div class="col-md-4 paddingNone">
        <a href="<?= Url::to('/profile/case-view/' . $model->id); ?>">
            <?php if ($model->preview_image) {
                $src = User::UPLOAD_WEB . DIRECTORY_SEPARATOR . $user->upload_dir . DIRECTORY_SEPARATOR . $model->preview_image;
                ?>
                <div class="img-our-project">
                    <img src="<?= $model->preview_image; ?>" style="width: 293px; height: 214px;"> <!--width: 315px; height: 150px;-->
                </div>
            <?php } else { ?>
                <div class="photo-news div-inline">
                    <p class="photo-bold">LeadLance</p>
                    <p class="photo-light">(no photo)</p>
                </div>
            <?php } ?>
        </a>
    </div>

    <div class="col-md-8 paddingNone opis-project-style-all">
        <h3 class="title-case">
            <a href="<?= Url::to('/profile/case-view/' . $model->id); ?>" class="title-case-link">
                <?= Html::encode($model->title); ?>
            </a>
            <span class="title-case__date">
                <?php if (!empty($model->created_at)): ?>
                    <a href="#">
                        <?= Yii::$app->formatter->asTime($model->created_at, 'H:m'); ?>
                    </a>
                    <span> | </span>
                    <a href="#">
                        <?= Yii::$app->formatter->asDate($model->created_at, 'dd.MM.yyyy'); ?>
                    </a>
                <?php endif; ?>
            </span>
        </h3>
        <p class="some-project-text">
            <?php
                $text = $model->description;
                $length = 300;
                $text = mb_substr($text, 0, $length,'UTF-8');
                $position = mb_strrpos($text, ' ', 'UTF-8');
                $text = mb_substr($text, 0, $position, 'UTF-8');
            ?>
            <?= $text; ?>
            <a href="<?= Url::to('/profile/case-view/' . $model->id); ?>" class="text-news__read-more">Читать дальше</a>
        </p>
        <div class="footer-news">
            <?php if (!empty($user)): ?>
                <a href="/profile?id=<?= $user->id; ?>" class="footer-news__link-author">
                    <?= $user->username; ?>
                </a>
            <?php endif; ?>

            <div class="footer-news__tags">
                <div class="counter-mess div-inline">
                    <a href="<?= Url::to('/profile/case-view/' . $model->id); ?>">
                        <img src="/themes/leadlance/img/message/message.png" alt="">
                        <span>
                            <?= !empty($model) ? $model->getComment()->count() : 0; ?>
                        </span>
                    </a>
                </div>
            </div>
            <?php if (!empty($widget->itemOptions['currentUserId'])
                && $widget->itemOptions['currentUserId'] == $user->id): ?>
                <div class="redact-and-delet-all edit-delete-links">
                <span class="delet-link-proj-inner">
                    <a href="<?= Url::to('/profile/delete-case/' . $model->id); ?>"
                       aria-label="<?= Yii::t('app/profile_case', 'Delete'); ?>"
                       data-confirm="<?= Yii::t('app/profile_case', 'Are you sure you want to delete this case?'); ?>">удалить</a>
                </span>
                    <span class="red-link-proj-inner">
                    <a href="<?= Url::to('/profile/create-case/' . $model->id); ?>">редактировать</a>
                </span>
                </div>
            <?php endif; ?>
        </div>
    </div>

</div>
