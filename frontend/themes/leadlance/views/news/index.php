<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/18/2018
 * Time: 8:57 PM
 */

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
?>

<div class="row ">
    <div class="col-md-7">
        <h2 class="news-titles page-titles message-title">Новости</h2>
    </div>
    <div class="col-md-5">
        <span class="search-wrap">
            <input type="text" id="search-input-news" name="search-input-news" class="search-input" placeholder="Поиск">
            <label class="submit-wrap">
                <input type="submit" id="start-search-news" class="start-search" value="">
            </label>
        </span>
    </div>
    <div class="col-md-12 col-xs-12">
        <a href="#" class="distribLink-left activeDistribLink">Все</a>
    </div>
</div>

<div id="news-list">
    <?= $this->render('_news_list', [
            'dataProvider' => $dataProvider,
    ]); ?>
</div>
