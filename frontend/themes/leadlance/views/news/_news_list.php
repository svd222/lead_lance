<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/25/2018
 * Time: 14:55
 */

use yii\widgets\ListView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'     => '_news_item',
    'itemOptions'  => [
        'tag' => false,
    ],
    'summary'      => false,
]); ?>
