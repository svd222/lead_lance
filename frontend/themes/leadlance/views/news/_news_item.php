<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/19/2018
 * Time: 11:18 AM
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use yii\web\View;
use common\models\News;

/**
 * @var View $this
 * @var News $model
 */

/**
 * @var User $user
 */
$user = $model->user;

?>
<div class="row">
    <div class="news-wrap col-md-12">
        <div class="photo-news div-inline">
            <a href="<?= Url::to('/news/view/' . $model->id); ?>" target="_blank">
                <?php if (!empty($model->image)): ?>
                    <img src="<?= News::UPLOAD_WEB . DIRECTORY_SEPARATOR . $model->image; ?>" alt="LeadLance">
                <?php else: ?>
                    <p class="photo-bold">LeadLance</p>
                    <p class="photo-light">(no photo)</p>
                <?php endif; ?>
            </a>
        </div>
        <div class="text-wrap-news div-inline">
            <h3 class="title-news">
                <a href="<?= Url::to('/news/view/' . $model->id); ?>" class="title-news-link">
                    <?= Html::encode($model->title); ?>
                </a>
                <span class="title-news__date">
                <?php if (!empty($model->created_at)): ?>
                    <a href="#">
                        <?= Yii::$app->formatter->asTime($model->created_at, 'H:m'); ?>
                    </a>
                    <span> | </span>
                    <a href="#">
                        <?= Yii::$app->formatter->asDate($model->created_at, 'dd.MM.yyyy'); ?>
                    </a>
                <?php endif; ?>
                </span>
            </h3>
            <div class="text-news">
                <?php
                    $text = $model->text;
                    $length = 300;
                    $text = mb_substr($text, 0, $length,'UTF-8');
                    $position = mb_strrpos($text, ' ', 'UTF-8');
                    $text = mb_substr($text, 0, $position, 'UTF-8');
                ?>
                <?= $text; ?>
                <a href="<?= Url::to('/news/view/' . $model->id); ?>" class="text-news__read-more">Читать дальше</a>
            </div>
            <div class="footer-news">
                <?php if (!empty($user)): ?>
                    <a href="/profile?id=<?= $user->id; ?>" class="footer-news__link-author">
                        <?= $user->username; ?>
                    </a>
                <?php endif; ?>
                <div class="footer-news__tags">

                    <p class="div-inline tags-news">
                        <?php if (!empty($model->tags)): ?>
                            Теги:
                            <?php
                                $tags = explode(',', $model->tags);
                                $tagsCount = count($tags);
                            ?>
                            <?php for ($i = 0; $i < $tagsCount; $i++): ?>
                                <?php
                                    $tagClass = ($i % 2) ? 'tags-news__link_red' : 'tags-news__link_blue';
                                ?>
                                <a href="#" class="tags-news__link <?= $tagClass; ?>">
                                    <?= $tags[$i]; ?>
                                </a>
                                <?php if ($i != $tagsCount - 1): ?>
                                    ,
                                <?php endif; ?>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </p>
                    <div class="counter-mess div-inline">
                        <a href="<?= Url::to('/news/view/' . $model->id); ?>">
                            <img src="/themes/leadlance/img/message/message.png" alt="">
                            <span>
                                <?= !empty($model) ? $model->getComment()->count() : 0; ?>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
