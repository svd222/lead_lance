<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/19/2018
 * Time: 7:27 PM
 */

use yii\helpers\Html;
use common\models\User;
use yii\web\View;
use common\models\News;
use yii\bootstrap\ActiveForm;
use common\models\ProfilePrivate;
use common\models\UserAvatar;
use common\models\Comment;

/**
 * @var View      $this
 * @var News      $news
 * @var Comment   $comment
 * @var int       $commentCount
 * @var Comment[] $rootComments
 */

/**
 * @var User $user
 */
$user = $news->user;

/**
 * @var ProfilePrivate $profilePrivate
 */
$profilePrivate = $user->profilePrivate;

if (!empty($profilePrivate) && !empty($profilePrivate->userAvatar)) {
    /**
     * @var UserAvatar $userAvatar
     */
    $userAvatar = $profilePrivate->userAvatar;
}
?>

<div class="row">
    <div class="news-wrap news-wrap_news-open col-md-12">
        <div class="photo-news div-inline">
            <p class="photo-bold">LeadLance</p>
            <p class="photo-light">(no photo)</p>
            <img src="/img/news/news-title.png" alt="">
        </div>
        <div class="text-wrap-news div-inline">
            <h3 class="title-news">
                <?= Html::encode($news->title); ?>
                <span class="title-news__date">
                    <a href="#">
                        <?= Yii::$app->formatter->asTime($news->created_at, 'H:m'); ?>
                    </a>
                    <span> | </span>
                    <a href="#">
                        <?= Yii::$app->formatter->asDate($news->created_at, 'dd.MM.yyyy'); ?>
                    </a>
                </span>
            </h3>
            <div class="text-news">
                <?= $news->text; ?>
            </div>

            <a href="/news" class="back-to-all-news">
                <img src="/img/news/arrow-back.png" alt="" class="back-to-all-news__img div-inline">
                <span class="back-to-all-news__text div-inline">Вернуться ко всем статьям</span>
            </a>
            <div class="footer-news">
                <?php if (!empty($user)): ?>
                    <?php if (!empty($userAvatar)): ?>
                        <img src="<?= UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $userAvatar->image; ?>" alt="" class="footer-news__logo-author" style="width: 50px; height: 50px;">
                    <?php else: ?>
                        <img src="/themes/leadlance/img/news/author-logo.png" alt="" class="footer-news__logo-author" style="width: 50px; height: 50px;">
                    <?php endif; ?>
                    <a href="/profile?id=<?= $user->id; ?>" class="footer-news__link-author">
                        <?= $user->username; ?>
                    </a>
                <?php endif; ?>
                <div class="footer-news__tags">

                    <p class="div-inline tags-news">
                        <?php if (!empty($news->tags)): ?>
                            Теги:
                            <?php
                            $tags = explode(',', $news->tags);
                            $tagsCount = count($tags);
                            ?>
                            <?php for ($i = 0; $i < $tagsCount; $i++): ?>
                                <?php
                                $tagClass = ($i % 2) ? 'tags-news__link_red' : 'tags-news__link_blue';
                                ?>
                                <a href="#" class="tags-news__link <?= $tagClass; ?>">
                                    <?= $tags[$i]; ?>
                                </a>
                                <?php if ($i != $tagsCount - 1): ?>
                                    ,
                                <?php endif; ?>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </p>
                    <div class="counter-mess div-inline">
                        <a href="#">
                            <img src="/themes/leadlance/img/message/message.png" alt="">
                            <span id="comment-count">
                                <?= !empty($commentCount) ? $commentCount : 0; ?>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$form = ActiveForm::begin([
    'id' => $comment->formName(),
    'options' => [
        'class' => 'write-comment'
    ],
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => '{input}',
        'options' => [
            'tag' => false,
        ],
    ]
]);

?>
    <div class="row">
        <div class="col-md-12 wrap-textarea">
            <label for="<?= $comment->formName() . '_message' ?>" class="wrap-textarea__title">Оставить комментарий</label>
            <!--<textarea name="comment-text" class="form-comment-area" id="comment-area" placeholder="Текст сообщения"></textarea>-->
            <?php
            echo $form->field($comment, 'message')
                ->textarea([
                    'class' => 'form-comment-area',
                    'placeholder' => 'Текст сообщения',
                ]);
            ?>
            <?php
            echo $form->field($comment, 'from_user_id')->hiddenInput();
            echo $form->field($comment, 'item_id')->hiddenInput();
            echo $form->field($comment, 'parent_id')->hiddenInput();
            echo Html::button(Yii::t('app/comment', 'Send comment'), [
                'class' => 'buttonFilters',
                'id'    => 'send-news-comment',
            ]);
            ?>
            <!--<button class="buttonFilters">Отправить сообщение</button>-->
        </div>
    </div>

<?php
ActiveForm::end();
?>
<br/>
<div id="news-comments">
    <?php if (!empty($rootComments)): ?>
        <?= $this->render('/comment/_comment_tree', [
            'rootComments' => $rootComments,
        ]); ?>
    <?php endif; ?>
</div>
