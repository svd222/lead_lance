<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 23.06.17
 * @time: 15:29
 */

use yii\web\View;
use common\models\Comment;
use yii\helpers\Url;
use common\models\UserAvatar;

/**
 * @var View    $this
 * @var Comment $comment
 */

$containerClass = $comment->depth == 0 ? 'col-md-12' : 'col-md-11 col-sm-11 col-xs-11 col-md-offset-1 col-sm-offset-1 col-xs-offset-1';
$fromUser = $comment->fromUser;
/**
 * @var ProfilePrivate $fromUserProfilePrivate
 */
$fromUserProfilePrivate = ($comment->fromUser->profilePrivate) ? $comment->fromUser->profilePrivate : '';
$fromUserFullName = '';
$fromUserHasPro = !empty($fromUser->proAccount);
$createdAt = strtotime($comment->created_at);
/**
 * @var UserAvatar $userAvatar
 */
$userAvatar = $fromUserProfilePrivate ? $fromUserProfilePrivate->userAvatar : null;
$userAvatarSrc = '';
if ($userAvatar && $userAvatar->image) {
    $userAvatarSrc = UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $userAvatar->image;
}
?>
<div class="<?= $containerClass; ?>">
    <div class="disput news-comment">
        <?php if ($userAvatarSrc) { ?>
        <div class="pers-avatar div-inline">
            <img src="<?= $userAvatarSrc; ?>" style="width:50px; height:50px"/>
        </div>
        <?php } else { ?>
        <div class="pers-avatar div-inline">

        </div>
        <?php } ?>
        <div class="div-inline comment ">
            <div class="agree-order_name div-inline">
                <p>
                    <?php if ($fromUserProfilePrivate && $fromUserProfilePrivate->fullName) { ?>
                        <span class="name"><a href="<?= Url::to(['/profile/index/' . $fromUser->id]); ?>" class="hover-text-decoration"><?= $fromUserProfilePrivate->fullName; ?></a> </span>
                    <?php } ?>
                    <span class="nickname">(<?= $fromUser->username; ?>)</span>
                    <?php if ($fromUserHasPro) {
                        //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
                        ?>
                        <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                        <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
                        <?php
                    }
                    ?>
                    <span class="date">(<?= date('H:i',$createdAt); ?> | <?= date('d.m.Y',$createdAt); ?>)</span>
                </p>
            </div>
            <p class="comment-text">
                <?= $comment->message; ?>
            </p>
            <a href="#" class="answer-comment" data-comment-id="<?= $comment->id; ?>">Ответить</a>
        </div>
    </div>
</div>
