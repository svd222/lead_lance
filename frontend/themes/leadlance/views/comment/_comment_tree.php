<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 02/03/2018
 * Time: 11:52 AM
 */

use common\models\Comment;

/**
 * @var Comment[] $rootComments
 */

?>

<div class="container blockPAddBorder disput-review">
    <div class="row">
        <?php
            for ($i = 0; $i < count($rootComments); $i++) {
                $comment = $rootComments[$i];
                echo $this->render('/comment/_comment', [
                    'comment' => $comment
                ]);
                $childComments = $comment->children()->all();
                foreach ($childComments as $comment2) {
                    echo $this->render('/comment/_comment', [
                        'comment' => $comment2
                    ]);

                }
            }
        ?>
    </div>
</div>
