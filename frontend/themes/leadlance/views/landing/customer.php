<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 11.01.17
 * @time: 21:20
 */
?>
<section class="section_1">
    <div class="container-fluid">
        <div class="container">
            <div class="row media-text-center">
                <p class="section-1-head-text">Устали от неэффективной рекламы?</p>
                <p class="section-1-head-text">Маркетологи сливают Ваши бюджеты впустую,</p>
                <p class="section-1-head-text">а клиентов не прибавляется?</p>
                <p class="big-blue-text big-blue-text-first-text">Выберите специалиста</p>
                <p class="big-blue-text">с оплатой за результат!</p>
                <div class="line-big-blue-text this-line-position"></div>
                <div class="block-section-1-text-bottom">
                    <p><span class="strong-text-style">Leadlance — специализированная биржа интернет-маркетологов.</span>
                        Здесь вы найдете профессионалов по интернет-маркетингу и арбитражу
                        трафика. Как на проекты с оплатой по факту достижения цели,
                        так и на вакансии, с фиксированным ежемесячным окладом. </p>
                </div>
                <div class="line-big-blue-text this-line-position"></div>
                <button class="all-blue-style-button section-1-button-position">Начать работу</button>
            </div>
        </div>
    </div>
</section>
<section class="section_2">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <p class="section-2-title">Здесь работают специалисты по следующим направлениям</p>
                <div class="col-md-6 col-sm-6 paddingNone">
                    <ul class="list-section-2-left all-style-section-2-list">
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Арбитраж трафика
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            SMM, SMO <span class="mini-text-list-sect-2">(маркетинг в социальных сетях)</span>
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Контекстная реклама <span class="mini-text-list-sect-2">(Я.Директ и G.Adwords)</span>
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Таргетированная реклама
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Тизерные сети
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 media-none-padding">
                    <ul class="list-section-2-right all-style-section-2-list">
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Реклама в мобильных приложениях
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            E-mail маркетинг
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Реклама на Youtube и видеореклама
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Avito и другие доски объявлений
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Блоггинг <span class="mini-text-list-sect-2">(владельцы блогов и интернет-сайтов)</span>
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <button class="all-white-button-style buton-section-2-position">Выбрать специалиста</button>
            </div>
        </div>
    </div>
</section>
<section class="section_3">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="title-section-3">
                    <h3>Как это работает?</h3>
                </div>
                <div class="content-section-3">
                    <div class="col-md-6 col-sm-6 section-content-padding">
                        <p class="big-number-block number-line-1">01</p>
                        <p class="some-text-content-first">Вы создаете проект</br>
                            и указываете за какие действия</br>
                            и сколько готовы платить</p>
                    </div>
                    <div class="col-md-6 col-sm-6 section-content-padding">
                        <p class="big-number-block number-line-2">02</p>
                        <p class="some-text-content-first">Получаете отклики</br>
                            от специалистов, готовых</br>
                            достичь нужных показателей</p>
                    </div>
                    <div class="col-md-6 col-sm-6 section-content-padding-bottom hidden-xs visible-lg visible-md visible-sm">
                        <p class="big-number-block text-right-bottom-position">04</p>
                        <p class="some-text-content-first text-right">После достижения специалистом оговоренных</br>
                            результатов, вы подтверждаете, что заказ</br>
                            выполнен и деньги с Вашего счета переходят</br>
                            на счет исполнителя</p>
                    </div>
                    <div class="col-md-6 col-sm-6 section-content-padding-bottom">
                        <p class="big-number-block number-line-3 text-right-bottom-position">03</p>
                        <p class="some-text-content-first text-right">На основе их отзывов и кейсов, выбираете</br>
                            одного или нескольких кандидатов</br>
                            и начинаете сотрудничество</p>
                    </div>
                    <div class="col-md-6 col-sm-6 section-content-padding-bottom visible-xs hidden-lg hidden-md hidden-sm">
                        <p class="big-number-block text-right-bottom-position">04</p>
                        <p class="some-text-content-first text-right">После достижения специалистом оговоренных</br>
                            результатов, вы подтверждаете, что заказ</br>
                            выполнен и деньги с Вашего счета переходят</br>
                            на счет исполнителя</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_4">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <h3 class="title-section-4">Давайте разберем на конкретных примерах</h3>
            </div>
        </div>
    </div>
</section>
<section class="section-nav-slider">
    <div class="slider-problem">
        <div>
            <section class="section_5">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="person-block-section-5 col-md-12">
                                <img src="/themes/leadlance/landing/img/landing/person-lend.png" alt="" class="div-inline vertical-middle-all">
                                <p class="some-person-text div-inline vertical-middle-all">Илья, розничная продажа товаров из Китая</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_6">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-problem ">
                                <div class="col-md-4 col-sm-4">
                                    <h2 class="title-problem">ПРОБЛЕМА</h2>
                                </div>
                                <div class="col-md-8  col-sm-8">
                                    <p class="some-text-problem">
                                        В связи с ажиотажем на iPhone 7, Илья оптом закупил партию в 100 его реплик,
                                        но он слабо разбирается в интернет-маркетинге и не знает, как самостоятельно привлечь покупателей.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_7">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-decision">
                                <div class="col-md-7  col-sm-6">
                                    <div class="col-md-5 col-sm-6 visible-xs hidden-sm hidden-md hidden-lg">
                                        <h2 class="title-decision">РЕШЕНИЕ</h2>
                                    </div>
                                    <p class="some-left-decision-text">С каждой продажи Илья готов тратить по 1 000 рублей на рекламу.
                                        Он создает проект на Leadlance, где указывает, что готов платить
                                        за  подтвержденные заявки на сайте
                                    </p>
                                    <ul class="decision-list">
                                        <li>Параметры проекта:</li>
                                        <li>— цена за лид – 1000 руб.</li>
                                        <li>— количество – 100 лидов</li>
                                        <li>— срок – 1 месяц.</li>
                                    </ul>
                                    <p class="some-left-decision-text">
                                        Далее получает отклики от заинтересовавшихся специалистов.
                                        Затем выбирает наиболее подходящего кандидата и создает заказ.
                                    </p>
                                </div>
                                <div class="col-md-5  col-sm-6">
                                    <h2 class="title-decision hidden-xs visible-lg visible-md visible-sm" >РЕШЕНИЕ</h2>
                                    <button class="all-blue-style-button-transparent style-position-button-section-7">Опубликовать аналогичный проект</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_8">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-total">
                                <div class="col-md-5  col-sm-5">
                                    <h2 class="total-title">ИТОГ</h2>
                                </div>
                                <div class="col-md-7  col-sm-7">
                                    <p class="total-some-text">Илья, больше не переживает о рекламе.
                                        Ему осталось только обрабатывать
                                        заказы на сайте и получать прибыль.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div>
            <section class="section_5">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="person-block-section-5 col-md-12">
                                <img src="/themes/leadlance/landing/img/landing/person-lend.png" alt="" class="div-inline vertical-middle-all">
                                <p class="some-person-text div-inline vertical-middle-all">Клим, владелец студии по разработке
                                    мобильных приложений</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_6">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-problem ">
                                <div class="col-md-4 col-sm-4">
                                    <h2 class="title-problem">ПРОБЛЕМА</h2>
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <p class="some-text-problem">
                                        Каждый раз после релиза очередного приложения, Климу нужно
                                        его тестировать на небольшой аудитории 5-10 тыс. человек,
                                        чтобы оценить  реакцию потребителей, а также найти возможные
                                        ошибки в разработке.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_7">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-decision">
                                <div class="col-md-7 col-sm-6">
                                    <div class="col-md-5 col-sm-6 visible-xs hidden-sm hidden-md hidden-lg">
                                        <h2 class="title-decision">РЕШЕНИЕ</h2>
                                    </div>
                                    <p class="some-left-decision-text">Клим создает проект на Лидланс, в котором уточняет требования к целевой
                                        аудитории, коэффициенты возврата и активности пользователей, а в качестве
                                        требуемого действия выбирает установки приложения.
                                    </p>
                                    <ul class="decision-list">
                                        <li>Параметры проекта:</li>
                                        <li>— цену за лид – 50 руб.</li>
                                        <li>— количество – 5000 лидов</li>
                                        <li>— срок – 15 дней.</li>
                                    </ul>
                                    <p class="some-left-decision-text">
                                        Выбирает специалиста с необходимым опытом и навыками, и создает заказ.
                                    </p>
                                </div>
                                <div class="col-md-5 col-sm-6">
                                    <h2 class="title-decision  hidden-xs visible-lg visible-md visible-sm">РЕШЕНИЕ</h2>
                                    <button class="all-blue-style-button-transparent style-position-button-section-7">Опубликовать аналогичный проект</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_8">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-total">
                                <div class="col-md-5 col-sm-5">
                                    <h2 class="total-title">ИТОГ</h2>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <p class="total-some-text">Теперь Клим может сконцентрироваться на разработке,
                                        а продвижением приложений займутся профессионалы с Leadlance.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div>
            <section class="section_5">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="person-block-section-5 col-md-12">
                                <img src="/themes/leadlance/landing/img/landing/person-lend.png" alt="" class="div-inline vertical-middle-all">
                                <p class="some-person-text div-inline vertical-middle-all">Екатерина, бизнес-тренер</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_6">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-problem ">
                                <div class="col-md-4 col-sm-4">
                                    <h2 class="title-problem">ПРОБЛЕМА</h2>
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <p class="some-text-problem">
                                        Екатерина — бизнес-тренер и профессионал своего дела, она
                                        могла бы многому научить своих слушателей, но вот с рекламой
                                        собственных тренингов у нее проблема. Она просто не умеет их продвигать.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_7">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-decision">
                                <div class="col-md-5 col-sm-6 visible-xs hidden-sm hidden-md hidden-lg">
                                    <h2 class="title-decision">РЕШЕНИЕ</h2>
                                </div>
                                <div class="col-md-7 col-sm-6">
                                    <p class="some-left-decision-text">Екатерина готова платить за каждого
                                        привлеченного участника тренинга по 700 рублей. Она создает
                                        проект на Leadlance, где описывает свою целевую аудиторию,
                                        уточняет географию и указывает дату, когда состоится событие.
                                    </p>
                                    <ul class="decision-list">
                                        <li>Параметры проекта:</li>
                                        <li>— цена за лид – 700 руб.</li>
                                        <li>— количество – 30 лидов</li>
                                        <li>— срок – до 15 февраля.</li>
                                    </ul>
                                    <p class="some-left-decision-text">
                                        Далее выбирает нужного специалиста и создает заказ.
                                    </p>
                                </div>
                                <div class="col-md-5 col-sm-6">
                                    <h2 class="title-decision  hidden-xs visible-lg visible-md visible-sm">РЕШЕНИЕ</h2>
                                    <button class="all-blue-style-button-transparent style-position-button-section-7">Опубликовать аналогичный проект</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_8">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-total">
                                <div class="col-md-5 col-sm-5">
                                    <h2 class="total-title">ИТОГ</h2>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <p class="total-some-text">Выбранный специалист проведет
                                        рекламную компанию и соберет людей на тренинг
                                        к указанной дате, а Екатерине сможет заниматься
                                        любимым делом, больше не переживая проблемах с продвижением.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div>
            <section class="section_5">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="person-block-section-5 col-md-12">
                                <img src="/themes/leadlance/landing/img/landing/person-lend.png" alt="" class="div-inline vertical-middle-all">
                                <p class="some-person-text div-inline vertical-middle-all">Антон, владелец интернет-магазина </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_6">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-problem ">
                                <div class="col-md-4 col-sm-4">
                                    <h2 class="title-problem">ПРОБЛЕМА</h2>
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <p class="some-text-problem">
                                        Антон регулярно повышает рекламные бюджеты, но заказов от этого
                                        больше не становится. Маркетологи «осваивают» бюджеты, а не
                                        работают на результат. Антон ввел в ассортимент магазина новый
                                        продукт и решил для его раскрутки воспользоваться  схемой с оплатой за результат.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_7">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-decision">
                                <div class="col-md-5 col-sm-6 visible-xs hidden-sm hidden-md hidden-lg">
                                    <h2 class="title-decision">РЕШЕНИЕ</h2>
                                </div>
                                <div class="col-md-7 col-sm-6">
                                    <p class="some-left-decision-text">Антон создает проект на Лидланс, в котором описывает
                                        товар, а в качестве требуемого действия указывает оплаченный заказ.
                                    </p>
                                    <ul class="decision-list">
                                        <li>Параметры проекта:</li>
                                        <li>— цена за лид – 1500 руб.</li>
                                        <li>— количество – 50 лидов</li>
                                        <li>— срок – 20 дней.</li>
                                    </ul>
                                    <p class="some-left-decision-text">
                                        Далее из откликнувшихся специалистов выбирает наиболее подходящих и начинает сотрудничество с ними.
                                    </p>
                                </div>
                                <div class="col-md-5 col-sm-6">
                                    <h2 class="title-decision hidden-xs visible-lg visible-md visible-sm">РЕШЕНИЕ</h2>
                                    <button class="all-blue-style-button-transparent style-position-button-section-7">Опубликовать аналогичный проект</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="section_8">
                <div class="container-fluid">
                    <div class="container">
                        <div class="row">
                            <div class="block-total">
                                <div class="col-md-3 col-sm-3">
                                    <h2 class="total-title">ИТОГ</h2>
                                </div>
                                <div class="col-md-9 col-sm-9">
                                    <p class="total-some-text">Антон больше не переживает о неэффективном использовании
                                        рекламного бюджета, ведь теперь он платит только за оплаченные заказы.
                                        Он может сконцентрироваться на управлении бизнесом, а
                                        клиентов ему привлекут профессионалы с Leadlance</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="block-nav-slider container">
        <!--<div class="left-hover-arrows">-->
        <div class="prev-arrow-slider"></i></div>

        <!--</div>-->

        <!--<div class="count-slide-block">-->
        <!--<span class="number-count-style before-slash active">1</span>-->
        <!--<span class="number-count-style">4</span>-->
        <!--</div>-->
        <div class="next-arrow-slider"></div>
    </div>
</section>
<section class="section_4 section_9">
    <div class="container-fluid">
        <div class="container">
            <div class="row"></div>
        </div>
    </div>
</section>
<section class="section_10">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="info-block">
                    <div class="title-section-3">
                        <h3>Почему это выгодно?</h3>
                    </div>
                    <div class="col-md-12 media-none-padding">
                        <div class="title-info-block ">
                            <h2 class="title-info">
                                Вы платите только за достижение цели
                            </h2>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="block-tuposty-dizaynera">
                        <div class="block-absolute-1 all-style-block-absolute">
                            <p class="some-text-absolute">Заявки на сайте</p>
                        </div>
                        <div class="block-absolute-2 all-style-block-absolute">
                            <p class="some-text-absolute">Оплаченные заказы</p>
                        </div>
                        <div class="block-absolute-3 all-style-block-absolute">
                            <p class="some-text-absolute">Установки мобильных</br>
                                приложений</p>
                        </div>
                        <div class="block-absolute-4 all-style-block-absolute">
                            <p class="some-text-absolute">Регистрации</br>
                                на вебинары и тренинги</p>
                        </div>
                        <div class="block-absolute-5 all-style-block-absolute">
                            <p class="some-text-absolute">Подписки на сообщества</br>
                                в соц. сетях</p>
                        </div>
                        <div class="block-absolute-6 all-style-block-absolute">
                            <p class="some-text-absolute">Подписки на рассылки</p>
                        </div>
                        <div class="block-absolute-7 all-style-block-absolute">
                            <p class="some-text-absolute">...и это лишь небольшой список</br>
                                того, что можно получить на Leadlance.</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-7 bottom-diz-text">
                        <p><span class="pink-text">Внимание!</span> Исполнитель получит доступ к деньгам,</br>
                            только после Вашего подтверждения, что условия проекта выполнены</p>
                    </div>
                    <div class="col-md-5 media-none-padding">
                        <button class="all-blue-style-button-transparent section-10-button-position">Опубликовать проект бесплатно</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_11">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="partners-block">
                    <h2 class="title-parners">Чем мы отличаемся от CPA партнерских сетей?</h2>
                    <h3 class="title-dop-partners">Работа напрямую с непосредственным исполнителем</h3>
                    <ul class="partners-list">
                        <li class="clearfix">
                            <span class="list-number-style">01</span>
                            <div class="dop-text-block div-inline">
                                <p class="dop-text-list-title">Больше никаких посредников.</p>
                                <p class="some-text-list">
                                    Вы можете самостоятельно оценить рекомендации и опыт исполнителя,</br>
                                    обсудить условия и выбрать наиболее подходящего кандидата для Вашего проекта.
                                </p>
                            </div>
                        </li>
                        <li  class="clearfix">
                            <span class="list-number-style">02</span>
                            <div class="dop-text-block div-inline">
                                <p class="dop-text-list-title">Никакого лишнего кода на сайте.</p>
                                <p class="some-text-list">
                                    Нет необходимости устанавливать на сайт дополнительный код</br>
                                    и платить за его настройку. Достаточно имеющихся у Вас систем аналитики.
                                </p>
                            </div>
                        </li>
                        <li  class="clearfix">
                            <span class="list-number-style">03</span>
                            <div class="dop-text-block div-inline">
                                <p class="dop-text-list-title">Нет ограничений по количеству лидов.</p>
                                <p class="some-text-list">
                                    Никаких условий по минимальному или максимальному количеству обрабатываемых заявок.
                                    Вы можете установить то количество, которое удобно Вам и исполнителю заказа.
                                </p>
                            </div>
                        </li>
                        <li  class="clearfix">
                            <span class="list-number-style">04</span>
                            <div class="dop-text-block div-inline">
                                <p class="dop-text-list-title">Никаких депозитов.</p>
                                <p class="some-text-list">
                                    Вам не требуется вносить какие-либо средства, чтобы начать</br>
                                    работу с сервисом. Вы можете бесплатно опубликовать проект</br>
                                    на нашем сайте и найти подходящего исполнителя уже сейчас.
                                </p>
                            </div>
                        </li>
                    </ul>
                    <p class="bottom-text-section-11">Деньги на Вашем счете будут заморожены только после того,</br>
                        как Вы найдете подходящего кандидата и начнете сотрудничество.</br>
                        И только в указанном Вами объеме!</p>
                    <button class="all-blue-style-button section-1-button-position">Найти исполнителя</button>
                </div>
            </div>
        </div>
    </div>
</section>
</main>
