<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 11.01.17
 * Time: 14:47
 */
?>
<h2 class="cap-title">
Выберите<br>
свою сторону
</h2>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="cap-content-all">
                <div class="block-cap-letter-circle-all">
                    <div class="block-cap-letter-circle-one"></div>
                    <div class="block-cap-letter-circle-two"></div>
                    <div class="block-cap-letter-circle-three">
                        <h3 class="cap-letter">Я</h3>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-6 col-xs-12 left-button-block-cap">
                    <p>На стороне
<span class="blue-text-cap">бизнеса</span></p>
                    <a href="/landing/customer" class="all-blue-style-button cap-position-button">Хочу много клиентов</a>
                </div>
                <div class="col-sm-6 col-xs-12 right-button-block-cap">
                    <p>На стороне
<span class="blue-text-cap">рекламы</span></p>
                    <a href="/landing/executor" class="all-blue-style-button cap-position-button">Ищу прибыльную работу</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>