<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 11.01.17
 * @time: 21:20
 */
?>
<section class="section_1 section_market">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <p class="section-1-head-text">Партнерки постоянно шейвят и диктуют свои условия?</p>
                <p class="section-1-head-text">Надоело лить трафик впустую?</p>
                <p class="big-blue-text big-blue-text-first-text">Сотрудничайте с рекламодателями</p>
                <p class="big-blue-text">напрямую со 100% гарантией</p>
                <p class="big-blue-text">оплаты Вашей работы</p>
                <div class="line-big-blue-text this-line-position"></div>
                <div class="block-section-1-text-bottom">
                    <p><span class="strong-text-style">Leadlance — специализированная биржа интернет-маркетологов.</span>
                        Здесь Вы найдете клиентов как на проекты по модели CPA, так и вакансии с
                        фиксированным ежемесячным окладом.  </p>
                </div>
                <div class="line-big-blue-text this-line-position"></div>
                <button class="all-blue-style-button section-1-button-position blue-style-button-market">Посмотреть предложения рекламодателей</button>
            </div>
        </div>
    </div>
</section>
<section class="section_2 section_market">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <p class="section-2-title">Здесь работают специалисты по следующим направлениям</p>
                <div class="col-md-6 col-sm-6 col-xs-12 list-wrap paddingNone">
                    <ul class="list-section-2-left all-style-section-2-list list-section-2-left-market">
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Арбитраж трафика
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            SMMщики</span>
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Директологи</span>
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Таргетологи
                        </li>
                        <li class="list-section-2-left__last">
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Любые другие специалисты по интернет-маркетингу
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 list-wrap ">
                    <ul class="list-section-2-right all-style-section-2-list">
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Специалисты по тизерным сетям
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Специалисты по Авито
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            Блогеры, владельцы сайтов
                        </li>
                        <li>
                            <div class="block-glyph-all div-inline">
                                <span class="glyphicon glyphicon-ok"></span>
                            </div>
                            E-mail маркетологи
                        </li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <p class="section-2__bottom-text">Если Вы один из этих талантливейших людей,
                        смело регистрируйтесь на сайте.
                        <span class="reg-text">Заказы Вам обеспечены!</span></p>
                </div>
                <button class="all-white-button-style buton-section-2-position">Попробовать бесплатно</button>
            </div>
        </div>
    </div>
</section>
<section class="section_3 section_market section-3_market">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="title-section-3">
                    <h3>Как это работает?</h3>
                </div>
                <div class="content-section-3 content-section-3_market">
                    <div class="col-md-6 col-sm-6 section-content-padding">
                        <p class="big-number-block number-line-1">01</p>
                        <p class="some-text-content-first">Из имеющихся предложений выбираете</br>
                            подходящие и оставляете заявки</p>
                    </div>
                    <div class="col-md-6 col-sm-6 section-content-padding">
                        <p class="big-number-block number-line-2">02</p>
                        <p class="some-text-content-first">Дожидаетесь ответа рекламодателя</br>
                            и обсуждаете условия сотрудничества</p>
                    </div>
                    <div class="col-md-4 col-sm-4 section-content-padding-bottom section-content-padding-bottom_market hidden-xs visible-lg visible-md visible-sm">
                        <p class="big-number-block text-right-bottom-position">04</p>
                        <p class="some-text-content-first text-right">Как только рекламодатель</br>
                            подтверждает, что работа выполнена,</br>
                            Вы получаете оплату на свой счет</p>
                    </div>
                    <div class="col-md-8 col-sm-8 section-content-padding-bottom">
                        <p class="big-number-block number-line-3 text-right-bottom-position">03</p>
                        <p class="some-text-content-first text-right">Подтверждаете свое</br>
                            решение и закрепляете</br>
                            условия сотрудничества.</p>
                        <p class="dotted-text">
                            В тот же момент на счету рекламодателя</br>
                            будет заморожена сумма,</br>
                            достаточная для оплаты вашей работы
                        </p>
                    </div>
                    <div class="col-md-4 col-sm-4 section-content-padding-bottom section-content-padding-bottom_market visible-xs hidden-lg hidden-md hidden-sm">
                        <p class="big-number-block text-right-bottom-position">04</p>
                        <p class="some-text-content-first text-right">Как только рекламодатель</br>
                            подтверждает, что работа выполнена,</br>
                            Вы получаете оплату на свой счет</p>
                    </div>
                    <div class="col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6 section-content-padding-bottom last">
                        <p class="big-number-block number-line-4">05</p>
                        <p class="some-text-content-first">В случае разногласий, наши независимые</br>
                            эксперты внимательно разберутся в ситуации</br>
                            и примут справедливое решение.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 section-3__quarantee">
                    <p class="section-3__guarantee-text">Мы гарантируем, что все выполненные лиды<br>
                        будут оплачены в обязательном порядке.</p>
                    <button class="all-blue-style-button-transparent section-3__guarantee-button">Найти работу</button>
                    <p class="section-3__guarantee-text section-3__guarantee-text_small">А для тех кто привык работать по классической модели, у нас есть раздел “Вакансии”,<br>
                        где вы найдете работу с ежемесячной оплатой, как удаленно, так и в офис.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-4_market section_market">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="section-4__title">В чем выгода?</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 benefit-left">
                    <p class="benefit-left__first-text">Доступ к большому количеству<br>
                        рекламодателей под любой тип трафика</p>
                    <p class="benefit-left__second-text">
                        Система, гарантирующая оплату<br>
                        со стороны рекламодателей
                    </p>
                </div>
                <div class="col-md-6 col-sm-6 benefit-right clearfix">
                    <p class="benefit-right__first-text">Работа напрямую с заказчиком</p>
                    <p class="benefit-right__second-text">Возможность работать как по модели CPA,<br>
                        так и за фиксированный оклад и бюджет рекламодателя</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="section-4__money-reserve">
                        Деньги под Ваши лиды замораживаются заранее.<br>
                        Вы гарантированно получаете к ним доступ сразу после достижения цели
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_11 section-11_market section_market">
    <div class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="partners-block">
                    <h2 class="title-parners">Чем мы отличаемся от CPA партнерских сетей?</h2>
                    <ul class="partners-list">
                        <li class="clearfix">
                            <span class="list-number-style">01</span>
                            <div class="dop-text-block div-inline">
                                <p class="dop-text-list-title">Больше никакого шейва.</p>
                                <p class="some-text-list">
                                    Мы заинтересованы в максимально честном сотрудничестве рекламодателей<br>
                                    и маркетологов и способствуем этому всеми способами.
                                </p>
                            </div>
                        </li>
                        <li  class="clearfix">
                            <span class="list-number-style">02</span>
                            <div class="dop-text-block div-inline">
                                <p class="dop-text-list-title">Оперативное решение проблем.</p>
                                <p class="some-text-list">
                                    В спорных ситуациях, независимые эксперты сервиса быстро<br>
                                    разберутся в проблеме и примут честное решение.
                                </p>
                            </div>
                        </li>
                        <li  class="clearfix">
                            <p class="bottom-text-section-11">Наша комиссия не зависит от решения по спорам,<br>
                                поэтому мы можем себе позволить быть объективными:) </p>
                        </li>
                        <li  class="clearfix">
                            <span class="list-number-style">03</span>
                            <div class="dop-text-block div-inline">
                                <p class="dop-text-list-title">Эксклюзивные предложения от рекламодателей.</p>
                                <p class="some-text-list">
                                    Мы сделали сервис максимально доступным для бизнеса, в том числе<br>
                                    и для небольших компаний. За счет этого появился большой выбор офферов<br>
                                    под любую нишу и тип трафика.
                                </p>
                            </div>
                        </li>
                        <li  class="clearfix">
                            <span class="list-number-style">04</span>
                            <div class="dop-text-block div-inline">
                                <p class="dop-text-list-title">Никаких посредников.</p>
                                <p class="some-text-list">
                                    Работа с рекламодателем напрямую, возможность подробно<br>
                                    обсудить все условия сотрудничества и при этом с гарантией<br>
                                    защиты от недобросовестных действий. Это ли не мечта?
                                </p>
                            </div>
                        </li>
                    </ul>
                    <p class="bottom-text-section-11">Над оффером работаете только Вы,<br>
                        а не тысячи арбитражников одновременно,<br>
                        которые “убивают” его в короткие сроки.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section-5_market section_market">
    <div class="continer-fluid">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="section-5__stock-text"><span class="bold-text">Акция!</span> Зарегистрируйтесь и заполните свой профиль на сайте,<br>
                        прямо сейчас и получите 2 месяца аккаунта PRO в подарок!</p>
                    <button class="all-blue-style-button buton-section-6-position">Получить PRO в подарок</button>
                </div>
            </div>
        </div>
    </div>
</section>

