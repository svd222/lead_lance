<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 28.12.16
 * @time: 13:32
 *
 * @var $this yii\web\View
 * @var $model common\models\Project
 * @todo @todo Realize the update action functional
 */
?>
<?php
$this->title = Yii::t('app/project', 'Update project');
$this->render('_form', [
    'model' => $model
]);

