<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 14.05.17
 * @time: 10:16
 */

use yii\helpers\Html;
use common\helpers\DateHelper;
use common\models\User;
use common\helpers\CommonHelper;
use yii\helpers\Url;
use yii\web\View;
use common\models\Project;
use yii\data\ArrayDataProvider;
use common\models\Notification;
use common\models\Image;

/**
 * @var View                $this
 * @var Project             $projectModel
 * @var ArrayDataProvider   $dataProvider
 * @var array               $collocutors
 * @var string              $conversation repressent current active conversation
 * @var string              $notification
 */

$colorsCssClasses = Yii::$app->params['themes']['leadlance']['colorsCssClasses'];

$notifications = $this->params['notifications'];
$notifications = Notification::selectNotificationsByEntityType($notifications, Notification::ENTITY_TYPE_PROJECT_MESSAGE);
$notificationsAllCount = 0;
$notificationsCountByCollocutor = [];
foreach ($collocutors as $k => $cId) {
    $count = count(Notification::selectNotificationsByInitiator($notifications, $cId));
    $notificationsCountByCollocutor[$cId] = $count;
    $notificationsAllCount += $count;
}

?>
<?php if (!empty($notification)): ?>
    <div class="row">
        <div class="col-md-12">
            <?php if (!empty($notification['red'])): ?>
                <p class="you-executive-title red">
                    <span class="attention">!</span>
                    <span class="attention-text">
                        <?= $notification['red']; ?>
                    </span>
                </p>
            <?php elseif (!empty($notification['green'])): ?>
                <p class="you-executive-title">
                    <span class="attention">!</span>
                    <span class="attention-text">
                        <?= $notification['green']; ?>
                    </span>
                </p>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<div class="project-2-content">
                <div class="titlesAccordionBlock open clearfix">
                    <div class="col-md-8 col-sm-8 paddingNone">
                        <h3 class="titlesAccordion div-inline project-2-titles">
                            <a href="<?= Url::to('/project/view-offers/' . $projectModel->id); ?>">
                                <?= Html::encode($projectModel->title); ?>
                            </a>
                        </h3>
                    </div>
                    <div class="col-md-4 col-sm-4 paddingNone">
                        <ul class="crambsTitleAccord">
                            <?php if ($projectModel->is_auction): ?>
                                <li>
                                    <a href="javascript:void(0);" class="colorCrambs">Аукцион </a><span> |</span>
                                </li>
                            <?php else: ?>
                                <li>
                                    <a href="javascript:void(0);" class="colorCrambs">
                                        <?php if (!empty($projectModel->lead_cost)): ?>
                                            <?= CommonHelper::humanReadableCost($projectModel->lead_cost); ?>p.
                                        <?php endif; ?>
                                    </a>
                                    <span> |</span>
                                </li>
                            <?php endif; ?>
                            <li>
                                <a href="#"><?= $projectModel->lead_count_required; ?> лид. </a>
                                <?php if (!$projectModel->is_urgent) { ?>
                                <span> |</span>
                                <?php } ?>
                            </li>
                            <?php
                                if (!$projectModel->is_urgent) {
                            ?>
                                <li>
                                    <a href="#"><?= $projectModel->execution_time; ?> дня</a>
                                </li>
                            <?php
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 paddingNone">
                        <?php
                        if ($projectModel->is_urgent) {
                        ?>
                        <p class="terms-proj div-inline">
                            <span class="iconProj_1 iconStyleSize"></span>
    Крайний срок:
                            <span class="dop-color-text-date">
                                <?php
                                $date = strtotime($projectModel->deadline_dt);
                                $m = DateHelper::translateRuMonth(date('m', $date), false, true);
                                echo date('d', $date) . ' ' . $m . ' ' . date('Y', $date);
                                ?>
                            </span>
                        </p>
                        <?php } ?>
                        <?php
                        if ($projectModel->is_multiproject) {
                        ?>
                        <p class="terms-proj multiple-terms-text div-inline">
                            <span class="iconProj_2 iconStyleSize"></span>
    Мультипроект
                            <span class="dop-size-text-min">(рекламодатель может выбрать для работы несколько вебмастеров одновременно)</span>
                        </p>
                        <?php } ?>
                        <p class="some-proj-text-opis">
                            <?= $projectModel->description ?>
                        </p>
                        <?php
                        $images = !empty($projectModel->batch) && !empty($projectModel->batch->images) ? $projectModel->batch->images : null;
                        if (!empty($images)) {
                            ?>
                            <?php
                            foreach ($images as $image) {
                                /**
                                 * @var Image $image
                                 */
                                ?>
                                <p class="added-mess-file">
                                    <a href="<?= Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $image->image; ?>" class="hover-text-decoration" target="_blank">
                                        <img src="/themes/leadlance/img/profile/add-file-mess.png" alt="">
                                        <?= $image->original_image; ?>
                                    </a>
                                </p>
                                <?php

                            }
                            ?>
                            <br />
                            <?php
                        }
                        ?>
                        <div class="clearfix"></div>
                        <div class="block-your-info-proj block-your-info-proj-big-padding style-project-2">
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 paddingNone handler-menu-style-block">
                                <div class="gray-amend div-inline">Автор:</div>
                                <?= $this->render(
                                    '/profile/_nickname_info',
                                    [
                                        'user'  => $projectModel->user,
                                        'class' => 'min-user-nick div-inline',
                                    ]
                                ); ?>
                                <?= $this->render(
                                    '/profile/_reviews',
                                    [
                                        'user'  => $projectModel->user,
                                        'class' => 'order-reviews div-inline',
                                    ]
                                ); ?>
                            </div>
                            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 paddingNone text-right text-media-left handler-menu-style-block" id="view-offers-project-control">
                                <?php if (Yii::$app->user->can('IsOwnThisEntity', [
                                    'model'    => $projectModel,
                                    'property' => 'user_id',
                                ])): ?>
                                    <span class="clip-project">
                                        <span class="iconProj_3 iconStyleSize"></span>
                                        <a href="#" class="hover-text-decoration">закрепить проект</a>
                                    </span>
                                    <span class="redact-project-link">
                                        <a href="<?= Url::to('/project/create/' . $projectModel->id); ?>" class="hover-text-decoration">редактировать</a>
                                    </span>
                                <?php
                                    if ($projectModel->canUnpublish()) {
                                        ?>
                                        <span class="deleted-project-link"><a
                                                href="<?= Url::to('/project/unpublish/' . $projectModel->id); ?>"
                                                data-confirm="<?= Yii::t('app/project', 'Are you sure you want to unpublish this project?') ?>"
                                                data-method="post"
                                                class="hover-text-decoration">снять публикации</a></span>
                                        <?php
                                    } else {
                                        if ($projectModel->isUnpublished()) {
                                            ?>
                                            <span class="deleted-project-link"><a
                                                    href="<?= Url::to('/project/publish/' . $projectModel->id); ?>"
                                                    data-confirm="<?= Yii::t('app/project', 'Are you sure you want to publish this project?') ?>"
                                                    data-method="post"
                                                    class="hover-text-decoration">Опубликовать</a></span>
                                            <?php
                                        }
                                    }
                                ?>
                                <?php endif; ?>
                                <?php if ($notificationsAllCount) { ?>
                                <span class="blue-number"><a href="#" class="hover-text-decoration"><?= $notificationsAllCount; ?></a></span>
                                <?php } ?>
                                <!--<span class="red-number"><a href="#" class="hover-text-decoration">3</a></span>-->
                                <span class="lastProjPublic-link">
                                    <a href="#" class="hover-text-decoration"><span class="listIcon"><?= $dataProvider->count; ?></span></a>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="pading-tegs-project container">
            <ul class="tagsList paddingNone">
                <li>
                    <span>Ниша: </span>
                    <?php
                    foreach ($projectModel->projectTypes as $type) {
                        $cssColor = CommonHelper::getRandomItems($colorsCssClasses);
                        ?>
                        <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?= $type->type; ?></a>
                        <?php
                    }
                    ?>
                </li>
                <li>
                    <span>Допустимый тип трафика: </span>
                    <?php
                    foreach ($projectModel->trafficTypes as $type) {
                        $cssColor = CommonHelper::getRandomItems($colorsCssClasses);
                        ?>
                        <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?= $type->type; ?></a>
                        <?php
                    }
                    ?>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="answer-block-all container paddingNone">
            <?php if ($dataProvider->count): ?>
                <?= $this->render('_offer_conversations', [
                    'dataProvider'                   => $dataProvider,
                    'notificationsCountByCollocutor' => $notificationsCountByCollocutor,
                    'conversation'                   => $conversation,
                    'projectModel'                   => $projectModel,
                ]); ?>
            <?php endif; ?>
