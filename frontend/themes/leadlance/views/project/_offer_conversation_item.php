<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 13.07.17
 * @time: 11:09
 */

use yii\web\View;
use common\models\ProjectMessage;
use yii\widgets\ListView;
use common\models\User;
use common\models\ProfilePrivate;
use common\models\UserAvatar;
use common\helpers\ConversationHelper;
use yii\data\ArrayDataProvider;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use dosamigos\fileupload\FileUpload;
use common\models\Image;
use common\models\Batch;
use yii\helpers\Url;
use common\models\Notification;
use common\models\ProfileCase;
use common\models\Project;
use common\models\ProjectUserRefuse;
use common\helpers\CommonHelper;

/**
 * @var View                                    $this
 * @var ProjectMessage $model                   the data model
 * @var mixed $key                              the key value associated with the data item
 * @var int $index                              the zero-based index of the data item in the items array returned by $dataProvider.
 * @var ListView $widget                        this widget instance
 * @var array $notificationsCountByCollocutor
 * @var Project $projectModel
 */

/**
 * @var [] $users [][
 *  'id' => integer,
 *  'username' => string,
 *  'email' => string,
 *  'password_hash' => string,
 *  'auth_key' => string,
 *  'confirmed_at' => integer,
 *  'unconfirmed_email' => string,
 *  'blocked_at' => integer,
 *  'registration_ip' => string,
 *  'created_at' => integer,
 *  'updated_at' => integer,
 *  'flags' => integer,
 *  'last_login_at' => integer,
 *  'upload_dir' => string,
 *  'fullName' => string,
 *  'hasPro' => string,
 * ]
 */
$users = $this->params['users'];

/**
 * @var ProjectMessage[] $projectMessages
 */
$projectMessages = $this->params['projectMessages'];
/**
 * @var ProjectMessage[][] $projectMessagesBranchNodes
 */
$projectMessagesBranchNodes = $this->params['projectMessagesBranchNodes'];
/**
 * @var ProjectMessage $projectMessage
 */
$projectMessage = $this->params['projectMessage'];
/**
 * @var Image $imageModel
 */
/*$imageModel = $this->params['imageModel'];*/

$conversationId = ConversationHelper::getConversationId($model->from_user_id, $model->to_user_id);

$myId = Yii::$app->user->id;
$collocutorId = $model->from_user_id == $myId ? $model->to_user_id : $model->from_user_id;

$customer = $users[$myId];
$executor = $users[$collocutorId];

$user = ($myId == $model->from_user_id) ? $customer : $executor;

?>
<div class="answer-content mediate-style-block avtor-project">
    <!-- collocutor info block -->
    <div class="name-block-answer-top">
        <div class="col-md-8 col-sm-12 paddingNone statick-avatars-reviews">
            <?php
                $userAvatarImage = null;
                if (!empty($model->fromUser->profilePrivate)) {
                    /**
                     * @var ProfilePrivate $profilePrivate
                     */
                    $profilePrivate = $model->fromUser->profilePrivate;
                    if ($profilePrivate->userAvatar) {
                        $userAvatarImage = UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $profilePrivate->userAvatar->image;
                    }
                }
                if ($userAvatarImage) { ?>
            <div class="block-reviews-avatar-img">
                <a href="<?= Url::to('/profile?id=' . $user['id']); ?>">
                    <img src="<?= $userAvatarImage; ?>" alt="" width="55px" height="53px">
                </a>
            </div>
                <?php } ?>
            <h2 class="nameUser div-inline">
                <?= $this->render(
                    '/profile/_nickname_info',
                    [
                        'user' => User::findOne($user['id']),
                    ]
                ); ?>
            </h2>
            <?php
            $createdAt = strtotime($model->created_at);
            ?>
            <ul class="list-date-answer div-inline">
                <li>(<span class="answer-time-up"><?= date('H:i', $createdAt); ?></span> | <span class="answer-date-up"><?= date('d.m.Y', $createdAt); ?></span>)</li>
            </ul>
        </div>
        <div class="col-md-4 col-sm-12 paddingNone block-info-stat text-media-left-991">
            <?= $this->render(
                '/profile/_reviews',
                [
                    'user'  => User::findOne($model->from_user_id),
                    'class' => 'order-reviews div-inline',
                ]
            ); ?>
            <p class="info-rating div-inline">
                <img src="/themes/leadlance/img/profile/reit.png" alt="">
Рейтинг:
                <span class="info-rating-count"><a href="#" class="hover-text-decoration">8267</a></span>
            </p>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="name-block-answer-top border-top-non">
        <p class="answer-some-text-style">
            <?= $model->message; ?>
        </p>
        <?php
        $images = !empty($model->batch) && !empty($model->batch->images) ? $model->batch->images : null;
        if (!empty($images)) {
            ?>
            <br />
            <?php
            foreach ($images as $image) {
                /**
                 * @var Image $image
                 */
                ?>
                <p class="added-mess-file">
                    <a href="<?= Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $image->image; ?>" class="hover-text-decoration" target="_blank">
                        <img src="/themes/leadlance/img/profile/add-file-mess.png" alt="">
                        <?= $image->original_image; ?>
                    </a>
                </p>
                <?php

            }
            ?>
            <?php
        }
        ?>
    </div>
        <?php
        $maxPossibleSelectedCasesCount = Yii::$app->params['maxPossibleSelectedCasesCount'];
        if (count($model->profileCases)) {
            ?>
    <div class="name-block-answer-top border-top-non">
        <?php for ($i = 0; $i < $maxPossibleSelectedCasesCount; $i++): ?>
            <?php $profileCase = isset($model->profileCases[$i]) ? $model->profileCases[$i] : null; ?>
            <?php if ($profileCase): ?>
                <div class="div-inline">
                    <?php if (!empty($profileCase->preview_image)): ?>
                        <div class="block-answer-dop-img">
                            <a href="<?= Url::to('/profile/case-view/' . $profileCase->id); ?>">
                                <img src="<?= User::UPLOAD_WEB . DIRECTORY_SEPARATOR . $user['upload_dir'] . DIRECTORY_SEPARATOR . $profileCase->preview_image; ?>"
                                     style="width:210px; height:110px">
                            </a>
                        </div>
                    <?php else: ?>
                        <div class="block-answer-dop-img div-inline no-profile-case-photo">
                            <a href="<?= Url::to('/profile/case-view/' . $profileCase->id); ?>">
                                <p class="block-answer-dop-img-text">
                                    Нет фото
                                </p>
                            </a>
                        </div>
                    <?php endif; ?>
                    <div class="case-title ">
                        <?= CommonHelper::truncText($profileCase->title, 20); ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endfor; ?>
    </div>
        <?php
        }
        ?>
    <!-- control block -->
    <div class="block-rewievs-print" data-conversation-id="<?= $conversationId; ?>">
        <?php
        $refused = false;
        $refuseInitiatorId = 0;
        $projectsUserRefuse = $projectModel->projectsUserRefuse;
        if (!empty($projectsUserRefuse)) {
            foreach ($projectsUserRefuse as $pUR) {
                /**
                 * @var ProjectUserRefuse $pUR
                 */
                if ($collocutorId == $pUR->refused_user_id) {
                    $refuseInitiatorId = $pUR->initiator_id;
                    $refused = true;
                    break;
                }
            }
        }
        if (!$refused) {
            ?>
            <ul class="print-reviews clearfix">
                <li class="answer-open" data-conversation-id="<?= $conversationId; ?>" data-index="<?= $index; ?>" >
                </li>
                <li></li>
                <li><a href="#" class="hover-text-decoration modal-link-projects create-order-btn">Выбрать исполнителем и оформить заказ</a></li>
                <!-- data-toggle="modal" data-target="#create-order"-->

                <?php
                $msg = 'to deny execution for this user?';
                ?>

                <li><a href="<?= Url::to('/project/refuse/'.$model->project_id.'/'.$collocutorId); ?>" class="hover-text-decoration"
                       data-method="GET"
                       data-confirm="<?= Yii::t('app/project_user_refuse', 'Are you sure you want') ?> <?= Yii::t('app/project_user_refuse',$msg); ?>"
                    >Отказать</a></li>
                <li>
                    <?php
                    $notificationsCount = !empty($notificationsCountByCollocutor[$model->from_user_id]) ? $notificationsCountByCollocutor[$model->from_user_id] : 0;
                    ?>
                    <?php if ($notificationsCount): ?>
                        <span class="blue-number reviews-blue-count"><!---->
                            <a href="#" class="hover-text-decoration">
                                <?= $notificationsCount; ?>
                            </a>
                        </span>
                        <a href="javascript:void(0)" class="link-reviews-drop-down hover-text-decoration">Развернуть</a>
                    <?php else: ?>
                        <a href="javascript:void(0)" class="link-reviews-drop-down hover-text-decoration">Написать ответ</a>
                    <?php endif; ?>
                </li>
            </ul>
            <?php
        } else {
            if ($refuseInitiatorId == Yii::$app->user->id) {
                $msg = 'Вы отказали в исполнении проекта';
            } else {
                $msg = 'Исполнитель отказался от исполнения проекта.';
            }
            ?>
            <div class="container paddingNone">
                <div class="row">
                    <div class="col-md-12">
                        <p class="you-executive-title red">
                            <span class="attention">!</span>
                            <span class="attention-text"><?= $msg; ?></span>
                        </p>
                    </div>
                </div>
            </div>
            <?php
        }
?>
    </div>
    <!-- respond form block -->
    <div class="print-answer-block-all hide-main answer-block" data-conversation-id="<?= $conversationId; ?>" id="<?= $conversationId; ?>" data-index="<?= $index; ?>">
        <?php
        $form = ActiveForm::begin([
            'id' => $projectMessage->formName().'_'.$conversationId,
            'options' => [
                'class' => 'create-order-form project-crate-form-style'
            ],
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => '{input}',
                'options' => [
                    'tag' => false,
                ],
            ]
        ]);
        ?>
        <input type="hidden" name="project_id" value="<?= $model->project_id; ?>" />
        <div class="col-md-8 block-mess-textarea">
            <?php
            echo $form->field($projectMessage, 'message')
                ->textarea([
                    'cols' => 30,
                    'rows' => 10,
                    'placeholder' => 'Текст сообщения'
                ])
            ?>
        </div>
        <div class="col-md-4 mess-answer-file-input-block">
            <div class="dropbox-mini right-input-file-download input-file-block">
                <label>
                    <?php
                    $imageModel = new Image();
                    $imageId = $imageModel->formName() . '-image_' . $index;
                    echo FileUpload::widget([
                        'options' => [
                            'id' => $imageId,
                        ],
                        'model' => $imageModel,
                        'attribute' => 'image',
                        'url' => ['/image/upload/' . Batch::ENTITY_TYPE_PROJECT_MESSAGE],
                        'useDefaultButton' => false,
                        'clientOptions' => [
                            'maxFileSize' => 2000000,
                            'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                        ],
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                        ],
                    ]); ?>
                    <?php
                    // @todo move below js code to separate file
                    $this->registerJs("
                            if (!collection.get('action').get('formName')) {
                                collection.get('action').set('formName', []);
                            }
                            collection.get('action').get('formName').push('".$projectMessage->formName().'_'.$conversationId."'); 
                            if (!collection.get('action').get('imageFormName')) {
                                collection.get('action').set('imageFormName', []);
                            }
                            collection.get('action').get('imageFormName').push('".$imageModel->formName().'-image_'.$index."');
                            collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                            collection.get('action').set('deleteUrl', '/image/delete/');
                            
                            
                        ", \yii\web\View::POS_BEGIN);
                    ?>
                    <span class="btn btn-default btn-file">

                            <p class="text-input-file">загрузите изображение<br>
                                или перетащите его сюда<br>
                                <span class="gray-regular-text">(Max: 10 mb)</span>
                            </p>
                        </span>
                </label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 paddingNone print-button-mess-block">
            <?= Html::button(Yii::t('app/project_respond', 'Send message'), [
                'class' => 'buttonFilters button-create-order button-send-project-message',
            ]); ?>
        </div>
        <?php
        ActiveForm::end();
        ?>
        <div class="clearfix"></div>
    </div>
    <br />
        <!-- conversation branch nodes block -->
        <div id="wrapper-conversation-<?= $conversationId; ?>" class="wrapper-slide-reviews hide-main">
            <?php if (isset($projectMessagesBranchNodes[$conversationId])): ?>
                <?= $this->render('_offer_conversation_item_messages', [
                    'projectConversationMessagesBranchNodes' => $projectMessagesBranchNodes[$conversationId],
                ]); ?>
            <?php endif; ?>
        </div>
</div>
