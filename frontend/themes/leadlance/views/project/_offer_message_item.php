<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 13.07.17
 * @time: 22:19
 */

use yii\web\View;
use common\models\ProjectMessage;
use yii\widgets\ListView;
use common\models\Image;
use common\models\User;

/**
 * @var View                    $this
 * @var ProjectMessage $model   the data model
 * @var mixed $key              the key value associated with the data item
 * @var int $index              the zero-based index of the data item in the items array returned by $dataProvider.
 * @var ListView $widget        this widget instance
 */

/**
 * @var [] $users [][
 *  'id' => integer,
 *  'username' => string,
 *  'email' => string,
 *  'password_hash' => string,
 *  'auth_key' => string,
 *  'confirmed_at' => integer,
 *  'unconfirmed_email' => string,
 *  'blocked_at' => integer,
 *  'registration_ip' => string,
 *  'created_at' => integer,
 *  'updated_at' => integer,
 *  'flags' => integer,
 *  'last_login_at' => integer,
 *  'upload_dir' => string,
 *  'fullName' => string,
 *  'hasPro' => string,
 * ]
 */
$isMy = $model->from_user_id == Yii::$app->user->id;
if (!empty($this->params['users'])) {
    $user = $this->params['users'][$model->from_user_id];
} else {
    $user = $isMy ? $this->params['user'] : $this->params['collocutor'];
}

$commentClass = $isMy ? 'comment-my' : 'comment-collocutor';

?>
<div class="name-block-answer-top-min-padd border-top-non reviews-all-block-rout <?= $commentClass; ?>">
    <?= $this->render(
        '/profile/_nickname_info',
        [
            'user'  => User::findOne($model->from_user_id),
            'class' => 'min-user-nick div-inline',
        ]
    ); ?>
    <?php
    $createdAt = strtotime($model->created_at);
    ?>
    <ul class="list-date-answer div-inline min-text-this-10">
        <li>(<span class="answer-time-up"><?= date('H:i', $createdAt); ?></span> | <span class="answer-date-up"><?= date('d.m.Y', $createdAt); ?></span>)</li>
    </ul>
    <p class="answer-some-text-style-min">
        <?= $model->message; ?>
    </p>
    <?php
    $images = !empty($model->batch) && !empty($model->batch->images) ? $model->batch->images : null;
    if (!empty($images)) {
        foreach ($images as $image) {
            /**
             * @var Image $image
             */
            ?>
            <p class="added-mess-file">
                <a href="<?= Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $image->image; ?>" class="hover-text-decoration" target="_blank">
                    <img src="/themes/leadlance/img/profile/add-file-mess.png" alt="">
                    <?= $image->original_image; ?>
                </a>
            </p>
            <?php

        }
    }
    ?>
</div>
<?php
if (!empty($model->newNotifications)) {
    ?>
    <div>new</div>
<?php } ?>
