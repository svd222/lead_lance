<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 04.10.17
 * @time: 21:08
 */

use yii\helpers\Html;
use common\helpers\DateHelper;
use common\helpers\CommonHelper;
use yii\web\View;
use common\models\Project;
use yii\helpers\Url;
use common\models\User;

/**
 * @var View                $this
 * @var Project             $project
 * @var string              $notification
 */

$colorsCssClasses = Yii::$app->params['themes']['leadlance']['colorsCssClasses'];

$currentUser = Yii::$app->user->id;
$projectUrl = '';
if (User::isCustomer($currentUser)) {
    $projectUrl =  Url::to('/project/view-offers/' . $project->id);
} elseif (User::isExecutor($currentUser)) {
    $projectUrl =  Url::to('/project/respond-project/' . $project->id);
}

?>
<?php if (!empty($notification)): ?>
    <div class="row">
        <div class="col-md-12">
            <?php if (!empty($notification['red'])): ?>
                <p class="you-executive-title red">
                    <span class="attention">!</span>
                    <span class="attention-text">
                        <?= $notification['red']; ?>
                    </span>
                </p>
            <?php elseif (!empty($notification['green'])): ?>
                <p class="you-executive-title">
                    <span class="attention">!</span>
                    <span class="attention-text">
                        <?= $notification['green']; ?>
                    </span>
                </p>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
    <div class="project-2-content">
        <div class="titlesAccordionBlock open clearfix">
            <div class="col-md-8 col-sm-8 paddingNone">
                <h3 class="titlesAccordion div-inline project-2-titles">
                    <a href="<?= $projectUrl; ?>">
                        <?= Html::encode($project->title); ?>
                    </a>
                </h3>
            </div>
            <div class="col-md-4 col-sm-4 paddingNone">
                <ul class="crambsTitleAccord">
                    <?php if ($project->is_auction): ?>
                        <li>
                            <a href="javascript:void(0);" class="colorCrambs">Аукцион </a><span> |</span>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="javascript:void(0);" class="colorCrambs">
                                <?php if (!empty($project->lead_cost)): ?>
                                    <?= CommonHelper::humanReadableCost($project->lead_cost); ?>p.
                                <?php endif; ?>
                            </a>
                            <span> |</span>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="#"><?= $project->lead_count_required; ?> лид. </a>
                        <?php if (!$project->is_urgent) { ?>
                        <span> |</span>
                        <?php } ?>
                    </li>
                    <?php
        if (!$project->is_urgent) {
            ?>
                        <li>
                            <a href="#"><?= $project->execution_time; ?> дня</a>
                        </li>
                        <?php
        }
        ?>
                </ul>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 paddingNone">
                <?php
    if ($project->is_urgent) {
        ?>
                    <p class="terms-proj div-inline">
                        <span class="iconProj_1 iconStyleSize"></span> Крайний срок:
                        <span class="dop-color-text-date">
                                <?php
                                $date = strtotime($project->deadline_dt);
                                $m = DateHelper::translateRuMonth(date('m', $date), false, true);
                                echo date('d', $date) . ' ' . $m . ' ' . date('Y', $date);
                                ?>
                            </span>
                    </p>
                    <?php } ?>
                    <?php
    if ($project->is_multiproject) {
        ?>
                        <p class="terms-proj multiple-terms-text div-inline">
                            <span class="iconProj_2 iconStyleSize"></span> Мультипроект
                            <span class="dop-size-text-min">(рекламодатель может выбрать для работы несколько вебмастеров одновременно)</span>
                        </p>
                        <?php } ?>
                        <p class="some-proj-text-opis">
                            <?= $project->description ?>
                        </p>
                                        <div class="clearfix"></div>

            </div>
        </div>
    </div>
    </div>
    <div class="clearfix"></div>
    <div class="pading-tegs-project container">
        <ul class="tagsList paddingNone">
            <li>
                <span>Ниша: </span>
                <?php
            foreach ($project->projectTypes as $type) {
                $cssColor = CommonHelper::getRandomItems($colorsCssClasses);
                ?>
                    <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?= $type->type; ?></a>
                    <?php
            }
            ?>
            </li>
            <li>
                <span>Допустимый тип трафика: </span>
                <?php
            foreach ($project->trafficTypes as $type) {
                $cssColor = CommonHelper::getRandomItems($colorsCssClasses);
                ?>
                    <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?= $type->type; ?></a>
                    <?php
            }
            ?>
            </li>
        </ul>
    </div>
