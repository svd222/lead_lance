<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 11.07.17
 * @time: 17:11
 */

use yii\web\View;
use common\models\ProfileCase;
use common\helpers\CommonHelper;
use common\components\Controller;

/**
 * @var View $this
 * @var ProfileCase[] $profileCases
 * @var string $previewImagePath
 */

/**
 * @var Controller $context
 */
$context = $this->context;
$action = $context->action;
$actionId = $action->id;
if ($actionId == 'respond-project') {
?>
<div class="modal fade" id="create-keys" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog create-order-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Выберите не более трех кейсов</h4>
            </div>
            <div class="modal-body">
                <div class="block-mess-textarea sroll-block-keys">
                    <?php
                        foreach ($profileCases as $profileCase) {
                            $previewImageId = 'check_preview_image_' . $profileCase->id;
                    ?>
                        <div class="block-answer-dop-img div-inline">
                            <?php if (!empty($profileCase->preview_image)): ?>
                                <span class="btn btn-default btn-file keys-input-file">
                                    <img src="<?= $profileCase->preview_image ? $previewImagePath . DIRECTORY_SEPARATOR . $profileCase->preview_image : ''; ?>" alt="" style="width:210px; height:110px;">
                                </span>
                            <?php else: ?>
                                <div class="case-img-empty div-inline">
                                    <p class="photo-bold">LeadLance</p>
                                    <p class="photo-light">(no photo)</p>
                                </div>
                            <?php endif; ?>
                            <p class="exceptionText ">
                                <input type="checkbox" class="checkboxStyle" id="<?= $previewImageId; ?>">
                                <label for="<?= $previewImageId; ?>"></label><?= CommonHelper::truncText($profileCase->title, 20); ?> </p>
                            <p class="exceptionText ">
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="modal-footer">
                <button class="buttonFilters button-create-order" id="append_profile_cases">Применить</button>
            </div>
        </div>
    </div>
</div>
<?php } else {
    if ($actionId == 'view-offers') {
?>
<div class="modal fade project-2-modal" id="create-order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog create-order-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Подтверждение заказа</h4>
            </div>
            <div class="modal-body">
                <h3 class="modal-subtitle">Наш сервис позволяет защитить ваши денежные средства исполнитель сможет получить доступ к деньгам только после того как полностью выполнит работу.</h3>
                <p class="modal-bold-text">Однако, рекомендуем перед оформление заказа обратить внимание на следующие моменты</p>
                <ul class="modal-list">
                    <li>
                        -  выбирая исполнителя, обратите внимания на наличие отзывов и кейсов. Вебмастера, имеющие опыт в вашей тематике, скорее всего, лучше справятся с вашим заданием.
                    </li>
                    <li>
                        - вебмастера, имеющие верифицированный аккаунт, уже прошли предварительную проверку администрацией сайта.
                    </li>
                    <li>
                        Их личность и опыт подтверждены нашими специалистами.
                    </li>
                </ul>
                <p class="modal-bold-text">Перед началом работы обязательно обсудите с вебмастером: </p>
                <ul class="modal-list">
                    <li>
                        -  как и в какой срок будет осуществляться проверка выполненной работы. В каких случаях вебмастеру может быть отказано в выплате вознаграждения.
                    </li>
                    <li>
                        - какие системы аналитики планируется использовать для отслеживания конверсий и будет ли предоставлен исполнителю доступ к данным системам аналитики.
                    </li>
                    <li>
                        - какие источники трафика разрешено использовать вебмастеру для выполнения работы
                    </li>
                    <li>
                        Обязательно сообщите вебмастеру если над данным заказом параллельно будет работать кто-то еще.
                    </li>
                </ul>
                <p class="modal-text">
                    В случае, если вы работаете с оплатой наложенным платежом,  дождитесь  выкупов отправленных товаров.
                    Обратите внимание, что для отказа в выплате должны быть серьезные основания, подтвержденные фактами. Будьте готовы в случае возникновения споров, предоставить доступ представителю нашего сервиса к системам аналитики вашего сайта, записям телефонных звонков и передать контактные данные клиентов, по которым были отказы.
                </p>
            </div>
            <div class="modal-footer">
                <button class="buttonFilters button-create-order">Создать заказ</button>
                <button class="button-return-order" type="button" data-dismiss="modal">Вернуться к обсуждению проекта</button>
            </div>
        </div>
    </div>
</div>
<?php
    }
}
?>
