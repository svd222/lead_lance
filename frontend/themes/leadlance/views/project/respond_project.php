<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 23.04.17
 * @time: 17:05
 */

use yii\helpers\Html;
use common\helpers\DateHelper;
use common\helpers\CommonHelper;
use dosamigos\fileupload\FileUpload;
use common\models\Batch;
use frontend\assets\AppAsset;
use yii\widgets\ActiveForm;
use common\models\User;
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;
use yii\web\View;
use yii\helpers\Url;
use common\helpers\ConversationHelper;
use common\models\ProjectMessage;
use common\models\Project;
use common\models\ProfileCase;
use common\models\Image;

/**
 * @var \common\models\Project $projectModel
 * @var \common\models\ProjectMessage $model
 * @var \common\models\ProjectMessage $projectMessage first response of executor to project
 * @var \common\models\Image $imageModel
 * @var \yii\web\View $this
 * @var ArrayDataProvider $dataProvider
 * @var \common\models\ProfileCase $profileCaseModel
 * @var int $isPost
 * @var string $notification
 */

$this->title = 'Откликнуться на проект \'' . $projectModel->title . '\'';
$colorsCssClasses = Yii::$app->params['themes']['leadlance']['colorsCssClasses'];

$this->registerJs("
    collection.get('action').set('profileCaseModelFormName', '" . $profileCaseModel->formName() . "');
", View::POS_READY);

/**
 * @var User $executor potential executor
 */
$executor = $this->params['user'];
$executorHasPro = User::hasPro($executor->id);

/**
 * @var User $customer
 */
$customer = $this->params['collocutor'];
$customerHasPro = User::hasPro($customer->id);

$allModels = $dataProvider->allModels;
if (!empty($projectMessage)) {
    /**
     * @var Project $project
     */
    $project = $projectMessage->project;
    $projectsUserRefuse = isset($project->projectsUserRefuse) ? $project->projectsUserRefuse : null;
}

AppAsset::register($this);
?>
<?php
if (!empty($projectsUserRefuse)) {
    if ($projectsUserRefuse[0]->initiator_id == Yii::$app->user->id) {
        $msg = 'Вы отказались от проекта';
    } else {
        $msg = 'Заказчик отказал Вам в исполнении проекта.';
    }
    ?>
    <div class="container paddingNone">
        <div class="row">
            <div class="col-md-12">
                <p class="you-executive-title red">
                    <span class="attention">!</span>
                    <span class="attention-text">Вы не можете отвечать на данный проект: <?= $msg; ?></span>
                </p>
            </div>
        </div>
    </div>
    <?php
}
?>
<?php if (!empty($notification) && empty($projectsUserRefuse)): ?>
    <div class="row">
        <div class="col-md-12">
            <?php if (!empty($notification['red'])): ?>
                <p class="you-executive-title red">
                <span class="attention">!</span>
                    <span class="attention-text">
                        <?= $notification['red']; ?>
                    </span>
                </p>
            <?php elseif (!empty($notification['green'])): ?>
                <p class="you-executive-title">
                    <span class="attention">!</span>
                    <span class="attention-text">
                        <?= $notification['green']; ?>
                    </span>
                </p>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
<div class="project-2-content">
    <div class="titlesAccordionBlock open clearfix">
        <div class="col-md-8 col-sm-8 paddingNone">
            <h3 class="titlesAccordion div-inline project-2-titles">
                <a href="">
                    <?= Html::encode($projectModel->title); ?>
                </a>
            </h3>
        </div>
        <div class="col-md-4 col-sm-4 paddingNone">
            <ul class="crambsTitleAccord">
                <?php if ($projectModel->is_auction) { ?>
                <li>
                    <a href="#" class="colorCrambs">Аукцион </a><span> |</span>
                </li>
                <?php } ?>
                <li>
                    <a href="#"><?= $projectModel->lead_count_required; ?> лид. </a>
                    <?php if (!$projectModel->is_urgent) { ?>
                        <span> |</span>
                    <?php } ?>
                </li>
                <?php
                if (!$projectModel->is_urgent) {
                    ?>
                    <li>
                        <a href="#"><?= $projectModel->execution_time; ?> дня</a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 paddingNone">
            <?php if ($projectModel->is_urgent) { ?>
            <p class="terms-proj div-inline">
                <span class="iconProj_1 iconStyleSize"></span>
Крайний срок:
                <span class="dop-color-text-date">
                    <?php
                        $date = strtotime($projectModel->deadline_dt);
                        $m = DateHelper::translateRuMonth(date('m', $date), false, true);
                        echo date('d', $date) . ' ' . $m . ' ' . date('Y', $date);
                    ?>
                </span>
            </p>
            <?php } ?>
            <?php if ($projectModel->is_multiproject) { ?>
            <p class="terms-proj multiple-terms-text div-inline">
                <span class="iconProj_2 iconStyleSize"></span>
Мультипроект
                <span class="dop-size-text-min">(рекламодатель может выбрать для работы несколько вебмастеров одновременно)</span>
            </p>
            <?php } ?>
            <p class="some-proj-text-opis">
                <?= $projectModel->description ?>
            </p>
            <?php
            $images = !empty($projectModel->batch) && !empty($projectModel->batch->images) ? $projectModel->batch->images : null;
            if (!empty($images)) {
                ?>
                <?php
                foreach ($images as $image) {
                    /**
                     * @var Image $image
                     */
                    ?>
                    <p class="added-mess-file">
                        <a href="<?= Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $image->image; ?>" class="hover-text-decoration" target="_blank">
                            <img src="/themes/leadlance/img/profile/add-file-mess.png" alt="">
                            <?= $image->original_image; ?>
                        </a>
                    </p>
                    <?php

                }
                ?>
                <br />
                <?php
            }
            ?>
            <div class="clearfix"></div>
            <div class="block-your-info-proj block-your-info-proj-big-padding style-project-2">
                <ul class="tagsList tags-rev-proj-3 paddingNone">
                    <li>
                        <span>Допустимый тип трафика: </span>
                        <?php
                            foreach ($projectModel->trafficTypes as $type) {
                                $cssColor = CommonHelper::getRandomItems($colorsCssClasses);
                        ?>
                        <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?= $type->type; ?></a>
                        <?php
                            }
                        ?>
                    </li>
                </ul>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 paddingNone">
                    <div class="gray-amend div-inline">Автор:</div>
                    <?= $this->render(
                        '/profile/_nickname_info',
                        [
                            'user'  => $customer,
                            'class' => 'min-user-nick div-inline',
                        ]
                    ); ?>
                    <?= $this->render(
                        '/profile/_reviews',
                        [
                            'user'  => $customer,
                            'class' => 'order-reviews div-inline',
                        ]
                    ); ?>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 paddingNone text-right text-media-left">
                    <?php if (empty($projectsUserRefuse) && !empty($projectMessage)) { ?>
                    <span class="clip-project complain-about-the-project">
                        <?php
                            //$msg = 'to deny execution for this user?';
                            $msg = 'to refuse execution this project?';
                        ?>
                        <a href="<?= Url::to('/project/refuse/'.$projectModel->id.'/'.Yii::$app->user->id); ?>"
                           data-method="GET"
                           data-confirm="<?= Yii::t('app/project_user_refuse', 'Are you sure you want') ?> <?= Yii::t('app/project_user_refuse',$msg); ?>"
                           class="hover-text-decoration complain-color refuse-project">Отказаться от проекта</a>
                    </span>
                    <!--<span class="project-action-delimiter"> | </span>-->
                    <?php } ?>
                    <span class="clip-project complain-about-the-project">
                        <a href="#" class="hover-text-decoration complain-color">пожаловаться на проект</a>
                    </span>
                    <!--<span class="lastProjPublic-link">
                        <a href="#"><span class="listIcon">45</span></a>
                    </span>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<?php if (!$projectMessage) { ?>
<div class="apply-for-project">
    <!--<form action="" class="create-order-form">-->
    <?php
    $form = ActiveForm::begin([
        'id' => $model->formName(),
        'options' => [
            'class' => 'create-order-form project-crate-form-style'
        ],
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'fieldConfig' => [
            'template' => '{input}',
            'options' => [
                'tag' => false,
            ],
        ]
    ]);
    ?>
        <h3 class="answer-title">Откликнуться на проект</h3>
        <!--<div class="name-block-answer-top">-->
            <!--<textarea name="" id="" cols="30" rows="10" class="name-block-answer-top answer-some-text-style" placeholder="Текст сообщения"></textarea>-->
    <?php
        echo $form->field($model, 'message')
            ->textarea([
                'cols' => 30,
                'rows' => 10,
                'class' => 'name-block-answer-top answer-some-text-style',
            ]);
    ?>

            <!--<p class="answer-some-text-style">-->
                <!--Нужно проводить анализ и смотреть, но необходимо понимание,-->
                <!--что делалось с сайтом в ближайшие пол года. Может пол года-->
                <!--назад ссылок сняли пару тысяч к примеру или месяц назад-->
                <!--протокол https поставили. Готов к выполнению анализа с вами совместными усилиями.-->
            <!--</p>-->
            <!--<p class="answer-some-text-style">-->
                <!--Возможно фильтр словил сайт, там везде порно подставляется у вас где его по факту нет.-->
            <!--</p>-->
        <!--</div>-->
        <!--<p class="keys-text-count">Выберите не более трех кейсов</p>-->
        <div class="col-md-8 block-mess-textarea">
            <div class="block-answer-dop-img div-inline" data-toggle="modal" data-target="#create-keys">
                <span class="btn btn-default btn-file keys-input-file">
                    <p class="block-answer-dop-img-text">добавить кейс</p>
                </span>
            </div>
            <div class="block-answer-dop-img div-inline " data-toggle="modal" data-target="#create-keys">
                <p class="block-answer-dop-img-text">добавить кейс</p>
                <span class="btn btn-default btn-file keys-input-file">

                </span>
            </div>
            <div class="block-answer-dop-img div-inline" data-toggle="modal" data-target="#create-keys">
                <p class="block-answer-dop-img-text">добавить кейс</p>
                <span class="btn btn-default btn-file keys-input-file">

                </span>
            </div>
        </div>
        <div class="col-md-4 mess-answer-file-input-block">
            <div class="dropbox-mini right-input-file-download   input-file-block">
                <label>
                    <!--<input type="file" name="dropbox">-->
                    <?= FileUpload::widget([
                        'model' => $imageModel,
                        'attribute' => 'image',
                        'url' => ['/image/upload/' . Batch::ENTITY_TYPE_PROJECT_MESSAGE],
                        'useDefaultButton' => false,
                        'clientOptions' => [
                            'maxFileSize' => 2000000,
                            'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                        ],
                        // ...
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                        ],
                    ]); ?>
                    <?php
                    // @todo move below js code to separate file
                    $this->registerJs("
                            collection.get('action').set('formName', '".$model->formName()."');
                            collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                            collection.get('action').set('deleteUrl', '/image/delete/');
                            collection.get('action').set('imageFormName', '".$imageModel->formName()."');
                            
                        ", \yii\web\View::POS_BEGIN);

                    ?>
                    <span class="download-text   btn-file">загрузите файл<br> или перетащите его сюда
                        <span class="download-size-mini">
                            Max: 5 mb
                        </span>
                    </span>

                </label>
            </div>
            <!--
            .mess-answer-file-input-block .input-file-block .btn-file {
                height: 110px;
                width: 100%;
                border: 1px solid #394869;
                padding-top: 20px;
            }-->

            <!--<div class="input-file-block">
                <span class="btn btn-default btn-file">
                    <p class="text-input-file">загрузите изображение<br>
                        или перетащите его сюда<br>
                        <span class="gray-regular-text">(Max: 10 mb)</span>
                    </p>
                    <input type="file">
                </span>
            </div>
            <p class="added-mess-file">
                <a href="#" class="hover-text-decoration">
                    <img src="/themes/leadlance/img/profile/add-file-mess.png" alt="">добавленый файл.pdf,doc и тд.
                </a>
            </p>-->
        </div>
        <div class="clearfix"></div>
    <?php
    if ($projectModel->is_auction) {
    ?>
        <div class="col-md-4 wrap-coast-lead project-lead wrap-coast-lead-respond">
            <p class="input-title ">Цена лида</p>
                <?php
                echo $form->field($model, 'lead_cost')
                    ->textInput([
                        'class' => 'coast-lead-input'
                    ]);
                /*echo $form->field($model, 'currency', [
                    'template' => '{input}',
                    'inputOptions' => [
                        'class' => 'coast-lead select2-hidden-accessible',
                        'tabindex' => 1,
                        'aria-hidden' => true,
                    ]
                ])
                    ->dropDownList([
                        'RUB' => 'Rub',
                        'USD' => '$',
                ]);*/
            ?>

            <select class="coast-lead select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                <option>Rub</option>
                <option>$</option>
            </select>
        </div>
        <?php
    }
    ?>
        <div class="clearfix"></div>
        <div class="col-md-4 paddingNone print-button-mess-block padding-top-10">
            <?php
                echo $form->field($model, 'from_user_id')
                    ->hiddenInput([
                        'value' => Yii::$app->user->id,
                    ]);
                echo $form->field($model, 'to_user_id')
                    ->hiddenInput([
                        'value' => $projectModel->user_id,
                    ]);
            ?>

            <?php
                echo Html::submitButton(Yii::t('app/project_respond', 'Publish answer'), [
                    'class' => 'buttonFilters button-create-order',
                ]);
            ?>
            <!--<button class="buttonFilters">Опубликовать ответ</button>-->
        </div>

    <?php
        ActiveForm::end();
    ?>
    <div class="clearfix"></div>
</div>
<?php } else { ?>
<div class="answer-block-all container paddingNone">
    <h3 class="answer-title">Ваш ответ</h3>
    <!-- delimiter -->

    <?php
    //My first response message
    $userFullName = $this->params['userFullname'];
    ?>



    <div class="answer-content mediate-style-block executor-project">
        <div class="name-block-answer-top">
            <div class="col-md-8 col-sm-12 paddingNone ">
                <?= $this->render(
                    '/profile/_nickname_info',
                    [
                        'user'  => $executor,
                        'class' => 'div-inline',
                    ]
                ); ?>
                <ul class="list-date-answer div-inline">
                    <?php
                    $createdAt = strtotime($projectMessage->created_at);
                    ?>
                    <li>(<span class="answer-time-up"><?= date('H:i', $createdAt); ?></span> | <span class="answer-date-up"><?= date('d.m.Y', $createdAt); ?></span>)</li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12 paddingNone block-info-stat text-media-left-991">
                <?= $this->render(
                    '/profile/_reviews',
                    [
                        'user'  => $customer,
                        'class' => 'order-reviews div-inline',
                    ]
                ); ?>
                <p class="info-rating div-inline">
                    <img src="/themes/leadlance/img/profile/reit.png" alt="">
                    Рейтинг:
                    <span class="info-rating-count"><a href="#" class="hover-text-decoration">8267</a></span>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="name-block-answer-top border-top-non">
            <p class="answer-some-text-style">
                <?= $projectMessage->message; ?>
            </p>
            <?php if ($projectModel->is_auction): ?>
                <div class="offered-lead-cost colorCrambs">
                    Предложенная цена лида: <?= CommonHelper::humanReadableCost($projectMessage->lead_cost); ?>p.
                </div>
            <?php endif; ?>
            <br />
            <?php
            $images = !empty($projectMessage->batch) && !empty($projectMessage->batch->images) ? $projectMessage->batch->images : null;
            if (!empty($images)) {
                foreach ($images as $image) {
                    /**
                     * @var Image $image
                     */
                    ?>
                    <p class="added-mess-file">
                        <a href="<?= Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $image->image; ?>" class="hover-text-decoration" target="_blank">
                            <img src="/themes/leadlance/img/profile/add-file-mess.png" alt="">
                            <?= $image->original_image; ?>
                        </a>
                    </p>
                    <?php

                }
            }
            ?>
        </div>
        <div class="name-block-answer-top border-top-non">
            <?php
            $maxPossibleSelectedCasesCount = Yii::$app->params['maxPossibleSelectedCasesCount'];
            if (count($projectMessage->profileCases)) {
                for ($i=0; $i<$maxPossibleSelectedCasesCount; $i++) {
                    /**
                     * @var ProfileCase $profileCase
                     */
                    $profileCase = !empty($projectMessage->profileCases[$i]) ? $projectMessage->profileCases[$i] : null;
                    if ($profileCase) {
                        if (!empty($profileCase->preview_image)) {
                            ?>
                            <div class="block-answer-dop-img div-inline">
                                <a href="<?= Url::to('/profile/case-view/'.$projectMessage->profileCases[$i]->id); ?>">
                                    <img src="<?= User::UPLOAD_WEB . DIRECTORY_SEPARATOR . $executor->upload_dir . DIRECTORY_SEPARATOR . $projectMessage->profileCases[$i]->preview_image; ?>" style="width:210px; height:110px" width="210px" height="110px">
                                </a>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="block-answer-dop-img div-inline no-profile-case-photo">
                                <a href="<?= Url::to('/profile/case-view/' . $profileCase->id); ?>">
                                    <p class="block-answer-dop-img-text">
                                        Нет фото
                                    </p>
                                </a>
                            </div>
                            <?php
                        }
                    }
                }
            }
            ?>
        </div>
    </div>
    <?php
    if (empty($projectsUserRefuse)) {
    ?>
    <div class="print-answer-block-all respond-project-answers">
        <?php
        $conversationId = ConversationHelper::getConversationId($projectMessage->from_user_id, $projectMessage->to_user_id);
        $form = ActiveForm::begin([
            'id' => $model->formName(),
            'options' => [
                'class' => 'create-order-form project-crate-form-style'
            ],
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => '{input}',
                'options' => [
                    'tag' => false,
                ],
            ]
        ]);
        ?>
        <input type="hidden" name="project_id" value="<?= $model->project_id; ?>" />
        <div class="col-md-8 block-mess-textarea">
            <?php
            echo $form->field($model, 'message')
                ->textarea([
                    'cols' => 30,
                    'rows' => 10,
                    'placeholder' => 'Текст сообщения'
                ])
            ?>
        </div>
        <div class="col-md-4 mess-answer-file-input-block">
            <div class="dropbox-mini right-input-file-download input-file-block">
                <label>
                    <?= FileUpload::widget([
                        'model' => $imageModel,
                        'attribute' => 'image',
                        'url' => ['/image/upload/' . Batch::ENTITY_TYPE_PROJECT_MESSAGE],
                        'useDefaultButton' => false,
                        'clientOptions' => [
                            'maxFileSize' => 2000000,
                            'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                        ],
                        // ...
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                        ],
                    ]); ?>
                    <?php
                    // @todo move below js code to separate file
                    $this->registerJs("
                            collection.get('action').set('formName', '".$model->formName()."');
                            
                            collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                            collection.get('action').set('deleteUrl', '/image/delete/');
                            collection.get('action').set('imageFormName', '".$imageModel->formName()."');
                            
                        ", \yii\web\View::POS_BEGIN);
                    ?>
                    <span class="btn btn-default btn-file">
                                <p class="text-input-file">загрузите изображение<br>
                                    или перетащите его сюда<br>
                                    <span class="gray-regular-text">(Max: 10 mb)</span>
                                </p>
                            </span>
                </label>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 paddingNone print-button-mess-block">
            <?php
            echo $form->field($model, 'from_user_id')
                ->hiddenInput([
                    'value' => Yii::$app->user->id,
                ]);
            echo $form->field($model, 'to_user_id')
                ->hiddenInput([
                    'value' => $projectModel->user_id,
                ]);
            ?>

            <?= Html::button(
                Yii::t('app/project_respond',
                    'Send message'),
                [
                    'class' => 'buttonFilters button-create-order button-send-project-message',
                ]
            ); ?>
        </div>
        <?php
            ActiveForm::end();
        ?>
        <div class="clearfix"></div>
    </div>
    <?php } ?>
    <?php
        $containerActive = $isPost ? ' active' : '';
    ?>
    <div id="wrapper-slide-reviews" class="wrapper-slide-reviews"<?= $containerActive; ?>>
        <?php
            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_respond_message_item',
                'summary' => '',
                'emptyText' => false,
                'itemOptions' => function ($model, $key, $index, $widget) {
                    /**
                     * @var ProjectMessage $model
                     */
                    if (!empty($model->newNotifications)) {
                        return [
                            'class' => 'new-message',
                            'data-id' => $model->id,
                        ];
                    }
                }
            ]);
        ?>
    </div>


    <?php
/*        echo ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_respond_conversation_item',
            'summary' => '',
        ]);

    */?>
<?php } ?>


