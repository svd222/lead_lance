<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 28.12.16
 * @time: 13:32
 *
 * @var yii\web\View                 $this
 * @var common\models\Project        $model
 * @var common\models\Image          $imageModel
 * @var common\models\ProjectType    $projectTypeModel
 * @var common\models\TrafficType    $trafficTypeModel
 * @var array $projectTypeSelected
 * [
 *  int
 * ]
 * @var array $trafficTypeSelected
 * [
 *  int
 * ]
 * @var array $projectTypesList
 * [
 *  int => type string
 * ]
 * @var array $trafficTypesList
 * [
 *  int => type string
 * ]
 */
use yii\widgets\ActiveForm;
use common\models\GuideDateType;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use dosamigos\fileupload\FileUpload;
use common\models\Batch;
use common\models\Image;
use common\models\Project;

/**
 * @var Project     $model
 * @var Batch       $batch
 * @var Image[]     $images
 * @var bool        $update
 */

AppAsset::register($this);

?>
<div class="row">
    <div class="col-md-12">
        <h2 class="projectTitles page-titles"><?= $this->title; ?></h2>
    </div>
</div>
<?php
    if ($model->hasErrors()) {
        foreach ($model->errors as $error) {
            ?>
    <div class="row form-error">
        <div class="div-inline">
            <p class="input-title">
                <?= $error[0]; ?>
            </p>
        </div>
    </div>
            <?php
        }
    }
$form = ActiveForm::begin([
    'id' => $model->formName(),
    'options' => [
        'class' => 'create-order-form project-crate-form-style'
    ],
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => '{input}',
        'options' => [
            'tag' => false,
        ],
    ]
]);
?>
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 ">
            <h3 class="title-input-added-order">Название проекта</h3>
            <?php
                echo $form->field($model, 'title')->textInput([
                    'class' => 'some-input-orders',
                ]);
            ?>
            <div class="main-max-width">
                <div class="col-md-6 left-select-order">
                    <div class="wrapper-custom-select">
                        <?php
                        echo $form->field($projectTypeModel, 'type', [
                            'template' => '{input}',
                            'inputOptions' => [
                                'class' => 'custom-select',
                            ]
                        ])->dropDownList($projectTypesList);

                        ?>
                        <!--<select name="custom-select" class="custom-select" >
                            <option value="#">Ниша</option>
                            <option value="#">Ниша2</option>
                            <option value="#">Ниша3</option>
                            <option value="#">Ниша4</option>
                        </select>-->
                        <input type="text" class="inputFiltersStyle" value="Ниша" >
                        <a href="#" class="select-next"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                        <a href="#" class="select-prev"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        <div class="wrapper-dropbox-input">
                            <?php
                            echo '<input type="hidden" name="' . $projectTypeModel->formName() .  '[type]" value="">';
                            foreach ($projectTypesList as $id => $type) {
                                $itemId = $projectTypeModel->formName().'_type_'.$id;
                                $itemName = $projectTypeModel->formName().'[type][]';
                                ?>
                                <p class="exceptionText">
                                    <input type="checkbox" class="checkboxStyle " id="<?= $itemId; ?>" name="<?= $itemName; ?>" value="<?= $id; ?>"
                                        <?php if (!empty($projectTypeSelected) && isset($projectTypeSelected[$id])) {
                                            echo 'checked="true"';
                                        }
                                        ?>
                                    >
                                    <label for="<?= $itemId; ?>"></label><?= $type; ?>
                                </p>
                                <?php
                            }
                            ?>
                            <!--<p class="exceptionText ">
                                <input type="radio"   class="checkboxStyle" id="check1_3">
                                <label for="check1_3"></label>интернет-магазин </p>
                            <p class="exceptionText ">
                                <input type="radio"   class="checkboxStyle" id="check1_4">
                                <label for="check1_4"></label>социальные сети</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_5">
                                <label for="check1_5"></label>товары почтой</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_6">
                                <label for="check1_6"></label>мобильное приложение</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_7">
                                <label for="check1_7"></label>онлайн игры</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_8">
                                <label for="check1_8"></label>знакомства</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_9">
                                <label for="check1_9"></label>финансы и кредитование</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_10">
                                <label for="check1_10"></label>софт</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_11">
                                <label for="check1_11"></label>инфобизнес</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_12">
                                <label for="check1_12"></label>услуги</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_13">
                                <label for="check1_13"></label>веб-сервис</p>
                            <p class="exceptionText ">
                                <input type="radio"  class="checkboxStyle" id="check1_14">
                                <label for="check1_14"></label>эротика</p>-->
                            <ul class="checkbox-control">
                                <li><a href="#" id="project_type_select_all">выбрать все</a></li>
                                <li><a href="#" id="project_type_deselect_all">снять все галочки</a></li>
                            </ul>
                            <a href="#" class="change-filter">Применить</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 right-select-order">
                    <div class="wrapper-custom-select">
                        <?php
                        echo $form->field($trafficTypeModel, 'type', [
                            'template' => '{input}',
                            'inputOptions' => [
                                'class' => 'custom-select',
                            ]
                        ])->dropDownList($trafficTypesList);

                        ?>
                        <input type="text" class="inputFiltersStyle" value="Допустимый тип трафика">
                        <a href="#" class="select-next"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                        <a href="#" class="select-prev"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                        <div class="wrapper-dropbox-input" style="display: none;">
                            <?php
                            echo '<input type="hidden" name="' . $trafficTypeModel->formName() . '[type]" value="">';
                            foreach ($trafficTypesList as $id => $type) {
                                $itemId = $trafficTypeModel->formName().'_type_'.$id;
                                $itemName = $trafficTypeModel->formName().'[type][]';
                                ?>
                                <p class="exceptionText">
                                    <input type="checkbox" class="checkboxStyle " id="<?= $itemId; ?>" name="<?= $itemName; ?>" value="<?= $id; ?>"
                                        <?php if (!empty($trafficTypeSelected) && isset($trafficTypeSelected[$id])) {
                                            echo 'checked="true"';
                                        }
                                        ?>
                                    >
                                    <label for="<?= $itemId; ?>"></label><?= $type; ?>
                                </p>
                                <?php
                            }
                            ?>
                            <!--<p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_3">
                                <label for="check_3"></label>интернет-магазин </p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_4">
                                <label for="check_4"></label>социальные сети</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_5">
                                <label for="check_5"></label>товары почтой</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_6">
                                <label for="check_6"></label>мобильное приложение</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_7">
                                <label for="check_7"></label>онлайн игры</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_8">
                                <label for="check_8"></label>знакомства</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_9">
                                <label for="check_9"></label>финансы и кредитование</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_10">
                                <label for="check_10"></label>софт</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_11">
                                <label for="check_11"></label>инфобизнес</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_12">
                                <label for="check_12"></label>услуги</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_13">
                                <label for="check_13"></label>веб-сервис</p>
                            <p class="exceptionText ">
                                <input type="radio" class="checkboxStyle" id="check_14">
                                <label for="check_14"></label>эротика</p>-->
                            <ul class="checkbox-control">
                                <li><a href="#" id="traffic_type_select_all">выбрать все</a></li>
                                <li><a href="#" id="traffic_type_deselect_all">снять все галочки</a></li>
                            </ul>
                            <a href="#" class="change-filter">Применить</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 paddingNone textarea-orders">
                    <h3 class="title-input-added-order">Описание проекта</h3>
                    <?php
                        echo $form->field($model, 'description')
                            ->textarea([
                                'cols' => 30,
                                'rows' => 10,
                                'class' => '',
                            ]);
                    ?>
                </div>
            </div>

        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="dropbox right-input-file-download">
                <label>
                    <!--<input type="file" name="dropbox">-->
                    <?= FileUpload::widget([
                        'model' => $imageModel,
                        'attribute' => 'image',
                        'url' => ['/image/upload/' . Batch::ENTITY_TYPE_PROJECT],
                        'useDefaultButton' => false,
                        'clientOptions' => [
                            'maxFileSize' => 2000000,
                            'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                        ],
                        // ...
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                        ],
                    ]); ?>
                    <?php
                        // @todo move below js code to separate file
                        $this->registerJs("
                            collection.get('action').set('formName', '".$model->formName()."');
                            
                            collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                            collection.get('action').set('deleteUrl', '/image/delete/');
                            collection.get('action').set('imageFormName', '".$imageModel->formName()."');
                            
                        ", \yii\web\View::POS_BEGIN);
                    ?>

                    <span class="download-text">загрузите файл<br> или перетащите его сюда</span>
                    <span class="download-size">Max: 5 mb</span>
                </label>
                <?php
                if (!empty($batch)) {
                    ?>
                    <input type="hidden" id="project-batch_id" name="<?= $model->formName() ?>[batch_id]" value="<?= $batch->id; ?>">
                    <?php
                    if (!empty($batch) && !empty($images)) {
                        ?>
                        <br />
                        <?php
                        foreach ($images as $image) {
                            ?>
                            <p class="added-mess-file">
                                <a href="<?= Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $image->image; ?>" style="font-size:130%;" class="hover-text-decoration">
                                    <img src="/themes/leadlance/img/profile/add-file-mess.png" alt=""><?= $image->original_image; ?></a>
                                <i class="glyphicon glyphicon-remove float-right right20" id="fDel_<?= $image->id; ?>"></i>
                            </p>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 wrap-coast-lead">
            <p class="input-title">Цена лида</p>
            <label>
                <!--<input type="text" name="project-name" class="coast-lead-input">-->
                <?php
                $inputOptions = [
                    'class' => 'coast-lead-input'
                ];
                if ($update && $model->is_auction) {
                    $inputOptions['disabled'] = true;
                }
                echo $form->field($model, 'lead_cost')
                    ->textInput($inputOptions);
                ?>
            </label>
            <?php
                $selOptions = [];
                if ($update && $model->is_auction) {
                    $selOptions['disabled'] = true;
                }
                echo $form->field($model, 'currency', [
                    'template' => '{input}',
                    'inputOptions' => [
                        'class' => 'coast-lead select2-hidden-accessible',
                        'tabindex' => 1,
                        'aria-hidden' => true,
                    ]
                ])
                    ->dropDownList([
                        'RUB' => 'Rub',
                        'USD' => '$',
                    ], $selOptions);
            ?>
            <!--<select class="coast-lead select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                <option>Rub</option>
                <option>$</option>
            </select>-->
        </div>
        <div class="col-md-7 wrap-coast-lead main-hot-style">
            <p class="exceptionText div-inline hot-order">
                <!--<input type="checkbox" name="filter" class="checkboxStyle" id="check_1">-->
                <?php
                    $checkBoxOptions = [
                        'class' => 'checkboxStyle',
                    ];
                    if ($update && $model->lead_cost) {
                        $checkBoxOptions['disabled'] = true;
                    }
                    echo $form->field($model, 'is_auction')
                        ->checkbox($checkBoxOptions, false);
                ?>
                <label for="project-is_auction"></label>аукцион (вебмастер сам назначает цену лида)
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5 wrap-coast-lead">
            <p class="input-title">
                <?= Yii::t('app/project', 'Execution Time'); ?>
            </p>
            <label>
                <?php
                    echo $form->field($model, 'execution_time')
                        ->textInput([
                            'class' => 'coast-lead-input'
                        ]);
                ?>
                <!--<input type="text" name="project-name" class="coast-lead-input">-->
            </label>
            <?php
                echo $form->field($model, 'dateType', [
                    'template' => '{input}',
                    'inputOptions' => [
                        'class' => 'coast-lead select2-hidden-accessible',
                        'tabindex' => 1,
                        'aria-hidden' => true,
                    ]
                ])
                    ->dropDownList(GuideDateType::getList());
            ?>
            <!--<select class="coast-lead select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                <option>День</option>
                <option>Год</option>
            </select>-->
        </div>
        <!--<div class="col-md-7 wrap-coast-lead main-hot-style">
            <p class="exceptionText div-inline hot-order">
                <span class="iconProj_1 iconStyleSize marg-icon-style"></span>
                <?php
/*                    $checkBoxOptions = [
                        'class' => 'checkboxStyle',
                    ];
                    if ($model->is_urgent) {
                        $checkBoxOptions['checked'] = true;
                    }
                    echo $form->field($model, 'is_urgent')
                        ->checkbox($checkBoxOptions, false);
                */?>
                <label for="<?php /*echo strtolower($model->formName().'-is_urgent'); */?>"></label><span class="hot-project-text">cрочный проект</span>
            </p>
            <div class="div-inline">
                <p class="input-title ">Крайний срок</p>
                <?php
/*                    echo $form->field($model, 'deadlineDtDay')
                        ->textInput([
                            'class' => 'last-time-day',
                            'placeholder' => date('d'),
                            'value' => date('d'),
                        ]);
                */?>
                <?php
/*                echo $form->field($model, 'deadlineDtMonth')
                    ->textInput([
                        'class' => 'last-time-month',
                        'placeholder' => date('m'),
                        'value' => date('m'),
                    ]);
                */?>
                <?php
/*                echo $form->field($model, 'deadlineDtYear')
                    ->textInput([
                        'class' => 'last-time-year',
                        'placeholder' => date('Y'),
                        'value' => date('Y'),
                    ]);
                */?>

            </div>
        </div>-->
    </div>
    <div class="row">
        <div class="col-md-5 wrap-coast-lead">
            <p class="input-title">Количество лидов</p>
            <label>
                <?php
                    echo $form->field($model, 'lead_count_required')
                        ->textInput([
                            'class' => 'coast-lead-input numbers-lead',
                        ]);
                ?>
                <!--<input type="text" name="numbers-lead" class="coast-lead-input numbers-lead">-->
            </label>
        </div>
    </div>
    <!--<div class="row">
        <div class="col-md-12 wrap-multiple-marg checked-posit-multiple">
            <span class="iconProj_2 iconStyleSize marg-icon-style"></span>
            <?php
/*                echo $form->field($model, 'is_multiproject')
                    ->checkbox([
                        'class' => 'checkboxStyle'
                    ], false);
            */?>
            <label for="project-is_multiproject"></label><span class="big-multy-projects">мультипроект</span><span class="min-multy-projects-text" > (рекламодатель может выбрать для работы несколько вебмастеров одновременно) </span>
        </div>
    </div>-->
    <div class="row agree-order">
        <div class="col-md-5 buttonsFilterBLock paddingRightNone">
            <?php
            $buttonTitle = $update ? Yii::t('app/project', 'Save') : Yii::t('app/project', 'Publish project');
            echo Html::submitButton($buttonTitle, [
                /*'form' => $form->id,*/
                'class' => 'buttonFilters button-create-order',
                /*'data-toggle' => 'modal',*/
                /*'data-target' => '#create-order'*/
            ]);
            ?>
            <!--<button class="buttonFilters button-create-order" data-toggle="modal" data-target="#create-order">Опубликовать проект</button>-->
        </div>
    </div>
<?php
    ActiveForm::end();
?>
