<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 02/05/2018
 * Time: 2:40 PM
 */

use yii\widgets\ListView;
use yii\data\ArrayDataProvider;
use common\models\Project;

/**
 * @var Project           $projectModel
 * @var ArrayDataProvider $dataProvider
 * @var array             $notificationsCountByCollocutor
 * @var string            $conversation
 */

?>
<?php if ($dataProvider->count): ?>
    <h3 class="answer-title">Ответы по проекту</h3>
    <!-- delimiter -->
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView'     => '_offer_conversation_item',
        'summary'      => '',
        'viewParams'   => [
            'notificationsCountByCollocutor' => $notificationsCountByCollocutor,
            'projectModel'                   => $projectModel,
        ],
        'itemOptions'  => function ($model, $key, $index, $widget) use ($conversation) {
            if ($key == $conversation) {
                return [
                    'active' => true,
                ];
            }
        },
        'showOnEmpty'  => true,
    ]); ?>
<?php endif; ?>
