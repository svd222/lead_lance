<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 28.12.16
 * @time: 13:32
 */

use common\models\Project;
use common\models\Image;
use common\models\Batch;
use common\models\ProjectType;
use common\models\TrafficType;

/**
 * @var yii\web\View            $this
 * @var Project                 $model
 * @var Image                   $imageModel
 * @var ProjectType             $projectTypeModel
 * @var TrafficType             $trafficTypeModel
 * @var array                   $projectTypeSelected
 * @var Batch                   $batch
 * @var Image[]                 $images
 * @var bool                    $update
 * [
 *  int
 * ]
 * @var $trafficTypeSelected array
 * [
 *  int
 * ]
 * @var $projectTypesList array
 * [
 *  int => type string
 * ]
 * @var $trafficTypesList array
 * [
 *  int => type string
 * ]
 */

$this->title = $update ? Yii::t('app/project', 'Update project') : Yii::t('app/project', 'Create project');
echo $this->render('_form', [
    'projectTypesList' => $projectTypesList,
    'trafficTypesList' => $trafficTypesList,
    'projectTypeModel' => $projectTypeModel,
    'trafficTypeModel' => $trafficTypeModel,
    'projectTypeSelected' => $projectTypeSelected,
    'trafficTypeSelected' => $trafficTypeSelected,
    'model' => $model,
    'imageModel' => $imageModel,
    'batch' => $batch,
    'images' => $images,
    'update' => $update,
]);