<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 25.12.16
 * @time: 1:33
 *
 * @var $model \common\models\Project
 * @var $key int
 * @var $index int
 * @var $widget \yii\widgets\ListView
 */

use common\helpers\CommonHelper;
use common\helpers\DateDiffHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use common\models\User;
use yii\widgets\ListView;
use common\models\ProjectMessage;

/**
 * @var mixed $model:       the data model
 * @var $key: mixed,        the key value associated with the data item
 * @var integer $index      the zero-based index of the data item in the items array returned by $dataProvider.
 * @var ListView $widget    this widget instance
 * @var ProjectMessage $projectMessage
 */

$colorsCssClasses = Yii::$app->params['themes']['leadlance']['colorsCssClasses'];
?>
<div class="accordionProjectFullBlock author">
    <div class="titlesAccordionBlock open <?= $model->is_urgent ? 'hot': ''; ?> clearfix">
        <div class="col-md-8 paddingNone">
            <?php
            $projectUrl = '';
            if (!Yii::$app->user->isGuest) {
                if (User::isCustomer(Yii::$app->user->id)) {
                    $projectUrl = Url::to('/project/view-offers/' . $model->id);
                } else {
                    $projectUrl = Url::to('/project/respond-project/' . $model->id);
                }
            }
            ?>
            <h3 class="projectLink div-inline">
                <a href="<?= $projectUrl; ?>">
                    <?= Html::encode($model->title); ?>
                </a>
            </h3>
            <?php if ($model->is_urgent): ?>
                <span class="iconProj_1 iconStyleSize"></span>
            <?php endif; ?>
            <?php if ($model->is_multiproject): ?>
                <span class="iconProj_2 iconStyleSize"></span>
            <?php endif; ?>
            <span class="iconProj_3 iconStyleSize"></span>
        </div>
        <div class="col-md-4 paddingNone">
            <ul class="crambsTitleAccord">
                <?php if (empty($profileView)): ?>
                    <?php if ($model->is_auction): ?>
                        <li>
                            <a href="#" class="colorCrambs">Аукцион </a><span> |</span>
                        </li>
                    <?php else: ?>
                        <li>
                            <a href="#" class="colorCrambs"><?= number_format($model->lead_cost,2) . 'р.'; ?> </a><span> |</span>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="#"><?= $model->lead_count_required; ?> </a>
                        <?php if($model->execution_time): ?>
                            <span> |</span>
                        <?php endif; ?>
                    </li>

                    <?php if($model->execution_time): ?>
                        <li>
                            <a href="#">
                                <?= $model->execution_time; ?>
                            </a>
                        </li>
                    <?php endif; ?>
                <?php else: ?>
                    <?php if ($model->is_auction): ?>
                        <?php
                            $showDelimiter = false;
                            if ($model->lead_count_required || !$model->is_urgent) {
                                $showDelimiter = true;
                            }
                        ?>
                        <li>
                            <a href="#" class="colorCrambs">Аукцион </a><span> <?= $showDelimiter ? '|' : ''; ?></span>
                        </li>
                    <?php endif; ?>
                    <?php if ($model->lead_count_required): ?>
                        <?php
                            $showDelimiter = false;
                            if (!$model->is_urgent) {
                                $showDelimiter = true;
                            }
                        ?>
                        <li>
                            <a href="#"><?= $model->lead_count_required; ?> лид. </a><span> <?= $showDelimiter ? '|' : ''; ?></span>
                        </li>
                    <?php endif; ?>
                    <?php if (!$model->is_urgent): ?>
                        <li>
                            <a href="#"><?= $model->execution_time; ?> дня</a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
    <div class="accordionContent open clearfix">
        <p class="textContentAccord">
            <?= $model->description; ?>
        </p>
        <ul class="tagsList col-md-12 paddingNone">
            <li>
                <span>Ниша: </span>
                <?php
                    foreach ($model->projectTypes as $pType) {
                        ?>
                        <a href="#" class="<?= CommonHelper::getRandomItems($colorsCssClasses); ?> tagsFontStyle"><?= $pType->type; ?></a>
                        <?php
                    }
                ?>
            </li>
            <li>
                <span>Допустимый тип трафика: </span>
                <?php
                foreach ($model->trafficTypes as $tType) {
                    ?>
                    <a href="#" class="<?= CommonHelper::getRandomItems($colorsCssClasses); ?> tagsFontStyle"><?= $tType->type; ?></a>
                    <?php
                }
                ?>
            </li>
            <li class="lastProjPublic">
                <span class="blackTextTags">Опубликован:</span> <?= DateDiffHelper::getHumanReadableDiff($model->created_at); ?>
                <span class="lastProjPublic-link">
                    <a href="<?= $projectUrl; ?>"><span class="eyeIcon">432</span></a>
                    <a href="<?= $projectUrl; ?>">
                        <span class="listIcon">
                            <?= count($model->getProjectAnswers()); ?>
                        </span>
                    </a>
                </span>
                <div class="div-inline float-right">
                    <?php if (Yii::$app->user->can('IsOwnThisEntity', [
                        'model'    => $model,
                        'property' => 'user_id',
                    ])): ?>
                        <a href="<?= Url::to('/project/create/' . $model->id); ?>" class="author-edit">редактировать</a>
                        <?php if ($model->canUnpublish()): ?>
                            <a href="<?= Url::to('/project/unpublish/' . $model->id); ?>"
                               data-confirm="<?= Yii::t('app/project', 'Are you sure you want to unpublish this project?') ?>"
                               data-method="post"
                               class="author-remove">
                                снять с публикации
                            </a>
                        <?php elseif ($model->isUnpublished()): ?>
                            <a href="<?= Url::to('/project/publish/' . $model->id); ?>"
                               data-confirm="<?= Yii::t('app/project', 'Are you sure you want to publish this project?') ?>"
                               data-method="post"
                               class="author-remove">
                                опубликовать
                            </a>
                        <?php endif; ?>
                        <a href="javascript:void(0);" class="author-secure">закрепить</a>
                    <?php endif; ?>
                </div>
            </li>
        </ul>
    </div>
    <div class="moderator-edit">
        <form>
            <input type="submit" value="Принять" class="agree">
            <div class="refuse-wrap">
                <input type="text" placeholder="Комментарий" class="refuse-text">
                <input type="submit" value="Отказать" class="refuse">
            </div>
        </form>
    </div>
</div>
