<?php
/**
 * @var $projectFilterForm common\models\ProjectFilterForm
 * @var $this yii\web\View
 * @var $projectsDataProvider \yii\data\ActiveDataProvider
 * @var $projectTypeSelected array
 * [
 *  int
 * ]
 * @var trafficTypeSelected array
 * [
 *  int
 * ]
 * @var $projectTypesList array
 * [
 *  int => type string
 * ]
 * @var $trafficTypesList array
 * [
 *  int => type string
 * ]
 */

use common\models\ProjectFilterForm;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\widgets\ListView;
use common\helpers\CommonHelper;

AppAsset::register($this);
$colorsCssClasses = Yii::$app->params['themes']['leadlance']['colorsCssClasses'];
?>

<div class="row">
    <div class="col-md-2 navAll">
        <h2 class="projectTitles index-title">Проекты</h2>
    </div>
    <div class="col-md-5 col-md-offset-5 projectFIlterText">
        <p>Фильтр по проектам<a href="#"> свернуть</a></p>
    </div>
    <div class="clearfix"></div>
    <!--<form class="filtersForm open">-->
    <?php
        $form = ActiveForm::begin([
            'id' => $projectFilterForm->formName(),
            'options' => [
                'class' => 'filtersForm open'
            ],
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'fieldConfig' => [
                'template' => '{input}',
                'options' => [
                    'tag' => false,
                ],
            ]
        ]);
    ?>
        <div class="col-md-4 paddingRightNone">
            <div class="wrapper-custom-select">
                <?php
                    echo $form->field($projectFilterForm, 'project_type', [
                        'template' => '{input}',
                        'inputOptions' => [
                            'class' => 'custom-select',
                        ]
                    ])->dropDownList($projectTypesList);

                ?>
                <input type="text" class="inputFiltersStyle" value="Ниша" >
                <a href="#" class="select-next"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                <a href="#" class="select-prev"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="wrapper-dropbox-input">
                    <?php
                        echo '<input type="hidden" name="ProjectFilterForm[project_type]" value="">';
                        foreach ($projectTypesList as $id => $type) {
                            $itemId = $projectFilterForm->formName().'_project_type_'.$id;
                            $itemName = $projectFilterForm->formName().'[project_type][]';
                    ?>
                        <p class="exceptionText">
                            <input type="checkbox" class="checkboxStyle " id="<?= $itemId; ?>" name="<?= $itemName; ?>" value="<?= $id; ?>"
                                <?php if (isset($projectTypeSelected[$id])) {
                                    echo 'checked="true"';
                                }
                                ?>
                            >
                            <label for="<?= $itemId; ?>"></label><?= $type; ?>
                        </p>
                    <?php
                        }
                    ?>
                    <ul class="checkbox-control">
                        <li><a href="#" id="project_type_select_all">выбрать все</a></li>
                        <li><a href="#" id="project_type_deselect_all">снять все галочки</a></li>
                    </ul>
                    <a href="#" class="change-filter">Применить</a>
                </div>

                    <?php
                    if(!empty($projectTypeSelected)) {
                        ?>
                    <ul class="listTags">
                        <?php
                        foreach ($projectTypeSelected as $typeSelected) {
                            $cssColor = CommonHelper::getRandomItems($colorsCssClasses);;
                            ?>
                            <li>
                                <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?= $typeSelected; ?> </a><span class="tagsClose <?= $cssColor; ?>">x</span>
                            </li>
                            <?php
                        }
                    ?>
                    </ul>
                    <?php
                    }

                    ?>
            </div>

        </div>
        <div class="col-md-4 paddingRightNone">
            <div class="wrapper-custom-select">
                <?php
                echo $form->field($projectFilterForm, 'traffic_type', [
                    'template' => '{input}',
                    'inputOptions' => [
                        'class' => 'custom-select',
                    ]
                ])->dropDownList($trafficTypesList);

                ?>
                <input type="text" class="inputFiltersStyle" value="Допустимый тип трафика" >
                <a href="#" class="select-next"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                <a href="#" class="select-prev"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="wrapper-dropbox-input">
                    <?php
                    echo '<input type="hidden" name="ProjectFilterForm[traffic_type]" value="">';
                    foreach ($trafficTypesList as $id => $type) {
                        $itemId = $projectFilterForm->formName().'_traffic_type_'.$id;
                        $itemName = $projectFilterForm->formName().'[traffic_type][]';
                        ?>
                        <p class="exceptionText">
                            <input type="checkbox" class="checkboxStyle " id="<?= $itemId; ?>" name="<?= $itemName; ?>" value="<?= $id; ?>"
                                <?php if (isset($trafficTypeSelected[$id])) {
                                    echo 'checked="true"';
                                }
                                ?>
                            >
                            <label for="<?= $itemId; ?>"></label><?= $type; ?>
                        </p>
                        <?php
                    }
                    ?>
                    <ul class="checkbox-control">
                        <li><a href="#" id="traffic_type_select_all">выбрать все</a></li>
                        <li><a href="#" id="traffic_type_deselect_all">снять все галочки</a></li>
                    </ul>
                    <a href="#" class="change-filter">Применить</a>
                </div>
                <?php
                if(!empty($trafficTypeSelected)) {
                    ?>
                    <ul class="listTags">
                        <?php
                        foreach ($trafficTypeSelected as $trafficSelected) {
                            $cssColor = CommonHelper::getRandomItems($colorsCssClasses);;
                            ?>
                            <li>
                                <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?= $trafficSelected; ?> </a><span class="tagsClose <?= $cssColor; ?>">x</span>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                }

                ?>
            </div>
        </div>
        <div class="col-md-4 paddingRightNone">
            <?php
                echo $form->field($projectFilterForm, 'keyWords')
                    ->textInput([
                        'class' => 'inputFiltersStyle inputFiltersNonBg',
                        'placeholder' => 'Поиск по ключевым словам',
                    ])
                    ->label(false)->error(false)->hint(false);;
            ?>
            <!--<input type="text" class="inputFiltersStyle inputFiltersNonBg" placeholder="Поиск по ключевым словам">-->
            <ul class="listException">
                <li>
                    <p class="exceptionText beforeIcon1">
                        <?php
                            echo $form->field($projectFilterForm, 'excludeUrgent', [
                            ])
                                ->checkbox([
                                    'class' => 'checkboxStyle',
                                ], false)
                                ->label(false)->error(false)->hint(false);
                        ?>
                        <!--<input type="checkbox" name="filter" class="checkboxStyle" id="check_1">-->
                        <label for="<?php echo strtolower($projectFilterForm->formName().'-excludeurgent'); ?>"></label>исключить срочные проекты</p>
                </li>
                <li>
                    <p class="exceptionText beforeIcon2">
                        <?php
                        echo $form->field($projectFilterForm, 'excludeMulti', [
                        ])
                            ->checkbox([
                                'class' => 'checkboxStyle',
                            ], false)
                            ->label(false)->error(false)->hint(false);
                        ?>
                        <!--<input type="radio"  name="filter" class="checkboxStyle" id="check_2">-->
                        <label for="<?php echo strtolower($projectFilterForm->formName().'-excludemulti'); ?>"></label>исключить мультипроекты</p>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    <?php
        echo $form->field($projectFilterForm, 'range')->hiddenInput();
    ?>
        <div class="col-md-4 col-md-offset-8 buttonsFilterBLock paddingRightNone">
            <button class="buttonFilters" type="submit">Применить</button>
            <p class="clearFilters"><a href="#">Очистить фильтр</a></p>
        </div>
    <!--</form>-->
    <?php
        ActiveForm::end();
    ?>
</div>
</div>
<div class="container blockPAddBorder">
    <div class="col-lg-12 col-md-12 listProjectAll paddingRight">
        <div class="projectDistributor">
            <div class="col-md-10 paddingRight dopZINdex">
                <div class="col-md-9 col-xs-8 paddingNone">
                    <a href="#" class="distribLink-left activeDistribLink filter-range" data-range="0">Все</a>
                    <a href="#" class="distribLink-left filter-range" data-range="1">Мои</a>
                    <!--<a href="#" class="distribLink-left filter-range" data-range="2">На модерации</a>-->
                </div>
                <div class="col-md-3 col-xs-4 paddingNone textAlignRight">
                    <a href="#" class="distribLink arrowOutline active open-all">
                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                    <a href="#" class="distribLink arrowOutline close-all">
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                        <i class="fa fa-angle-up" aria-hidden="true"></i>
                    </a>
                </div>
                <div class="clearfix"></div>
                <?php
                    echo ListView::widget([
                        'dataProvider' => $projectsDataProvider,
                        'itemView' => '_item',
                        'summary' => '',
                        'emptyText' => false,
                    ])
                ?>
                <!--<div class="accordionProjectFullBlock author">
                    <div class="titlesAccordionBlock open hot clearfix">
                        <div class="col-md-8 paddingNone">
                            <h3 class="titlesAccordion div-inline"><a href="#">Продажа уникальных футболок с авторским дизайном!</a></h3>
                            <span class="iconProj_1 iconStyleSize"></span>
                            <span class="iconProj_2 iconStyleSize"></span>
                            <span class="iconProj_3 iconStyleSize"></span>
                        </div>
                        <div class="col-md-4 paddingNone">
                            <ul class="crambsTitleAccord">
                                <li>
                                    <a href="#" class="colorCrambs">Аукцион </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">380 лид. </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">23 дня</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="accordionContent open clearfix">
                        <p class="textContentAccord">
                            Нужно решить вопрос с возможностью выбора цвета и размера для виртумарта
                            (версия 2.6.6). Исходные характеристики товара: артикул, цвет, размер.
                            Артикул-общая характеристика товара (модель, узор,отделка). Цвет и
                            размер-зависимые опции. Покупатель должен видеть реальный цвет
                            товара (либо это вывод картинкию. Жду ваших предложений!
                        </p>
                        <ul class="tagsList col-md-9 paddingNone">
                            <li>
                                <span>Ниша: </span>
                                <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>
                                <a href="#" class="greenTags tagsFontStyle">социальные сети </a>
                            </li>
                            <li>
                                <span>Допустимый тип трафика: </span>
                                <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>
                                <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>
                                <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>
                            </li>
                            <li class="lastProjPublic">
                                <span class="blackTextTags">Опубликован:</span> 38 минут назад
                                <span class="lastProjPublic-link">
                                            <a href="#"><span class="eyeIcon">432</span></a>
                                            <a href="#"><span class="listIcon">23</span></a>
                                        </span>
                            </li>
                        </ul>
                        <a href="#" class="author-edit">редактировать</a>
                        <a href="#" class="author-remove">снять с публикации</a>
                        <a href="#" class="author-secure">закрепить</a>
                    </div>
                    <div class="moderator-edit">
                        <form>
                            <input type="submit" value="Принять" class="agree">
                            <div class="refuse-wrap">
                                <input type="text" placeholder="Комментарий" class="refuse-text">
                                <input type="submit" value="Отказать" class="refuse">
                            </div>
                        </form>
                    </div>
                </div>-->







                <!--<div class="accordionProjectFullBlock">
                    <div class="titlesAccordionBlock open clearfix">
                        <div class="col-md-8 paddingNone">
                            <h3 class="titlesAccordion div-inline"><a href="#">Сделать 100 холодных звонков</a></h3>
                            <span class="iconProj_1 iconStyleSize"></span>
                        </div>
                        <div class="col-md-4 paddingNone">
                            <ul class="crambsTitleAccord">
                                <li>
                                    <a href="#" class="">145p. </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">380 лид. </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">23 дня</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="accordionContent open clearfix">
                        <p class="textContentAccord">
                            Необходимо сделать 100 холодных звонков с целью офоррмления заявки.
                            Предложение для клиента бесплатное, так что конверсия высокая. Звонки физическим лицам.
                            База, связь, скрипты предоставляются.
                            Оплата по договоренности за каждый звонок + премии за оформленные заявки
                        </p>
                        <ul class="tagsList col-md-9 paddingNone">
                            <li>
                                <span>Ниша: </span>
                                <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>
                                <a href="#" class="greenTags tagsFontStyle">социальные сети </a>
                            </li>
                            <li>
                                <span>Допустимый тип трафика: </span>
                                <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>
                                <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>
                                <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>
                            </li>
                            <li class="lastProjPublic">
                                <span class="blackTextTags">Опубликован:</span> 38 минут назад
                                <span class="lastProjPublic-link">
                                            <a href="#"><span class="eyeIcon">432</span></a>
                                            <a href="#"><span class="listIcon">23</span></a>
                                        </span>
                            </li>
                        </ul>
                        <a href="#" class="author-edit">редактировать</a>
                        <a href="#" class="author-remove">снять с публикации</a>
                    </div>
                </div>
                <div class="accordionProjectFullBlock">
                    <div class="titlesAccordionBlock clearfix">
                        <div class="col-md-8 paddingNone">
                            <h3 class="titlesAccordion div-inline"><a href="#">Отправка коммерческих прделожений по нашей базе</a></h3>
                        </div>
                        <div class="col-md-4 paddingNone">
                            <ul class="crambsTitleAccord">
                                <li>
                                    <a href="#" class="colorCrambs">Аукцион </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">380 лид. </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">23 дня</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="accordionContent open clearfix">
                        <p class="textContentAccord">
                            По базе 50 контактов обзвонить, узнать лицо принимающее
                            решение и отправить презентацию. Уточнить получение.
                        </p>
                        <ul class="tagsList col-md-9 paddingNone">
                            <li>
                                <span>Ниша: </span>
                                <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>
                                <a href="#" class="greenTags tagsFontStyle">социальные сети </a>
                            </li>
                            <li>
                                <span>Допустимый тип трафика: </span>
                                <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>
                                <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>
                                <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>
                            </li>
                            <li class="lastProjPublic">
                                <span class="blackTextTags">Опубликован:</span> 38 минут назад
                                <span class="lastProjPublic-link">
                                            <a href="#"><span class="eyeIcon">432</span></a>
                                            <a href="#"><span class="listIcon">23</span></a>
                                        </span>
                            </li>
                        </ul>
                        <a href="#" class="author-edit">редактировать</a>
                        <a href="#" class="author-remove">снять с публикации</a>
                    </div>
                </div>
                <div class="accordionProjectFullBlock">
                    <div class="titlesAccordionBlock open clearfix">
                        <div class="col-md-8 paddingNone">
                            <h3 class="titlesAccordion div-inline"><a href="#">Поиск клиентов на электронный документооборот</a></h3>
                        </div>
                        <div class="col-md-4 paddingNone">
                            <ul class="crambsTitleAccord">
                                <li>
                                    <a href="#" class="">345p. </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">380 лид. </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">23 дня</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="accordionContent open clearfix">
                        <p class="textContentAccord">
                            Занимаюсь организацией продвижения электронного
                            документооборота собственной разработки АМАС.
                            Отличный фунционал, простота и удобство, очень
                            низкая стоимость владения. Оплата -10-20 % от
                            суммы заказа. Ищу партнеров для поиска клиентов.
                        </p>
                        <ul class="tagsList col-md-9 paddingNone">
                            <li>
                                <span>Ниша: </span>
                                <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>
                                <a href="#" class="greenTags tagsFontStyle">социальные сети </a>
                            </li>
                            <li>
                                <span>Допустимый тип трафика: </span>
                                <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>
                                <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>
                                <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>
                            </li>
                            <li class="lastProjPublic">
                                <span class="blackTextTags">Опубликован:</span> 38 минут назад
                                <span class="lastProjPublic-link">
                                            <a href="#"><span class="eyeIcon">432</span></a>
                                            <a href="#"><span class="listIcon">23</span></a>
                                        </span>
                            </li>
                        </ul>
                        <a href="#" class="author-edit">редактировать</a>
                        <a href="#" class="author-remove">снять с публикации</a>
                    </div>
                </div>
                <div class="accordionProjectFullBlock">
                    <div class="titlesAccordionBlock open clearfix">
                        <div class="col-md-8 paddingNone">
                            <h3 class="titlesAccordion div-inline"><a href="#">Привлечение лидов на автокредит</a></h3>
                            <span class="iconProj_1 iconStyleSize"></span>
                            <span class="iconProj_2 iconStyleSize"></span>
                        </div>
                        <div class="col-md-4 paddingNone">
                            <ul class="crambsTitleAccord">
                                <li>
                                    <a href="#" class="">145p. </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">380 лид. </a><span> |</span>
                                </li>
                                <li>
                                    <a href="#">23 дня</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="accordionContent open clearfix">
                        <p class="textContentAccord">
                            Требуется привлечение клиентов на заключение договора на автокредит.
                            Одобрение 99%, взнос клиента 15 % от стоимости автомобиля (от 120 000 т)
                            за каждый подписанный договор 10 000 тысяч. Всю необходимую информацию
                            можем предоставить.
                            Наши клиенты это люди с плохой банковской кредитной истории
                            и люди класса ниже среднего. Условия для клиента не самые гуманные,
                            первоначальный взнос 15 % 25 % годовых.
                        </p>
                        <ul class="tagsList col-md-9 paddingNone">
                            <li>
                                <span>Ниша: </span>
                                <a href="#" class="blueTags tagsFontStyle">интернет-магазин, </a>
                                <a href="#" class="greenTags tagsFontStyle">социальные сети </a>
                            </li>
                            <li>
                                <span>Допустимый тип трафика: </span>
                                <a href="#" class="pincedTags tagsFontStyle">таргетированная реклама </a>
                                <a href="#" class="yellowTags tagsFontStyle">группы в соц. сетях </a>
                                <a href="#" class="coralTags tagsFontStyle">контекстная реклама </a>
                            </li>
                            <li class="lastProjPublic">
                                <span class="blackTextTags">Опубликован:</span> 38 минут назад
                                <span class="lastProjPublic-link">
                                            <a href="#"><span class="eyeIcon">432</span></a>
                                            <a href="#"><span class="listIcon">23</span></a>
                                        </span>
                            </li>
                        </ul>
                        <a href="#" class="author-edit">редактировать</a>
                        <a href="#" class="author-remove">снять с публикации</a>
                    </div>
                </div>-->

                <!-- Временно будем использовать дефолтную пагинацию -->
                <!--<div class="col-lg-12 pagination-wrap">
                    <nav aria-label="Page navigation">
                        <ul class="pagination only-next">
                            <li>
                                <a href="#" aria-label="Previous">
                                    <span aria-hidden="true"><span class="next-mob">&laquo;</span><span class="next-desc"><img src="/themes/leadlance/img/index/pagin-arrow.png" alt=""> предыдущая</span></span>
                                </a>
                            </li>
                            <li class="numberPagination active"><a href="#">1</a></li>
                            <li class="numberPagination"><a href="#">2</a></li>
                            <li class="numberPagination"><a href="#">3</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                                    <span aria-hidden="true"><span class="next-mob">&raquo;</span><span class="next-desc">следующая <img src="/themes/leadlance/img/index/pagin-arrow.png" alt=""></span></span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>-->
            </div>
        </div>
    </div>