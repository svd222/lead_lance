<?php

use yii\data\ArrayDataProvider;
use yii\widgets\ListView;
use common\models\ProjectMessage;

/**
 * @var ProjectMessage[][] $projectConversationMessagesBranchNodes
 */
?>

<?php
    $projectMessagesBranchNodesDataProvider = new ArrayDataProvider([
        'allModels' => $projectConversationMessagesBranchNodes,
    ]);
    ?>
<?= ListView::widget([
    'dataProvider' => $projectMessagesBranchNodesDataProvider,
    'itemView' => '_offer_message_item',
    'summary' => '',
    'itemOptions' => function ($model, $key, $index, $widget) {
        /**
         * @var ProjectMessage $model
         */
        if (!empty($model->newNotifications)) {
            return [
                'class' => 'new-message',
                'data-id' => $model->id,
            ];
        }
    }
]); ?>
