<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 08.08.17
 * @time: 20:44
 */

use yii\widgets\ActiveForm;
use common\models\PartAccepted;
use yii\helpers\Html;

/**
 * @var PartAccepted $partAccepted
 */
?>
<div class="modal fade" id="customer-accept-part-work" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Оплатить лиды</h4>
            </div>
            <?php
            $form = ActiveForm::begin([
                'id' => $partAccepted->formName(),
                'options' => [
                    'class' => 'create-order-form project-crate-form-style'
                ],
                'enableAjaxValidation' => false,
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => '{input}',
                    'options' => [
                        'tag' => false,
                    ],
                ]
            ]);
            ?>
            <div class="modal-body">
                <!--<input type="text" class="enter-number-pay" placeholder="Введите кол-во лидов">-->
                <?php
                    echo $form->field($partAccepted, 'lead_count_accepted')
                        ->textInput([
                            'class' => 'enter-number-pay',
                            'placeholder' => 'Введите кол-во лидов',
                        ]);
                ?>
            </div>
            <div class="modal-footer">
                <!--<button class="end-work">Оплатить %13% лидов</button>-->
                <?php
                echo Html::button('Оплатить лиды', [
                    'class' => 'end-work',
                ]);
                ?>
            </div>
            <?php
                echo $form->field($partAccepted, 'order_id')->hiddenInput();
            ?>
            <?php
                ActiveForm::end();
            ?>
        </div>
    </div>
</div>
