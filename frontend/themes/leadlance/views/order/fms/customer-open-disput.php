<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 08.08.17
 * @time: 20:38
 */

use common\models\Disput;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var Disput $disput
 */

?>
<div class="modal fade" id="customer-open-dispute" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title disput-title" >Спор %Название заказа%</h4>
            </div>
            <?php
            $form = ActiveForm::begin([
                'id' => $disput->formName(),
                'options' => [
                    'class' => 'create-order-form project-crate-form-style'
                ],
                'enableAjaxValidation' => false,
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => '{input}',
                    'options' => [
                        'tag' => false,
                    ],
                ]
            ]);
            ?>
            <div class="modal-body">
                <!--<textarea placeholder="Опишите причину, по которой вы открываете спор..." class="enter-review"></textarea>-->
                <?php
                echo $form->field($disput, 'reason')
                    ->textarea([
                        'placeholder' => 'Опишите причину, по которой вы открываете спор...',
                        'class' => 'enter-review'
                    ]);
                ?>
            </div>
            <div class="modal-footer">
                <!--<button class="end-work disput-title">Открыть спор!</button>-->
                <?php
                echo Html::button('Открыть спор!', [
                    'class' => 'end-work disput-title',
                ]);
                ?>
            </div>
            <?php
            echo $form->field($disput, 'order_id')->hiddenInput();
            ?>
            <?php
            ActiveForm::end();
            ?>
        </div>
    </div>
</div>