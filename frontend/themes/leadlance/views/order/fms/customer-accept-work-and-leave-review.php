<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 08.08.17
 * Time: 18:45
 */

use common\models\OrderReview;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/**
 * @var OrderReview $orderReview
 */
?>
<div class="modal fade" id="customer-accept-work-and-leave-review" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" >Оставить отзыв и завершить сотрудничество</h4>
            </div>
            <?php
            $form = ActiveForm::begin([
                'id' => $orderReview->formName(),
                'options' => [
                    'class' => 'create-order-form project-crate-form-style'
                ],
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'enableClientScript' => true,
                'fieldConfig' => [
                    'template' => '{input}',
                    'options' => [
                        'tag' => false,
                    ],
                ]
            ]);
            ?>
            <div class="modal-body">
                <p class="exceptionText div-inline plus">
                    <?php
                    $checkBoxId = strtolower($orderReview->formName().'_emotion_1');
                    $checkBoxName = $orderReview->formName().'[emotion][]';
                    ?>
                    <input type="radio" name="<?= $checkBoxName; ?>" class="checkboxStyle" id="<?= $checkBoxId; ?>" value="1" checked>
                    <label for="<?= $checkBoxId; ?>"></label><span class="">+Положительный</span>
                </p>
                <p class="exceptionText div-inline minus">
                    <?php
                    $checkBoxId = strtolower($orderReview->formName().'_emotion_2');
                    $checkBoxName = $orderReview->formName().'[emotion][]';
                    ?>
                    <input type="radio" name="<?= $checkBoxName; ?>" class="checkboxStyle" id="<?= $checkBoxId; ?>" value="2">
                    <label for="<?= $checkBoxId; ?>"></label><span class="">-Отрицательный</span>
                </p>
                <!--<textarea placeholder="Введите текст отзыва" class="enter-review"></textarea>-->
                <?php
                echo $form->field($orderReview, 'review')
                    ->textarea([
                        'placeholder' => 'Введите текст отзыва',
                        'class' => 'enter-review'
                    ]);
                ?>
            </div>
            <div class="modal-footer">
                <?php
                echo Html::button('Завершить сотрудничество', [
                    'class' => 'end-work',
                ]);
                ?>
                <!--<button class="end-work">Завершить сотрудничество</button>-->
            </div>
            <?php
            echo $form->field($orderReview, 'order_id')->hiddenInput();
            ?>
            <?php
            ActiveForm::end();
            ?>
        </div>
    </div>
</div>