<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 18.01.17
 * @time: 22:19
 */

use yii\widgets\ActiveForm;
use common\helpers\CommonHelper;
use yii\helpers\Html;
use dosamigos\fileupload\FileUpload;
use frontend\assets\AppAsset;
use common\models\Batch;
use common\models\User;
use yii\web\View;
use common\models\Order;
use common\models\TrafficType;
use common\models\Project;
use common\models\Image;

/**
 * @var View $this
 * @var Order $model
 * @var Image $imageModel
 * @var TrafficType $trafficTypeModel
 * @var User $executor
 * @var int $userId
 * @var string $action maybe 'create' or 'update'
 *
 * @var array $trafficTypesList
 * [
 *  int
 * ]
 * @var array $trafficTypeSelected
 * [
 *  int => string
 * ]
 */


AppAsset::register($this);
$colorsCssClasses = Yii::$app->params['themes']['leadlance']['colorsCssClasses'];

/**
 * @var User $owner
 */

?>
<div class="row">
    <div class="col-md-12">
        <h2 class="projectTitles page-titles"><?= $this->title; ?></h2>
    </div>
</div>
<?php
if ($model->hasErrors()) {
    foreach ($model->errors as $error) {
        $errMess = "    
            $.gritter.add({
                title: '',
                text: '".$error[0]."',
                class_name: 'gritter-error',
                time: 8000,
            });
        ";
        $this->registerJs($errMess, View::POS_LOAD);
    }
}
$form = ActiveForm::begin([
    'id' => $model->formName(),
    'options' => [
        'class' => 'create-order-form'
    ],
    'enableAjaxValidation' => false,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => '{input}',
        'options' => [
            'tag' => false,
        ],
    ]
]);
?>
    <div class="row">
        <div class="col-lg-8 col-md-8">
            <p class="input-title">Название</p>
            <!--<input type="text" name="project-name" class="project-name">-->
            <?php
                echo $form->field($model, 'title')
                    ->textInput([
                        'class' => 'project-name',
                    ]);
            ?>
            <?php
                echo $form->field($model, 'user_id')->hiddenInput();
                echo $form->field($model, 'executor_id')->hiddenInput();
                echo $form->field($model, 'project_id')->hiddenInput();
            ?>
            <div class="wrapper-custom-select order-select">
                <?php
                    echo $form->field($trafficTypeModel, 'type', [
                        'template' => '{input}',
                        'inputOptions' => [
                            'class' => 'custom-select',
                        ]
                    ])->dropDownList($trafficTypesList);
                ?>
                <input type="text" class="inputFiltersStyle" value="Допустимый тип трафика" >
                <a href="#" class="select-next"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
                <a href="#" class="select-prev"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <div class="wrapper-dropbox-input">
                    <?php
                    echo '<input type="hidden" name="' . $trafficTypeModel->formName() .  '[type]" value="">';
                    foreach ($trafficTypesList as $id => $type) {
                        $itemId = $trafficTypeModel->formName().'_type_'.$id;
                        $itemName = $trafficTypeModel->formName().'[type][]';
                        ?>
                        <p class="exceptionText">
                            <input type="checkbox" class="checkboxStyle " id="<?= $itemId; ?>" name="<?= $itemName; ?>" value="<?= $id; ?>"
                                <?php if (!empty($trafficTypeSelected) && isset($trafficTypeSelected[$id])) {
                                    echo 'checked="true"';
                                }
                                ?>
                            >
                            <label for="<?= $itemId; ?>"></label><?= $type; ?>
                        </p>
                        <?php
                    }
                    ?>
                    <ul class="checkbox-control">
                        <li><a href="#">выбрать все</a></li>
                        <li><a href="#">снять все галочки</a></li>
                    </ul>
                    <a href="#" class="change-filter">Применить</a>
                </div>
                <?php
                if(!empty($trafficTypeSelected)) {
                    ?>
                    <ul class="listTags">
                        <?php
                        foreach ($trafficTypeSelected as $trafficSelected) {
                            $cssColor = CommonHelper::getRandomItems($colorsCssClasses);;
                            ?>
                            <li>
                                <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?= $trafficSelected; ?> </a><span class="tagsClose <?= $cssColor; ?>">x</span>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                }
                ?>
            </div>
            <div class="wrapper-description">
                <p class="input-title">Описание</p>
                <!--<textarea name="project-description" ></textarea>-->
                <?php
                    echo $form->field($model, 'description')->textarea([
                        'cols' => 30,
                        'rows' => 10,
                        'class' => '',
                    ]);
                ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="dropbox">
                <label>
                    <!--<input type="file" name="dropbox">-->
                    <?= FileUpload::widget([
                        'model' => $imageModel,
                        'attribute' => 'image',
                        'url' => ['/image/upload/' . Batch::ENTITY_TYPE_ORDER],
                        'useDefaultButton' => false,
                        'clientOptions' => [
                            'maxFileSize' => 2000000,
                            'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                        ],
                        // ...
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                /*console.log(e);
                                console.log(data);*/
                            }',
                        ],
                    ]); ?>
                    <?php
                    $this->registerJs("
                        collection.get('action').set('formName', '".$model->formName()."');    
                        collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                        collection.get('action').set('deleteUrl', '/image/delete/');
                        collection.get('action').set('imageFormName', '".$imageModel->formName()."');
                    ", \yii\web\View::POS_BEGIN);
                    ?>
                     <span class="download-text">загрузите файл<br> или перетащите его сюда</span>
                     <span class="download-size">Max: 5 mb</span>
                 </label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-md-5 wrap-coast-lead">
            <p class="input-title">Цена лида</p>
            <?php
                $project = Project::findOne($model->project_id);
            ?>
            <?php if (!empty($project) && $project->is_auction): ?>
                    Руб.
            <?php else: ?>
                <?= $form->field($model, 'lead_cost')
                    ->textInput([
                        'class' => 'coast-lead-input',
                    ]);
                ?>
                <label>
                    <select class="coast-lead">
                        <option>Rub</option>
                        <option>$</option>
                    </select>
                </label>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-md-5 wrap-coast-lead">
            <p class="input-title">Срок исполнения</p>
            <?php
            echo $form->field($model, 'execution_time')
                ->textInput([
                    'class' => 'coast-lead-input'
                ]);
            ?>
            <!--<input type="text" name="project-name" class="coast-lead-input">-->
            <label>
                <select class="coast-lead">
                    <option>День</option>
                    <option>Год</option>
                </select>
            </label>
        </div>
        <!--<div class="col-lg-7 col-md-7 wrap-coast-lead hot-order-check-style">
            <p class="exceptionText div-inline hot-order">
                <span class="iconProj_1 iconStyleSize marg-icon-style"></span>
                <!--<input type="checkbox" name="filter" class="checkboxStyle" id="check_2">-- >
                <?php
/*                $checkBoxOptions = [
                    'class' => 'checkboxStyle',
                ];
                if ($model->is_urgent) {
                    $checkBoxOptions['checked'] = true;
                }
                echo $form->field($model, 'is_urgent')
                    ->checkbox($checkBoxOptions, false);
                */?>
                <label for="check_2"></label><span class="hot-project-text">cрочный проект</span>
            </p>
            <div class="div-inline">
                <p class="input-title ">Крайний срок</p>
                <!--<input type="text" name="project-day" class="last-time-day" placeholder="16">-->
                <?php
/*                    echo $form->field($model, 'deadlineDtDay')
                        ->textInput([
                            'class' => 'last-time-day',
                            'placeholder' => $model->deadlineDtDay,
                        ]);
                */?>
                <!--<input type="text" name="project-month" class="last-time-month" placeholder="август">-->
                <?php
/*                echo $form->field($model, 'deadlineDtMonth')
                    ->textInput([
                        'class' => 'last-time-month',
                        'placeholder' => CommonHelper::getShortMonthList($model->deadlineDtMonth - 1),
                    ]);
                */?>
                <!--<input type="text" name="project-year" class="last-time-year" placeholder="2017">-- >
                <?php
/*                echo $form->field($model, 'deadlineDtYear')
                    ->textInput([
                        'class' => 'last-time-year',
                        'placeholder' => $model->deadlineDtYear,
                    ]);
                */?>
            </div>
        </div>-->

    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 wrap-coast-lead">
            <p class="input-title">Количество лидов</p>
            <!--<input type="text" name="numbers-lead" class="coast-lead-input numbers-lead">-->
            <?php
            echo $form->field($model, 'lead_count_required')
                ->textInput([
                    'class' => 'coast-lead-input numbers-lead',
                ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 wrap-coast-lead">
            <p class="input-title">Срок принятия выполненного задания</p>
            <?php
                echo $form->field($model, 'acceptance_time')
                    ->textInput([
                        'class' => 'coast-lead-input',
                    ]);
            ?>
            <!--<input type="text" name="project-name" class="coast-lead-input">-->
            <label>
                <select class="coast-lead">
                    <option>День</option>
                    <option>Два</option>
                </select>
            </label>
        </div>
    </div>
    <div class="row agree-order">
        <div class="col-md-5  buttonsFilterBLock paddingRightNone">
            <!--<button class="buttonFilters button-create-order" data-toggle="modal" data-target="#create-order">Создать заказ</button>-->
            <?=
                Html::submitButton(Yii::t('app/order', $action == 'create' ? 'Create order' : 'Update order'), [
                'class' => 'buttonFilters button-create-order',
                'data-toggle' => 'modal',
                'data-target' => '#create-order',
            ]);
            ?>
            <div class="agree-order_name">
                <?= $this->render(
                    '/profile/_nickname_info',
                    [
                        'user' => $executor,
                    ]
                ); ?>
            </div>
            <ul class="agree-order_reit">
                <li>
                    <img src="/themes/leadlance/img/profile/reit.png" alt="">
                    <a href="#" class="hover-text-decoration"><span class="text">Рейтинг: </span> </a>
                    <a href="#" class="blue-link">8267</a>
                </li>
                <li>
                    <?= $this->render(
                        '/profile/_reviews',
                        [
                            'user' => $executor,
                        ]
                    ); ?>
                </li>
            </ul>
        </div>
        <div class="col-md-7">
            <button class="button-return-order">Вернуться к обсуждению проекта</button>
        </div>
    </div>
<?php
    ActiveForm::end();
?>
