<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 06.08.17
 * @time: 4:01
 */

use common\interfaces\fms\FMSInterface;
use common\models\Order;
use frontend\themes\leadlance\widgets\OrderButtonActionsWidget;
use common\models\TrafficType;
use frontend\assets\AppAsset;
use common\helpers\CommonHelper;
use common\models\User;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\OrderMessage;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use common\models\Batch;
use dosamigos\fileupload\FileUpload;
use common\models\Image;

/**
 * @var FMSInterface $fms
 * @var array $orderMessages
 * @var Order $order
 * @var string $notify
 * @var TrafficType[] $trafficTypes
 * @var User $user The collocutor
 * @var bool $userHasPro true if collocutor have pro
 * @var bool $userIsCustomer true if collocutor is customer
 * @var bool $userIsExecutor true if collocutor is executor
 * @var string $userFullName
 * @var OrderMessage $orderMessage
 * @var array $currentUser
 * @var array $collocutor
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 */

AppAsset::register($this);
$this->registerJs("
    var action = collection.get('controller').get('action');
    action.set('currentUserInfo', {
        id: ".$currentUser['id'].",
        hasPro: '".$currentUser['hasPro']."',
        nickName: '".$currentUser['nickName']."',
        fullName: '".$currentUser['fullName']."' 
    });
    action.set('collocutor', {
        id: ".$collocutor['id'].",
        hasPro: '".$collocutor['hasPro']."',
        nickName: '".$collocutor['nickName']."',
        fullName: '".$collocutor['fullName']."' 
    });
    action.set('image_rel_path', '".Image::UPLOAD_WEB.DIRECTORY_SEPARATOR."');
", View::POS_BEGIN);
$colorsCssClasses = Yii::$app->params['themes']['leadlance']['colorsCssClasses'];
?>
<!--<style type="text/css">
    .dropbox label {
        margin-top: 0px;
    }
</style>-->
<div class="row">
    <div class="col-md-12">
        <p class="you-executive-title"><span class="attention">!</span><span class="attention-text"><?= $notify; ?></span></p>
        <!--<p class="you-executive-title grey"><span class="attention">!</span><span class="attention-text"> Вас выбрали иcполнителем подтвердите заказ для начала работы</span></p>-->
        <!--<p class="you-executive-title lightgrey"><span class="attention">!</span><span class="attention-text"> Вас выбрали иcполнителем подтвердите заказ для начала работы</span></p>-->
        <!--<p class="you-executive-title red"><span class="attention">!</span><span class="attention-text"> Вас выбрали иcполнителем подтвердите заказ для начала работы</span></p>-->
    </div>
</div>
<div class="row">
    <div class=" col-md-12">
        <div class="titlesAccordionBlock clearfix">
            <div class="col-md-9 paddingNone">
                <h3 class="titlesAccordion div-inline"><?= $order->title; ?></h3>
            </div>
            <div class="col-md-3 paddingNone">
                <ul class="crambsTitleAccord">
                    <li>
                        <a href="#" class=""><?= CommonHelper::humanReadableCost($order->lead_cost); ?>p. </a><span> |</span>
                    </li>
                    <li>
                        <a href="#"><?= $order->lead_count_required ?> лид. </a><span> |</span>
                    </li>
                    <li>
                        <a href="#"><?= $order->execution_time; ?> дня</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?php $executionDate = gmdate('Y-m-d', strtotime($order->created_at) + ($order->execution_time * 86400)); ?>
        <p class="last-date-text"><span class="iconProj_1 iconStyleSize"></span> Дата исполнения: <span class="last-date date"><?= $executionDate; ?></span></p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 order-executive-body">
        <?= $order->description; ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <!--<button type="button" class="order-executive-agree" data-toggle="modal" data-target="#end-order">Подтвердить выполнение заказа</button>
        <button type="button" class="order-executive-part" data-toggle="modal" data-target="#pay-lead">Выполнен частично</button>
        <button type="button" class="order-executive-reject" data-toggle="modal" data-target="#open-dispute">Отклонить (открыть спор)</button>-->
        <?= OrderButtonActionsWidget::widget([
                'order' => $order
        ]); ?>
    </div>
</div>
</div>
<?php
if (!empty($user)) {
?>
<div class="container blockPAddBorder">
    <div class="agree-order_name div-inline">
        <span class="role">
            <?= $userIsCustomer ? 'Заказчик' : ( $userIsExecutor ? 'Исполнитель' : ''); ?>:
        </span>
        <?= $this->render(
            '/profile/_nickname_info',
            [
                'user' => $user,
            ]
        ); ?>
    </div>
    <ul class="agree-order_reit div-inline">
        <li>
            <?= $this->render(
                '/profile/_reviews',
                [
                    'user' => $user,
                ]
            ); ?>
        </li>
    </ul>
    <!--<a href="#" class="number-one">1</a>-->
</div>
<?php
}
?>
<div class="container blockPAddBorder">
    <div class="type-trafic">
        <p><span class="text">Допустимый тип трафика:</span>
        <?php
        foreach ($trafficTypes as $k => $trafficType) {
            $cssColor = CommonHelper::getRandomItems($colorsCssClasses);;
            ?>

                <a href="#" class="<?= $cssColor; ?> tagsFontStyle"><?php
                    echo $trafficType->type;
                    if (isset($trafficTypes[$k + 1])) {
                        ?>,<?php
                    }
                    ?></a>

            <?php
        }
        ?>
        </p>
    </div>
</div>
<div class="container blockPAddBorder" id="order-message-form-container">
    <?php
    $form = ActiveForm::begin([
        'id' => $orderMessage->formName(),
        'options' => [
            'class' => 'write-comment'
        ],
        'enableAjaxValidation' => true,
        'validationUrl' => '/order/validate-order-message-form',

        'enableClientScript' => true,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => '{input}',
            'options' => [
                'tag' => false,
            ],
        ]
    ]);
    ?>
        <div class="row">
            <div class="col-md-8 wrap-textarea">
                <?php
                    echo $form->field($orderMessage, 'message')
                        ->textarea([
                            'class' => 'form-comment-area',
                        ]);
                ?>
                <?php
                    echo Html::submitButton('Отправить сообщение', [
                        'class' => 'buttonFilters',
                    ]);
                ?>
            </div>
            <div class="col-md-4">
                <div class="dropbox">
                    <label>
                        <?= FileUpload::widget([
                            'model' => $imageModel,
                            'attribute' => 'image',
                            'url' => ['/image/upload/' . Batch::ENTITY_TYPE_ORDER_MESSAGE],
                            'useDefaultButton' => false,
                            'clientOptions' => [
                                'maxFileSize' => 2000000,
                                'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                            ],
                        ]); ?>
                        <?php
                        // @todo move below js code to separate file
                        $this->registerJs("
                            collection.get('action').set('formName', '".$orderMessage->formName()."');
                            collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                            collection.get('action').set('deleteUrl', '/image/delete/');
                            collection.get('action').set('imageFormName', '".$imageModel->formName()."');
                            
                        ", \yii\web\View::POS_BEGIN);

                        ?>
                        <span class="download-text   btn-file">загрузите файл<br> или перетащите его сюда
                        <span class="download-size-mini">
                            Max: 10 mb
                        </span>
                    </span>
                        <!--<input type="file" name="dropbox">
                        <span class="download-text">загрузите файл<br> или перетащите его сюда</span>
                        <span class="download-size">(Max: 10 mb)</span>-->
                    </label>
                </div>
                <!--<p class="downloaded-files">
                    <a href="#" class="hover-text-decoration">
                        <img src="/themes/leadlance/img/order/downloaded-files.png" alt="">
                        <span class="text">загруженый файл.pdf,doc и тд.</span>
                    </a>
                </p>-->
            </div>
        </div>
    <?php
    echo $form->field($orderMessage, 'from_user_id')->hiddenInput();
    echo $form->field($orderMessage, 'to_user_id')->hiddenInput();
    echo $form->field($orderMessage, 'order_id')->hiddenInput();
    ActiveForm::end();
    ?>
</div>
<?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'itemView' => '_order_message',
        'showOnEmpty' => false,
        'summary' => false,
        'viewParams' => [
            'currentUser' => $currentUser,
            'collocutor' => $collocutor,
            'userIsCustomer' => $userIsCustomer,
            'userIsExecutor' => $userIsExecutor,
            'order' => $order,
        ],
    ]);
?>


<!--<div class="container blockPAddBorder">
    <div class="row">
        <div class="col-lg-12 comment">
            <div class="agree-order_name div-inline">
                <p>
                    <span class="name"><a href="#" class="hover-text-decoration">Артем Тимофеев</a> </span>
                    <span class="nickname">(nickname)</span>
                    <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                    <span class="date">(13:23 | 21.08.2019)</span>
                </p>
            </div>
            <p class="comment-text">Добрый день, когда ждать результатов?</p>
        </div>
    </div>
</div>
<div class="container blockPAddBorder">
    <div class="row">
        <div class="col-lg-12 comment">
            <div class="agree-order_name div-inline">
                <p>
                    <span class="name"><a href="#" class="hover-text-decoration">Артем Тимофеев </a></span>
                    <span class="nickname">(nickname)</span>
                    <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                    <span class="date">(18:23 | 20.08.2019)</span>
                </p>
            </div>
            <p class="comment-text">Смотрите, у нас есть скрипт. его демку и скрины я прислал.</p>
            <p class="comment-text">есть wwpay – дизайн который мне нравится.</p>
            <p class="comment-text"> нужно оставить весь функционал имеющего скрипта (из демки), но максимально приблизив его к ввпаю. тоесть в шапке у нас получается 2
                столбца, а не три. нужно обращать внимание именно на функциональные мелочи, их нужно оставить неизменными от демки, тк в противном
                случае мы не натянем потом дизайн на скрипт.</p>
        </div>
    </div>
</div>
<div class="container blockPAddBorder">
    <div class="row">
        <div class="col-lg-12 comment">
            <div class="agree-order_name div-inline">
                <p>
                    <span class="name"><a href="#" class="hover-text-decoration">Андрей Сидоров</a> </span>
                    <span class="nickname">(nickname)</span>
                    <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                    <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
                    <span class="date">(13:36 | 20.08.2019)</span>
                </p>
            </div>
            <p class="comment-text">Добрый день! Расскажите о проекте!</p>
        </div>
    </div>-->
