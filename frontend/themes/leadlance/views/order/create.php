<?php

use common\models\User;
use yii\web\View;
use common\models\Order;
use common\models\Image;
use common\models\TrafficType;

/**
 * @var View $this
 * @var Order $model
 * @var Image $imageModel
 * @var TrafficType $trafficTypeModel
 * @var User $executor
 * @var int $executorId
 * @var bool $executorHasPro
 * @var string $action maybe 'create' or 'update'
 *
 * @var array $trafficTypesList
 * [
 *  int
 * ]
 * @var array $trafficTypeSelected
 * [
 *  int => string
 * ]
 */

$this->title = Yii::t('app/order', $action == 'create' ? 'Creating order' : 'Updating order');
echo $this->render('_form', [
    'model' => $model,
    'imageModel' => $imageModel,
    'trafficTypesList' => $trafficTypesList,
    'trafficTypeModel' => $trafficTypeModel,
    'trafficTypeSelected' => $trafficTypeSelected,
    'executor' => $executor,
    'executorId' => $executor->id,
    'executorHasPro' => User::hasPro($executor->id),
    'action' => $action
]);

?>

