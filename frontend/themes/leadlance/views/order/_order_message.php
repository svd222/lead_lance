<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 14.08.17
 * @time: 15:05
 */

use common\models\OrderMessage;
use yii\widgets\ListView;
use common\models\User;
use common\models\Order;
use common\models\Image;

/**
 * @var OrderMessage $model the data model
 * @var mixed $key          the key value associated with the data item
 * @var integer $index      the zero-based index of the data item in the items array returned by $dataProvider.
 * @var ListView            $widget
 *
 * @var array $currentUser
 * @var array $collocutor
 * @var bool $userIsCustomer
 * @var bool $userIsExecutor
 * @var Order $order
 */

if ($model->from_user_id == $currentUser['id']) {
    $user = $currentUser;
} else {
    $user = $collocutor;
}
$conainerCssClass = '';
if ($currentUser['id'] == $model->to_user_id) {
    $conainerCssClass = ' comment-hot';
}

?>
<div class="container blockPAddBorder">
    <div class="row">
        <div class="col-lg-12 comment <?= $conainerCssClass; ?>">
            <div class="agree-order_name div-inline">
                <p>
                    <?php
                    if ($user['fullName']) {
                        ?>
                        <span class="name"><a href="#"
                                              class="hover-text-decoration"><?= $user['fullName']; ?></a> </span>
                        <?php
                    }
                    ?>
                    <span class="nickname">(<?= $user['nickName']; ?>)</span>
                    <?php
                    if ($user['hasPro']) {
                        //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
                        ?>
                        <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
                        <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
                        <?php
                    }
                    ?>
                    <?php
                        $d = strtotime($model->created_at);
                        $d = date('H:i | d.m.Y', $d);
                    ?>
                    <span class="date">(<?= $d; ?>)</span>
                </p>
            </div>
            <p class="comment-text"><?= $model->message; ?></p>
            <?php
            if ($model->batch && $model->batch->images) {
                /**
                 * @var Image $image
                 */
                foreach ($model->batch->images as $image) {

                ?>
                    <p class="downloaded-files">
                        <a href="<?= Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $image->image; ?>" class="hover-text-decoration">
                            <img src="/themes/leadlance/img/order/downloaded-files.png" alt="">
                            <span class="text"><?= $image->original_image; ?></span>
                        </a>
                    </p>
                <?php
                }
            }
            ?>
        </div>
    </div>
</div>
