<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 17.06.17
 * Time: 1:52
 */

use yii\web\View;
use common\models\ProfileCase;
use yii\helpers\Url;
use common\models\User;
use common\models\ProfilePrivate;
use common\models\UserAvatar;
use yii\bootstrap\ActiveForm;
use common\models\Comment;
use yii\helpers\Html;

/**
 * @var View        $this
 * @var ProfileCase $profileCase
 * @var Comment     $comment
 * @var int         $commentCount
 * @var Comment[]   $rootComments
 * @var int         $currentUserId
 */

$this->registerCssFile('@webTheme' . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR . 'case-view-experimental.css');

/**
 * @var User $user
 */
$user = $profileCase->user;

/**
 * @var ProfilePrivate $profilePrivate
 */
$profilePrivate = $user->profilePrivate;

if ($profilePrivate && $profilePrivate->userAvatar) {
    /**
     * @var UserAvatar $userAvatar
     */
    $userAvatar = $profilePrivate->userAvatar;
}
?>

<div class="row">
    <div class="news-wrap news-wrap_news-open col-md-12">
        <div class="text-wrap-news div-inline">
            <div class="row">
                <div class="title-news col-md-10">
                    <?= Html::encode($profileCase->title); ?>
                </div>
            </div>

            <div class="clearfix">&nbsp;</div>
            <div class="presentation-container">
                <?= $profileCase->presentation; ?>
            </div>
            <div class="clearfix">&nbsp;</div>
            <a href="<?= Url::to('/profile/index'); ?>" class="back-to-all-news">
                <img src="/themes/leadlance/img/news/arrow-back.png" alt="" class="back-to-all-news__img div-inline">
                <span class="back-to-all-news__text div-inline">Вернуться ко всем статьям</span>
            </a>
            <div class="footer-news">
                <?php if (!empty($userAvatar)) { ?>
                    <img src="<?= UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $userAvatar->image; ?>" alt="" class="footer-news__logo-author" style="width: 50px; height: 50px;">
                <?php } else { ?>
                    <img src="/themes/leadlance/img/news/author-logo.png" alt="" class="footer-news__logo-author" style="width: 50px; height: 50px;">
                <?php } ?>
                <a href="/profile?id=<?= $user->id; ?>" class="footer-news__link-author">
                    <?= $user->username; ?>
                </a>
                <div class="footer-news__tags">
                    <div class="counter-mess div-inline">
                        <img src="/themes/leadlance/img/message/message.png" alt="">
                        <span id="comment-count">
                            <?= $commentCount; ?>
                        </span>
                    </div>
                </div>
                <?php if (!empty($currentUserId)
                    && $currentUserId == $user->id): ?>
                    <div class="redact-and-delet-all edit-delete-links">
                        <span class="delet-link-proj-inner">
                            <a href="<?= Url::to('/profile/delete-case/' . $profileCase->id); ?>"
                               aria-label="<?= Yii::t('app/profile_case', 'Delete'); ?>"
                               data-confirm="<?= Yii::t('app/profile_case', 'Are you sure you want to delete this case?'); ?>">удалить</a>
                        </span>
                        <span class="red-link-proj-inner">
                            <a href="<?= Url::to('/profile/create-case/' . $profileCase->id); ?>">редактировать</a>
                        </span>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?php
    $form = ActiveForm::begin([
        'id' => $comment->formName(),
        'options' => [
            'class' => 'write-comment'
        ],
        'enableAjaxValidation' => false,
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => '{input}',
            'options' => [
                'tag' => false,
            ],
        ]
    ]);

?>
    <div class="row">
        <div class="col-md-12 wrap-textarea">
            <label for="<?= $comment->formName() . '_message' ?>" class="wrap-textarea__title">Оставить комментарий</label>
            <?php
                echo $form->field($comment, 'message')
                    ->textarea([
                        'class' => 'form-comment-area',
                        'placeholder' => 'Текст сообщения',
                    ]);
            ?>
            <?php
                echo $form->field($comment, 'from_user_id')->hiddenInput();
                echo $form->field($comment, 'item_id')->hiddenInput();
                echo $form->field($comment, 'parent_id')->hiddenInput();
                echo Html::button(Yii::t('app/comment', 'Send comment'), [
                    'class' => 'buttonFilters',
                    'id'    => 'send-case-comment',
                ]);
            ?>
        </div>
    </div>

<?php
    ActiveForm::end();
?>
<br />
<div id="case-comments">
    <?php if (!empty($rootComments)): ?>
        <?= $this->render('/comment/_comment_tree', [
            'rootComments' => $rootComments,
        ]); ?>
    <?php endif; ?>
</div>
