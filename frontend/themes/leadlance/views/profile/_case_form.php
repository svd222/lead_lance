<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 13.06.17
 * @time: 15:23
 */

use yii\web\View;
use common\models\ProfileCase;
use common\models\User;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use dosamigos\fileupload\FileUpload;

/**
 * @var View $this
 * @var ProfileCase $profileCase
 */

/**
 * @var User $user
 */
$user = $profileCase->user;
?>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">

        <?php
        //start the Profile cases form
        $form = ActiveForm::begin([
            'id' => $profileCase->formName(),
            'options' => [
                'class' => 'modal-add-project-form'
            ],
            //'action' => '/profile/create-case',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            //'validationUrl' => '/profile/create-case',
            'fieldConfig' => [
                'template' => '{input}',
                'options' => [
                    'tag' => false,
                ],
            ]
        ])
        ?>
        <div class="top-modal-block clearfix">
            <div class="col-md-6 col-sm-6 col-xs-12 left-block-form-modal">
                <h4 class="modal-title div-inline">Название проекта</h4>
                <?php
                echo $form->field($profileCase, 'title')
                    ->textInput([
                        'class' => 'all-modal-add-proj-input',
                        'placeholder' => $profileCase->title,
                    ]);
                ?>
                <!--<input type="text" class="all-modal-add-proj-input" placeholder="Продажа елок в социальных сетях">-->
                <h4 class="project-modal-add-info">Описание проекта <span class="gray-amend">(осталось: 143 символа)</span></h4>
                <!--<textarea name="" id="" cols="30" rows="10" class="textarea-modal-add-min"></textarea>-->
                <!--<style type="text/css">
                    .textarea-modal-add-min {
                        width: 100%;
                        height: 115px;
                        border: 1px solid #394869;
                        padding: 5px 10px;
                    }
                </style>-->
                <?php
                echo $form->field($profileCase, 'description')
                    ->textarea([
                        'rows' => 10,
                        'cols' => 30,
                        'class' => 'textarea-modal-add-min',
                    ]);
                ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 right-block-form-modal">
                <h4 class="project-modal-add-info">Превью <span class="gray-amend">(Max: 3mb)</span></h4>
                <div class="img-added-modal-block">
                    <?php
                        if ($profileCase->preview_image) {
                            $src = User::UPLOAD_WEB . DIRECTORY_SEPARATOR . $user->upload_dir . DIRECTORY_SEPARATOR . $profileCase->preview_image;
                            ?>
                    <img src="<?= $src; ?>" style="width: 342px; height: 120px; display: inline-block; padding-left:40px;" id="profile_case_preview_image" />
                    <i class="glyphicon glyphicon-remove float-right right20 fDel" id="fDel_<?= $profileCase->id; ?>"></i>
                            <?php
                        }
                    ?>

                </div>
                <?php
                    if ($profileCase->id) {
                        $profileCase->modelId = $profileCase->id;
                        echo $form->field($profileCase, 'modelId')->hiddenInput();
                    }
                ?>
                <div class="input-file-block">
                    <span class="btn btn-default btn-file">
                        <p class="text-input-file">загрузите изображение</p>
                        <?= FileUpload::widget([
                            'model' => $profileCase,
                            'attribute' => 'preview_image',
                            'url' => ['/profile/upload-case-preview-image'],
                            'useDefaultButton' => false,
                            'clientOptions' => [
                                'maxFileSize' => 2000000,
                                'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                            ],
                        ]); ?>
                        <?php
                        $this->registerJs("
                            collection.get('action').set('formName', '".$profileCase->formName()."');
                            collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                            collection.get('action').set('deleteUrl', '/profile/preview-image-delete/');
                            collection.get('action').set('imageFormName', '".$profileCase->formName()."');
                        ", View::POS_BEGIN);
                        ?>
                    </span>
                </div>
                <!--<div class="input-file-block">
                            <span class="btn btn-default btn-file">
                                <p class="text-input-file">загрузить изображение</p>
                                <input type="file">
                            </span>
                </div>-->
                <div class="modal-scrollbar-block">
                    <span class="some-scroll-background"></span>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br />
        <div class="col-md-12 col-sm-12 col-xs-12 right-block-form-modal">
            <h4 class="project-modal-add-info">Презентация</h4>
            <?php
            echo $form->field($profileCase, 'presentation')->widget(Widget::className(), [
                'settings' => [
                    'lang' => 'ru',
                    'minHeight' => 400,
                    'imageManagerJson' => Url::to(['/profile/images-get']),
                    'imageUpload' => Url::to(['/profile/image-upload']),
                    'plugins' => [
                        'fullscreen',
                        'imagemanager',
                        'table',
                    ]
                ]
            ]);
            ?>
            <div class="modal-footer-button-block">
                <button class="buttonFilters">Опубликовать </button>
            </div>
            <div class="block-text-count-textarea">
                <p class="limited-textarea-sim-top">Лимит на количество символов: <span>3500</span></p>
                <p class="limited-textarea-sim-bot">Осталось: <span>3320</span></p>
            </div>

        </div>
        <?php
        echo $form->field($profileCase, 'user_id')->hiddenInput();;
        ActiveForm::end();
        ?>
    </div>
</div>
<!--<div class="row">
    <div class="news-wrap news-wrap_news-open col-md-12">
        <div class="photo-news div-inline">
            <p class="photo-bold">LeadLance</p>
            <p class="photo-light">(no photo)</p>
            <img src="/img/news/news-title.png" alt="">
        </div>
        <div class="text-wrap-news div-inline">
            <h3 class="title-news">Наш сайт начинает работу <span class="title-news__date">13:36 | 20.08.2019</span></h3>
            <p class="text-news">
                В этом году системе лиг КВН Москвы и Подмосковья исполняется 10 лет, и мы подумали: а почему у нас нет до сих пор своего официального сайта? Ну что ж, встречайте! Перед вами официальный сайт всего КВН Москвы и Подмосковья. Чем он отличается от остальных? В целом наполнением и оформлением. Можно сказать, что это не просто сайт, а глянцевый интернет-журнал, где мы будем публиковать множество материалов о командах веселых и находчивых и об их выступлениях.
            </p>
            <p class="text-news">
                Приятного прочтения! И спасибо всем, кто помогал нам сделать из сайта то, чем он стал в данный момент.
            </p>
            <p class="text-news">
                Ps. А еще мы объявляем перепись КВН Московского региона!
                Если вы представляете городские, вузовские или школьные лиги Москвы или Московской области, присылайте на info@moskvn.ru информацию о вашей лиге, и мы разместим ее в разделе «Лиги». Формат информации можно посмотреть здесь.
            </p>
            <a href="#" class="back-to-all-news">
                <img src="/img/news/arrow-back.png" alt="" class="back-to-all-news__img div-inline">
                <span class="back-to-all-news__text div-inline">Вернуться ко всем статьям</span>
            </a>
            <div class="footer-news">
                <img src="/img/news/author-logo.png" alt="" class="footer-news__logo-author">
                <a href="#" class="footer-news__link-author">Администрация сайта</a>
                <div class="footer-news__tags">
                    <p class="div-inline tags-news">Теги:<a href="#" class="tags-news__link tags-news__link_red">новости</a>,<a href="#" class="tags-news__link tags-news__link_blue">важное</a></p>
                    <div class="counter-mess div-inline">
                        <img src="/img/message/messag.png" alt="">
                        <span>24</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<form class="write-comment">
    <div class="row">
        <div class="col-md-12 wrap-textarea">
            <label for="comment-area" class="wrap-textarea__title">Оставить комментарий</label>
            <textarea name="comment-text" class="form-comment-area" id="comment-area" placeholder="Текст сообщения"></textarea>
            <button class="buttonFilters">Отправить сообщение</button>
        </div>
    </div>
</form>
</div>
<div class="container blockPAddBorder disput-review">
    <div class="row">
        <div class="col-md-12">
            <div class="disput news-comment">
                <div class="pers-avatar div-inline">

                </div>
                <div class="div-inline comment ">
                    <div class="agree-order_name div-inline">
                        <p>
                            <span class="name"><a href="#" class="hover-text-decoration">Андрей Сидоров</a> </span>
                            <span class="nickname">(nickname)</span>
                            <img src="/img/index/person-status.jpg" alt="" class="person-status">
                            <img src="/img/index/person-test.jpg" alt="" class="person-test">
                            <span class="date">(19:36 | 21.08.2019)</span>
                        </p>
                    </div>
                    <p class="comment-text">
                        Некоторые нечистоплотные личности заводят пачку аккаунтов, чтобы блокировать неугодные им тема
                        в сообществе "Черный Список". Поскольку администрация множественные регистрации не отслеживает,
                        предлагаю дать возможность устанавливать порог кармы для блокировки создателям сообществ.
                    </p>
                    <a href="#" class="answer-comment">Ответить</a>
                </div>
            </div>
        </div>
        <div class="col-md-11 col-sm-11 col-xs-11 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
            <div class="disput news-comment-answer">
                <div class="pers-avatar div-inline">

                </div>
                <div class="div-inline comment ">
                    <div class="agree-order_name div-inline">
                        <p>
                            <span class="name"><a href="#" class="hover-text-decoration">Андрей Сидоров</a> </span>
                            <span class="nickname">(nickname)</span>
                            <img src="/img/index/person-status.jpg" alt="" class="person-status">
                            <img src="/img/index/person-test.jpg" alt="" class="person-test">
                            <span class="date">(19:36 | 21.08.2019)</span>
                        </p>
                    </div>
                    <p class="comment-text">
                        Некоторые нечистоплотные личности заводят пачку аккаунтов, чтобы блокировать неугодные им тема
                        в сообществе "Черный Список". Поскольку администрация множественные регистрации не отслеживает,
                        предлагаю дать возможность устанавливать порог кармы для блокировки создателям сообществ.
                    </p>
                    <a href="#" class="answer-comment">Ответить</a>
                </div>
            </div>
        </div>
        <div class="col-md-11 col-sm-11 col-xs-11 col-md-offset-1 col-sm-offset-1 col-xs-offset-1">
            <div class="disput news-comment-answer">
                <div class="pers-avatar div-inline">

                </div>
                <div class="div-inline comment ">
                    <div class="agree-order_name div-inline">
                        <p>
                            <span class="name"><a href="#" class="hover-text-decoration">Андрей Сидоров</a> </span>
                            <span class="nickname">(nickname)</span>
                            <img src="/img/index/person-status.jpg" alt="" class="person-status">
                            <img src="/img/index/person-test.jpg" alt="" class="person-test">
                            <span class="date">(19:36 | 21.08.2019)</span>
                        </p>
                    </div>
                    <p class="comment-text">
                        Некоторые нечистоплотные личности заводят пачку аккаунтов, чтобы блокировать неугодные им тема
                        в сообществе "Черный Список". Поскольку администрация множественные регистрации не отслеживает,
                        предлагаю дать возможность устанавливать порог кармы для блокировки создателям сообществ.
                    </p>
                    <a href="#" class="answer-comment">Ответить</a>
                </div>
            </div>
        </div>
    </div>
-->
