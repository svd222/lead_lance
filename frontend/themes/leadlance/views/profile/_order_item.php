<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 27.05.17
 * @time: 15:39
 */

use common\models\Order;
use yii\helpers\Html;
use common\models\User;
use yii\helpers\Url;
use common\models\ProjectMessage;

/**
 * @var Order $model
 * @var mixed $key
 * @var int $index
 * @var \yii\widgets\ListView $widget
 * @var \yii\web\View $this
 */

$isOrderByMyProject = $model->user_id == Yii::$app->user->id;
$meIsExecutor = User::isExecutor(Yii::$app->user->id);
$meIsPonetntialExecutor = false;
if (!$isOrderByMyProject && ($model->executor_id == Yii::$app->user->id) && !$model->acceptance_time && $meIsExecutor) {
    $meIsPonetntialExecutor = true;
}
$executorFullName = '';
if ($model->executor_id) {
    $executor = $model->executor;
    /**
     * @var \common\models\ProfilePrivate $executorProfilePrivate
     */
    $executorProfilePrivate = $executor->profilePrivate;
    $executorFullName = $executorProfilePrivate->fullName;
    $executorHasPro = User::hasPro($model->executor_id);
}
?>
<?php
    $orderNewActionNotifications = $model->getNewNotifications()->count();
    $orderMessages = $model->orderMessages;
    $orderNewMessageNotifications = 0;
    if (!empty($orderMessages)) {
        foreach ($orderMessages as $orderMessage) {
            /**
             * @var ProjectMessage $orderMessage
             */
            if (!empty($orderMessage->newNotifications)) {
                $orderNewMessageNotifications += count($orderMessage->newNotifications);
            }
        }
    }
?>
<div class="project-tab-rout">
    <div class="accordionProjectFullBlock author">
        <div class="titlesAccordionBlock open clearfix">
            <div class="col-md-8 paddingNone">
                <h3 class="titlesAccordion div-inline"><a href="<?= Url::to(['/order/' . $model->id]); ?>"><?= Html::encode($model->title); ?></a></h3>
                <span class="iconProj_1 iconStyleSize"></span>
            </div>
            <div class="col-md-4 paddingNone">
                <ul class="crambsTitleAccord">
                    <?php
                        /**
                         * @var \common\models\Project $project
                         */
                        $project = $model->project;
                        $showDelimiter = false;
                        if (!$project->is_auction) {
                            if ($model->lead_count_required || !$model->is_urgent) {
                                $showDelimiter = true;
                            }
                            ?>
                            <li>
                                <a href="#"><?= $model->lead_cost ?> руб. </a><span><?= $showDelimiter ? ' |' : ''; ?></span>
                            </li>
                            <?php
                        }
                    ?>
                    <?php
                        if ($model->lead_count_required) {
                            $showDelimiter = false;
                            if (!$model->is_urgent) {
                                $showDelimiter = true;
                            }
                    ?>
                    <li>
                        <a href="#"><?= $model->lead_count_required; ?> лид. </a><span><?= $showDelimiter ? ' |' : ''; ?></span>
                    </li>
                    <?php } ?>
                    <?php if (!$model->is_urgent) { ?>
                    <li>
                        <a href="#"><?= $model->execution_time; ?> дня</a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="accordionContent open clearfix content-accordion-tabs-text">
            <p class="textContentAccord">
                <?= $model->description; ?>
            </p>
        </div>
        <div class="block-your-info-proj">
            <?php
                if ($meIsPonetntialExecutor) {
                    ?>
                    <div class="col-md-9 col-sm-9 col-xs-12 paddingNone">
                        <h4 class="your-info-proj">
                            <a href="<?= Url::to(['/order/accept/', 'orderId' => $model->id, 'executorId' => $model->executor_id]); ?>" class="hover-text-decoration">Вас выбрали иcполнителем
                                подтвердите заказ для начала работы</a>
                        </h4>
                    </div>
                    <?php
                }
            ?>
            <div
                class="col-md-3 col-sm-3 col-xs-12 paddingNone text-right media-text-centr mobile-text-left pull-right">
                <button class="button-go-to-order" data-id="<?= $model->id; ?>">Перейти в заказ</button>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="block-your-info-proj block-your-info-proj-big-padding">
            <?php if ($model->executor_id): ?>
                <div class="col-md-11 col-sm-11 col-xs-9 paddingNone">
                    <span class="gray-amend">Исполнитель:</span>
                    <?= $this->render(
                        '/profile/_nickname_info',
                        [
                            'user'  => $executor,
                            'class' => 'nameUser min-user-nick',
                        ]
                    ); ?>
                </div>
            <?php endif; ?>
            <div class="col-md-1 col-sm-1 col-xs-3 paddingNone text-right pull-right">
                <?php if (!empty($orderNewMessageNotifications)): ?>
                    <span class="blue-number">
                        <?= $orderNewMessageNotifications; ?>
                    </span>
                <?php endif; ?>
                <?php if (!empty($orderNewActionNotifications)): ?>
                    <span class="red-number">
                        <?= $orderNewActionNotifications; ?>
                    </span>
                <?php endif; ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
