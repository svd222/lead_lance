<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 28.01.17
 * @time: 16:13
 */

use frontend\assets\AppAsset;
use yii\helpers\Url;
use common\models\ProfileCase;
use yii\widgets\ListView;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use common\models\ProfilePhone;
use libphonenumber\PhoneNumberFormat;
use yii\web\View;
use common\models\Message;
use common\models\User;
use common\models\Project;
use common\models\Order;
use common\models\GuideProjectStatus;
use common\models\GuideOrderStatus;
use common\helpers\DateDiffHelper;
use common\models\Country;
use conquer\select2\Select2Widget;
use yii\helpers\ArrayHelper;
use common\models\ChangePasswordForm;
use dosamigos\fileupload\FileUpload;
use common\models\UserAvatar;
use common\helpers\OrderHelper;

AppAsset::register($this);

/**
 * @var int $uid id пользователя владельца профиля
 */

/**
 * @var \common\models\User $user
 */


/**
 * @var \common\models\Profile $profile
 */

/**
 * @var View $this
 */

/**
 * @var ChangePasswordForm $changePasswordForm
 */

/**
 * @var UserAvatar $userAvatar
 */

/**
 * @var \common\models\ProfilePrivate $profilePrivate
 */
$profilePrivate = $profile->profilePrivate;

$get = Yii::$app->request->get();
$profileOwnerId = ArrayHelper::keyExists('id', $get) ? intval($get['id']) : Yii::$app->user->id;

$this->registerJs("
    var action = collection.get('controller').get('action');
    action.set('profileOwnerId', ".$profileOwnerId.");
");

/**
 * @var UserAvatar $userAvatar
 */
$userAvatar = isset($this->params['userAvatar']) ? $this->params['userAvatar'] : new UserAvatar();
$uid = (int)$uid;
$isMyProfile = ($uid == Yii::$app->user->id);
//below are lists of allowed permissions
$canSendPersonalMessage = false;
$canOfferOrder = false;
$canSeePersonalInfo = false;

$userHasPro = User::hasPro($uid);

$canSendPersonalMessage = (!$isMyProfile && (Yii::$app->user->can('SendPrivateMessage') || Message::find()->meToOrToMeFrom($uid, Yii::$app->user->id)->count()));
$isCustomer = $isExecutor = false;
$canOfferOrder = (!$isMyProfile && User::isCustomer(Yii::$app->user->id) && User::isExecutor($uid) && Yii::$app->user->can('OfferOrder'));
$canSeePersonalInfo = $isMyProfile || (!$isMyProfile && Yii::$app->user->can('SeePersonalInfo'));
?>
<div class="profile-all-block">
    <div class="col-md-7 col-sm-12 col-xs-6 media-logo-all-block paddingNone">
        <div class="col-md-3 col-sm-3 col-xs-12 logo-media-tablet paddingNone">
            <div class="logo-profile">
                <?php
                    if ($userAvatar->image) {
                        ?>
                        <a href="#">
                            <img src="<?= UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $userAvatar->image; ?>" style="width:90px; height:90px" class="avatar-photo2"/>
                        </a>
                        <?php
                    } else {
                        ?>
                        <a href="#">
                            <img src="/themes/leadlance/img/profile/profile-photo.png" alt=""  style="width:90px; height:90px" class="avatar-photo2">
                        </a>
                        <?php
                    }
                ?>
            </div>
            <?php if ($canSeePersonalInfo) { ?>
            <div class="social-logo-block">
                <?php if (!empty($profilePrivate->vk)) { ?>
                <div class="style-logo-social-all div-inline">
                    <a href="#"><i class="fa fa-vk" aria-hidden="true"></i></a>
                </div>
                <?php } ?>
                <?php if (!empty($profilePrivate->twitter)) { ?>
                <div class="style-logo-social-all div-inline">
                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                </div>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-12 paddingNone profile-info-user">
            <h2 class="nameUser">
                <?php if ($canSeePersonalInfo): ?>
                    <?= $this->render(
                        '/profile/_nickname_info',
                        [
                            'user' => $user,
                        ]
                    ); ?>
                <?php endif; ?>
            </h2>
            <ul class="personal-status">
                <li>На сайте:
                    <span class="status-days"><?= DateDiffHelper::getHumanReadableDiff($user->created_at); ?></span>
                    <span class="status-on status-all-off-on">Online</span>
                    <!--<span class="status-off status-all-off-on">Off. <span class="off-timer-out">Заходил 3 минуты назад.</span></span>-->
                </li>
                <li>
Возраст:
                    <span class="status-age status-days"><?= DateDiffHelper::getHumanReadableDiffInt($profilePrivate->birth_date); ?></span>
                    <span class="status-experience nickName">(опыт: <?= $profilePrivate->experience; ?> лет)</span>
                </li>
            </ul>
            <?php
                if ($canSendPersonalMessage) {
                    ?>
                    <button class="all-button-style button-send-message" data-to-id="<?= $uid; ?>">Отправить сообщение</button>
                    <?php
                }
                if ($canOfferOrder) {
                    ?>
                    <button class="all-button-style button-offer-order button-order-redir" data-to-id="<?= $uid; ?>">Предложить заказ</button>
                    <?php
                }
            ?>
        </div>
    </div>
    <div class="col-md-5 col-sm-12 col-xs-6 paddingNone info-right-all pro-acc-bying">
        <div class="col-md-6 border-rev-info paddingNone">
            <p class="info-rating">
                <span class="reit-first"></span>
                <span class="reit-second"></span>
                <span class="reit-third"></span>
Рейтинг:
                <span class="info-rating-count"><a href="#" class="hover-text-decoration">8267</a></span>
            </p>
        </div>
        <div class="col-md-6 border-rev-info paddingNone">
            <?= $this->render(
                '/profile/_reviews',
                [
                    'user' => $user,
                ]
            ); ?>
        </div>
        <div class="clearfix"></div>
        <?php
        if ($canSeePersonalInfo) {
            ?>
            <ul class="user-info-com">
                <?php
                    if ($profilePrivate->city_id) {
                        ?>
                        <li><img src="/themes/leadlance/img/profile/forma-1.png" alt=""><a href="#"
                                                                                           class="hover-text-decoration"><?= $profilePrivate->city->region->country->name; ?>,
                                г. <?= $profilePrivate->city->name; ?></a></li>
                        <?php
                    }
                ?>
                <?php
                if ($profilePrivate->phone) {
                    ?>
                    <li><img src="/themes/leadlance/img/profile/tel-ico.png" alt="phone"><a href="#"
                                                                                            class="hover-text-decoration">
                            <?php
                            echo ProfilePhone::format($profilePrivate->phone, PhoneNumberFormat::E164);
                            ?>
                        </a></li>
                    <?php
                }
                ?>
                <?php
                if ($profilePrivate->email) {
                    ?>
                    <li><img src="/themes/leadlance/img/profile/message-ico.png" alt="email"><a href="#"
                                                                                                class="hover-text-decoration">
                            <?php
                            echo $profilePrivate->email;
                            ?>
                        </a></li>
                    <?php
                }
                ?>
                <?php
                if ($profilePrivate->skype) {
                    ?>
                    <li><img src="/themes/leadlance/img/profile/skype-ico.png" alt="skype"><a href="#"
                                                                                              class="hover-text-decoration">
                            <?php
                            echo $profilePrivate->skype;
                            ?>
                        </a></li>
                    <?php
                }
                ?>
                <?php
                if ($profilePrivate->viber) {
                    ?>
                    <li><img src="/themes/leadlance/img/profile/viber-ico.png" alt="viber" width="12px" height="12px"><a
                            href="#" class="hover-text-decoration">
                            <?php
                            echo $profilePrivate->viber;
                            ?>
                        </a></li>
                    <?php
                }
                ?>
                <?php
                if ($profilePrivate->whatsapp) {
                    ?>
                    <li><img src="/themes/leadlance/img/profile/whatsapp-ico.png" alt="whatsapp" width="12px"
                             height="12px"><a href="#" class="hover-text-decoration">
                            <?php
                            echo $profilePrivate->whatsapp;
                            ?>
                        </a></li>
                    <?php
                }
                ?>
                <?php
                if ($profilePrivate->vk) {
                    ?>
                    <li><img src="/themes/leadlance/img/profile/vk-ico.png" alt="vk" width="12px"
                             height="12px"><a href="#" class="hover-text-decoration">
                            <?php
                            echo $profilePrivate->vk;
                            ?>
                        </a></li>
                    <?php
                }
                ?>
                <?php
                if ($profilePrivate->twitter) {
                    ?>
                    <li><img src="/themes/leadlance/img/profile/twitter-ico.png" alt="twitter" width="12px"
                             height="12px"><a href="#" class="hover-text-decoration">
                            <?php
                            echo $profilePrivate->twitter;
                            ?>
                        </a></li>
                    <?php
                }
                ?>
                <!--<li><img src="/themes/leadlance/img/profile/tel-ico.png" alt=""><a href="#" class="hover-text-decoration">+7 (920) 139-24-90</a></li>-->
                <!--<li><img src="/themes/leadlance/img/profile/message-ico.png" alt=""><a href="#" class="hover-text-decoration">namesdlinadresemail@yandex.ru</a></li>-->
                <!--<li><img src="/themes/leadlance/img/profile/skype-ico.png" alt=""><a href="#" class="hover-text-decoration">adresskypename</a></li>-->
            </ul>
            <?php
        } else {
            if (!$isMyProfile && !$userHasPro) {
                ?>
                <div class="block-get-pro-acc">
                    <p class="text-added-pro-acc">Для просмотра контактной информации купите <a href="#">аккаунт про</a>
                    </p>
                    <button class="button-added-pro-acc all-button-style">
                        Купить PRO-аккаунт
                    </button>
                </div>
                <?php
            } elseif ($isMyProfile && !$userHasPro) {
                ?>
                <div class="block-get-pro-acc">
                    <button class="button-added-pro-acc all-button-style">
                        Купить PRO-аккаунт
                    </button>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="block-tabs-all">
        <div class="tabs-nav">
            <ul class="tabs-nav-list">
                <li><!-- class="active-link-tabs" -->
                    <!--<span class="test-icon-tabs"></span>-->
                    <img src="/themes/leadlance/img/profile/icon-tabs-1.png" class="nav-tabs-img" alt="">
                    <img src="/themes/leadlance/img/profile/icon-tabs-1-active.png" class="nav-tabs-img active-img-tabs" alt="">
Кейсы
                </li>
                <?php
                if ($isMyProfile) {
                    ?>
                    <li>
                        <img src="/themes/leadlance/img/profile/icon-tabs-2.png" class="nav-tabs-img marg-bot-img-nav"
                             alt="">
                        <img src="/themes/leadlance/img/profile/icon-tabs-2-active.png"
                             class="nav-tabs-img marg-bot-img-nav active-img-tabs" alt="">
                        Мои проекты
                        <?php if (!empty($this->params['projectsNewMessageNotifications'])): ?>
                            <span class="blue-number">
                                <?= $this->params['projectsNewMessageNotifications']; ?>
                            </span>
                        <?php endif; ?>
                    </li>
                    <li class="profile-menu-orders">
                        <img src="/themes/leadlance/img/profile/icon-tabs-3.png" class="nav-tabs-img" alt="">
                        <img src="/themes/leadlance/img/profile/icon-tabs-3-active.png"
                             class="nav-tabs-img active-img-tabs " alt="">
                        Мои заказы
                        <?php if (!empty($this->params['ordersNewActionNotifications'])): ?>
                            <span class="red-number">
                                <?= $this->params['ordersNewActionNotifications']; ?>
                            </span>
                        <?php endif; ?>
                        <?php if (!empty($this->params['ordersNewMessageNotifications'])): ?>
                            <span class="blue-number">
                                <?= $this->params['ordersNewMessageNotifications']; ?>
                            </span>
                        <?php endif; ?>
                    </li>
                    <?php
                }
                ?>
                <li>
                    <img src="/themes/leadlance/img/profile/icon-tabs-4.png" class="nav-tabs-img" alt="">
                    <img src="/themes/leadlance/img/profile/icon-tabs-4-active.png" class="nav-tabs-img active-img-tabs" alt="">
Отзывы
                </li>
                <?php
                if ($isMyProfile) {
                    ?>
                <li class="profile-menu-account">
                    <img src="/themes/leadlance/img/profile/icon-tabs-5.png" class="nav-tabs-img" alt="">
                    <img src="/themes/leadlance/img/profile/icon-tabs-5-active.png" class="nav-tabs-img active-img-tabs" alt="">
Счет
                </li>
                <li>
                    <img src="/themes/leadlance/img/profile/icon-tabs-6.png" class="nav-tabs-img" alt="">
                    <img src="/themes/leadlance/img/profile/icon-tabs-6-active.png" class="nav-tabs-img active-img-tabs" alt="">
Услуги
                </li>
                <li>
                    <img src="/themes/leadlance/img/profile/icon-tabs-8.png" class="nav-tabs-img marg-bot-img-nav" alt="">
                    <img src="/themes/leadlance/img/profile/icon-tabs-8-active.png" class="nav-tabs-img marg-bot-img-nav active-img-tabs" alt="">
Настройки
                </li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div class="tabs-content-all">
            <div class="tabs-content" data-datasource="<?= Url::to(['/profile/cases']); ?>" data-datacontext="cases" id="tabs-content-cases">
                <div class="skins-all-block">
                    <div class="basic-information-all">
                        <h3 class="title-basic-inf">Основная информация</h3>
                        <div class="block-management">
                            <a href="#" class="link-redact">редактировать</a>
                        </div>
                        <p class="text-basic-inf">Дизайн, если заниматься им правильно, –
                            это не статья расхода в бюджете. Дизайн – это инвестиция в
                            инфраструктуру, он обеспечивает бесперебойную работу предприятия.
Хороший дизайн означает более эффективный товар или услугу.
Дизайн означает прибыль! () Майк Монтейро
</p>
                    </div>
                    <div class="basic-information-all">
                        <h3 class="title-basic-inf">Кейсы <span class="gray-amend">(выполненые проекты)</span></h3>
                        <div class="block-management">
                            <a class="link-redact" href="<?= Url::to('/profile/create-case'); ?>">добавить кейс</a>
                        </div>
                        <?php
                            $searchParams = [
                                'id' => $uid
                            ];
                            echo ListView::widget([
                                'dataProvider' => ProfileCase::search($searchParams),
                                'itemView' => '_profile_case_item',
                                'itemOptions' => [
                                    'tag' => false,
                                ],
                                'summary' => false,
                                'viewParams' => [
                                    'user' => User::getActiveUser($uid),
                                ],
                            ]);
                        ?>
                        <!--<div class="content-project-all clearfix">
                            <div class="col-md-4 paddingNone">
                                <div class="img-our-project">
                                    <h3 class="text-img-proj">IMG</h3>
                                </div>
                            </div>

                            <div class="col-md-8 paddingNone opis-project-style-all">
                                <h3 class="title-addeds-project">
                                    <a href="#" class="project-link-width hover-text-decoration">Продажа елок в социальных сетях</a>
                                    <span class="redact-and-delet-all">
                                        <span class="delet-link-proj-inner">
                                            <a href="#">уд.</a>
                                        </span>
                                        <span class="red-link-proj-inner">
                                            <a href="#">ред.</a>
                                        </span>
                                    </span>
                                </h3>
                                <p class="some-project-text">
Предновогодняя пора - это время акций, скидок и высоких ставок на рекламу.
Опытные таргетологи рекомендуют в это время "придержать" бюджет. Но если ваш
                                    товар - новогодние атрибуты, надо искать максимально заинтересованные
                                    аудитории и тестировать разные форматы таргетированной рекламы.
                                </p>
                            </div>
                        </div>
                        <div class="content-project-all clearfix">
                            <div class="col-md-4 paddingNone">
                                <div class="img-our-project">
                                    <h3 class="text-img-proj">IMG</h3>
                                </div>
                            </div>
                            <div class="col-md-8 paddingNone opis-project-style-all">
                                <h3 class="title-addeds-project">
                                    <a href="#" class="project-link-width hover-text-decoration">Продажа елок в социальных сетях</a>
                                    <span class="redact-and-delet-all">
                                        <span class="delet-link-proj-inner">
                                            <a href="#">уд.</a>
                                        </span>
                                        <span class="red-link-proj-inner">
                                            <a href="#">ред.</a>
                                        </span>
                                    </span>
                                </h3>
                                <p class="some-project-text">
Предновогодняя пора - это время акций, скидок и высоких ставок на рекламу.
Опытные таргетологи рекомендуют в это время "придержать" бюджет. Но если ваш
                                    товар - новогодние атрибуты, надо искать максимально заинтересованные
                                    аудитории и тестировать разные форматы таргетированной рекламы.
                                </p>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
            <?php
            if ($isMyProfile) {
                ?>
                <div class="tabs-content" id="tabs-content-projects">
                    <div class="my-project-all-style">
                        <div class="col-md-12 col-xs-12 paddingNone">
                            <a href="<?= Url::to(['/profile/index/', '#' => 'my_projects']) ?>"
                               class="distribLink-left activeDistribLink">
                                Все
                            </a>
                            <a href="<?= Url::to(['/profile/index/', GuideProjectStatus::getFilterName() => [GuideProjectStatus::STATUS_NEW, GuideProjectStatus::STATUS_PUBLISHED], '#' => 'my_projects']) ?>"
                               class="distribLink-left">
                                Активные
                            </a>
                            <a href="<?= Url::to(['/profile/index/', GuideProjectStatus::getFilterName() => [GuideProjectStatus::STATUS_UNPUBLISHED, GuideProjectStatus::STATUS_ACCOMPLISHED], '#' => 'my_projects']) ?>"
                               class="distribLink-left">
                                Закрытые
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        echo ListView::widget([
                            'dataProvider' => Project::filterSearch(),
                            'itemView' => '/project/_item',
                            'viewParams' => [
                                'profileView' => true,
                            ],
                            'itemOptions' => [
                                'tag' => false,
                            ],
                            'summary' => false,
                        ]);
                        ?>
                    </div>
                </div>
                <div class="tabs-content" id="tabs-content-orders">
                    <div class="my-project-all-style">
                        <div class="col-md-12 col-xs-12 paddingNone">
                            <a href="<?= Url::to(['/profile/index/', '#' => 'my_orders']) ?>" class="distribLink-left activeDistribLink">Все</a>
                            <a href="<?= Url::to(['/profile/index/', OrderHelper::getFilterName() => 'new', '#' => 'my_orders']) ?>" class="distribLink-left">Активные</a>
                            <a href="<?= Url::to(['/profile/index/', OrderHelper::getFilterName() => 'disput', '#' => 'my_orders']) ?>" class="distribLink-left">В споре</a>
                            <a href="<?= Url::to(['/profile/index/', OrderHelper::getFilterName() => 'closed', '#' => 'my_orders']) ?>" class="distribLink-left">Закрытые</a>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        echo ListView::widget([
                            'dataProvider' => Order::search(),
                            'itemView' => '_order_item',
                            'itemOptions' => [
                            'tag' => false,
                            ],
                            'summary' => false,
                        ]);
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="tabs-content" id="tabs-content-testimonials">
                <div class="tabs-reviews-all">
                    <div class="tabs-rev-content">
                        <ul class="nav-reviews">
                            <li class="positively-rev">
                                <a href="javascript:void(0);">Положительные (<span class="count-rev-true">0</span>)</a>
                            </li>
                            <li class="negative-rev">
                                <a href="javascript:void(0);">Отрицательные (<span class="count-rev-fal">0</span>)</a>
                            </li>
                        </ul>
                        <ul class="list-reviews-tabs">

                        </ul>
                    </div>
                </div>
            </div>
            <?php
            if ($isMyProfile) {
                ?>
                <div class="tabs-content" id="tabs-content-balance">
                    <div class="history-operation-all">
                        <div class="col-md-7 col-sm-8 col-xs-12 paddingNone">
                            <h4 class="title-history">История операций</h4>
                            <ul class="list-history-operation">
                                <li>
                                    <span class="date-history">21.11</span> —
                                    <span class="enrollment-position">Зачисление на ваш счет </span>
                                    <span class="summ-enrollment">+3.560р</span>
                                </li>
                                <li>
                                    <span class="date-history">03.11</span> —
                                    <span class="write-off-position">Списание с вашего счета </span>
                                    <span class="summ-write-off">-24.500р</span>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-5 col-sm-4 col-xs-12 paddingNone padding-balans-block">
                            <div class="block-status-money">
                                <p class="balans">Баланс</p>
                                <p class="balans-number">3.560 р</p>
                            </div>
                            <form action="" class="conclude-form">
                                <div class="col-md-6 paddingNone padd-right media-balans-button-style">
                                    <input type="submit" class="conclude-input-all conclude-input-this"
                                           value="Вывести средства">
                                </div>
                                <div class="col-md-6 paddingNone padd-left media-balans-button-style">
                                    <input type="submit" class="conclude-input-all entry-input-this"
                                           value="Пополнить баланс">
                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="tabs-content" id="tabs-content-services">
                    <div class="services-all">
                        <div class="services-pro clearfix">
                            <h3 class="pro-account">PRO-аккаунт</h3>
                            <div class="col-md-4 paddingNone">
                                <div class="img-pro-block">
                                    <img src="/themes/leadlance/img/profile/pro-acc.png" alt="">
                                </div>
                                <form action="" class="payments-pro">
                                    <button class="payments-1-mounth">
                                        Оплатить 1 месяц ( 149р )
                                    </button>
                                    <button class="payments-3-mounth">
                                        Оплатить 3 месяца ( 299р )
                                        <span class="sales-text-button">скидка 12%</span>
                                    </button>
                                    <button class="payments-1-years">
                                        Оплатить 1 год ( 1149р )
                                        <span class="sales-text-button">скидка 18% </span>
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-8 services-prof-text">
                                <p class="come-serv-prof-text">
                                    Характеристики, преимущества и цена аккаунтов</br>
                                    Начальный аккаунт работодателя – это бесплатный аккаунт,
                                    получаемый после регистрации на сайте в качестве работодателя.
                                    Скидка на все платные услуги сайта.</br>
                                    Отображение ваших контактов всем пользователям.</br>
                                    Просмотр контактных данных всех пользователей</br>
                                    Возможность обмена сообщениями на сайте с любым пользователем на сайте
                                    вне зависимости от наличия\отсутствия аккаунта PRO</br>
                                    Публикация проектов на вкладке "Вакансии" за 799 руб. (для
                                    обладателей начального аккаунта - 1399 рублей).</br>
                                    Возможность выбирать до трех разделов каталога при публикации
                                    проекта/конкурса (в отличие от одного - для пользователей с начальным
                                    аккаунтом).</br>
                                    Стоимость аккаунта PRO для работодателя – 1199 руб. Срок действия
                                    – 1 месяц. При покупке PRO на 3 или 12 месяцев можно сэкономить
                                    (стоимость PRO при приобретении на год составляет 959 руб/мес.).</br>
                                </p>
                                <p class="come-serv-prof-text">
                                    При покупке аккаунта PRO на 1 месяц можно подключить
                                    автоплатеж посредством банковской карты, при этом услуга
                                    будет автоматически оплачиваться раз в месяц.
                                    Отключить автоплатеж можно в любое время.
                                </p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="services-pro clearfix">
                            <h3 class="pro-account verification-color">Верификация</h3>
                            <div class="col-md-4 paddingNone">
                                <div class="img-pro-block">
                                    <img src="/themes/leadlance/img/profile/verification.png" alt="">
                                </div>
                                <form action="" class="payments-pro">
                                    <button class="payments-1-years verification-button">
                                        Пройти верефикацию
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-8 services-prof-text">
                                <p class="come-serv-prof-text">
                                    Статус «Верифицирован» получает Пользователь, связавший
                                    учетную запись на сайте fl.ru с учетной записью в одной из платежных
                                    систем, где хранится подтвержденная информация о его личности.
                                    После верификации рейтинг фрилансера увеличивается на 20%!</br>
                                </p>
                                <p class="come-serv-prof-text">
                                    Важно: для получения статуса «Верифицирован» пользователю не нужно передавать нашему
                                    сайту свои персональные данные.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tabs-content active-tabs" id="tabs-content-settings">
                    <?php
                    //start the Profile private form
                    $form = ActiveForm::begin([
                        'id' => $profilePrivate->formName(),
                        'options' => [
                            'class' => 'modal-add-project-form'
                        ],
                        'action' => '/profile/save-private',
                        'enableAjaxValidation' => true,
                        'enableClientValidation' => false,
                        'validationUrl' => '/profile/save-private',
                        'fieldConfig' => [
                            'template' => '{input}',
                            'options' => [
                                'tag' => false,
                            ],
                        ]
                    ]);
                    ?>
                    <div class="col-md-5 col-sm-12 paddingNone">
                        <h2 class="title-tabs">Основная информация</h2>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Имя</h3>
                            <!--<input type="text" class="input-all-style-group">-->
                            <?php
                            echo $form->field($profilePrivate, 'first_name')->textInput([
                                'class' => 'input-all-style-group',
                            ]);
                            ?>
                        </div>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Фамилия</h3>
                            <!--<input type="text" class="input-all-style-group input-err-valid">-->
                            <?php
                            echo $form->field($profilePrivate, 'last_name')->textInput([
                                'class' => 'input-all-style-group'
                            ]);
                            ?>
                            <!--<p class="input-err-mess">Заполните это<br/>
                                поле пожалуйста.
                            </p>-->
                        </div>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Опыт</h3>
                            <!--<input type="text" class="input-all-style-group">-->
                            <?php
                            echo $form->field($profilePrivate, 'experience')->textInput([
                                'class' => 'input-all-style-group'
                            ]);
                            ?>
                        </div>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Дата рождения</h3>
                            <?php
                            if ($profilePrivate->birthdateDay) {
                                $dOptions = [
                                    'class' => 'input-all-style-group days-input',
                                    'value' => $profilePrivate->birthdateDay
                                ];
                            } else {
                                $dOptions = [
                                    'class' => 'input-all-style-group days-input',
                                ];
                            }
                            echo $form->field($profilePrivate, 'birthdateDay')->textInput($dOptions);
                            if ($profilePrivate->birthdateMonth) {
                                $mOptions = [
                                    'class' => 'input-all-style-group mounth-input',
                                    'value' => $profilePrivate->birthdateMonth,
                                ];
                            } else {
                                $mOptions = [
                                    'class' => 'input-all-style-group mounth-input',
                                ];
                            }
                            echo $form->field($profilePrivate, 'birthdateMonth')->textInput($mOptions);
                            if ($profilePrivate->birthdateYear) {
                                $yOptions = [
                                    'class' => 'input-all-style-group years-input',
                                    'value' => $profilePrivate->birthdateYear
                                ];
                            } else {
                                $yOptions = [
                                    'class' => 'input-all-style-group years-input',
                                ];
                            }
                            echo $form->field($profilePrivate, 'birthdateYear')->textInput($yOptions);
                            ?>
                            <!--<input type="text" class="input-all-style-group days-input" value="16">-->
                            <!--<input type="text" class="input-all-style-group mounth-input" value="август">-->
                            <!--<input type="text" class="input-all-style-group years-input" value="1976">-->
                        </div>
                        <h2 class="title-tabs">Контакты</h2>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Телефон</h3>
                            <!--<input type="text" class="input-all-style-group">-->
                            <?php
                            echo $form->field($profilePrivate, 'phone')->textInput([
                                'class' => 'input-all-style-group',
                            ]);
                            ?>
                            <p class="text-confirm">
                                <!--<a href="#"  data-toggle="modal" data-target="#confirm-telephone" id="change-phone">Подтвердить!</a>-->
                                <?php
                                echo Html::a('Подтвердить', '/profile/validate-phone', [
                                    'id' => 'change-phone',
                                ]);
                                ?>
                            </p>
                        </div>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Skype</h3>
                            <!--<input type="text" class="input-all-style-group">-->
                            <?php
                            echo $form->field($profilePrivate, 'skype')
                                ->textInput([
                                    'class' => 'input-all-style-group'
                                ]);
                            ?>
                        </div>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Viber</h3>
                            <!--<input type="text" class="input-all-style-group">-->
                            <?php
                            echo $form->field($profilePrivate, 'viber')
                                ->textInput([
                                    'class' => 'input-all-style-group'
                                ]);
                            ?>
                        </div>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Whatsapp</h3>
                            <!--<input type="text" class="input-all-style-group">-->
                            <?php
                            echo $form->field($profilePrivate, 'whatsapp')
                                ->textInput([
                                    'class' => 'input-all-style-group'
                                ]);
                            ?>
                        </div>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Vk</h3>
                            <!--<input type="text" class="input-all-style-group">-->
                            <?php
                            echo $form->field($profilePrivate, 'vk')
                                ->textInput([
                                    'class' => 'input-all-style-group'
                                ]);
                            ?>
                        </div>
                        <div class="input-text-group">
                            <h3 class="title-input-all">Twitter</h3>
                            <!--<input type="text" class="input-all-style-group">-->
                            <?php
                            echo $form->field($profilePrivate, 'twitter')
                                ->textInput([
                                    'class' => 'input-all-style-group'
                                ]);
                            ?>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 first-tabs-right">
                        <div class="col-lg-2 col-md-3 col-sm-2 paddingNone">
                            <div class="avatar-load-block">
                                <?php
                                    if ($userAvatar->image) {
                                        ?>
                                            <img src="<?= UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $userAvatar->image; ?>" style="width:90px; height:90px" class="avatar-photo"/>
                                        <?php
                                    } else {
                                        ?>
                                            <img src="/themes/leadlance/img/profile/profile-photo.png" alt="" style="width:90px; height:90px" class="avatar-photo">
                                        <?php
                                    }
                                ?>
                                <!--<img src="/themes/leadlance/img/profile/profile-photo.png" alt="">-->
                            </div>
                        </div>
                        <div class="col-lg-10 col-md-9 col-sm-8 col-xs-12 medeia-non-padding-767">
                            <div class="input-file-block">
                                <span class="btn btn-default btn-file">
                                    <?= FileUpload::widget([
                                        'model' => $userAvatar,
                                        'attribute' => 'image',
                                        'url' => ['/user-avatar/upload'],
                                        'useDefaultButton' => false,
                                        'clientOptions' => [
                                            'maxFileSize' => 2000000,
                                            'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
                                        ],
                                    ]); ?>
                                    <?php
                                    $this->registerJs("
                                        collection.get('action').set('formName', '".$profilePrivate->formName()."');
                                        
                                        collection.get('action').set('csrfParam', '".Yii::$app->request->csrfParam."');
                                        /*collection.get('action').set('deleteUrl', '/image/delete/');*/
                                        collection.get('action').set('imageFormName', '".$userAvatar->formName()."');
                                    ", View::POS_BEGIN);
                                    ?>
                                    <p class="text-input-file">загрузите изображение</br>
                                        или перетащите его сюда</br>
                                        <span class="gray-regular-text">(рекомендуемый размер 90х90)</span>
                                    </p>
                                    <!--<input type="file">-->
                                </span>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-9 paddingNone select-tabs-profile">
                            <div class="input-text-group">
                                <h3 class="title-input-all">Страна</h3>
                                <?php
                                    echo $form->field($profilePrivate, 'country_id')->widget(
                                        Select2Widget::className(),
                                        [
                                            'items'=>ArrayHelper::map(Country::find()->all(), 'id', 'name'),
                                            'options' => [
                                                'class' => 'input-all-style-group',
                                            ]
                                        ]
                                    );
                                ?>
                            </div>
                            <div class="input-text-group select-tabs-profile">
                                <h3 class="title-input-all">Город</h3>
                                <?php
                                    echo $form->field($profilePrivate, 'city_id')->widget(
                                        Select2Widget::className(),
                                        [
                                            'options' => [
                                                'class' => 'input-all-style-group',
                                            ]
                                        ]
                                    );
                                    if ($profilePrivate->city_id) {
                                        $this->registerJs('
                                            var selected = $("<option value=\''. $profilePrivate->city_id .'\' selected=\'selected\'>'. $profilePrivate->city->name .'</option>");
                                            selected.appendTo($("#profileprivate-city_id"));
                                        ', View::POS_READY);
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12 paddingNone">
                        <div class="col-md-5 paddingNone">
                            <h2 class="title-tabs">Смена пароля</h2>
                            <div class="input-text-group">
                                <h3 class="title-input-all">Введите старый пароль</h3>
                                <?php
                                    echo $form->field($changePasswordForm, 'oldPassword')
                                        ->passwordInput([
                                            'class' => 'input-all-style-group',
                                        ]);
                                ?>
                                <!--<input type="password" class="input-all-style-group">-->
                            </div>
                            <div class="input-text-group">
                                <h3 class="title-input-all">Введите новый пароль</h3>
                                <?php
                                echo $form->field($changePasswordForm, 'newPassword')
                                    ->passwordInput([
                                        'class' => 'input-all-style-group',
                                    ]);
                                ?>
                            </div>
                            <div class="input-text-group">
                                <h3 class="title-input-all">Введите новый пароль ещё раз</h3>
                                <?php
                                echo $form->field($changePasswordForm, 'retypePassword')
                                    ->passwordInput([
                                        'class' => 'input-all-style-group',
                                    ]);
                                ?>
                                <!--<input type="password" class="input-all-style-group">-->
                                <p class="text-confirm amend-text-controll">
                                    <a id="change-password" href="<?= Url::to(['/profile/change-password']) ?>">Изменить!</a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-5 marg-style-left paddingNone">
                            <h2 class="title-tabs">Смена e-mail</h2>
                            <div class="input-text-group">
                                <h3 class="title-input-all">Введите новый e-mail</h3>
                                <?php
                                echo $form->field($profilePrivate, 'email')->textInput([
                                    'class' => 'input-all-style-group',
                                ]);
                                ?>
                                <p class="text-confirm">
                                    <a href="<?= Url::to(['/profile/check-email']) ?>" id="check-email">Подтвердить!</a>
                                </p>
                            </div>
                            <!--<div class="input-text-group">
                                <h3 class="title-input-all">Введите полученый код</h3>
                                <input type="text" class="input-all-style-group">
                                <p class="text-confirm gray-amend amend-text-controll">
                                    <a href="#">Изменить!</a>
                                </p>
                            </div>-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="block-profile-submit">
                        <div class="col-md-12 paddingNone">
                            <div class="email-notice bottom-link-style">
                                <a href="#" class="email-notice-link" data-toggle="modal"
                                   data-target="#confirm-setings">Настроить e-mail уведомления </a>
                            </div>
                        </div>
                        <div class="col-md-12 paddingNone">
                            <div class="delete-profile bottom-link-style">
                                <?php
                                $title = 'Удалить аккаунт';
                                $options = [
                                    'title' => $title,
                                    'aria-label' => $title,
                                    //'data-pjax' => '0',
                                    'data-confirm' => Yii::t('app/profile_settings', 'Are you sure you want to delete account?'),
                                    'data-method' => 'post',
                                    'class' => 'email-notice-link gray-amend',
                                    'onclick' => '
                                        $("#' . $profilePrivate->formName() . '").off("beforeSubmit");
                                    ',
                                ];
                                echo Html::a($title, '/profile/delete-profile/' . Yii::$app->user->id, $options);
                                ?>
                                <!--<a href="#" class="email-notice-link gray-amend" data-toggle="modal"
                                   data-target="#podsk-blue">Удалить аккаунт</a>-->
                            </div>
                        </div>
                        <div class="col-md-12 paddingNone">
                            <button class="buttonFilters button-profile-form">Сохранить</button>
                            <!-- data-toggle="modal" data-target="#podsk-pink" -->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    echo $form->field($profilePrivate, 'user_id')->hiddenInput();
                    ActiveForm::end();

                    //@todo: move js code to separate file
                    $this->registerJs('
                        $("#' . $profilePrivate->formName() . '").on("beforeSubmit", function() {
                            var form = $(this);
                            $.ajax({
                                url: form.attr("action"),                    
                                type: "POST",
                                data: form.serialize(),
                                success:function(data, status, jqXHR) {
                                    if (data.length == 0) {
                                        //gritterAdd("' . Yii::t('app', 'Success') . '", "' . Yii::t('app/profile_private', 'Profile info was successful saved') . '", "gritter_success", undefined, {sticky:true});
                                        /*$.gritter.add({
                                            title: "' . Yii::t('app', 'Success') . '", 
                                            text: "' . Yii::t('app/profile_private', 'Profile info was successful saved') . '", 
                                            class_name: "gritter_success",
                                            sticky: true,
                                        });*/
                                        /*$("#added-proj-modal").modal("hide");
                                        var navsTabsContent = $("#tabs-content-cases");
                                        reloadProfileTabContent(navsTabsContent);*/
                                        window.location.reload();
                                    } else {
                                        console.log("' . Yii::t('app', 'Ajax data was sended, but internal error occured') . '");
                                        console.log(status);
                                        console.log(jqXHR);
                                    }
                                },
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.log(jqXHR);
                                    console.log(textStatus);
                                    console.log(errorThrown);
                                }
                            });
                            return false;
                        });
                    ', View::POS_LOAD);
                    ?>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>
