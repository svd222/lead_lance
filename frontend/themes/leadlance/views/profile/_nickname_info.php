<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 02/09/2018
 * Time: 8:31 PM
 */
use yii\helpers\Url;
use common\models\User;

?>
<?php
    if (empty($user)) {
        return false;
    }
?>
<span class="nameUser <?= !empty($class) ? $class : ''; ?>">
    <?php if ($user->profilePrivate->fullName) { ?>
        <span class="name">
            <a href="<?= Url::to(['/profile?id=' . $user->id]) ?>" class="hover-text-decoration">
                <?= $user->profilePrivate->fullName; ?>
            </a>
        </span>
    <?php } ?>
    <span class="nickname nickName">(<?= $user->username; ?>)</span>
    <?php
    if (User::hasPro($user->id)) {
        //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
        ?>
        <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">
        <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">
        <?php
    }
    ?>
</span>
