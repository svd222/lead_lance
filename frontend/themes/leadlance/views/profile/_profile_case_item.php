<?php
/**
 * @var \yii\base\View $this
 * @var \common\models\ProfileCase $model
 * @var mixed $key
 * @var int $index
 * @var ListView $widget
 */

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\User;
use yii\widgets\ListView;

/**
 * @var User $user
 */
$user = $widget->viewParams['user'];

?>
<div class="content-project-all clearfix">
    <div class="col-md-4 paddingNone">
        <?php if ($model->preview_image) {
            $src = User::UPLOAD_WEB . DIRECTORY_SEPARATOR . $user->upload_dir . DIRECTORY_SEPARATOR . $model->preview_image;
        ?>
            <div class="img-our-project">
                <img src="<?= $src; ?>" style="width: 293px; height: 214px;"> <!--width: 315px; height: 150px;-->
            </div>
        <?php } else { ?>
            <div class="photo-news div-inline">
                <p class="photo-bold">LeadLance</p>
                <p class="photo-light">(no photo)</p>
            </div>
        <?php } ?>
    </div>

    <div class="col-md-8 paddingNone opis-project-style-all">
        <h3 class="title-addeds-project">
            <a href="<?= Url::to('case-view/'.$model->id); ?>" class="project-link-width hover-text-decoration"><?= Html::encode($model->title); ?></a>
            <span class="redact-and-delet-all">
                                        <span class="delet-link-proj-inner">
                                            <a href="<?= Url::to('/profile/delete-case/'.$model->id); ?>" aria-label="<?= Yii::t('app/profile_case', 'Delete'); ?>" data-confirm="<?= Yii::t('app/profile_case', 'Are you sure you want to delete this case?'); ?>">уд.</a>
                                        </span>
                                        <span class="red-link-proj-inner">
                                            <a href="<?= Url::to('/profile/create-case/'.$model->id); ?>">ред.</a>
                                        </span>
                                    </span>
        </h3>
        <p class="some-project-text">
            <?= $model->description; ?>
        </p>
    </div>
</div>
