<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 02.02.17
 * @time: 19:10
 */
use common\models\ProfileCase;
//use dosamigos\ckeditor\CKEditor;
//use dosamigos\tinymce\TinyMce;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use vova07\imperavi\Widget;
use yii\helpers\Url;

/**
 * @var \common\models\User $user
 */
$user = Yii::$app->user->identity;

/**
 * @var \common\models\Profile $profile
 */
$profile = $user->profile;

/**
 * @var \common\models\ProfileCase $profileCase
 */
$profileCase = new ProfileCase;
$profileCase->user_id = Yii::$app->user->id;

/**
 * @var \yii\web\View $this
 */
?>
<div class="modal fade project-2-modal" id="confirm-telephone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog create-order-modal" role="document">
        <div class="modal-content">
            <?php
/*                $form = ActiveForm::begin([

                ]);
            */?>
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Подтвердите номер телефона</h4>
            </div>
            <div class="modal-body">
                <input type="text" class="input-all-style-group modal-input-code-telephone" placeholder="Введите код из смс">
                <p class="modal-telephone-return">
                    Не пришло сообщение?<br/>
                    Нажмите <a href="#">сюда</a> для повторной отправки!
                </p>
                <button class="buttonFilters button-create-order">Подтвердить!</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade project-2-modal" id="confirm-telephone-error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog create-order-modal" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Ошибка</h4>
            </div>
            <div class="modal-body">
                <p class="modal-telephone-return" style="color: #f22c40; font-size: 18px; text-align: center;">
                    {error_message}
                </p>
            </div>
        </div>
    </div>
</div>
<div class="modal fade project-2-modal" id="podsk-blue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog create-order-modal" role="document">
        <div class="modal-content">
            <div class="exit-block close" data-dismiss="modal" aria-label="Close">
                <a href="#"></a>
            </div>
            <p class="blue-text-modal">Заполните кейсы, чтобы ваша
                анкета стала привлекательнее
                для рекламодателей</p>
            <button class="modal-color-button">Заполнить</button>
        </div>
    </div>
</div>
<div class="modal fade project-2-modal" id="podsk-pink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog create-order-modal" role="document">
        <div class="modal-content">
            <div class="exit-block close" data-dismiss="modal" aria-label="Close">
                <a href="#"></a>
            </div>
            <p class="blue-text-modal">Чтобы опубликовать проект
                подтвердите ваш номер телефона. </p>
            <button class="modal-color-button">Заполнить</button>
        </div>
    </div>
</div>

<div class="modal fade project-2-modal" id="confirm-setings" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog create-order-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Настроить email уведомления</h4>
            </div>
            <div class="modal-body">
                <div class="wrapper-dropbox-input">
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_3">
                        <label for="check1_3" class="div-inline"></label><span class="div-inline">о смене пароля </span></p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_4">
                        <label for="check1_4" class="div-inline"></label>
                        <span class="div-inline">о изменении профиля</br>
                            (сменили имя, номер телефона, email)  </span>
                    </p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_5">
                        <label for="check1_5" class="div-inline"></label><span class="div-inline">Уведомления об операциях</br>
                            со счетом (любое движение средств)  </span></p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_6">
                        <label for="check1_6" class="div-inline"></label><span class="div-inline">Уведомление о публикации/отклонении</br>
                            проекта модераторами(для заказчика)</span> </p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_7">
                        <label for="check1_7" class="div-inline"></label><span class="div-inline">Уведомление об ответах/сообщениях по проекту</span></p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_8">
                        <label for="check1_8" class="div-inline"></label><span class="div-inline">Уведомление о личном сообщении</span> </p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_9">
                        <label for="check1_9" class="div-inline"></label><span class="div-inline">Уведомление о выборе исполнителей</br>
                            или принятии/отклонения заказа</span></p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_10">
                        <label for="check1_10" class="div-inline"></label><span class="div-inline">Уведомления о сообщениях внутри “заказа” </span></p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_11">
                        <label for="check1_11" class="div-inline"></label><span class="div-inline">Уведомление о выполнении “заказа” исполнителем</br>
                            или отклонении/ принятии “заказа” рекламодателем </span></p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_12">
                        <label for="check1_12" class="div-inline"></label><span class="div-inline">Уведомление об истечении срока по заказу</span> </p>
                    <p class="exceptionText ">
                        <input type="radio" class="checkboxStyle" id="check1_13">
                        <label for="check1_13" class="div-inline"></label><span class="div-inline">ведомления об ответе на тикеты</br>
                            и об ответах в переписке к спору</span> </p>
                    <ul class="checkbox-control col-sm-5">
                        <li><a href="#">снять все галочки</a></li>
                        <li><a href="#">выделить все</a></li>
                    </ul>
                    <a href="#" class="change-filter col-sm-7">сохранить</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
