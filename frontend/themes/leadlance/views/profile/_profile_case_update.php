<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 13.06.17
 * @time: 15:16
 */
use yii\web\View;
use common\models\ProfileCase;
use frontend\assets\ProfileCreateCaseAsset;

/**
 * @var View $this
 * @var ProfileCase $profileCase
 */

ProfileCreateCaseAsset::register($this);
$this->title = Yii::t('app/profile_case', 'Update case');

echo $this->render('_case_form', [
    'profileCase' => $profileCase
]);