<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 02/09/2018
 * Time: 1:58 PM
 */

use yii\helpers\Url;

?>
<?php
    if (empty($user)) {
        return false;
    }
?>
<p class="info-reviews <?= !empty($class) ? $class : ''; ?>">
    <img src="/themes/leadlance/img/profile/ico-mess2.png" class="marg-img-rev" alt="">
    Отзывы:
    <span class="info-reviews-count-up">
        <a href="<?= Url::to(['/profile?id=' . $user->id, '#' => 'positive_reviews']) ?>" class="hover-text-decoration">
            +<?= $user->getOrderReview()->positive()->count(); ?>
        </a>
    </span>
    <span class="info-reviews-count-down">
        <a href="<?= Url::to(['/profile?id=' . $user->id, '#' => 'negative_reviews']) ?>" class="hover-text-decoration">
            -<?= $user->getOrderReview()->negative()->count(); ?>
        </a>
    </span>
</p>
