<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 04.06.17
 * @time: 21:20
 */

use common\models\User;
use yii\widgets\ListView;
use common\models\UserAvatar;
use common\models\City;
use common\models\Country;
use common\helpers\DateDiffHelper;
use common\models\ProfilePrivate;

/**
 * @var User $model
 * @var mixed $key
 * @var int $index
 * @var ListView $widget
 */

/**
 * @var ProfilePrivate $profilePrivate
 */
$profilePrivate = $model->profilePrivate;
$userAvatar = $city = $country = null;
if ($profilePrivate) {
    /**
     * @var UserAvatar $userAvatar
     */
    $userAvatar = $profilePrivate->userAvatar ? $profilePrivate->userAvatar : null;
    /**
     * @var City $city
     */
    $city = $profilePrivate->city ? $profilePrivate->city : null;
    /**
     * @var Country $country
     */
    $country = $profilePrivate->city ? $profilePrivate->city->region->country : null;
}

?>
<div class="block-users-all-rotate">
    <div class="name-block-answer-top">
        <div class="col-md-9 col-sm-9 paddingNone statick-avatars-reviews">
            <?php if ($userAvatar) { ?>
                <div class="col-md-2 col-sm-2 col-xs-2 paddingNone users-all-block-img">
                    <a href="#"><img src="<?= UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $userAvatar->image; ?>" alt="" style="width:90px; height:90px;"></a> <!--/themes/leadlance/img/index/person-photo.jpg-->
                </div>
            <?php } else { ?>
                <div class="col-md-2 col-sm-2 col-xs-2 paddingNone users-all-block-img">
                    <a href="#"><img src="/themes/leadlance/img/index/person-photo.jpg" alt="" style="width:90px; height:90px;"></a>
                </div>
            <?php } ?>
            <ul class="list-info-users-all user-info-com">
                <li class="nameUser">
                    <?php if ($profilePrivate && $profilePrivate->fullName) { ?>
                    <a href="#" class="hover-text-decoration"><?= $profilePrivate->fullName; ?></a>
                    <?php } ?>
                    <span class="nickName">(<?= $model->username; ?>)</span>
                    <?php
                        if ($model->proAccount) {
                        //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
                        ?>
                            <img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="iconNick">
                            <img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="iconNick">
                        <?php
                        }
                    ?>
                    <span class="status-off status-all-off-on">Off. <span class="off-timer-out">Заходил 3 минуты назад.</span></span>
                </li>
                <li class="nameUser">
                    <img src="/themes/leadlance/img/profile/forma-1.png" alt="" class="marg-user-img-dop">Россия. г. Санкт-Петербург
                </li>
                <?php if ($profilePrivate) { ?>
                <li class="nameUser">
Возраст:
                    <?php if ($profilePrivate->birth_date) { ?>
                    <span class="status-age status-days"><?= DateDiffHelper::getHumanReadableDiffInt($profilePrivate->birth_date); ?> лет</span>
                    <?php } ?>
                    <?php if ($profilePrivate->experience) { ?>
                    <span class="status-experience nickName">(опыт: <?= $profilePrivate->experience; ?> лет)</span>
                    <?php } ?>
                </li>
                <?php } ?>
            </ul>
        </div>
        <div class="col-md-3 col-sm-3 paddingNone block-info-stat">
            <p class="info-rating">
                <img src="/themes/leadlance/img/profile/reit.png" alt="">
Рейтинг:
                <span class="info-rating-count"><a href="#" class="hover-text-decoration">8267</a></span>
            </p>
            <?= $this->render(
                    '/profile/_reviews',
                [
                    'user'  => $model,
                    'class' => 'order-reviews',
                ]
            ); ?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
