<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 04.06.17
 * @time: 21:17
 */

use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use common\models\User;

/**
 * @var ActiveDataProvider $usersDataProvider
 */

/**
 * @var string $type
 */

/**
 * @var string $q
 */

?>
<div class="row">
    <div class="col-lg-12 media-padd-none-767">
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 paddingNone">
            <h2 class="projectTitles page-titles">Каталог пользователей</h2>
        </div>
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 block-search-users">
            <div class="position-input-search-block">
                <input type="text" class="input-search-users" id="input-search-users" placeholder="Поиск по пользователям"<?php if ($q) { echo ' value="'.$q.'"'; } ?>>
                <i class="fa fa-neuter" aria-hidden="true"></i>
            </div>
        </div>
        <div class="clearfix"></div>
        <a href="<?= Url::to(['/user-list/index']); ?>" class="distribLink-left<?php if (!$type) { echo " activeDistribLink"; } ?>">Все</a>
        <a href="<?= Url::to(['/user-list/index?type=' . User::ROLE_EXECUTOR]); ?>" class="distribLink-left<?php if ($type == User::ROLE_EXECUTOR) { echo " activeDistribLink"; } ?>">Вебмастера</a>
        <a href="<?= Url::to(['/user-list/index?type=' . User::ROLE_CUSTOMER]); ?>" class="distribLink-left<?php if ($type == User::ROLE_CUSTOMER) { echo " activeDistribLink"; } ?>">Рекламодатели</a>
        <?php
            echo ListView::widget([
                'dataProvider' => $usersDataProvider,
                'itemView' => '_item',
                'summary' => '',
            ])
        ?>
    </div>
</div>