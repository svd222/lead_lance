<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
use yii\web\View;

$this->title = $message;
?>
<div class="site-error">
    <div class="error-404">
        404
    </div>
    <h1><?= Html::encode($this->title) ?></h1>
</div>
<?php
    $script = "
        $('.wrapperNav').remove();
        $('.wrapperContent').css('margin-left', 0);
    ";

    $this->registerJs($script, View::POS_LOAD);
?>
