<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 24.01.17
 * @time: 17:49
 */
return [
    'The requested page does not exist.' => 'Запрашиваемая страница не существует.',
    'Internal error occured' => 'Произошла внутрення ошибка',
    'Widget must be inherited from yii\base\Widget' => 'Виджет должен наследоваться от yii\base\Widget',
    'Required class not exists' => 'Требуемый класс не существует',
    'Success' => 'Успех',
    'Successful' => 'Успешно',
    'Ajax data was sended, but internal error occured' => 'Данные были посланы, но произошла внутрення ошибка сервера',
    'Customer' => 'Заказчик',
    'Executor' => 'Исполнитель',
    'An internal error occured' => 'Произошла внутренняя ошибка',

    'created' => 'создан',
    'published' => 'опубликован',
    'unpublished' => 'снят с публикации',
    'unpublished by admin' => 'снят с публикации администратором',
    'repaired' => 'исправлен',
    'accomplished' => 'завершён',
    'deleted' => 'удалён',

    'closed' => 'закрыт',
    'Day' => 'День',
    'Week' => 'Неделя',
    'Month' => 'Месяц',
];
