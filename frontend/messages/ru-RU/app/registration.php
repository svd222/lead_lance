<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 23.02.17
 * @time: 19:20
 */
return [
    'Incorrect user role' => 'Недействительная роль пользователя',
    'You must set user role' => 'Выберите роль',
];