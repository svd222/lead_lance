<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 04.06.17
 * @time: 16:26
 */
return [
    'Please confirm your new email' => 'Пожалуйста подтвердите ваш новый email',
    'Your account was deleted' => 'Ваш аккаунт был удалён',
    'You can`t delete this account' => 'Вы не можете удалить этот аккаунт',
];