<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/19/2018
 * Time: 3:32 PM
 */

return [
    'ID'                                          => 'ID',
    'Title'                                       => 'Название',
    'Image'                                       => 'Изображение',
    'Text'                                        => 'Содержание',
    'User ID'                                     => 'ID пользователя',
    'Tags'                                        => 'Тэги',
    'Created At'                                  => 'Создан',
    'Updated At'                                  => 'Обновлен',
    'Create News'                                 => 'Создание новости',
    'Create'                                      => 'Создать',
    'Update'                                      => 'Обновить',
    'Update News'                                 => 'Обновление новости',
    'Status'                                      => 'Статус',
    'Publish'                                     => 'Опубликвать',
    'Unpublish'                                   => 'Снять с публикации',
    'News'                                        => 'Новости',
    'Published'                                   => 'Опуликован',
    'Unpublished'                                 => 'Снят с публикации',
    'Delete'                                      => 'Удалить',
    'The news was successfully published'         => 'Новость была успешно опубликована',
    'The news was successfully unpublished'       => 'Новость была снята с публикации',
    'News successful created'                     => 'Новость успешно создана',
    'Are you sure you want to delete this item?' => 'Вы действительно хотите удалить новость?',
];
