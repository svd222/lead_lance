<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 10.07.17
 * @time: 17:37
 */

return [
    'ID' => 'ID',
    'Message' => 'Сообщение',
    'From User ID' => 'ID Отправителя',
    'To User ID' => 'ID Получателя',
    'Status' => 'Статус',
    'Lead Cost' => 'Цена лида',
    'Project ID' => 'ID проекта',
    'Created At' => 'Дата создания',
    'Batch Id' => 'ID пакета',
];