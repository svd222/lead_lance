<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 13.02.17
 * @time: 22:09
 */
return [
    'Invalid email' => 'Недействительный емайл',
    'Email successfull changed' => 'Емайл успешно изменён',
];