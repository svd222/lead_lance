<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 03.02.17
 * @time: 0:10
 */
return [
    'Profile case was deleted successfull' => 'Кейс был успешно удалён',
    'Can`t delete profile case' => 'Невозможно удалить кейс',
    'Are you sure you want to delete this case?' => 'Вы уверены что хотите удалить этот кейс?',
];