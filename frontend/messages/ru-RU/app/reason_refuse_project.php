<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 04.10.17
 * @time: 20:17
 */
return [
    'ID' => 'ID',
    'Project ID' => 'ID проекта',
    'Reason' => 'Причина',
    'Created At' => 'Время создания',
    'Type' => 'Тип',
];