<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 24.01.17
 * @time: 18:48
 */
return [
    'Creating order' => 'Создание заказа',
    'Updating order' => 'Редактирование заказа',
    'Create order' => 'Создать заказ',
    'Update order' => 'Обновить заказ',
    'Order was successfull updated' => 'Заказ был успешно обновлён',
    'Unable to save data' => 'Не могу сохранить данные',
    'Title' => 'Название',
    'Description' => 'Описание',
    'Lead Count Required' => 'Требуемое количество лидов',
    'Execution Time' => 'Срок исполнения',
    'Is Urgent' => 'Срочный проект',
    'Is Auction' => 'Аукцион',
    'Lead Cost' => 'Цена лида',
    'User ID' => 'ID пользователя',
    'Deadline Dt' => 'Крайний срок',
    'Is Multiproject' => 'Мультипроект',
    'Created At' => 'Создан',
    'Updated At' => 'Обновлен',
    'Project ID' => 'ID проекта',
    'Executor ID' => 'ID исполнителя',
    'Currency' => 'Валюта',
    'Date type' => 'Тип даты',
    'Deadline Year' => 'Год дедлайна',
    'Deadline Month' => 'Месяц дедлайна',
    'Deadline Day' => 'День дедлайна',
    'Acceptance Time' => 'Срок принятия выполненного задания',
    'You are not allowed to update order, you are not owner of order' => 'Вы не можете обновлять заказ, Вы не владелец заказа',
    'You are not allowed to create order, you are not owner of project' => 'Вы не можете создавать заказ, Вы не владелец проекта',
];