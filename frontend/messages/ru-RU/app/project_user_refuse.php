<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 21.07.17
 * @time: 18:45
 */
return [
    'The user was denied the execution of the project' => 'Пользователю было отказано в исполнении проекта',
    'You are refuse from project' => 'Вы отказались от проекта',
    'Are you sure you want' => 'Вы уверены',
    'to deny execution for this user?' => 'что хотите отказать этому пользователю?',
    'to refuse execution this project?' => 'что хотите отказаться от выполнения этого проекта?',
];