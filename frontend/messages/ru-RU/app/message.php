<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 08.04.17
 * @time: 14:01
 */
return [
    'Message was sent' => 'Сообщение было отправлено',
    'Send message' => 'Отправить сообщение',
];