<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 23.02.17
 * @time: 19:57
 */
return [
    'Your account has been created and confirmed' => 'Ваш аккаунт был создан и подтверждён',
    'Upload directory' => 'Директория для загрузки',
    'A user with such email address does not exist' => 'Пользователь с таким email не существует',
];
