<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'bootstrap', 'timeStampUTC', 'frontend\components\Alias'],
    'controllerNamespace' => 'frontend\controllers',
    'sourceLanguage' => 'en-US',
    'language' => 'ru-RU',
    'components' => [
        'timeStampUTC' => [
            'class' => 'common\components\TimeStampUTC',
            'propertiesMap' => [
                'common\models\Project' => [
                    //TimestampUTC behavior for the created_at && updated_at attrs of Project model
                ],
                'common\models\Message' => [
                    //TimestampUTC behavior for the created_at attribute of Message model
                    'updatedAtAttribute' => [
                        'enableUTCBehavior' => false,
                    ]
                ],
            ]
        ],
        'currentRoute' => [
            'class' => 'frontend\components\CurrentRoute',
        ],
        'bootstrap' => [
            'class' => 'frontend\components\Bootstrap',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
            //'enableCsrfValidation' => false,
        ],
        'footerWidgetLoader' => [
            'class' =>  'frontend\components\FooterWidgetLoader',
        ],
        'user' => [
            'identityClass' => 'common\models\User',//'dektrium\user\models\User',
            'loginUrl' => ['user/login'],
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [//default behavior
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'en-US',
                ],
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'baseUrl' => '/',
            'enableStrictParsing' => true,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'pattern' => '',
                    'route' => '/project/index',
                ],
                [
                    'pattern' => 'order/create/<projectId:\d+>/<executorId:\d+>',
                    'route' => 'order/create',
                ],
                [
                    'pattern' => '<controller:[\w-]+>/<id:\d+>',
                    'route' => '<controller>/view',
                ],
                [
                    'pattern' => 'profile/get-city-list/<countryId:\d+>',
                    'route' => 'profile/get-city-list',
                ],
                [
                    'pattern' => 'profile/get-country-by-city/<cityId:\d+>',
                    'route' => 'profile/get-country-by-city',
                ],
                [
                    'pattern' => 'profile/delete-case/<id:\d+>',
                    'route' => 'profile/delete-case',
                ],

                [
                    'pattern' => '<controller:(order)>/<id:\d+>/<type:[\w-]+>',
                    'route' => '<controller>/<type>',
                ],
                [
                    'pattern' => '<controller:(order)>/<id:\d+>',
                    'route' => 'order/view',
                ],
                /*[
                    'pattern' => 'profile/get-country-list',
                    'route' => 'profile/get-country-list',
                ],*/
                [
                    'pattern' => 'order/accept/<orderId:\d+>/<executorId:\d+>',
                    'route' => 'order/accept',
                ],
                [
                    'pattern' => 'profile/confirm-email/<guid:[\w0-9]{32}>',
                    'route' => 'profile/confirm-email',
                ],
                [
                    'pattern' => 'project/refuse/<projectId:\d+>/<refusedUserId:\d+>',
                    'route' => 'project/refuse',
                ],
                [
                    //'host' => 'http://frontend.ll.local',//!important for functional tests
                    // For list of possible entity types @see [[common\models\Batch::ENTITY_TYPE_*]]
                    'pattern' => '<controller:image>/<action:upload>/<entity_type:(1|2|3|4|5)>',
                    'route' => '<controller>/<action>',
                ],
                [
                    //'host' => 'http://frontend.ll.local',//!important for functional tests
                    'pattern' => '<controller:\w+>/<action:[\w-]+>/<id:\d+>',
                    'route' => '<controller>/<action>',
                ],
                [
                    //'host' => 'http://frontend.ll.local',//!important for functional tests
                    'pattern' => '<module:\w+>/<controller:\w+>/<action:\w+>',
                    'route' => '<module>/<controller>/<action>',
                ],
                [
                    //'host' => 'http://frontend.ll.local',//!important for functional tests
                    'pattern' => '<controller:[\w0-9-]+>/<action:[\w0-9-]+>',
                    'route' => '<controller>/<action>',
                ],
                [   'pattern' => '<controller:[0-9\w-]+>',
                    'route' => '<controller>/index'
                ],
            ],
        ],
        'view' => [
            'theme' => [
                'basePath' => '@webroot/themes/leadlance',
                'baseUrl' => '@web/themes/leadlance',
                'pathMap' => [
                    '@app/views' => '@frontend/themes/leadlance/views',
                ]
            ]
        ],
    ],
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableGeneratingPassword' => true,
            'enableConfirmation' => false,
            'modelMap' => [
                'User' => 'common\models\User',
                'Profile' => 'common\models\Profile',
                'RegistrationForm' => 'frontend\models\RegistrationForm',
            ],
            'controllerMap' => [
                'security'     => 'frontend\controllers\SecurityController',
                'registration' => 'frontend\controllers\RegistrationController',
                'recovery'     => 'frontend\controllers\RecoveryController',
            ],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
        ],
    ],
    'params' => $params,
];
