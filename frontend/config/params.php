<?php
return [
    'adminEmail' => 'noreply@leadlance.ru',
    'themes' => [
        'leadlance' => [
            'colorsCssClasses' => ['greenTags', 'carryTags', 'radTags', 'pincTags', 'pincedTags', 'yellowTags', 'coralTags'],
        ],
    ],
    'project' => [
        'left_menu' => [
            'count' => 7, // show n projects in left drop down menu
        ],
        'conversation' => [
            'update_status' => 3000,
        ]
    ],
    'order' => [
        'left_menu' => [
            'count' => 7, // show n orders in left drop down menu
        ],
        'conversation' => [
            'update_status' => 3000,
        ]
    ]
];
