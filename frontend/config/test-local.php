<?php
$localHost = 'http://frontend.ll.local';

$mainConfig = require(__DIR__ . '/main.php');

if (isset($mainConfig['components']) && isset($mainConfig['components']['urlManager']) && $mainConfig['components']['urlManager']['enablePrettyUrl']) {
    $urlRules = $mainConfig['components']['urlManager']['rules'];
    for ($i = 0; $i < count($urlRules); $i++) {
        if (!isset($urlRules[$i]['host'])) {
            $mainConfig['components']['urlManager']['rules'][$i]['host'] = $localHost;//!important for functional tests
        }
    }
}

return yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/test-local.php'),
    $mainConfig,
    require(__DIR__ . '/main-local.php'),
    require(__DIR__ . '/test.php'),
    [
        'id' => 'app-tests',
        'components' => [
           'user' => [
               'identityClass' => 'common\models\User',//'dektrium\user\models\User',
               'loginUrl' => ['user/login'],
               'enableAutoLogin' => true,
               'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
           ],
        ]
    ]
);
