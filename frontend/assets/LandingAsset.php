<?php
namespace frontend\assets;
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 09.01.17
 * @time: 14:35
 */


use yii\web\AssetBundle;

/**
 * Main landing application asset bundle.
 */
class LandingAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance/landing';

    public $baseUrl = '@web/themes/leadlance/landing';

    public $css = [
        'css/slick.css',
        'css/main.css',
        'css/landing-style.css',
        'css/landing-style-market.css',
        'css/media-landing.css',
        'css/jquery.custom-scrollbar.css',
        'css/media.css',
    ];

    public $js = [
        'js/common.js',
        'js/jquery.custom-scrollbar.js',
        'js/select2.full.min.js',
        /*'js/jquery-mob.js',*/
        'js/slick.min.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        /*'yii\bootstrap\BootstrapAsset',*/
        'frontend\assets\AutoloadAsset',
        'skinka\widgets\gritter\GritterAsset',
    ];
}