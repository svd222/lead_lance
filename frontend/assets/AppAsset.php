<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';
    public $baseUrl = '@web/themes/leadlance';
    public $css = [
        /*'css/site.css',*/
        /*'css/bootstrap.min.css',*/
        'css/font-awesome.min.css',
        'css/jquery.custom-scrollbar.css',
        'css/main.css',
        'css/style-mark.css',
        'css/style-pavel.css',
        'css/media.css',
        'css/select2/4.0.3/select2.min.css',
    ];
    public $js = [
        'js/jquery.custom-scrollbar.js',
        'js/common.js',
        'js/select2/4.0.3/select2.min.js',
        'js/bootstrap.min.js',
        'js/ws_command_list.js',
        'js/ws_client.js',
    ];
    public $depends = [
        'frontend\assets\CollectionAsset',
        'frontend\assets\CurrentRouteAsset',
        'frontend\assets\CommonAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'skinka\widgets\gritter\GritterAsset',
        'frontend\assets\AutoloadAsset',
    ];
}
