<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 17.08.17
 * Time: 18:23
 */
namespace frontend\assets;

use yii\web\AssetBundle;

class WsOrderReviewAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';

    public $baseUrl = '@web/themes/leadlance';

    public $js = [
        'js/ws/ws_order_review_handler.js',
    ];
}