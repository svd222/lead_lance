<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 19.07.17
 * @time: 17:05
 */
namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class CurrentRouteAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';

    public $baseUrl = '@web/themes/leadlance';

    public $js = [
        'js/current_route.js',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    public $depends = [
        'frontend\assets\CollectionAsset',
    ];
}