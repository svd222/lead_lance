<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 06.07.17
 * Time: 13:15
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class WsOrderActionChangedStateAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';

    public $baseUrl = '@web/themes/leadlance';

    public $js = [
        'js/ws/ws_order_action_changed_state.js',
    ];
}