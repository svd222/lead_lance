<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 13.06.17
 * Time: 17:24
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class ProfileCreateCaseAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';
    public $baseUrl = '@web/themes/leadlance';

    public $depends = [
        'frontend\assets\AppAsset'
    ];

    public $css = [
        'css/profile_create_case.css',
    ];
}