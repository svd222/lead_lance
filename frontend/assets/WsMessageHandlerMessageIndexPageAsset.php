<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 06.07.17
 * @time: 13:15
 */

namespace frontend\assets;

use yii\web\AssetBundle;

class WsMessageHandlerMessageIndexPageAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';

    public $baseUrl = '@web/themes/leadlance';

    public $js = [
        'js/ws/ws_message_handler_message_index_page.js',
    ];
}