<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 14.03.17
 * Time: 15:01
 */
namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class CollectionAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';

    public $baseUrl = '@web/themes/leadlance';

    public $js = [
        'js/collection.js',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}