<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 14.08.17
 * @time: 19:52
 */
namespace frontend\assets;

use yii\web\AssetBundle;

class WsOrderMessageHandlerAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';

    public $baseUrl = '@web/themes/leadlance';

    public $js = [
        'js/ws/ws_order_message_handler.js',
    ];
}