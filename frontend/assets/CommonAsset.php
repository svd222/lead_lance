<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 19.07.17
 * @time: 17:54
 */

namespace frontend\assets;

use yii\web\AssetBundle;
use yii\web\View;

class CommonAsset extends AssetBundle
{
    public $basePath = '@webroot/themes/leadlance';

    public $baseUrl = '@web/themes/leadlance';

    public $js = [
        'js/common_functions.js',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];

    public $depends = [
        'frontend\assets\CurrentRouteAsset',
    ];
}