<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 04.06.17
 * @time: 21:34
 */
namespace frontend\controllers;

use Yii;
use common\models\User;
use common\components\Controller;
use yii\base\DynamicModel;
use yii\base\InvalidCallException;
use yii\data\ActiveDataProvider;
use common\models\ProAccountQuery;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\validators\InlineValidator;

/**
 * Class UserController Provides access to users page
 * @package frontend\controllers
 */
class UserListController extends Controller
{
    /**
     * Show user list page
     *
     * @return string
     */
    public function actionIndex()
    {
        $type = Yii::$app->request->get('type');
        $q = Yii::$app->request->get('q');
        $validator = new \yii\validators\RangeValidator([
            'range' => [
                User::ROLE_CUSTOMER,
                User::ROLE_EXECUTOR,
            ],
            'message' => Yii::t('app/user_list', 'Incorrect filter value'),
            'skipOnEmpty' => true,
        ]);
        //i don know why skipOnEmpty property of \yii\validators\Validator don`t work...
        //@todo repare it
        $validated = true;
        if ($type && !$validator->validate($type, $error)) {
            $validated = false;
        }
        if ($validated) {
            /**
             * @var ActiveQuery $usersQuery
             */
            $usersQuery = User::find()
                ->active()
                ->with([
                        'proAccount' => function($query) {
                            /**
                             * @var ProAccountQuery $query
                             */
                            $query->active();
                        },
                        'profilePrivate' => function($query) {
                            /**
                             * @var ActiveQuery $query
                             */
                            /*if ($q) {
                                $query->orWhere("{{%profile_private}}.first_name LIKE '%".$q."%' OR {{%profile_private}}.last_name LIKE '%".$q."%'");
                            }*/

                            $query->with([
                                'city',
                                'city.region.country',
                                'userAvatar',
                            ]);
                        }
                    ]
                );

            if ($q) {
                $usersQuery->andWhere("{{%user}}.username LIKE '%".$q."%'");
            }

            if ($type) {
                $assignments = (new Query())
                    ->from('{{%auth_assignment}}')
                    ->where("item_name = '".$type."'")
                    ->select('user_id')
                    ->all();
                $assignments = array_map(function($item) {
                    return $item['user_id'];
                }, $assignments);
                $usersQuery->andWhere(['IN', 'id', $assignments]);
            }

            $usersDataProvider = new ActiveDataProvider([
                'query' => $usersQuery,
                'pagination' => [
                    'pageSize' => 25
                ],
            ]);

            return $this->render('index', [
                'usersDataProvider' => $usersDataProvider,
                'type' => $type,
                'q' => $q
            ]);
        } else {
            throw new InvalidCallException($error);
        }
    }
}