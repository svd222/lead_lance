<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/18/2018
 * Time: 8:37 PM
 */

namespace frontend\controllers;

use common\components\Controller;
use common\models\NewsSearch;
use common\models\News;
use common\models\Comment;
use Ratchet\Wamp\Exception;
use Yii;
use yii\web\Response;

class NewsController extends Controller
{
    protected $exceptActions = [
        'frontend_news_index', 'frontend_news_view',
    ];

    public function actionIndex()
    {
        $dataProvider = NewsSearch::searchFrontendNews([]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        if (empty($id)) {
            throw new Exception('News id was not found');
        }
        $news = News::findOne($id);
        if (empty($news)) {
            throw new Exception('Such news was not found');
        }

        $comment = new Comment();

        $comment->from_user_id = Yii::$app->user->id ? Yii::$app->user->id : null;
        $comment->item_id = $id;
        $comment->parent_id = null;

        $rootComments = $comment->getRootComments($id, Comment::COMMENT_TYPE_NEWS);

        return $this->render('view', [
            'news'         => $news,
            'comment'      => $comment,
            'rootComments' => $rootComments,
            'commentCount' => $news->getComment()->count(),
        ]);

    }

    /**
     * Adds comment and returns updated comment tree html
     *
     * @return bool
     */
    public function actionAddComment()
    {
        $request = Yii::$app->request;

        if (!$request->isAjax) {
            return false;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        if (empty($post['Comment']) || empty($post['Comment']['item_id'])) {
            Yii::$app->response->data['message'] = 'News id was not found';
            Yii::$app->response->send();
            Yii::$app->end();
        }
        $newsId = $post['Comment']['item_id'];
        $news = News::findOne($newsId);
        if (empty($news)) {
            Yii::$app->response->data['message'] = 'Such news was not found';
            Yii::$app->response->send();
            Yii::$app->end();
        }

        $comment = new Comment();
        if ($comment->load($post)) {
            $comment->type = Comment::COMMENT_TYPE_NEWS;
            if ($post[$comment->formName()]['parent_id']) {
                // find parent node
                $parentNode = Comment::findOne($post[$comment->formName()]['parent_id']);
                if ($comment->appendTo($parentNode)) {
                }
            } else {
                if ($comment->makeRoot()) {
                }
            }
        }

        $comment->from_user_id = Yii::$app->user->id ? Yii::$app->user->id : null;
        $comment->item_id = $newsId;
        $comment->parent_id = null;
        $rootComments = $comment->getRootComments($newsId, Comment::COMMENT_TYPE_NEWS);

        Yii::$app->response->data['html'] = $this->renderPartial('/comment/_comment_tree', [
            'rootComments' => $rootComments,
        ]);
        Yii::$app->response->data['commentCount'] = $news->getComment()->count();

        Yii::$app->response->send();
        Yii::$app->end();
    }

    public function actionSearch()
    {
        $request = Yii::$app->request;

        if (!$request->isAjax) {
            return false;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        $dataProvider = NewsSearch::searchFrontendNews([
            'searchQuery' => $post['searchQuery'],
        ]);
        Yii::$app->response->data['html'] = $this->renderPartial('_news_list', [
            'dataProvider' => $dataProvider,
        ]);
        Yii::$app->response->send();
        Yii::$app->end();
    }
}
