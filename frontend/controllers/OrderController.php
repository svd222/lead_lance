<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 18.01.17
 * @time: 7:51
 */
namespace frontend\controllers;

use common\components\Controller;
use common\helpers\OrderHelper;
use common\interfaces\fms\FMSInterface;
use common\models\Batch;
use common\models\Disput;
use common\models\OrderMessage;
use common\models\OrderReview;
use common\models\PartAccepted;
use common\models\Order;
use common\models\ProfilePrivate;
use common\models\Project;
use common\models\ProjectType;
use common\models\TrafficType;
use common\models\User;
use common\widgets\GritterAlert;
use frontend\themes\leadlance\widgets\OrderFooterWidget;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use common\models\Image;
use yii\base\Model;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
use common\traits\IsOwnerTrait;

class OrderController extends Controller
{
    use IsOwnerTrait;

    const AJAX_PARAM = 'ajax';

    public $footerWidget = [
        'class' => OrderFooterWidget::class,
    ];

    public $footerTemplate;

    public $footerData;

    /**
     * @var Order $order
     */
    protected $order;

    /**
     * @var array
     */
    protected $orderMessages = [];

    /**
     * @var FMSInterface $fms
     */
    protected $fms;

    /**
     * @var string $actionId
     */
    protected $actionId;

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->actionId = $action->id;
            $actions = ['create', 'update', 'validate-order-message-form', 'order-message-send'];
            if (!in_array($this->actionId, $actions)) {
                $this->view->params['bodyClass'] = 'order-executive';
                $this->order = $this->findModel(Yii::$app->request->get('id'));
                $this->fms = Yii::$container->get(FMSInterface::class, [
                    'Order', $this->order, []
                ]);
                if ($this->actionId != 'view') {
                    if (!$this->fms->checkAccess($action->id)) {
                        return false;
                    }
                }
                $this->orderMessages = []; // @todo load order messages
            }
            /*if ($this->actionId == 'view') {
                $this->loadFooterTemplate();
            }*/
        }
        return true;
    }

    protected function getViewParams()
    {
        $userId = 0;
        if (Yii::$app->user->id == $this->order->executor_id) {
            $userId = $this->order->user_id;
        } else {
            if (Yii::$app->user->id == $this->order->user_id) {
                $userId = $this->order->executor_id;
            }
        }
        $params = [
            'order' => $this->order,
            'fms' => $this->fms,
            'orderMessages' => $this->orderMessages,
            'trafficTypes' => $this->order->trafficTypes,
        ];
        if ($userId) {
            /**
             * @var User $user
             */
            $user = User::getActiveUser($userId);
            $profilePrivate = $user->profilePrivate;
            $fullName = '';
            if ($profilePrivate) {
                $fullName = $profilePrivate->fullName;
            }
            $params = ArrayHelper::merge($params, [
                'user' => $user,
                'userHasPro' => User::hasPro($userId),
                'userIsCustomer' => User::isCustomer($userId),
                'userIsExecutor' => User::isExecutor($userId),
            ]);
            $params = ArrayHelper::merge($params, [
                'userFullName' => $fullName
            ]);
        }
        return $params;
    }

    public function actionView($id) {
        $userId = Yii::$app->user->id;
        $order = $this->findModel($id);
        if (User::isCustomer($userId)) {
            if (!Yii::$app->user->can('IsOwnThisEntity', [
                'model' => $order,
                'property' => 'user_id'
            ])) {
                throw new ForbiddenHttpException();
            }
        }

        $post = Yii::$app->request->post();
        if (!empty($post['OrderReview'])) {
            $orderReview = new OrderReview();
            if ($orderReview->load($post)) {
                $orderReview->save();
            }
        }

        if (!empty($post['Disput'])) {
            $disput = new Disput();
            if ($disput->load($post)) {
                $disput->save();
            }
        }

        if (!empty($post['PartAccepted'])) {
            $partAccepted = new PartAccepted();
            if ($partAccepted->load($post)) {
                $partAccepted->save();
            }
        }

        if (User::isExecutor($userId)) {
            if (!Yii::$app->user->can('IsOwnThisEntity', [
                'model' => $order,
                'property' => 'executor_id'
            ])) {
                throw new ForbiddenHttpException();
            }
        }
        $orderMessage = new OrderMessage();
        $orderMessage->from_user_id = $userId;
        $collocutorId = ($userId == $order->user_id) ? $order->executor_id : $order->user_id;
        $orderMessage->to_user_id = $collocutorId;
        $orderMessage->order_id = $order->id;

        $notify = OrderHelper::getCurrentNotify($this->order);

        $user = User::findOne($userId);
        $userHasPro = User::hasPro($userId);
        /**
         * @var ProfilePrivate $userProfilePrivate
         */
        $userProfilePrivate = $user->profilePrivate ? $user->profilePrivate : null;
        $userFullName = ($userProfilePrivate && $userProfilePrivate->fullName) ? $userProfilePrivate->fullName : '';

        $collocutor = User::findOne($collocutorId);
        $collocutorHasPro = User::hasPro($collocutorId);
        /**
         * @var ProfilePrivate $collocutorProfilePrivate
         */
        $collocutorProfilePrivate = $collocutor->profilePrivate;
        $collocutorFullName = ($collocutorProfilePrivate && $collocutorProfilePrivate->fullName) ? $collocutorProfilePrivate->fullName : '';

        $query = $order
            ->getOrderMessages()
            ->with([
                'batch' => function ($query) {
                    /**
                     * @var ActiveQuery $query
                     */
                    $query->andWhere(['entity_type' => Batch::ENTITY_TYPE_ORDER_MESSAGE])->with('images');
                }
            ])
            ->sortDesc();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 25,
            ],
        ]);

        $params = ArrayHelper::merge($this->getViewParams(), [
            'currentUser' => [
                'id' => $userId,
                'hasPro' => $userHasPro,
                'nickName' => $user->username,
                'fullName' => $userFullName
            ],
            'collocutor' => [
                'id' => $collocutorId,
                'hasPro' => $collocutorHasPro,
                'nickName' => $collocutor->username,
                'fullName' => $collocutorFullName
            ],
            'notify' => $notify,
            'dataProvider' => $dataProvider,
            'orderMessage' => $orderMessage,
            'imageModel' => new Image(),
        ]);
        $this->loadFooterTemplate();
        return $this->render('view', $params);
    }

    /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        $request = Yii::$app->request;
        $post = $request->post();
        if ($request->isAjax && $model->load($post)) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            echo Json::encode(ActiveForm::validate($model));
            Yii::$app->end();
        }
    }

    /**
     * Validate Registration data through ajax
     *
     * @return string json encoded string
     */
    public function actionValidateOrderMessageForm()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new OrderMessage();
        $post = $request->post();
        if (isset($post[self::AJAX_PARAM])) {
            $this->performAjaxValidation($model);
        }
    }

    /**
     * Save order message
     */
    public function actionOrderMessageSend()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $post = $request->post();
        $model = new OrderMessage();
        if ($request->isAjax && $model->load($post)) {
            if ($model->validate()) {
                if ($model->save()) {
                    if ($model->batch_id) {
                        $batch = Batch::findOne($model->batch_id);
                        $images = $batch->images;
                    }
                    $data = [
                        'id' => $model->id,
                        'from_user_id' => $model->from_user_id,
                        'to_user_id' => $model->to_user_id,
                        'message' => $model->message,
                        'order_id' => $model->order_id,
                        'created_at' => $model->created_at,
                    ];
                    if (!empty($images)) {
                        $data['images'] = $images;
                        $data['batch_id'] = $model->batch_id;
                    }
                    echo Json::encode($data);
                } else {
                    echo Json::encode($model->errors);
                }
            } else {
                echo Json::encode($model->errors);
            }
        }
        Yii::$app->end();
    }



    // ---------------------- start FMS actions -------------------------------

    public function actionExecutorRefuseExecuteOrder()
    {
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionExecutorAcceptOrderToExecute()
    {
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionCustomerRefuseOrder() {
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionWorkIsDoneByExecutor()
    {
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionCustomerAcceptWorkAndLeaveReview()
    {
        $this->processPost();
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionCustomerOpenDisput()
    {
        $this->processPost();
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionCustomerAcceptPartWork()
    {
        $this->processPost();
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionCustomerDeclineAcceptWork()
    {
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionExecutorAgreedAndLeaveReview()
    {
        $this->processPost();
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionExecutorOpenDisput()
    {
        $this->processPost();
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionWinnerLeaveResponse()
    {
        $this->processPost();
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    public function actionCollocutorsLeaveResponse()
    {
        $this->processPost();
        $this->fms->switchState($this->actionId);
        return $this->redirect('/order/'. $this->order->id);
    }

    // --------------- end FMS actions -------------------------------

    /**
     * Process $_POST
     *
     * @return bool
     */
    protected function processPost()
    {
        $fms = $this->fms;
        $actionsConfig = OrderHelper::getOrderActionsConfig($fms);
        foreach ($actionsConfig as $k => $v) {
            if ($this->actionId == $k) {
                $config = isset($v['footer']) ? $v['footer'] : null;
                if ($config) {
                    /**
                     * @var ActiveRecord $dependency
                     */
                    $post = Yii::$app->request->post();
                    $dependency = new $config['dependencyClass'];
                    if ($dependency instanceof OrderReview) {
                        if (!empty($post) && !empty($post[$dependency->formName()]) && !empty($post[$dependency->formName()]['emotion'])) {
                            $post[$dependency->formName()]['emotion'] = $post[$dependency->formName()]['emotion'][0];
                        }
                    }
                    if ($dependency->load($post)) {
                        $dependency->user_id = Yii::$app->user->id;
                        $dependency->created_at = gmdate('Y-m-d H:i:s');
                        if ($dependency instanceof OrderReview) {
                            $dependency->updated_at = gmdate('Y-m-d H:i:s');
                        }
                        if ($dependency->validate()) {
                            return $dependency->save(false);
                        }
                        return false;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Preapre config for footerWidget
     */
    protected function loadFooterTemplate()
    {
        $footerConfig = [];
        $fms = $this->fms;
        $actionsConfig = OrderHelper::getOrderActionsConfig($fms);
        if ($actionsConfig) {
            foreach ($actionsConfig as $k => $v) {
                if ($fms->checkAccess($k)) {
                    if (isset($v['footer'])) {
                        $dependency = $this->getDependency($v['footer']['dependencyClass']);
                        $dependencyVarName = lcfirst((new \ReflectionClass($dependency))->getShortName());
                        $footerConfig[] = [
                            'class' => 'frontend\themes\leadlance\widgets\OrderFooterWidget',
                            'template' => 'fms/' . $k,
                            'data' => [
                                $dependencyVarName => $dependency
                            ]
                        ];
                    }
                }
            }
        }
        if (!empty($footerConfig)) {
            $this->footerWidget = $footerConfig;
        }
    }

    protected function getDependency($className)
    {
        $entity = new $className;
        if ($entity->hasProperty('user_id')) {
            $entity->user_id = Yii::$app->user->id;
        }
        if ($entity->hasProperty('order_id')) {
            $entity->order_id = $this->order->id;
        }
        return $entity;
    }

    public function actionCreate($projectId = null, $executorId = null)
    {
        $model = new Order();
        $imageModel = new Image();
        $post = Yii::$app->request->post();
        $project = Project::findOne($projectId);

        if (!$this->checkIsOwner($project, 'user_id')) {
            throw new ForbiddenHttpException(Yii::t('app/order', 'You are not allowed to create order, you are not owner of project'));
        }

        $trafficTypeModel = new TrafficType();
        $trafficTypesList = TrafficType::getList();
        $trafficTypes = $project->trafficTypes;

        $trafficTypeSelected = [];
        foreach ($trafficTypes as $trafficType) {
            /**
             * @var TrafficType $trafficType
             */
            $trafficTypeSelected[$trafficType->id] = $trafficTypesList[$trafficType->id];
        }

        if (!isset($post[$model->formName()])) {
            if ($project === null) {
                throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
            }
            $model->title = $project->title;
            $model->description = $project->description;
            $model->lead_count_required = $project->lead_count_required;
            $model->execution_time = $project->execution_time;
            $model->lead_cost = $project->lead_cost;
            $model->user_id = Yii::$app->user->id;
            $model->executor_id = $executorId;
            $model->deadline_dt = $project->deadline_dt;
            $model->project_id = $projectId;

        } else {
            if ($model->load($post) && $model->save()) {
                $orderTrafficTypeInsertSql = 'INSERT INTO {{%order_traffic_type}}([[order_id]], [[traffic_type_id]]) VALUES';
                if (!empty($trafficTypes)) {
                    foreach ($trafficTypes as $trafficType) {
                        $orderTrafficTypeInsertSql .= '('.$model->id.', '.$trafficType->id.'),';
                    }
                    $orderTrafficTypeInsertSql = substr($orderTrafficTypeInsertSql, 0, strlen($orderTrafficTypeInsertSql) - 1);
                    $orderTrafficTypeInsertSqlExecuteResult = Yii::$app->db->createCommand($orderTrafficTypeInsertSql)->execute();
                } else {
                    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
                }

                if ($orderTrafficTypeInsertSqlExecuteResult) {
                    return $this->redirect(['/order/' . $model->id]);
                } else {
                    Yii::$app->session->setFlash(GritterAlert::TYPE_ERROR, Yii::t('app/order', 'Unable to save relative data'));
                    return $this->redirect(['/order/' . $model->id]);
                }
            }
        }

        $executor = User::getActiveUser(Yii::$app->request->get('executorId'));//$model->project->user;
        $executorId = $executor->id;
        $executorHasPro = User::hasPro($executorId);

        return $this->render('create', [
            'model' => $model,
            'trafficTypesList' => $trafficTypesList,
            'trafficTypeModel' => $trafficTypeModel,
            'trafficTypeSelected' => $trafficTypeSelected,
            'imageModel' => $imageModel,
            'executor' => $executor,
            'executorId' => $executorId,
            'executorHasPro' => $executorHasPro,
            'action' => 'create'
        ]);
    }

    public function actionUpdate($id)
    {
        /**
         * @var Order $model
         */
        $model = Order::find()
            ->andWhere('id = :id', [
                'id' => $id
            ])
            ->with([
                'trafficTypes',
                'project',
                'batch' => function ($query) {
                    /**
                     * @var ActiveQuery $query
                     */
                    $query->with('images');
                }
        ])->one();

        if (!$this->checkIsOwner($model, 'user_id')) {
            throw new ForbiddenHttpException(Yii::t('app/order', 'You are not allowed to update order, you are not owner of order'));
        }

        if (!Yii::$app->user->can('IsOwnThisEntity', [
            'model' => $model,
            'property' => 'user_id'
        ])) {
            throw new ForbiddenHttpException();
        }

        if (!$model) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $imageModel = new Image();
        $post = Yii::$app->request->post();

        /**
         * @var Project $project
         */
        $project = $model->project;

        /**
         * @var User $executor
         */
        $executor = $model->executor;

        /**
         * @var User $user
         */
        $user = $model->user;

        $trafficTypeModel = new TrafficType();
        $trafficTypesList = TrafficType::getList();

        $trafficTypeSelected = [];
        if (empty($post[$trafficTypeModel->formName()]['type'])) {
            $trafficTypeSelected = ArrayHelper::map($model->trafficTypes, 'id', 'type');
        } else {
            foreach ($post[$trafficTypeModel->formName()]['type'] as $trafficType)
            $trafficTypeSelected[$trafficType] = $trafficTypesList[$trafficType];
        }

        if ($model->load($post) && $model->save()) {

            $deleteTrafficTypeSql = 'DELETE FROM {{%order_traffic_type}} WHERE [[order_id]] = ' . $model->id;
            Yii::$app->db->createCommand($deleteTrafficTypeSql)->execute();

            $orderTrafficTypeInsertSql = 'INSERT INTO {{%order_traffic_type}}([[order_id]], [[traffic_type_id]]) VALUES';
            if (!empty($trafficTypeSelected)) {
                foreach ($trafficTypeSelected as $id => $type) {
                    $orderTrafficTypeInsertSql .= '('.$model->id.', '.$id.'),';
                }
                $orderTrafficTypeInsertSql = substr($orderTrafficTypeInsertSql, 0, strlen($orderTrafficTypeInsertSql) - 1);
                $orderTrafficTypeInsertSqlExecuteResult = Yii::$app->db->createCommand($orderTrafficTypeInsertSql)->execute();
            } else {
                throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
            }

            if ($orderTrafficTypeInsertSqlExecuteResult) {
                Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/order', 'Order was successfull updated'));
                return $this->redirect(['/order/' . $model->id]);
            } else {
                Yii::$app->session->setFlash(GritterAlert::TYPE_ERROR, Yii::t('app/order', 'Unable to save data'));
                return $this->redirect(['/order/' . $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'trafficTypesList' => $trafficTypesList,
            'trafficTypeModel' => $trafficTypeModel,
            'trafficTypeSelected' => $trafficTypeSelected,
            'imageModel' => $imageModel,
            'executor' => $executor,
            'executorId' => $executor->id,
            'executorHasPro' => User::hasPro($executor->id),
            'action' => 'update'
        ]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}