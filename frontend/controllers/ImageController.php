<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 12.03.17
 * @time: 15:13
 */
namespace frontend\controllers;

use Yii;
use common\models\Batch;
use common\models\Image;
use yii\data\ActiveDataProvider;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * ImageController implements auxiliary actions for manipulations with Image model.
 */
class ImageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['get-next-batch-id'],
                'rules' => [
                    [
                        'actions' => ['get-next-batch-id'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'actions' => ['upload', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-next-batch-id' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Checks that upload directory exists and it writable & readable
     * If not than create dir
     *
     * @return string
     */
    private static function getUploadDirectory()
    {
        $path = Yii::getAlias(Image::UPLOAD_DIRECTORY);
        if (!file_exists($path)) {
            FileHelper::createDirectory($path, 0755);
        }
        if (!is_writable($path) || !is_readable($path)) {
            chmod($path, 0755);
        }
        return $path;
    }

    /**
     * Upload image
     *
     * @param int $entity_type The entity type @see [[common\models\Batch::ENTITY_TYPE_*]]
     * @return string
     * @throws HttpException
     */
    public function actionUpload($entity_type)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new Image();
        $imageFile = UploadedFile::getInstance($model, 'image');
        $directory = self::getUploadDirectory();
        $uid = uniqid(time(), true);
        $fileName = $uid;
        if (isset($imageFile->extension) && $imageFile->extension) {
            $fileName .= '.' . $imageFile->extension;
        }

        $post = Yii::$app->request->post();
        $csrfParam = str_replace('-', '_', Yii::$app->request->csrfParam);
        if (!empty($post[$csrfParam])
            && Yii::$app->request->csrfTokenFromHeader == $post[$csrfParam]
        ) {
            if ($imageFile->saveAs($directory . DIRECTORY_SEPARATOR . $fileName)) {
                $image = new Image();
                $image->image = $fileName;
                $image->owner_id = Yii::$app->user->id;
                $image->original_image = $imageFile->name;
                if (!isset($post['batch_id']) || !$post['batch_id']) {
                    $batch = new Batch();
                    $batch->entity_type = $entity_type;
                    $batch->owner_id = Yii::$app->user->id;
                    $batch->save();
                    $batchId = $batch->id;
                }
                if (!isset($batchId)) {
                    $batchId = $post['batch_id'];
                }
                $batchId = intval($batchId);
                $image->batch_id = $batchId;
                if ($image->save()) {
                    return Json::encode([
                        'info' => [
                            'id' => $image->id,
                            'batch_id' => $batchId,
                        ],
                        'files' => [
                            [
                                'name' => $fileName,
                                'originalName' => $image->original_image,
                                'size' => $imageFile->size,
                                'url' => Image::UPLOAD_WEB . DIRECTORY_SEPARATOR . $fileName,
                                'deleteUrl' => 'delete?name=' . $fileName,
                                'deleteType' => 'POST',
                            ],
                        ],
                    ]);
                }
            }
            return '';
        } else {
            throw new HttpException(400, Yii::t('app', 'The CSRF token could not be verified.' . Yii::$app->request->csrfTokenFromHeader . ' ' . VarDumper::dumpAsString($post)));
        }
    }

    /**
     * Delete image
     *
     * @param $id
     * @return array json encoded array
     * [
     *  'success' => true
     * ]
     * |
     * [
     *  'error' => string
     * ]
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $userId = Yii::$app->user->id;
        $model = Image::find()->where('id = :id', ['id' => $id])->one();
        $result = [];
        if ($model) {
            if ($userId == $model->owner_id) {
                if ($model->deleteFile()) {
                    if ($model->delete()) {
                        $result = [
                            'success' => true,
                        ];
                    } else {
                        $result = [
                            'error' => Yii::t('app/image', 'Can`t delete record'),
                        ];
                    }
                } else {
                    $result = [
                        'error' => Yii::t('app/image', 'Can`t delete file from disk'),
                    ];
                }
            } else {
                $result = [
                    'error' => Yii::t('app/image', 'You are not allowed to perform this action'),
                ];
            }
        } else {
            $result = [
                'error' => Yii::t('app/image', 'Image does not exist'),
            ];
        }
        return $result;
    }
}
