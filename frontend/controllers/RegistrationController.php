<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 12.01.17
 * Time: 16:17
 */
namespace frontend\controllers;

use Yii;
use dektrium\user\controllers\RegistrationController as BaseRegistration;
use frontend\models\RegistrationForm;
use yii\base\Model;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use yii\helpers\Json;

class RegistrationController extends BaseRegistration
{
    /**
     * @todo Вынести в компонент Request (тоже самое и в компоненте common\components\Controller)
     */
    const AJAX_PARAM = 'ajax';

    /** @inheritdoc */
    public function behaviors()
    {
        $parentBehavior = parent::behaviors();
        $parentBehavior['access']['rules'][] = [
            'allow' => true, 'actions' => ['validate-form'], 'roles' => ['?']
        ];
        return $parentBehavior;
    }

    /**
     * @inheritdoc
     */
    public function actionRegister()
    {
        $request = Yii::$app->request;
        $post = $request->post();
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Yii::createObject(RegistrationForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        if ($model->load($post) && $request->isAjax && $model->register() ) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);
            $result = [
                'success' => true,
            ];
            echo Json::encode($result);
            Yii::$app->end();
        } else {
            $this->performAjaxValidation($model);
        }
    }

    /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(Model $model)
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $result = [];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isAjax) {
            $recaptcha = $post['g-recaptcha-response'];
            $google_url = "https://www.google.com/recaptcha/api/siteverify";
            $secret = '6Lflo0QUAAAAAA3hqxx7hqsHCVnSAm_83Jh3GzOF';
            $ip = $_SERVER['REMOTE_ADDR'];
            $url = $google_url . "?secret=" . $secret . "&response=" . $recaptcha . "&remoteip=" . $ip;
            $res = $this->getCurlData($url);
            $res = json_decode($res, true);
            $result = empty($res['success'])
                ? [
                    "register-form-recaptcha" => [
                        "Подтвердите, что вы не робот",
                    ],
                ]
                : '';
            if (empty($result) && $model->load($post)) {
                $result = ActiveForm::validate($model);
            }
        }
        echo Json::encode($result);
        Yii::$app->end();
    }

    protected function getCurlData($url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
        $curlData = curl_exec($curl);
        curl_close($curl);
        return $curlData;
    }

    /**
     * Validate Registration data through ajax
     *
     * @return string json encoded string
     */
    public function actionValidateForm()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new RegistrationForm();
        $post = $request->post();
        if (isset($post[self::AJAX_PARAM])) {
            $this->performAjaxValidation($model);
        }
    }
}
