<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 12.01.17
 * @time: 22:01
 */
namespace frontend\controllers;

use Yii;
use dektrium\user\models\LoginForm;
use dektrium\user\controllers\SecurityController as BaseSecurity;

class SecurityController extends BaseSecurity
{
    /**
     * @inheritdoc
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->redirect(['/project/index']);
            //return $this->goBack();
        } else {
            return $this->goBack();
        }
    }
}