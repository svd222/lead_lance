<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 20.06.17
 * @time: 16:11
 */
namespace frontend\controllers;

use common\models\Comment;
use common\models\ProfileCase;
use common\components\Controller;
use Yii;

class CaseController extends Controller
{
    public function actionIndex()
    {
        $dataProvider = ProfileCase::search([]);
        return $this->render('index', [
            'dataProvider'  => $dataProvider,
            'currentUserId' => Yii::$app->user->id,
        ]);
    }
}
