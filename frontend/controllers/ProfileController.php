<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @Date: 27.01.17
 * @time: 11:19
 */
namespace frontend\controllers;

use common\models\ChangeValue;
use common\models\Country;
use common\models\OrderReview;
use common\models\ProfileCase;
use common\models\Comment;
use common\models\ProfileChangeEmail;
use common\models\ProfilePhone;
use common\models\ProfilePrivate;
use common\models\User;
use common\models\UserAvatar;
use common\models\UserQuery;
use dektrium\user\helpers\Password;
use vova07\imperavi\actions\GetImagesAction;
use Yii;
use common\components\Controller;
use yii\base\ErrorException;
use yii\db\Query;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use common\widgets\GritterAlert;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\base\Model as BaseModel;
use yii\web\NotFoundHttpException;
use common\models\ChangePasswordForm;
use yii\web\UploadedFile;
use common\models\ProAccountQuery;

class ProfileController extends Controller
{
    public $footerWidget = [
        'class' => 'frontend\themes\leadlance\widgets\ProfileFooterWidget',
        'template' => '_footer',
        'data' => []
    ];

    /**
     * Show the profile tabs
     *
     *  @return string
     */
    public function actionIndex($id = null)
    {
        if (!$id) {
            $id = Yii::$app->user->id;
        }
        $user = User::getActiveUser($id);
        if ($user) {
            $profile = $user->profile;
            $this->view->params['footerWidgetParams'] = [
                'profile' => $profile,
            ];
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }
        $changePasswordForm = new ChangePasswordForm();
        return $this->render('index', [
            'uid' => $id,
            'user' => $user,
            'profile' => $profile,
            'changePasswordForm' => $changePasswordForm,
        ]);
    }

    /**
     * Returns json list of order reviews/testimonials
     */
    public function actionGetOrderReviews()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = Yii::$app->request->post();
        $emotion = ArrayHelper::keyExists('emotion', $post) ? intval($post['emotion']) : 1;// $emotion may be 1 or 2, default 1 (positive)
        $userId = ArrayHelper::keyExists('userId', $post) ? intval($post['userId']) : Yii::$app->user->id;
        $orderReviews = OrderReview::find()
            ->byCollocutor($userId)
            /*->andWhere('emotion = :emotion', [
                'emotion' => $emotion
            ])*/
            ->andWhere('user_id != :user_id', [
                'user_id' => $userId,
            ])
            ->with([
                'user' => function ($query) {
                    /**
                     * @var $query UserQuery
                     */
                    $query->active()
                        ->with([
                                'proAccount' => function($query) {
                                    /**
                                     * @var ProAccountQuery $query
                                     */
                                    $query->active();
                                },
                                'profilePrivate' => function($query) {
                                    $query->with('userAvatar');
                                },
                            ]
                        );
                },
                'order',
            ])
            ->asArray()
            ->all();
        //@todo move filter below to separate component
        for ($i = 0; $i < count($orderReviews); $i++) {//remove security related info about user
            if (!empty($orderReviews[$i]['user'])) {
                $user = $orderReviews[$i]['user'];
                $allowedFields = ['id', 'username', 'profilePrivate', 'proAccount', 'created_at', 'updated_at'];
                foreach ($user as $k => $v) {
                    if (!in_array($k, $allowedFields)) {
                        unset($user[$k]);
                    }
                }
                $orderReviews[$i]['user'] = $user;
                if (!empty($orderReviews[$i]['user']['profilePrivate'])) {
                    $allowedFieldsPP = ['first_name', 'last_name', 'userAvatar'];
                    $profilePrivate = $orderReviews[$i]['user']['profilePrivate'];
                    foreach ($profilePrivate as $kP => $kV) {
                        if (!in_array($kP, $allowedFieldsPP)) {
                            unset($profilePrivate[$kP]);
                        }
                    }
                    $orderReviews[$i]['user']['profilePrivate'] = $profilePrivate;
                }
            }
        }

        //get the counters
        $allOrderReviewsCount = count($orderReviews);
        $positiveOrderReviewsCount = $negativeOrderReviewsCount = 0;
        $filteredOrderReviews = [];
        for ($i = 0; $i < $allOrderReviewsCount; $i++) {
            $oW = $orderReviews[$i];
            if ($oW['emotion'] == 1) {
                $positiveOrderReviewsCount++;
            } else if ($oW['emotion'] == 2) {
                $negativeOrderReviewsCount++;
            }
            if (($emotion == 1 && $oW['emotion'] == 2) || ($emotion == 2 && $oW['emotion'] == 1)) {
                continue;
            } else {
                $filteredOrderReviews[] = $oW;
            }
        }
        return [
            'allOrderReviewsCount' => $allOrderReviewsCount,
            'positiveOrderReviewsCount' => $positiveOrderReviewsCount,
            'negativeOrderReviewsCount' => $negativeOrderReviewsCount,
            'orderReviews' => $filteredOrderReviews,
            'userAvatarUploadDir' => UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR,
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;
        $ret = [];
        $userUploadDir = User::UPLOAD_WEB . DIRECTORY_SEPARATOR . $user->upload_dir;
        if ($user) {
            $ret = ArrayHelper::merge([
                //actions for load and get images in profile case editor
                'images-get' => [
                    'class' => GetImagesAction::class,
                    'url' => Yii::$app->params['frontendDomain'] . $userUploadDir, // '/upload/user_content', // URL адрес папки где хранятся изображения.
                    'path' => $user->ensureUploadDirExists(), // Или абсолютный путь к папке с изображениями.
                ],
                'image-upload' => [
                    'class' => 'vova07\imperavi\actions\UploadAction',
                    'url' => Yii::$app->params['frontendDomain'] . $userUploadDir, // '/upload/user_content', // URL адрес папки куда будут загружатся изображения.
                    'path' => $user->ensureUploadDirExists(), // Или абсолютный путь к папке куда будут загружатся изображения.
                ],
            ], parent::actions());
        }
        return $ret;
    }

    /**
     * Perform ajax based validation on the server side
     *
     * @return bool
     */
    /*public function actionValidateCase()
    {
        $request = Yii::$app->request;
        $model = new ProfileCase();
        //ensure that request is post and is ajax
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isPost && $request->isAjax) {
            $post = $request->post();
            if ($model->load($post) && $model->validate()) {
                return true;
            }
        }
        return false;
    }*/

    /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performAjaxValidation(BaseModel $model)
    {
        $request = Yii::$app->request;
        $post = $request->post();
        $result = [];
        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($request->isAjax && $model->load($post)) {
            $formData = $post[$model->formName()];
            if ($formData['birthdateDay'] && $formData['birthdateMonth'] && $formData['birthdateYear']) {
                $model->birth_date = $formData['birthdateYear'].'-'.$formData['birthdateMonth'].'-'.$formData['birthdateDay'].' 23:59:59';
            }
            $result = ActiveForm::validate($model);
        }
        echo Json::encode($result);
        Yii::$app->end();
    }

    /**
     * Validate & Create case through ajax
     * @todo merge code in this method with actionCreatePrivate
     *
     * @return string json encoded string
     */
    public function actionCreateCase($id = 0)
    {
        $post = Yii::$app->request->post();
        if ($id) {
            $profileCase = $this->loadProfileCaseModel($id);
            $view = '_profile_case_update';
        } else {
            $profileCase = new ProfileCase();
            if (!empty($post) && !empty($post[$profileCase->formName()]['modelId'])) {
                $profileCase = $this->loadProfileCaseModel($post[$profileCase->formName()]['modelId']);
            }
            $profileCase->user_id = Yii::$app->user->id;
            $view = '_profile_case_create';
        }
        $profileCase->scenario = ProfileCase::SCENARIO_CREATE_CASE;
        $previewImage = null;
        if (!empty($profileCase->preview_image)) {
            $previewImage = $profileCase->preview_image;
        }

        $profileCase->load($post);
        $profileCase->preview_image = $previewImage;
        if (!empty($post) && $profileCase->validate()) {
            if (!$profileCase->save()) {
                var_dump($profileCase->errors);
                die;
            }
            return $this->redirect(['/profile/index']);
        }
        return $this->render($view, [
            'profileCase' => $profileCase,
        ]);
    }

    /**
     * Upload user avatar
     *
     * @return string
     * @throws HttpException
     */
    public function actionUploadCasePreviewImage()
    {
        $post = Yii::$app->request->post();
        $csrfParam = str_replace('-', '_', Yii::$app->request->csrfParam);

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new ProfileCase();
        $previewImageFile = UploadedFile::getInstance($model, 'preview_image');
        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;
        $directory = $user->ensureUploadDirExists();

        $uid = uniqid(time(), true);
        $fileName = $uid;
        if (!empty($previewImageFile->extension)) {
            $fileName .= '.' . $previewImageFile->extension;
        }

        if (Yii::$app->request->csrfTokenFromHeader == $post[$csrfParam]) {
            $fullPath = $directory . DIRECTORY_SEPARATOR . $fileName;
            if ($previewImageFile->saveAs($fullPath)) {
                if (!empty($post) && !empty($post['modelId'])) {
                    $profileCase = ProfileCase::findOne($post['modelId']);
                } else {
                    $profileCase = new ProfileCase();
                }
                $profileCase->scenario = ProfileCase::SCENARIO_UPLOAD_PREVIEW_IMAGE;
                $profileCase->preview_image = $fileName;
                $profileCase->user_id = Yii::$app->user->id;

                if ($profileCase->save()) {
                    return Json::encode([
                        'info' => [
                            'id' => $profileCase->id,
                        ],
                        'files' => [
                            [
                                'name' => $fileName,
                                'size' => $previewImageFile->size,
                                'url' => $user->uploadDirUrl() . DIRECTORY_SEPARATOR . $fileName,
                                'deleteType' => 'POST',
                            ],
                        ],
                    ]);
                }
            }
            return '';
        } else {
            throw new HttpException(400, Yii::t('app', 'The CSRF token could not be verified.' . Yii::$app->request->csrfTokenFromHeader . ' ' . VarDumper::dumpAsString($post)));
        }
    }

    /**
     * Delete image
     *
     * @param $id
     * @return array json encoded array
     * [
     *  'success' => true
     * ]
     * |
     * [
     *  'error' => string
     * ]
     */
    public function actionPreviewImageDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $userId = Yii::$app->user->id;
        /**
         * @var ProfileCase $model
         */
        $model = ProfileCase::find()->where('id = :id AND user_id = :user_id', ['id' => $id, 'user_id' => $userId])->one();
        $result = [];
        if ($model) {
            if ($model->deletePreviewImage()) {
                $result = [
                    'success' => true,
                ];
            } else {
                $result = [
                    'error' => Yii::t('app/image', 'Can`t delete file from disk'),
                ];
            }
        } else {
            $result = [
                'error' => Yii::t('app/profile_case', 'Preview image does not exist'),
            ];
        }
        return $result;
    }

    protected function loadProfileCaseModel($id)
    {
        $profileCase = ProfileCase::findOne($id);
        if ($profileCase === null) {
            throw new BadRequestHttpException();
        }
        return $profileCase;
    }

    public function actionDeleteCase($id)
    {
        $profileCase = $this->loadProfileCaseModel($id);
        $deleteResult = false;
        if (Yii::$app->user->can('DeleteOwnEntity', [
            'model' => $profileCase,
            'property' => 'user_id'
        ])) {
            $deleteResult = $profileCase->delete();
        }
        if ($deleteResult) {
            $message = Yii::t('app/profile_case', 'Profile case was deleted successfull');
        } else {
            $message = Yii::t('app/profile_case', 'Can`t delete profile case');
        }
        Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/profile_case', $message));
        return $this->redirect(['/profile/index']);
    }

    public function actionCaseView($id)
    {
        $profileCase = $this->loadProfileCaseModel($id);
        $comment = new Comment();

        $comment->from_user_id = Yii::$app->user->id ? Yii::$app->user->id : null;
        $comment->item_id = $id;
        $comment->parent_id = null;

        $rootComments = $comment->getRootComments($id, Comment::COMMENT_TYPE_CASE);

        return $this->render('case_view', [
            'profileCase'   => $profileCase,
            'comment'       => $comment,
            'rootComments'  => $rootComments,
            'commentCount'  => $profileCase->getComment()->count(),
            'currentUserId' => Yii::$app->user->id,
        ]);
    }

    /**
     * Validate & Save private info through ajax
     * @todo merge code in this method with actionCreateCase
     *
     * @return string json encoded string
     */
    public function actionSavePrivate()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        /**
         * @var ProfilePrivate $model
         */
        $model = ProfilePrivate::find()->where('id = :id', ['id' => Yii::$app->user->id])->one();
        if (isset($post[self::AJAX_PARAM])) {
            $this->performAjaxValidation($model);
        } else {
            if ($model->load($post)) {
                $formData = $post[$model->formName()];
                if ($formData['birthdateDay'] && $formData['birthdateMonth'] && $formData['birthdateYear']) {
                    $model->birth_date = $formData['birthdateYear'].'-'.$formData['birthdateMonth'].'-'.$formData['birthdateDay'].' 23:59:59';
                }
                if ($model->save()) {
                    echo Json::encode([]);
                } else {
                    echo Json::encode($model->errors);
                }
                Yii::$app->end();
            }
        }
    }

    /**
     * Return case models (in json encoded format) through ajax
     *
     * @return string json encoded string
     */
    public function actionCases()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;

        $profileCases = ProfileCase::find()->where(['=', 'user_id', Yii::$app->user->id])->orderBy('id')->all();
        echo Json::encode($profileCases);
        Yii::$app->end();
    }

    /**
     * Validate phone through ajax
     *
     * @return string json encoded string
     */
    public function actionValidatePhone()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        $model = new ProfilePhone;
        $model->phone = $post['phone'];
        $result = ActiveForm::validate($model);
        echo Json::encode($result);
        Yii::$app->end();
    }

    /**     *
     * Return the phone in E164 format though ajax
     *
     * @return string json encoded string
     */
    public function actionPhoneE164()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        $phone = $post['phone'];
        $phone = ProfilePhone::format($phone);
        $result = [
            'phone' => $phone,
        ];
        echo Json::encode($result);
        Yii::$app->end();
    }

    /**
     * Validate email through ajax
     *
     * @return string json encoded string
     */
    public function actionValidateEmail()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        $model = new ProfileChangeEmail();
        $email = $post['email'];
        $result = ActiveForm::validate($model);
        echo Json::encode($result);
        Yii::$app->end();
    }

    public function actionCheckEmail()
    { 
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        $email = $post['email'];
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = Yii::getAlias('@currentThemeViewBasePath');
        $mailer->htmlLayout = 'profile/email/layouts/html';

        $changeValueModel = ChangeValue::request($email, 24 * 3600, function() {
             return md5(Yii::$app->security->generateRandomString());
        });
        $url = Url::to(['/profile/confirm-email/' . $changeValueModel->guid], true);

        $result = $mailer->compose([
                'html' => 'profile/email/confirmation'
            ],
            [
                'url' => $url,
            ])
            ->setTo($email)
            ->setFrom(Yii::$app->params['noreplyEmail'])
            ->setSubject(Yii::t('app/profile','Please confirm your new email'))
            ->send();
        if ($result) {
            return [];
        }
    }

    /**
     * Validate data & change password
     */
    public function actionChangePassword()
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        $changePasswordForm = new ChangePasswordForm;
        $result = [];
        if ($changePasswordForm->load($post)) {
            $result = ActiveForm::validate($changePasswordForm);
        }
        if (empty($result)) {
            $newPassword = $post[$changePasswordForm->formName()]['newPassword'];
            if (Yii::$app->modules['user']) {

                $password_hash = Password::hash($newPassword);

            } else {
                $password_hash = Yii::$app->security->generatePasswordHash($newPassword);
            }
            /**
             * @var User $user
             */
            $user = Yii::$app->user->identity;
            $user->password_hash = $password_hash;
            $user->update(true, ['password_hash']);
        }
        echo Json::encode($result);
        Yii::$app->end();
    }


    public function actionConfirmEmail($guid)
    {
        /**
         * @var ChangeValue $model
         */
        $model = ChangeValue::find()->byGuid($guid)->byStatusNot(ChangeValue::STATUS_DELETED)->one();
        if ($model) {
            $newEmail = $model->new_value;
            $user = User::findOne($model->user_id);
            $user->scenario = 'update';
            $user->email = $newEmail;
            if ($user->update(true, ['email'])) {
                Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/profile_change_email', 'Email successfull changed'));
                return $this->redirect(['/profile/index/#settings']);
            } else {
                $errorMessage = '';
                foreach ($user->errors as $error) {
                    foreach ($error as $msg) {
                        $errorMessage .= $msg . ' ';
                    }
                }
                //@todo сообщение об ошибке не видно во всплывающем окне
                Yii::$app->session->setFlash(GritterAlert::TYPE_ERROR, Yii::t('app/profile_change_email', $errorMessage));
                return $this->redirect(['/profile/index/#settings']);
            }
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'The Requested page does not exist'));
        }
    }

    public function actionGetCityList($countryId)
    {
        $q = Yii::$app->request->get('q');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $countryList = Country::find()->where('id = :id', ['id' => $countryId])->with([
            'regions' => function($query) use ($q) {
                $query->with(
                    [
                        'cities' => function($query2) use ($q) {
                            $query2->filterWhere(['like', 'name', $q]);
                        }
                    ]
                );
            }
        ])->all();
        $country = $countryList[0];
        $cities = [];
        foreach ($country->regions as $region) {
            foreach ($region->cities as $city) {
                $obj = new \stdClass();
                $obj->id = $city->id;
                $obj->text = $city->name;
                $cities[] = $obj;
            }
        }
        echo Json::encode($cities);
        Yii::$app->end();
    }

    /**
     * Block user
     *
     * @param $id
     */
    public function actionDeleteProfile($id)
    {
        if ($id == Yii::$app->user->id) {
            $user = User::findOne($id);
            if ($user) {
                $user->blocked_at = time();
                $user->update(true, ['blocked_at']);
                Yii::$app->user->logout(true);
                Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/profile', 'Your account was deleted'));
                return $this->redirect('/project/index');
            }
        } else {
            Yii::$app->session->setFlash(GritterAlert::TYPE_ERROR, Yii::t('app/profile', 'You can`t delete this account'));
            return $this->goHome();
        }
    }

    /**
     * Adds comment and returns updated comment tree html
     *
     * @return bool
     */
    public function actionCaseAddComment()
    {
        $request = Yii::$app->request;

        if (!$request->isAjax) {
            return false;
        }
        Yii::$app->response->format = Response::FORMAT_JSON;
        $post = $request->post();
        if (empty($post['Comment']) || empty($post['Comment']['item_id'])) {
            Yii::$app->response->data['message'] = 'Case id was not found';
            Yii::$app->response->send();
            Yii::$app->end();
        }
        $caseId = $post['Comment']['item_id'];
        $case = ProfileCase::findOne($caseId);
        if (empty($case)) {
            Yii::$app->response->data['message'] = 'Such case was not found';
            Yii::$app->response->send();
            Yii::$app->end();        }

        $comment = new Comment();
        if ($comment->load($post)) {
            $comment->type = Comment::COMMENT_TYPE_CASE;
            if ($post[$comment->formName()]['parent_id']) {
                // find parent node
                $parentNode = Comment::findOne($post[$comment->formName()]['parent_id']);
                if ($comment->appendTo($parentNode)) {
                }
            } else {
                if ($comment->makeRoot()) {
                }
            }
        }

        $comment->from_user_id = Yii::$app->user->id ? Yii::$app->user->id : null;
        $comment->item_id = $caseId;
        $comment->parent_id = null;
        $rootComments = $comment->getRootComments($caseId, Comment::COMMENT_TYPE_CASE);

        Yii::$app->response->data['html'] = $this->renderPartial('/comment/_comment_tree', [
            'rootComments' => $rootComments,
        ]);
        Yii::$app->response->data['commentCount'] = $case->getComment()->count();

        Yii::$app->response->send();
        Yii::$app->end();
    }

}
