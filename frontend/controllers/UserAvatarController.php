<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 12.03.17
 * @time: 15:13
 */
namespace frontend\controllers;

use Yii;
use common\models\UserAvatar;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\AccessControl;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * UserAvatarController implements auxiliary actions for manipulations with UserAvatar model.
 */
class UserAvatarController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['upload', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

    /**
     * Checks that upload directory exists and it writable & readable
     * If not than create dir
     *
     * @return string
     */
    private static function getUploadDirectory()
    {
        $path = Yii::getAlias(UserAvatar::UPLOAD_DIRECTORY);
        if (!file_exists($path)) {
            FileHelper::createDirectory($path, 0755);
        }
        if (!is_writable($path) || !is_readable($path)) {
            chmod($path, 0755);
        }
        return $path;
    }

    /**
     * Upload user avatar
     *
     * @return string
     * @throws HttpException
     */
    public function actionUpload()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new UserAvatar();
        $userAvatarFile = UploadedFile::getInstance($model, 'image');
        $directory = self::getUploadDirectory();
        $uid = uniqid(time(), true);
        $fileName = $uid;
        if (isset($userAvatarFile->extension) && $userAvatarFile->extension) {
            $fileName .= '.' . $userAvatarFile->extension;
        }

        $post = Yii::$app->request->post();
        $csrfParam = str_replace('-', '_', Yii::$app->request->csrfParam);

        if (Yii::$app->request->csrfTokenFromHeader == $post[$csrfParam]) {
            //first we need delete past user avatars
            $userAvatars = UserAvatar::find()
                ->where('user_id = :user_id', ['user_id' => Yii::$app->user->id])
                ->all();
            foreach ($userAvatars as $ua) {
                /**
                 * @var UserAvatar $ua
                 */
                $ua->deleteFile();
                $ua->delete();
            }
            //and save new...
            if ($userAvatarFile->saveAs($directory . DIRECTORY_SEPARATOR . $fileName)) {
                $userAvatar = new UserAvatar();
                $userAvatar->image = $fileName;
                $userAvatar->user_id = Yii::$app->user->id;
                $userAvatar->original_image = $userAvatarFile->name;


                if ($userAvatar->save()) {
                    return Json::encode([
                        'info' => [
                            'id' => $userAvatar->id,
                        ],
                        'files' => [
                            [
                                'name' => $fileName,
                                'originalName' => $userAvatar->original_image,
                                'size' => $userAvatarFile->size,
                                'url' => UserAvatar::UPLOAD_WEB . DIRECTORY_SEPARATOR . $fileName,
                                'deleteType' => 'POST',
                            ],
                        ],
                    ]);
                }
            }
            return '';
        } else {
            throw new HttpException(400, Yii::t('app', 'The CSRF token could not be verified.' . Yii::$app->request->csrfTokenFromHeader . ' ' . VarDumper::dumpAsString($post)));
        }
    }

    /**
     * Delete image
     *
     * @param $id
     * @return array json encoded array
     * [
     *  'success' => true
     * ]
     * |
     * [
     *  'error' => string
     * ]
     */
    public function actionDelete($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $userId = Yii::$app->user->id;
        $userAvatar = UserAvatar::find()->where('id = :id', ['id' => $id])->one();
        $result = [];
        if ($userAvatar) {
            if ($userId == $userAvatar->user_id) {
                if ($userAvatar->deleteFile()) {
                    if ($userAvatar->delete()) {
                        $result = [
                            'success' => true,
                        ];
                    } else {
                        $result = [
                            'error' => Yii::t('app/user_avatar', 'Can`t delete record'),
                        ];
                    }
                } else {
                    $result = [
                        'error' => Yii::t('app/user_avatar', 'Can`t delete file from disk'),
                    ];
                }
            } else {
                $result = [
                    'error' => Yii::t('app/user_avatar', 'You are not allowed to perform this action'),
                ];
            }
        } else {
            $result = [
                'error' => Yii::t('app/user_avatar', 'Image does not exist'),
            ];
        }
        return $result;
    }
}