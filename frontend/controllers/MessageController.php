<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 06.04.17
 * @time: 8:09
 */
namespace frontend\controllers;

use common\models\Batch;
use common\models\Image;
use common\models\Notification;
use common\models\User;
use Yii;
use common\components\Controller;
use common\models\Message;
use yii\data\ArrayDataProvider;
use yii\data\Pagination;

class MessageController extends Controller
{
    /**
     * Выводит список сообщений
     *
     * @return string
     */
    public function actionIndex()
    {
        $pagination = new Pagination;
        $models = [];
        $allModels = Message::find()
            ->listConversation(Yii::$app->user->id)
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $senders = [];
        foreach ($allModels as $m) {
            if (empty($models)) {
                $models[] = $m;
                $senders[] = $m->from_user_id;
                continue;
            }
            $conversationExist = false;
            foreach ($models as $e) {
                if (($e['to_user_id'] == $m['from_user_id']) && ($e['from_user_id'] == $m['to_user_id'])) {
                    $conversationExist = true;
                }
            }
            if (!$conversationExist) {
                $models[] = $m;
                $senders[] = $m->from_user_id;
            }
        }
        $pagination->totalCount = count($models);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'key' => 'id',
            'pagination' => $pagination
        ]);

        $notifications = $this->view->params['notifications'];
        $notificationsByInitiators = Notification::selectNotificationsByInitiator($notifications, $senders);
        $this->view->params['notificationsByInitiators'] = $notificationsByInitiators;

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Показывает форму создания сообщения, и суммарно прошлый диалог
     *
     * @return string
     */
    public function actionCreate($id)
    {
        $user = User::findOne($id);
        $query = Message::find()->meToOrToMeFrom($id)->with([
            'batch' => function($query) {
                $query->andWhere(['entity_type' => Batch::ENTITY_TYPE_MESSAGE])->with('images');
            }
        ]);
        $models = $query->all();

        $model = new Message();
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            //Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/message', 'Message was sent'));
            return $this->redirect(['/message/create', 'id' => $id]);
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'key' => 'id',
            'pagination' => [
                'pageSize' => 50,
            ]
        ]);

        $imageModel = new Image();

        return $this->render('create', [
            'user' => $user,
            'model' => $model,
            'imageModel' => $imageModel,
            'uid' => $id,
            'dataProvider' => $dataProvider,
        ]);
    }
}