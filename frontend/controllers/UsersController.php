<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 19.07.17
 * Time: 18:29
 */
namespace frontend\controllers;

use Yii;
use yii\web\Response;
use common\components\Controller;
use common\models\User;

class UsersController extends Controller
{
    /**
     * Returns user info
     *
     * @return mixed json encoded user info or []
     */
    public function actionGetInfo()
    {
        $uId = Yii::$app->request->post('id');
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = User::findOne($uId);
        $ret = [];
        if ($user) {
            $ret = new class($uId, User::hasPro($uId), $user->username, $user->profilePrivate ? $user->profilePrivate->fullName : '') {
                public $id;

                public $hasPro;

                public $nickname;

                public $fullName;

                function __construct($id, $hasPro, $nickname, $fullName)
                {
                    $this->id = $id;
                    $this->hasPro = $hasPro;
                    $this->nickname = $nickname;
                    $this->fullName = $fullName;
                }
            };
        }
        return $ret;
    }
}

