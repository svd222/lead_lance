<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 09.01.17
 * Time: 16:15
 */
namespace frontend\controllers;

use Yii;
use yii\base\InvalidConfigException;
use yii\web\Controller;
use frontend\models\RegistrationForm;
use dektrium\user\models\LoginForm;
use dektrium\user\models\RecoveryForm;
use dektrium\user\Module;
use dektrium\user\traits\EventTrait;
use dektrium\user\traits\AjaxValidationTrait;

class LandingController extends Controller
{
    use AjaxValidationTrait;
    use EventTrait;
    /**
     * @var string $layout
     */
    public $layout = '//landing_common';

    public function init()
    {
        $requiredModule = Module::className();
        $loadedModules = Yii::$app->loadedModules;
        if (!isset($loadedModules[$requiredModule])) {
            throw new InvalidConfigException($requiredModule. ' required');
        }
        parent::init();

    }

    /**
     * отображает главную страницу лендинга
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/project/index']);
        }
        $registrationForm = $this->getRegistrationForm();
        $loginForm = $this->getLoginForm();
        $recoveryForm = $this->getRecoveryForm();
        $this->view->params['registrationForm'] = $registrationForm;
        $this->view->params['loginForm'] = $loginForm;
        $this->view->params['recoveryForm'] = $recoveryForm;
        $this->view->params['checkedRole'] = $this->checkedRole();

        $request = Yii::$app->request;
        $get = $request->get();
        if (!empty($get['id']) && !empty($get['code'])) {
            $this->view->params['code'] = $get['code'];
            $this->view->params['resetUserId'] = $get['id'];
        }

        return $this->render('index');
    }

    /**
     * отображает страницу лендинга для заказчика
     *
     * @return string
     */
    public function actionCustomer()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/project/index']);
        }
        $registrationForm = $this->getRegistrationForm();
        $loginForm = $this->getLoginForm();
        $recoveryForm = $this->getRecoveryForm();
        $this->view->params['registrationForm'] = $registrationForm;
        $this->view->params['loginForm'] = $loginForm;
        $this->view->params['recoveryForm'] = $recoveryForm;
        $this->view->params['isCustomerPage'] = true;
        $this->view->params['checkedRole'] = $this->checkedRole();
        return $this->render('customer');
    }

    /**
     * отображает страницу лендинга для исполнителя
     *
     * @return string
     */
    public function actionExecutor()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/project/index']);
        }
        $registrationForm = $this->getRegistrationForm();
        $loginForm = $this->getLoginForm();
        $recoveryForm = $this->getRecoveryForm();
        $this->view->params['registrationForm'] = $registrationForm;
        $this->view->params['loginForm'] = $loginForm;
        $this->view->params['recoveryForm'] = $recoveryForm;
        $this->view->params['isExecutorPage'] = true;
        $this->view->params['checkedRole'] = $this->checkedRole();
        return $this->render('executor');
    }

    /**
     * Возвращает выбранную роль
     * 1) при сабмите формы
     * 2) в зависимости от actionId
     *
     * @return string
     */
    public function checkedRole()
    {
        $registrationForm = $this->getRegistrationForm();
        $post = Yii::$app->request->post();
        $checkedRole = '';
        if (isset($post[$registrationForm->formName()])) {
            $checkedRole = !empty($post[$registrationForm]['role']) ? $post[$registrationForm]['role'] : '';
        } else {
            $actionId = $this->action->id;
            if ($actionId != 'index') {
                if ($actionId == 'executor') {
                    $checkedRole = RegistrationForm::ROLE_EXECUTOR;
                } elseif ($actionId == 'customer') {
                    $checkedRole = RegistrationForm::ROLE_CUSTOMER;
                }
            }
        }
        return $checkedRole;
    }

    /**
     * Возвращает модель регистрационной формы
     *
     * @return \dektrium\user\models\RegistrationForm
     */
    public function getRegistrationForm()
    {
        return Yii::createObject(RegistrationForm::className());
    }

    /**
     * Возвращает модель формы входа
     *
     * @return \dektrium\user\models\LoginForm
     */
    public function getLoginForm()
    {
        return Yii::createObject(LoginForm::className());
    }

    /**
     * Возвращает модель формы восстановления пароля
     *
     * @return \dektrium\user\models\RecoveryForm
     */
    public function getRecoveryForm()
    {
        return Yii::createObject(RecoveryForm::className());
    }
}
