<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/19/2018
 * Time: 10:23 PM
 */
namespace frontend\controllers;

use Yii;
use dektrium\user\controllers\RecoveryController as BaseRecovery;
use dektrium\user\models\RecoveryForm;
use yii\base\Model;
use yii\web\Response;
use yii\bootstrap\ActiveForm;
use dektrium\user\models\Token;
use dektrium\user\models\User;
use dektrium\user\Mailer;

class RecoveryController extends BaseRecovery
{
    /**
     * Shows page where user can request password recovery.
     *
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionRequest()
    {
        /** @var RecoveryForm $model */
        $model = \Yii::createObject([
            'class'    => RecoveryForm::className(),
            'scenario' => RecoveryForm::SCENARIO_REQUEST,
        ]);

        $this->performRecoveryAjaxValidation($model);
        $post = \Yii::$app->request->post();

        $user = $this->finder->findUserByEmail($post['recovery-form']['email']);
        if (empty($user)) {
            Yii::$app->response->data['success'] = 0;
            Yii::$app->response->data['recovery-form-email'] = [\Yii::t('app/user', 'A user with such email address does not exist')];
            Yii::$app->response->send();
            Yii::$app->end();
        }
        Yii::$app->response->send();

        $event = $this->getFormEvent($model);
        $this->trigger(self::EVENT_BEFORE_REQUEST, $event);

        if ($model->load($post)) {
            if (!$model->validate()) {
                return false;
            }

            if ($user instanceof User) {
                /** @var Token $token */
                $token = \Yii::createObject([
                    'class' => Token::className(),
                    'user_id' => $user->id,
                    'type' => Token::TYPE_RECOVERY,
                ]);

                if (!$token->save(false)) {
                    return false;
                }

                $mailer = new Mailer();
                if (!$mailer->sendRecoveryMessage($user, $token)) {
                    return false;
                }
            }
            $this->trigger(self::EVENT_AFTER_REQUEST, $event);
        }

    }

    /**
     * Performs ajax validation.
     * @param Model $model
     * @throws \yii\base\ExitException
     */
    protected function performRecoveryAjaxValidation(Model $model)
    {
        $request = Yii::$app->request;
        $post = $request->post();
        if ($request->isAjax && $model->load($post)) {
            $result = ActiveForm::validate($model);
            Yii::$app->response->format = Response::FORMAT_JSON;

            if (!empty($result)) {
                Yii::$app->response->data = $result;
                Yii::$app->response->send();
                Yii::$app->end();
            } else {
                $result = [
                    'success' => true,
                ];
                Yii::$app->response->data = $result;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function actionReset($id, $code)
    {
        if (!Yii::$app->request->isAjax) {
            $this->redirect('/landing?id=' . $id . '&code=' . $code);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var Token $token */
        $token = $this->finder->findToken(['user_id' => $id, 'code' => $code, 'type' => Token::TYPE_RECOVERY])->one();
        if (empty($token) || ! $token instanceof Token || $token->isExpired || $token->user === null) {
            Yii::$app->response->data['message'] = [\Yii::t('user', 'Recovery link is invalid or expired. Please try requesting a new one.')];
            Yii::$app->response->send();
            Yii::$app->end();
        }
        $event = $this->getResetPasswordEvent($token);

        /** @var RecoveryForm $model */
        $model = \Yii::createObject([
            'class'    => RecoveryForm::className(),
            'scenario' => RecoveryForm::SCENARIO_RESET,
        ]);
        $event->setForm($model);

        $this->performRecoveryAjaxValidation($model);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->resetPassword($token)) {
            Yii::$app->response->send();
        }
        Yii::$app->end();
    }
}
