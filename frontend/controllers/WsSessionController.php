<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 03.07.17
 * @time: 19:40
 */
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use common\models\WsSession;

/**
 * Class WsSessionController process ws session
 *
 * @package frontend\controllers
 */
class WsSessionController extends Controller
{
    /**
     * Register ws session
     *
     * @return \stdClass
     */
    public function actionRegister()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $resourceId = Yii::$app->request->post('resourceId');
        $obj = new \stdClass();
        $obj->success = false;
        if ($resourceId) {
            /**
             * @var WsSession $wsSession
             */
            $wsSession = WsSession::find()
                ->last()
                ->byResourceId($resourceId)
                ->notRegistered()
                ->one();

            $wsSession->status = WsSession::STATUS_REGISTERED;
            if ($wsSession->update(true, ['status'])) {
                $obj->success = true;
            }
        }
        return $obj;
    }
}