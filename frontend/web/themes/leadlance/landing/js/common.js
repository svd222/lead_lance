$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });


    $(document).on('click', '.btn-select', function (e) {
        e.preventDefault();
        var ul = $(this).find("ul");
        if ($(this).hasClass("active")) {
            if (ul.find("li").is(e.target)) {
                var target = $(e.target);
                target.addClass("selected").siblings().removeClass("selected");
                var value = target.html();
                $(this).find(".btn-select-input").val(value);
                $(this).find(".btn-select-value").html(value);
            }
            ul.hide();
            $(this).removeClass("active");
        }
        else {
            $('.btn-select').not(this).each(function () {
                $(this).removeClass("active").find("ul").hide();
            });
            ul.slideDown(300);
            $(this).addClass("active");
        }
    });

    $(document).on('click', function (e) {
        var target = $(e.target).closest(".btn-select");
        if (!target.length) {
            $(".btn-select").removeClass("active").find("ul").hide();
        }
    });




    //Link distributions
    $('.distribLink').on('click', function (event) {
        event.preventDefault();
        $('.distribLink').removeClass('active');
        var elem = $('.accordionContent'),
            elemTitle = $('.titlesAccordionBlock');
        $(this).addClass('active');
        if ($(this).hasClass('open-all')){
            elem.slideDown();
            elem.addClass('open');
            elemTitle.addClass('open');
        } else {
            elem.slideUp();
            elem.removeClass('open');
            elemTitle.removeClass('open');
        }
    });
    $('.titlesAccordion a').on('click', function(e){
        if(!$(this).hasClass('open')){
            e.preventDefault();
           $(this).parent().parent().parent().next().slideDown();
           $(this).parent().parent().parent().next().addClass('open');
        }
    });

    $('.distribLink-left').on('click', function (event) {
        event.preventDefault();
        $('.distribLink-left').removeClass('activeDistribLink');
        $(this).addClass('activeDistribLink');
    });

    /*dropdown menu*/
    $(document).on('click', '.submenu-dropdown', function(e){
        e.preventDefault();
        var elem = $(this);
        if(elem.hasClass('open')){
            elem.removeClass('open');
            elem.children().addClass('fa-angle-right').removeClass('fa-angle-down');
            elem.siblings('.menu-dropdown').slideUp();
        } else {
            elem.addClass('open');
            elem.children().removeClass('fa-angle-right').addClass('fa-angle-down');
            elem.siblings('.menu-dropdown').slideDown();
        }
    });
    /*dropdown menu*/

    /*left main menu*/

    $(document).on('click', '.toogle-menu-btn', function(){
        toogleMenu();
    });

    $(document).on('click', '.left-menu-icon', function (e) {
        e.stopPropagation();
        e.preventDefault();
        if($(window).width() < 768) {
            toogleMenu();
        }
    });

    function toogleMenu(){
        $('.submenu-dropdown').removeClass('open').children().addClass('fa-angle-right').removeClass('fa-angle-down');
        $('.menu-dropdown').css('display','none');
        $('.wrapperNav').toggleClass('closed');
        $('.wrapperContent').toggleClass('menu-closed').toggleClass('menu-open') ;
    }

    /*left main menu*/
    $(document).on('click', '.projectFIlterText a' , function(e){
        e.preventDefault();
        $('.filtersForm').slideToggle().toggleClass('open');
        if($('.filtersForm').hasClass('open')){
            $(this).text(' свернуть');
        } else {
            $(this).text(' развернуть');
        }
    });


    //border-menu added
    var menuBord = $('.left-menu-toogle');
    menuBord.on('click','li', function (event) {
        // event.preventDefault();

        menuBord.find('a').removeClass('active');
        $(this).find('a').first().addClass('active');
    });

    var avatarElem = document.getElementById('linkToTop');

    $(function (){
        $("#linkToTop").hide();

        $(window).scroll(function (){
            if ($(this).scrollTop() > 100){
                $("#linkToTop").fadeIn();
            } else{
                $("#linkToTop").fadeOut();
            }
        });

        $("#linkToTop a").click(function (){
            $("body,html").animate({
                scrollTop:0
            }, 800);
            return false;
        });
    });



    //Nav burger
    var navTop = $('.navList');
    $('.navBurgers').on('click', function () {
        navTop.slideToggle();
    });

    function menuAddClass () {
        if($(window).width() > 767) {
            $('.wrapperNav').removeClass('closed');
            $('.wrapperContent').removeClass('menu-closed').addClass('menu-open');
        } else {
            $('.wrapperNav').addClass('closed');
            $('.wrapperContent').removeClass('menu-open').addClass('menu-closed');
        }
    }

    menuAddClass();
    $(window).on('resize', function () {
        if($(window).width() > 767) {
            $('.wrapperNav').removeClass('closed');
            $('.wrapperContent').removeClass('menu-closed').addClass('menu-open');

        } else {
            $('.wrapperNav').addClass('closed');
            $('.wrapperContent').removeClass('menu-open').addClass('menu-closed');

        }
        if($(window).width() > 991) {
            navTop.css({
                "display":"block"
            });
        } else {
            navTop.css({
                "display":"none"
            });
        }
    });


    //paginations
    $('.pagination').on('click', 'li', function () {
        $('.pagination').children('li').removeClass('active');
        $(this).addClass('active');
    })

    //swipe
    // $(function(){
    //     // Bind the swipeHandler callback function to the swipe event on div.box
    //     $( ".wrapperNav" ).on( "swipe", swipeHandler );
    //
    //     // Callback function references the event target and adds the 'swipe' class to it
    //     function swipeHandler( event ){
    //         if($( ".wrapperNav" ).hasClass('closed')) {
    //             $( ".wrapperNav" ).removeClass( "closed" );
    //         } else {
    //             $( ".wrapperNav" ).addClass( "closed" );
    //         }
    //
    //     }
    // });

    //custom scroll bar
    scrollBarSize();
    $(window).on('resize', function () {
        scrollBarSize();
    });

    // function scrollBarSize() {
    //     if($(window).height() < 570) {
    //         $('.wrap-swipe').customScrollbar({
    //             hScroll: false,
    //             updateOnWindowResize: true
    //         });
    //     } else {
    //         $('.wrap-swipe').customScrollbar("remove")
    //     }
    // }






//
//     var isMobile = false; //initiate as false
// // device detection
//     if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
//         || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) {
//         isMobile = true;
//     }




    $('.sroll-block-keys').customScrollbar({
        hScroll: false,
        updateOnWindowResize: true
    });








    function scrollBarSize(isMobile) {
        if($(window).height() < 570) {
            $('.wrapperNav').customScrollbar({
                hScroll: false,
                updateOnWindowResize: true
            });
        } else {
            $('.wrapperNav').customScrollbar("remove")
        }
    }
    $(document).on('click', '.select-next, .select-prev', function(e){
        e.preventDefault();
    });
    $(document).on('click', '.inputFiltersStyle', function(){
        $(this).parent().children('.wrapper-dropbox-input').fadeIn();
    });
    $(document).on('blur', '.inputFiltersStyle', function(){
        $(this).parent().children('.wrapper-dropbox-input').fadeOut();
    })

    $('.tabs-nav-list').find('li').hover(function () {
        $('.tabs-nav').css({'border-color':"transparent"})
    }, function () {
        $('.tabs-nav').css({'border-color':"#e8e8e8"})
    });


    //tabs profile
    var navsTabs = $('.tabs-nav-list').find('li');

    navsTabs.on('click', function () {
        var thisObtIndex = $(this).index();

        navsTabs.removeClass('active-link-tabs');
        $(this).addClass('active-link-tabs');

        $('.tabs-content').css({'display':'none'});
        $('.tabs-content').removeClass('active-tabs').eq(thisObtIndex).fadeIn(500).addClass('active-tabs');
    });

/*$('.buttonFilters,.modal-link-projects').on('click', function (event) {
    event.preventDefault();
});*/



    var linkReviewsAll = $('.link-reviews-drop-down');

    //$('.reviews-blue-count').text($('.reviews-blue-count').closest('.block-rewievs-print').prev('.wrapper-slide-reviews.hide-main').find('.name-block-answer-top-min-padd').length);
    linkReviewsAll.on('click', function (event) {
        event.preventDefault();

        var allRevBlock = $(this).closest('.block-rewievs-print').prev('.wrapper-slide-reviews'),
            countRevNumb = $(this).prev(),
            countRev = allRevBlock.find('.reviews-all-block-rout').length;
        if($(this).hasClass('up-rev-act')) {
            $(this).text('Развернуть').prev().removeClass('not-blue-count');
            allRevBlock.slideUp('slow',function () {
                $(event.target).removeClass('up-rev-act');
            });
            $(this).closest('.block-rewievs-print').next('.print-answer-block-all').slideUp();
        } else {
            $(this).text('Свернуть').prev().addClass('not-blue-count');
            countRevNumb.text(countRev);
            allRevBlock.slideDown('slow',function () {
                $(event.target).addClass('up-rev-act');
            });
            $(this).closest('.block-rewievs-print').next('.print-answer-block-all').slideDown();
        }
    });
    /* select2 */
    $('.coast-lead,.select-city').select2({
        minimumResultsForSearch: -1
    });
    /* select2 */

    $("#create-keys").on("shown.bs.modal", function () {
        $('.sroll-block-keys').customScrollbar({
            hScroll: false,
            updateOnWindowResize: true
        });
    });

});
