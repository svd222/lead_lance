function leadZero(v) {
    if (v < 10) {
        v = '0' + v.toString();
    }
    return v;
}

$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });


    $(document).on('click', '.btn-select', function (e) {
        e.preventDefault();
        var ul = $(this).find("ul");
        if ($(this).hasClass("active")) {
            if (ul.find("li").is(e.target)) {
                var target = $(e.target);
                target.addClass("selected").siblings().removeClass("selected");
                var value = target.html();
                $(this).find(".btn-select-input").val(value);
                $(this).find(".btn-select-value").html(value);
            }
            ul.hide();
            $(this).removeClass("active");
        }
        else {
            $('.btn-select').not(this).each(function () {
                $(this).removeClass("active").find("ul").hide();
            });
            ul.slideDown(300);
            $(this).addClass("active");
        }
    });

    $(document).on('click', function (e) {
        var target = $(e.target).closest(".btn-select");
        if (!target.length) {
            $(".btn-select").removeClass("active").find("ul").hide();
        }
    });




    //Link distributions
    $('.distribLink').on('click', function (event) {
        event.preventDefault();
        $('.distribLink').removeClass('active');
        var elem = $('.accordionContent'),
            elemTitle = $('.titlesAccordionBlock');
        $(this).addClass('active');
        if ($(this).hasClass('open-all')){
            elem.slideDown();
            elem.addClass('open');
            elemTitle.addClass('open');
        } else {
            elem.slideUp();
            elem.removeClass('open');
            elemTitle.removeClass('open');
        }
    });
    $('.titlesAccordion a').on('click', function(e){
        if(!$(this).hasClass('open')){
            e.preventDefault();
           $(this).parent().parent().parent().next().slideDown();
           $(this).parent().parent().parent().next().addClass('open');
        }
    });

    /*$('.distribLink-left').on('click', function (event) {
        event.preventDefault();
        $('.distribLink-left').removeClass('activeDistribLink');
        $(this).addClass('activeDistribLink');
    });*/

    //@todo это пипец костыль, как будет время сделать норм фильтры
    //Фильтры на проектах и заказах в профиле
    $('.distribLink-left').on('click', function () {
        var url = $(this).prop('href');
        var currentUrl = window.location.href;
        if (currentUrl != url) {
            window.location.href = url;
        }
    });

    //@todo это пипец костыль, как будет время сделать норм фильтры
    //Фильтры на проектах и заказах в профиле
    $('.distribLink-left').each(function (index, elm) {
        var url = $(this).prop('href');
        var currentUrl = window.location.href;
        if (url == currentUrl) {
            $(this).siblings().removeClass('activeDistribLink');
            $(this).addClass('activeDistribLink');
        }
    });

    /*dropdown menu*/
    $(document).on('click', '.submenu-dropdown', function(){
        var elem = $(this);
        if(elem.hasClass('open')){
            elem.removeClass('open');
            elem.children().addClass('fa-angle-right').removeClass('fa-angle-down');
            elem.siblings('.menu-dropdown').slideUp();
        } else {
            elem.addClass('open');
            elem.children().removeClass('fa-angle-right').addClass('fa-angle-down');
            elem.siblings('.menu-dropdown').slideDown();
        }
    });
    /*dropdown menu*/

    /*left main menu*/

    $(document).on('click', '.toogle-menu-btn', function(){
        toogleMenu();
    });

    $(document).on('click', '.left-menu-icon', function (e) {
        e.stopPropagation();
        e.preventDefault();
        if($(window).width() < 768) {
            toogleMenu();
        }
    });

    function toogleMenu(){
        $('.submenu-dropdown').removeClass('open').children().addClass('fa-angle-right').removeClass('fa-angle-down');
        $('.menu-dropdown').css('display','none');
        $('.wrapperNav').toggleClass('closed');
        $('.wrapperContent').toggleClass('menu-closed').toggleClass('menu-open') ;
    }

    /*left main menu*/
    $(document).on('click', '.projectFIlterText a' , function(e){
        e.preventDefault();
        $('.filtersForm').slideToggle().toggleClass('open');
        if($('.filtersForm').hasClass('open')){
            $(this).text(' свернуть');
        } else {
            $(this).text(' развернуть');
        }
    });


    //border-menu added
    var menuBord = $('.left-menu-toogle');
    menuBord.on('click','li', function (event) {
        //event.preventDefault();

        menuBord.find('a').removeClass('active');
        $(this).find('a').first().addClass('active');
    });

    var avatarElem = document.getElementById('linkToTop');

    $(function (){
        $("#linkToTop").hide();

        $(window).scroll(function (){
            if ($(this).scrollTop() > 100){
                $("#linkToTop").fadeIn();
            } else{
                $("#linkToTop").fadeOut();
            }
        });

        $("#linkToTop a").click(function (){
            $("body,html").animate({
                scrollTop:0
            }, 800);
            return false;
        });
    });



    //Nav burger
    var navTop = $('.navList');
    $('.navBurgers').on('click', function () {
        navTop.slideToggle();
    });

    function menuAddClass () {
        if($(window).width() > 767) {
            $('.wrapperNav').removeClass('closed');
            $('.wrapperContent').removeClass('menu-closed').addClass('menu-open');
        } else {
            $('.wrapperNav').addClass('closed');
            $('.wrapperContent').removeClass('menu-open').addClass('menu-closed');
        }
    }

    menuAddClass();
    $(window).on('resize', function () {
        if($(window).width() > 767) {
            $('.wrapperNav').removeClass('closed');
            $('.wrapperContent').removeClass('menu-closed').addClass('menu-open');
            navTop.css({
                "display":"block"
            });
        } else {
            $('.wrapperNav').addClass('closed');
            $('.wrapperContent').removeClass('menu-open').addClass('menu-closed');
            navTop.css({
                "display":"none"
            });
        }
    });


    //paginations
    $('.pagination').on('click', 'li', function () {
        $('.pagination').children('li').removeClass('active');
        $(this).addClass('active');
    })

    //custom scroll bar
    scrollBarSize();
    $(window).on('resize', function () {
        scrollBarSize();
    });

    function scrollBarSize() {
        if($(window).height() < 570) {
            $('.wrapperNav').customScrollbar({
                hScroll: false,
                updateOnWindowResize: true
            });
        } else {
            $('.wrapperNav').customScrollbar("remove")
        }
    }
    $(document).on('click', '.select-next, .select-prev', function(e){
        e.preventDefault();
    });
    $(document).on('click', '.inputFiltersStyle', function(){
        $(this).parent().children('.wrapper-dropbox-input').fadeIn();
    });
    /*$(document).on('blur', '.inputFiltersStyle', function(){
        $(this).parent().children('.wrapper-dropbox-input').fadeOut();
    })*/

    /*$('div.wrapper-dropbox-input .exceptionText input').on('click', function () {
        console.log($(this).closest('label'));
        console.log($(this).prop('checked'));
    })*/

    $('div.wrapper-dropbox-input > a.change-filter').on('click', function () {
        var parent = $(this).parent();
        parent.fadeOut();
    })

    /*$('#ProjectFilterForm').on('submit', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var form = $(this);
        form.find('input[name="ProjectFilterForm[project_type][]"]').each(function (index, elm) {
            console.log(elm);
            console.log($(elm).prop('checked'));
        })
    });*/

    $('.container .projectDistributor .filter-range').each(function (obj, callback) {
        $(this).on('click', function(e) {
            var range = this.dataset.range;
            $('#projectfilterform-range').val(range);
            $('form#ProjectFilterForm').submit();
        });
    });

    $('.tabs-nav-list').find('li').hover(function () {
        $('.tabs-nav').css({'border-color':"transparent"})
    }, function () {
        $('.tabs-nav').css({'border-color':"#e8e8e8"})
    });



    //tabs profile
    var navsTabs = $('.tabs-nav-list').find('li');

    navsTabs.on('click', function () {
        var thisObtIndex = $(this).index();

        navsTabs.removeClass('active-link-tabs');
        $(this).addClass('active-link-tabs');

        var navsTabsContent = $('.tabs-content');
        navsTabsContent.css({'display':'none'});
        navsTabsContent = navsTabsContent.eq(thisObtIndex);

        navsTabsContent.fadeIn(500).addClass('active-tabs');
        reloadProfileTabContent(navsTabsContent);
    });

    /*$('.person .left-menu-toogle li a').each(function (index, elm) {
        var href =  $(this).prop('href');
        if (href != '#' && ((new RegExp('#')).test(href))) {
            $(this).on('click', function (index, elm) {
                if (href != window.location.href) {
                    window.location.href = href;
                }
            })
        }
    });*/


    /* select2 */
    $('.coast-lead').select2({
        minimumResultsForSearch: -1
    });
    /* select2 */

    /*$("#create-keys").on( "shown", function () {
        $('.sroll-block-keys').customScrollbar({
            hScroll: false,
            updateOnWindowResize: true
        });
    });*/

    $("#create-keys").on("shown.bs.modal", function () {
        $('.sroll-block-keys').customScrollbar({
            hScroll: false,
            updateOnWindowResize: true
        });
    });

    //wrapper for popup gritter alert messages  https://github.com/jboesch/Gritter/wiki
    var gritterAdd = function($title, $text, $class_name, $image, $options) {
        console.log($.gritter);
        if ($options === undefined) {
            $options = {time:7000};
        }
        if ($image === undefined) {
            $image = '';
        }

        $.extend($.gritter.options, $options);
        $.gritter.add({title: $title, text: $text, image: $image, class_name: $class_name});
    }

    //show selected items below checkbox list when apply button clicked (for project types and traffic types)
    $('.wrapper-dropbox-input .change-filter').on('click', function(e) {
        var checkBoxContainer = $(this).closest('.wrapper-dropbox-input');
        if (typeof checkBoxContainer.get(0) != "undefined") {
            var wrapperCustomSelect = $(this).closest('.wrapper-custom-select');
            var listTagsContainer = $('ul.listTags', wrapperCustomSelect);

            //checks that ul container exists and create them if not found
            if (typeof listTagsContainer.get(0) == "undefined") {
                $('<ul class="listTags"></ul>').appendTo('.wrapper-custom-select');
                listTagsContainer = $('ul.listTags', wrapperCustomSelect);
            }

            //clear list
            $('li', listTagsContainer).each(function (index, item) {
                $(this).remove();
            });

            //pass through each selected item and add them to list
            $('.exceptionText', checkBoxContainer).each(function (index, item) {
                if ($(item).find('input').prop('checked')) {
                    var label = $(item).text().trim();
                    //get random color
                    var action = collection.get('controller').get('action');
                    var colorsCssClasses = action.get('colorsCssClasses');
                    var rnd = Math.floor(Math.random() * (colorsCssClasses.length - 0)) + 0;
                    var color = colorsCssClasses[rnd];
                    $('<li><a href="#" class="' + color + ' tagsFontStyle">' + label + ' ' + '</a><span class="tagsClose greenTags">x</span></li>').appendTo(listTagsContainer);
                }
            });

            //uncheck and remove item when X link is clicked
            $('.tagsClose').on('click', function (e) {
                var label = $(this).siblings('a').text().trim();
                $('.exceptionText', checkBoxContainer).each(function (index, item) {
                    var checkedText = $(item).text().trim();
                    if (checkedText == label) {
                        $(item).find('input').prop('checked', false);
                    }
                });
                $(this).closest('li').remove();
            });
        }
    })
});


