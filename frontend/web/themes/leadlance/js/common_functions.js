/**
 * Created by svd on 19.07.17.
 */
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

function getConversationId(id1, id2) {
    var ret;
    if (id1 < id2) {
        ret = id1 + '_' + id2;
    } else {
        if (id2 < id1) {
            ret = id2 + '_' + id1;
        } else {
            throw new Error('ids are equal');
        }
    }
    return ret;
}