$(document).on('wsOrderMessage', function(e) {
    var wsTransferObject = e.detail;
    var currentRoute = collection.get('currentRoute')

    if (currentRoute.controller == 'order' && $.isArray(currentRoute.params) && currentRoute.params.length == 0 && (parseInt(currentRoute.action) == currentRoute.action)) {
        // action is view
        // update current conversation
        var data = {
            id: wsTransferObject.attributes.id,
            from_user_id: wsTransferObject.attributes.from_user_id,
            to_user_id: wsTransferObject.attributes.to_user_id,
            message: wsTransferObject.attributes.message,
            order_id: wsTransferObject.attributes.order_id,
            created_at: wsTransferObject.attributes.created_at,
        };

        if (typeof wsTransferObject.attributes.images != "undefined") {
            data.images = wsTransferObject.attributes.images;
        }

        var msgBlock = $(getOrderMessage(data));
        var formContainer = $('#order-message-form-container');
        formContainer.after(msgBlock);
    } else {
        // update left menu counter
        var orderCounterContainer = $('ul.left-menu-toogle li a.menu-orders');
        var orderCounter = orderCounterContainer.find('.menu-counter');
        if (typeof orderCounter.get(0) != "undefined") {
            var orderCounterVal = parseInt(orderCounter.text()) + 1;
            orderCounter.text(orderCounterVal);
        } else {
            var beforeContainer;
            var orderActionCounter = orderCounterContainer.find('.menu-counter-red');
            if (typeof orderActionCounter.get(0) != "undefined") {
                beforeContainer = orderActionCounter;
            } else {
                beforeContainer = orderCounterContainer.find('.menu-text');
            }
            beforeContainer.after('<span class="menu-counter" id="menu-counter-orders-messages">1</span>');
        }

        $('ul#order-project-list li').each(function (index, elm) {
            var linkContainer = $(this).find('a');
            /*console.log(linkContainer.get(0));
            console.log(linkContainer.get(0).dataset.orderId);
            console.log(wsTransferObject);*/
            if (linkContainer.get(0).dataset.orderId == parseInt(wsTransferObject.attributes.order_id)) {
                console.log('found');
                var orderActionMessageCounter = linkContainer.find('span.submenu-counter');
                var orderActionMessageCounterVal = 0;
                if (typeof orderActionMessageCounter.get(0) != 'undefined') {
                    orderActionCounterVal = parseInt(orderActionMessageCounter.text()) + 1;
                    orderActionMessageCounter.text(orderActionCounterVal);
                } else {
                    $('<span class="submenu-counter">1</span>').appendTo(linkContainer);
                }
            }
        })
    }
});