/**
 * Created by svd on 30.07.17.
 */
$(document).on('wsActionProjectUserRefuse', function (e) {
    var wsTransferObject = e.detail;
    var allProjectActionLeftCounterContainer = $('div.person ul.left-menu-toogle > li > a.menu-projects');
    console.log(allProjectActionLeftCounterContainer.get(0));
    var allProjectActionLeftCounter = $('span.menu-counter-red', allProjectActionLeftCounterContainer); //menu-counter-red
    if (typeof allProjectActionLeftCounter.get(0) !== "undefined") {
        var allProjectActionLeftCounterVal = allProjectActionLeftCounter.text();
        allProjectActionLeftCounterVal++;
        allProjectActionLeftCounter.text(allProjectActionLeftCounterVal);
    } else {
        var mt = allProjectActionLeftCounterContainer.find('.menu-text');
        console.log(mt.get(0));
        mt.after($('<span class="menu-counter-red" id="menu-counter-red-actions">1</span>'));
    }

    // refresh project messages counters by project
    var allProjectsMessagesContainer = $('.left-menu-toogle > li > ul#person-project-list li');
    var projectMessageCounter = $('a', allProjectsMessagesContainer); //  span.submenu-counter
    projectMessageCounter.each(function (index, elm) {
        if (parseInt($(this).get(0).dataset.projectId) == parseInt(wsTransferObject.attributes.project_id)) {
            var projectMessageCounter = $('span.submenu-counter-red', $(this));
            var projectMessageCounterVal = 0;
            if (typeof projectMessageCounter.get(0) != "undefined") {
                projectMessageCounterVal = parseInt(projectMessageCounter.text()) + 1;
                projectMessageCounter.text(projectMessageCounterVal);
            } else {
                $(this).find('.submenu-text').after($('<span class="submenu-counter-red">' + (projectMessageCounterVal + 1) + '</span>'));
            }
        }
    })
});
