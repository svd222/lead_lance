/**
 * Created by svd on 06.07.17.
 */
/*$(document).on('wsMessage', function (e) {
    var wsTransferObject = e.detail;
    var menuCounter = $('#menu-counter-messages');
    var menuCounterVal = 0;

    if (menuCounter.get(0) !== undefined) {
        menuCounterVal = parseInt(menuCounter.html());
        if (!menuCounterVal) {
            menuCounterVal = 0;
        }
        menuCounter.html(menuCounterVal + 1);
    } else {
        $('<span class="menu-counter" id="menu-counter-messages">' + (menuCounterVal + 1) + '</span>').appendTo($('.left-menu-toogle li a.menu-messages'));
    }
});*/

$(document).on('wsMessage', function (e) {
    var wsTransferObject = e.detail;
    var allMessagesContainer = $('div.dialog-wrap div.counter-mess');
    var allMessagesCounter = $('a span', allMessagesContainer);
    var allMessagesCounterVal = 0;

    if (allMessagesCounter.get(0) !== undefined) {
        allMessagesCounterVal = parseInt(allMessagesCounter.html());
        if (!allMessagesCounterVal) {
            allMessagesCounterVal = 0;
        }
        allMessagesCounter.html(parseInt(allMessagesCounterVal + 1));
    } else {
        $('<span>' + (allMessagesCounterVal + 1) + '</span>').appendTo(allMessagesContainer);
    }

    var newMessagesContainer = $('div.open-dialog');
    var newMessagesCounter = $('a span.counter', newMessagesContainer);
    var newMessagesCounterVal = 0;

    if (newMessagesCounter.get(0) !== undefined) {
        newMessagesCounterVal = parseInt(newMessagesCounter.html());
        if (!newMessagesCounterVal) {
            newMessagesCounterVal = 0;
        }
        newMessagesCounter.html(parseInt(newMessagesCounterVal + 1));
    } else {
        $('<span class="counter">' + (newMessagesCounterVal + 1) + '</span>').appendTo(newMessagesContainer);
    }
});


