/**
 * Created by svd on 06.07.17.
 */
$(document).on('wsMessage', function (e) {
    var wsTransferObject = e.detail;
    var menuCounter = $('#menu-counter-messages');
    var menuCounterVal = 0;

    if (menuCounter.get(0) !== undefined) {
        menuCounterVal = parseInt(menuCounter.html());
        if (!menuCounterVal) {
            menuCounterVal = 0;
        }
        menuCounter.html(menuCounterVal + 1);
    } else {
        $('<span class="menu-counter" id="menu-counter-messages">' + (menuCounterVal + 1) + '</span>').appendTo($('.left-menu-toogle li a.menu-messages'));
    }
});