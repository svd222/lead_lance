$(document).on('wsOrderReview', function(e) {
    var wsTransferObject = e.detail;

    var personOrderReviewDecorator = function (val) {
        return '<span class="order-review-counter-new">' + val + '</span>';
    }

    var orderReviewPersonMainContainer = $('div.person ul.person-active');
    var orderReviewPersonContainer = orderReviewPersonMainContainer.find('li#person-order-review-count');

    if (typeof orderReviewPersonContainer.get(0) != "undefined") {
        var orderReviewPerson = orderReviewPersonContainer.find('a.person-message');
        //if (typeof orderReviewPerson.get(0) != "undefined") {
            var orderReviewPersonVal = orderReviewPerson.text();
            //console.log(orderReviewPersonVal);
            if (orderReviewPersonVal.indexOf('+') > -1) {
                orderReviewPersonVal = orderReviewPersonVal.split('+');
                orderReviewPersonVal = parseInt(orderReviewPersonVal[0]).toString() + ' ' + personOrderReviewDecorator('+' + (parseInt(orderReviewPersonVal[1]) + 1).toString());
            } else {
                orderReviewPersonVal = orderReviewPersonVal.toString() + personOrderReviewDecorator('+1');
            }
            orderReviewPerson.html(orderReviewPersonVal);
        //}
    } else {
        $('<li class="div-inline" id="person-order-review-count"><a href="/profile/index" class="person-message">1</a></li>').appendTo(orderReviewPersonMainContainer);
    }

});