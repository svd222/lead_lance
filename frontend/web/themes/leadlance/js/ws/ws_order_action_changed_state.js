/**
 * Created by svd on 06.07.17.
 */
$(document).on('wsActionOrderChangedState', function (e) {

    var wsTransferObject = e.detail;
    /*console.log(wsTransferObject);*/

    var orderCounterContainer = $('ul.left-menu-toogle li a.menu-orders');
    var orderCounter = orderCounterContainer.find('span.menu-counter-red');
    if (typeof orderCounter.get(0) != "undefined") {
        orderCounterVal = parseInt(orderCounter.text()) + 1;
        orderCounter.text(orderCounterVal);
    } else {
        orderCounterContainer.find('.menu-text').after($('<span class="menu-counter-red" id="menu-counter-orders">1</span>'));
    }

    $('ul#order-project-list li').each(function (index, elm) {
        var linkContainer = $(this).find('a');
        if (linkContainer.get(0).dataset.orderId == wsTransferObject.attributes.id) {
            var orderActionCounter = linkContainer.find('span.submenu-counter-red');
            var orderActionCounterVal = 0;
            if (typeof orderActionCounter.get(0) != 'undefined') {
                orderActionCounterVal = parseInt(orderActionCounter.text()) + 1;
                orderActionCounter.text(orderActionCounterVal);
            } else {
                linkContainer.find('span.submenu-text').after('<span class="submenu-counter-red">1</span>');
            }
        }
    })
});