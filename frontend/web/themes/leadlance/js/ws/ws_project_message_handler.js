/**
 * Created by svd on 06.07.17.
 */
$(document).on('wsProjectMessage', function (e) {

    var wsTransferObject = e.detail;
    //return;
    var collocutorId = parseInt(wsTransferObject.attributes.from_user_id);
    var currentRoute = collection.get('currentRoute');

    var getMessage = function () {
        var messageTemplate = '';
        messageTemplate += '<div data-id="' + wsTransferObject.attributes.id + '" class="new-message">';
        messageTemplate += '<div class="name-block-answer-top-min-padd border-top-non reviews-all-block-rout comment-collocutor">';
        messageTemplate += '<h2 class="nameUser div-inline min-text-this-12">';
        var fullName = wsTransferObject.attributes.fromUserProfilePrivate;
        if (fullName) {
            if (fullName.attributes.first_name || fullName.attributes.last_name) {
                var lastName = fullName.attributes.last_name;
                var firstName = fullName.attributes.first_name;
                if (lastName && firstName) {
                    fullName = lastName + ' ' + firstName;
                } else if (lastName && !firstName) {
                    fullName = lastName;
                } else {
                    fullName = firstName;
                }
            } else {
                fullName = '';
            }
        }
        if (fullName) {
            messageTemplate += '<a href="#" class="hover-text-decoration">' + fullName + '</a>&nbsp;';
        }
        messageTemplate += '<span class="nickName">(' + wsTransferObject.attributes.fromUser.attributes.username + ')</span>&nbsp;';
        if (wsTransferObject.attributes.fromUserProAccount) {
            //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
            messageTemplate += '<img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="iconNick">&nbsp;';
            messageTemplate +=  '<img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="iconNick">';
        }
        messageTemplate += '</h2>';
        messageTemplate += '<ul class="list-date-answer div-inline min-text-this-10">';
        var createdAt = wsTransferObject.attributes.created_at;

        createdAt = createdAt.split(' ');
        var dt = createdAt[0];
        var tm = createdAt[1];
        tm = tm.substr(0, tm.lastIndexOf(':'));
        messageTemplate += '<li>(<span class="answer-time-up">' + tm + '</span> | <span class="answer-date-up">' + dt + '</span>)</li>';
        messageTemplate += '</ul>';
        messageTemplate += '<p class="answer-some-text-style-min">';
        messageTemplate += wsTransferObject.attributes.message;
        messageTemplate += '</p>';
        if (wsTransferObject.attributes.images) {
            var imagesCount = wsTransferObject.attributes.images.length;
            for (var i = 0; i < imagesCount; i++) {
                messageTemplate += '<p class="added-mess-file">';
                messageTemplate += '<a href="' + wsTransferObject.attributes.images[i].attributes.image + '" class="hover-text-decoration" target="_blank">';
                messageTemplate += '<img src="/themes/leadlance/img/profile/add-file-mess.png" alt="">';
                messageTemplate += wsTransferObject.attributes.images[i].attributes.original_image;
                messageTemplate += '</a>';
                messageTemplate += '</p>';

            }
        }
        messageTemplate += '</div>';
        messageTemplate += '<div>new</div>';
        messageTemplate += '</div>';
        return messageTemplate;
    }

    //Add current project message & skip current page if we are on project messages page
    if (currentRoute.controller == 'project' && inArray(currentRoute.action, ['view-offers', 'respond-project'])) {
        if (parseInt(currentRoute.params[0]) == parseInt(wsTransferObject.attributes.project_id)) {
            //if (currentRoute.action == 'view-offers') { // customer page
            var conversationContainer;
            if (currentRoute.action == 'view-offers') {
                // refresh common message counter on view-offers page
                var allProjectMessageCounterTopContainer = $('#view-offers-project-control > span.blue-number');
                var allProjectMessageCounterTop = ('> a', allProjectMessageCounterTopContainer);
                var allProjectMessageCounterTopVal = 0;
                if (typeof allProjectMessageCounterTop.get(0) != "undefined") {
                    allProjectMessageCounterTopVal = parseInt(allProjectMessageCounterTop.text()) + 1;
                    allProjectMessageCounterTop.text(allProjectMessageCounterTopVal);
                } else {
                    var cnt = $('span.redact-project-link', allProjectMessageCounterTopContainer);
                    $('<span class="blue-number"><a href="#" class="hover-text-decoration">' + (allProjectMessageCounterTopVal + 1) + '</a></span>').after(cnt);
                }

                // add current message to messages pull
                // find the conversation container
                conversationContainer= $('.wrapperContent > .answer-block-all > .list-view');
                var conversationId = getConversationId(userId, collocutorId);
                var found = false;
                $('> div', conversationContainer).each(function(index, item) {
                    if (found) {
                        return;
                    }
                    var key = (typeof item.dataset != "undefined" && item.dataset.key) ? item.dataset.key : 0;
                    if (key && key == conversationId) {
                        var messageContainer = $('.wrapper-slide-reviews > .list-view', $(this));
                        var newMessage = getMessage();
                        $(newMessage).prependTo(messageContainer);
                        found = true;
                    }
                });
                return;
            }
            if (currentRoute.action == 'respond-project') {
                conversationContainer = $('.answer-block-all .wrapper-slide-reviews .list-view');
                var newMessage = getMessage();
                $(newMessage).prependTo(conversationContainer);
            }
        }
    }
    // refresh projects messages counter in left menu by all projects
    var allProjectMessageCounterContainer = $('.left-menu-toogle > li > a.menu-projects');
    var allProjectMessageCounter = $('span.menu-counter:eq(0)', allProjectMessageCounterContainer);
    var allProjectMessageCounterVal = 0;
    if (typeof allProjectMessageCounter.get(0) != "undefined") {
        allProjectMessageCounterVal = parseInt(allProjectMessageCounter.html()) + 1;
        allProjectMessageCounter.html(allProjectMessageCounterVal);
    } else {
        $('<span class="menu-counter" id="menu-counter-projects-messages">' + (allProjectMessageCounterVal + 1) + '</span>').appendTo(allProjectMessageCounterContainer);
    }

    // refresh project messages counters by project
    var allProjectsMessagesContainer = $('.left-menu-toogle > li > ul#person-project-list li');
    var projectMessageCounter = $('a', allProjectsMessagesContainer); //  span.submenu-counter
    projectMessageCounter.each(function (index, elm) {
        if (parseInt($(this).get(0).dataset.projectId) == parseInt(wsTransferObject.attributes.project.attributes.id)) {
            var projectMessageCounter = $('span.submenu-counter', $(this));
            var projectMessageCounterVal = 0;
            if (typeof projectMessageCounter.get(0) != "undefined") {
                projectMessageCounterVal = parseInt(projectMessageCounter.html()) + 1;
                projectMessageCounter.html(projectMessageCounterVal);
            } else {
                $('<span class="submenu-counter">' + (projectMessageCounterVal + 1) + '</span>').appendTo($(this));
            }
        }
    })
});
