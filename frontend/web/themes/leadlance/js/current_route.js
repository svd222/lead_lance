/**
 * Created by svd on 19.07.17.
 */
function CurrentRoute() {
    this.path = window.location.href;
    this.relativePath = this.path.substr(frontendDomain.length + 1);
    this.route = this.relativePath.split('/',5);
    this.controller = this.route[0];
    this.action = typeof this.route[1] !== "undefined" ? this.route[1] : 'index';
    this.params = this.route.length >= 2 ? this.route.splice(2) : [];
}
collection.set('currentRoute', new CurrentRoute());
