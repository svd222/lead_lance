/**
 * Created by svd on 03.07.17.
 */
$(document).ready(function() {

    var registerWsSession = function () {
        var userId = collection.get('userId');

        if (userId) {
            var wsConfig = collection.get('ws');
            var protocol = wsConfig.isSSL ? 'wss' : 'ws';
            var port = wsConfig.isSSL ? 443 : wsConfig.port;
            var dsn = protocol + '://' + wsConfig.serverIp + ':' + port + wsConfig.location;
            var ws = new WebSocket(dsn);

            ws.onopen = function() {
                ws.send(JSON.stringify({
                    command: 'registerSession',
                    userId: userId
                }));
            }

            ws.onmessage = function(messageEvent) {
                var data = JSON.parse(messageEvent.data);
                var command = data.command || data.wsTransferObject.command;
                var check = inArray(command, wsList.list);
                if (check) {
                    switch (command) {
                        case 'registerSession' : {
                            if (data.resourceId) { //register session
                                $.ajax({
                                    method: 'POST',
                                    url: '/ws-session/register',
                                    data: {
                                        resourceId: data.resourceId
                                    },
                                    dataType: 'json',
                                    success: function(data) {
                                        if (typeof data.success != 'undefined' && data.success === true) {
                                            //success
                                        }
                                    }
                                });
                            } else {
                                console.log('subscribe is fail: ' + data.errorMessage);
                            }
                            break;
                        }
                        case 'message' : {
                            var wsTransferObject = data.wsTransferObject;
                            var event = new CustomEvent('wsMessage', {
                                detail: wsTransferObject
                            });
                            document.dispatchEvent(event)
                            break;
                        }
                        case 'project_message' : {
                            var wsTransferObject = data.wsTransferObject;
                            var event = new CustomEvent('wsProjectMessage', {
                                detail: wsTransferObject
                            });
                            document.dispatchEvent(event)
                            break;
                        }
                        case 'action' :
                        {
                            var wsTransferObject = data.wsTransferObject;
                            var event = new CustomEvent('wsAction', {
                                detail: wsTransferObject
                            });
                            document.dispatchEvent(event)
                            break;
                        }
                        case 'project_user_refuse' : {
                            var wsTransferObject = data.wsTransferObject;
                            var event = new CustomEvent('wsActionProjectUserRefuse', {
                                detail: wsTransferObject
                            });
                            document.dispatchEvent(event)
                            break;
                        }
                        case 'order_changed_state' : {
                            var wsTransferObject = data.wsTransferObject;
                            var event = new CustomEvent('wsActionOrderChangedState', {
                                detail: wsTransferObject
                            });
                            document.dispatchEvent(event)
                            break;
                        }
                        case 'order_message' : {
                            var wsTransferObject = data.wsTransferObject;
                            var event = new CustomEvent('wsOrderMessage', {
                                detail: wsTransferObject
                            });
                            document.dispatchEvent(event)
                            break;
                        }
                        case 'order_review' : {
                            var wsTransferObject = data.wsTransferObject;
                            var event = new CustomEvent('wsOrderReview', {
                                detail: wsTransferObject
                            });
                            document.dispatchEvent(event)
                            break;
                        }
                    }
                } else {
                    throw new TypeError('unknown ws command ' + command);
                }
            };

            ws.onerror = function(err)
            {
                console.log('ws err: ' + err);
            }
        }
    }

    registerWsSession();
});