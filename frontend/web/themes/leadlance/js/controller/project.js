/**
 * Created by svd on 10.03.17.
 *
 * Represent project specific functions
 */

$(document).ready(function () {
    //сбрасывает параметры поискового фильтра
    $('#ProjectFilterForm .clearFilters a').on('click', function(e) {
        var form = $('#ProjectFilterForm');
        $('input[name="ProjectFilterForm[project_type][]"]').each(function (obj) {
            $(this).prop('checked', false);
        });
        $('input[name="ProjectFilterForm[traffic_type][]"]').each(function (obj) {
            $(this).prop('checked', false);
        });
        $('input[name="ProjectFilterForm[range]"]').val(0);
        $('input[name="ProjectFilterForm[excludeUrgent]"]').val(0);
        $('input[name="ProjectFilterForm[excludeMulti]"]').val(0);
    });

    //select all, deselect all handlers for selectors on "index page"
    $('#ProjectFilterForm #traffic_type_select_all').on('click', function (e) {
        $('input[name="ProjectFilterForm[traffic_type][]"]').each(function (obj) {
            $(this).prop('checked', true);
        });
    });

    $('#ProjectFilterForm #traffic_type_deselect_all').on('click', function (e) {
        $('input[name="ProjectFilterForm[traffic_type][]"]').each(function (obj) {
            $(this).prop('checked', false);
        });
    });

    $('#ProjectFilterForm #project_type_select_all').on('click', function (e) {
        $('input[name="ProjectFilterForm[project_type][]"]').each(function (obj) {
            $(this).prop('checked', true);
        });
    });

    $('#ProjectFilterForm #project_type_deselect_all').on('click', function (e) {
        $('input[name="ProjectFilterForm[project_type][]"]').each(function (obj) {
            $(this).prop('checked', false);
        });
    });

    //select all, deselect all handlers for selectors on "create page"
    $('#Project #project_type_select_all').on('click', function (e) {
        $('input[name="ProjectType[type][]"]').each(function (obj) {
            $(this).prop('checked', true);
        });
    });

    $('#Project #project_type_deselect_all').on('click', function (e) {
        $('input[name="ProjectType[type][]"]').each(function (obj) {
            $(this).prop('checked', false);
        });
    });


    $('#Project #traffic_type_select_all').on('click', function (e) {
        $('input[name="TrafficType[type][]"]').each(function (obj) {
            $(this).prop('checked', true);
        });
    });

    $('#Project #traffic_type_deselect_all').on('click', function (e) {
        $('input[name="TrafficType[type][]"]').each(function (obj) {
            $(this).prop('checked', false);
        });
    });
});