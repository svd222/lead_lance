function getOrderMessage(data) {
    var currentUser = collection.get('controller').get('action').get('currentUserInfo');//{id: 1, hasPro: 1, nickName: "svd", fullName: "Evgeny Berezhnoy"}
    var collocutor = collection.get('controller').get('action').get('collocutor');
    var imageRelPath = collection.get('image_rel_path');
    var userId = collection.get('userId');
    var user;

    var cssClassName = '';
    if (data.from_user_id == currentUser.id) {
        user = currentUser;
    } else {
        if (data.from_user_id == collocutor.id) {
            user = collocutor;
            cssClassName = ' comment-hot';
        } else {
            throw new TypeError('unknown user');
        }
    }

    var message = '';
    message += '<div class="container blockPAddBorder">';
    message +=     '<div class="row">';
    message += '<div class="col-lg-12 comment ' + cssClassName + '">';
    message += '<div class="agree-order_name div-inline">';
    message += '<p>';
    if (user.fullName) {
        message += '<span class="name"><a href="#" class="hover-text-decoration">' + user.fullName + '</a> </span>';
    }
    message += '<span class="nickname">(' + user.nickName + ')</span>';
    if (user.hasPro) {
        //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
        message += '&nbsp;';
        message += '<img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">';
        message += '&nbsp;';
        message += '<img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">';
    }
    var d = new Date(Date.parse(data.created_at));
    var dStr = leadZero(d.getHours()) + ':' + leadZero(d.getMinutes()) + ' | ' + leadZero(d.getDate()) + '.' + leadZero(d.getMonth() + 1) + '.' + d.getFullYear();

    message += '<span class="date">(' + dStr + ')</span>'; //"2017-08-14 10:33:37"
    message += '</p>';
    message += '</div>';
    message += '<p class="comment-text">' + data.message + '</p>';

    if (typeof data.images != undefined && $.isArray(data.images)) {
        for (var i = 0; i < data.images.length; i++) {
            var image = data.images[i];
            message += '<p class="downloaded-files">';
            message += '<a href="' + imageRelPath + image.image + '" class="hover-text-decoration">';
            message += '<img src="/themes/leadlance/img/order/downloaded-files.png" alt="">&nbsp;';
            message += '<span class="text">' + image.original_image + '</span>';
            message += '</a>';
        }
    }
    message += '</div>';
    message += '</div>';
    message += '</div>';
    return message;
}