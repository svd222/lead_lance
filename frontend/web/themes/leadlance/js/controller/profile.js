var getTemplateCase = function() {
    var str = '<div class="content-project-all clearfix">' +
        '<div class="col-md-4 paddingNone">' +
        '<div class="img-our-project">' +
        '<h3 class="text-img-proj">IMG</h3>' +
        '</div>' +
        '</div>' +

        '<div class="col-md-8 paddingNone opis-project-style-all">' +
        '<h3 class="title-addeds-project">' +
        '<a href="#" class="project-link-width hover-text-decoration">{title}</a>' +
        '<span class="redact-and-delet-all">' +
        '<span class="delet-link-proj-inner">' +
        '<a href="#">уд.</a>' +
        '</span>' +
        '<span class="red-link-proj-inner">' +
        '<a href="#">ред.</a>' +
        '</span>' +
        '</span>' +
        '</h3>' +
        '<p class="some-project-text">' +
        '{presentation}' +
        '</p>' +
        '</div>' +
        '</div>';
    return str;
}

var reloadProfileTabContent = function(contextObj)
{
    var dataContext = contextObj.get(0).dataset.datacontext;
    var dataSource = contextObj.get(0).dataset.datasource;
    if (dataContext == 'cases') {
        //clear tab content
        var templateCase;
        var navsTabsContentItems = $('.content-project-all', contextObj);//navsTabsContent
        var navsTabsContentItemsParent = navsTabsContentItems.parent();
        navsTabsContentItems.remove();
        var errorThrown = false;
        //and reload it
        $.post({
            url: dataSource,
            method: 'POST',
            dataType: 'json',
            success: function (data) {
                data.forEach(function (item) {
                    templateCase = getTemplateCase();

                    templateCase = templateCase.replace(/{title}/gim, item.title);
                    templateCase = templateCase.replace(/{presentation}/gim, item.presentation);
                    navsTabsContentItemsParent.append(templateCase);
                })
            },
            error: function (xhr, status, error) {
                //navsTabsContentItems.html(tempContent);
                console.log(xhr);
                console.log(status);
                console.log(error);
            },
        });
    }
};

//validate phone (on settings page request) and show modal form to change phone
$(document).ready(function(){
    $('#change-phone').click(function(e) {
        var url = $(this).attr('href');
        var phone = $('#profileprivate-phone').val();

        $.post({
            url: url,
            data: {phone: phone},
            dataType: 'json',
            success: function (data) {
                if (typeof data != undefined && data.length == 0) {
                    //get phone in E164 format
                    $.post({
                        url: '/profile/phone-e164',
                        data: {phone: phone},
                        dataType: 'json',
                        success: function (data) {
                            phone = data.phone;
                            //set the correct phone
                            if (phone) {
                                $('#profileprivate-phone').val(phone);
                            }
                            //and send sms
                            //@todo send sms
                            $('#confirm-telephone').modal();
                        },
                        error: function (xhr, status, error) {
                            console.log(xhr);
                            console.log(status);
                            console.log(error);
                        }
                    });
                } else {
                    var error = '';
                    for(var i in data) {
                        error += data[i] + '<br />';
                    }
                    var html = $('#confirm-telephone-error').html().replace(/{error_message}/gim, error);
                    $('#confirm-telephone-error').html(html);
                    $('#confirm-telephone-error').modal();
                }
            },
            error: function (xhr, status, error) {
                console.log(xhr);
                console.log(status);
                console.log(error);
            }
        });
        e.returnValue = false;
        return false;
    });

    $('#check-email').on('click', function (e) {
        var url = $(this).attr('href');
        var email = $('#profileprivate-email').val();

        $.post({
            url: url,
            data: {email: email},
            dataType: 'json',
            success: function (data) {
                if (typeof data != undefined && data.length == 0) {
                    $.gritter.add({
                        title: '',
                        text: 'На ваш емайл ушло письмо с подтверждением',
                        class_name: 'gritter-success',
                        time: 4000,
                    });
                }
            }
        });
        e.returnValue = false;
        return false;
    });

    $('.button-go-to-order').click(function(e) {
        var id = $(this).get(0).dataset.id
        window.location.href = '/order/' + id;
    });

    $('#profileprivate-country_id').on("select2:select", function (e) {
        var data = e.params.data;
        var $city = $('#profileprivate-city_id');
        $city.val(null).trigger('change');
        $city.select2({
            ajax: {
                url: "/profile/get-city-list/" + data.id,
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, params) {
                    return {
                        results: data
                    };
                },
                cache: false
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
        });
        $city.on("select2:select", function(e) {
            $city.val(e.params.data.id);
        });
    });

    $('#change-password').on('click', function (e) {
        var url = $(this).attr('href');
        var form = $('#ProfilePrivate');
        $.post({
            url: url,
            data: form.serialize(),
            dataType: 'json',
            success: function(data) {
                if (typeof data != undefined && data.length == 0) {
                    $.gritter.add({
                        title: '',
                        text: 'Пароль был успешно изменён',
                        class_name: 'gritter-success',
                        time: 4000,
                    });
                } else {
                    $.each(data, function(i, item) {
                        $.each(item, function (j, err) {
                            $.gritter.add({
                                title: '',
                                text: err,
                                class_name: 'gritter-error',
                                time: 4000,
                            });
                        })
                    })
                }
            },
        });
        e.returnValue = false;
        return false;
    })
});
















