/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 09.04.17
 * @time: 15:51
 */
$(document).ready(function(){
    var formName = collection.get('action').get('formName');
    var csrfParam = collection.get('action').get('csrfParam');
    var deleteUrl = collection.get('action').get('deleteUrl');
    var imageFormName = collection.get('action').get('imageFormName');

    var mainForm = $('#' + formName);
    var csrf = mainForm.find("input[name='" + csrfParam + "']").val();
    var imageContainerId = (imageFormName + '-image').toLowerCase();
    var batch_id;
    var batchHiddenField = false;
    var files = [];


    $('#' + imageContainerId).bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            _csrf_frontend: csrf
        };
        if (batch_id) {
            data.formData.batch_id = batch_id;
        }
    });

    $('#' + imageContainerId).bind('fileuploaddone', function (e, data) {
        var imageContainer = $('#' + imageContainerId.toLowerCase());
        var parent = imageContainer;
        while(true) {
            parent = parent.parent();
            if (parent.get(0).tagName == 'DIV' && (parent.hasClass('dropbox') || parent.hasClass('dropbox-mini'))) {
                break;
            }
        }
        // <div class='dropbox right-input-file-download'>
        var result = JSON.parse(data.result);
        var f = result.files[0];
        batch_id = result.info.batch_id;

        if (!batchHiddenField) {
            var fForm = imageContainer;
            while(true) {
                fForm = fForm.parent();
                if (fForm.get(0).tagName == 'FORM') {
                    break;
                }
            }
            $('<input type="hidden" name="' + formName + '[batch_id]" value="' + batch_id + '">').appendTo(fForm);
            batchHiddenField = true;
        }

        var loadedImageId = result.info.id;

        if (!files.length) {
            $('<br />').appendTo(parent);
        }

        var fLink = '<p class="added-mess-file">';
        fLink += '<a href="#" style="font-size:130%;" class=\"hover-text-decoration">';
        fLink += '<img src="/themes/leadlance/img/profile/add-file-mess.png\" alt="">' + f.originalName;
        fLink += '</a>';
        fLink += '<i class="glyphicon glyphicon-remove float-right right20" id="fDel_' + loadedImageId + '">';
        fLink += '</p>';
        $(fLink).appendTo(parent);

        $('.added-mess-file i').on('click', function(e) {
            var fId = $(this).prop('id');
            var id = parseInt(fId.substr(fId.indexOf('_') + 1));

            var pParent = $(this);
            while(true) {
                pParent = pParent.parent();
                if (pParent.get(0).tagName == 'P' && pParent.hasClass('added-mess-file')) {
                    break;
                }
            }

            $.ajax({
                url: deleteUrl + id,
                method: 'POST',
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        pParent.remove();
                    }
                }
            });
        });

        files[loadedImageId] = f;
    });

    // ws wsMessage handler

    var collocutor =  {
        id: 0,
        hasPro: false,
        nickname: '',
        fullName: '',
    };

    var getMessage = function (model) {
        //console.log(collocutor);
        messageTemplate = '<div data-key="' + model.attributes.id + '">';
        messageTemplate += '<div class="container blockPAddBorder order-executive">';
        messageTemplate += '<div class="row">';
        messageTemplate += '<div class="col-lg-12 comment comment-hot comment-message">';
        messageTemplate += '<div class="agree-order_name div-inline">';
        messageTemplate += '<p>';

        if (collocutor.fullName) {
            messageTemplate += '<span class="name"><a href="#" class="hover-text-decoration">' + collocutor.fullName + '</a></span>&nbsp;';
        }

        messageTemplate += '<span class="nickname"><a href="/profile/index/' + collocutor.id + '" class="hover-text-decoration">' + collocutor.nickname + '</a></span>&nbsp;';

        if (collocutor.hasPro) {
            //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
            messageTemplate += '<img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">&nbsp;';
            messageTemplate += '<img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">';
        }

        messageTemplate += '<span class="date">' + model.attributes.created_at + '</span>';
        messageTemplate += '</p>';
        messageTemplate += '</div>';
        messageTemplate += '<p class="comment-text">' + model.attributes.message + '</p>';

        //batch images
        if (model.attributes.images) {
            for (var i = 0; i < model.attributes.images.length; i++) {
                image = model.attributes.images[i];
                messageTemplate += '<p class="downloaded-files">';
                messageTemplate += '    <a href="#" class="hover-text-decoration">';
                messageTemplate += '    <img src="/themes/leadlance/img/order/downloaded-files.png" alt="">';
                messageTemplate += '    <span class="text">' + image.attributes.original_image + '</span>';
                messageTemplate += '    </a>';
                messageTemplate += '</p>';
            }
        }

        messageTemplate += '</div>';
        messageTemplate += '</div>';
        messageTemplate += '</div>';
        messageTemplate += '</div>';

        return messageTemplate;
    }

    $(document).on('wsMessage', function (e) {
        var wsTransferObject = e.detail;
        var newMessage;
        // get collocutor info
        if (!collocutor.id) {
            var url = window.location.href;
            var cId = parseInt(url.substr(url.lastIndexOf('/') + 1));
            $.ajax({
                url: '/users/get-info',
                dataType: 'json',
                method: 'post',
                data: {
                    id: cId
                },
                success: function(data) {
                    collocutor.id = cId;
                    collocutor.hasPro = data.hasPro;
                    collocutor.nickname = data.nickname;
                    collocutor.fullName = data.fullName;
                    //console.log(collocutor);
                    newMessage = getMessage(wsTransferObject);
                    $(newMessage).prependTo($('.list-view'));
                },
            });
        } else {
            newMessage = getMessage(wsTransferObject);
            $(newMessage).prependTo($('.list-view'));
        }
    });
});
