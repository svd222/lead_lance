/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@clubautomation.com>
 * Date: 01/24/2018
 * Time: 14:21
 */

$(function() {
    $('#start-search-news').on('click', function () {
        searchNews();
    });

    $('#search-input-news').on('keyup', function () {
        searchNews();
    });

    var searchNews = function () {
        $.ajax({
            url: 'news/search',
            type: "POST",
            data: {
                'searchQuery': $('#search-input-news').val()
            },
            success:function(data) {
                if (data.html != undefined) {
                    $('#news-list').html(data.html);
                }
            }
        });
    }
});
