/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@clubautomation.com>
 * Date: 01/27/2018
 * Time: 11:21 PM
 */

$(function() {
    $('body').on('click', '.answer-comment', function (e) {
        var item = $(this);
        var commentId = item.get(0).dataset.commentId;
        $('#comment-parent_id').val(commentId);
        $(window).scrollTop($('#comment-message').scrollTop());
        $('#comment-message').focus();
        e.returnValue = false;
        return false;
    });

    $('body').on('click', '#send-news-comment', function () {
        var self = $(this);
        self.prop("disabled", true);
        var form = $('#Comment');
        $.ajax({
            url: '/news/add-comment',
            type: "POST",
            data: form.serialize(),
            success:function(data) {
                if (data.html != undefined && data.message == undefined) {
                    $('#news-comments').html(data.html);
                    $('#comment-count').html(data.commentCount);
                    $('#comment-message').val('');
                }
            }
        });

        setTimeout(function () {
                self.prop("disabled", false);
            },
            4000
        );
    });

});
