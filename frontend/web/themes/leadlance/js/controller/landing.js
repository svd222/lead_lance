/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 27.02.17
 * @time: 18:15
 */

$(document).ready(function () {

    var registerFormSelectorsTemplate = '#register-form';
    var registerFormRoleSelectorTemplate = 'input[name="register-form[role]"]';
    var registerFormRoleCustomerTemplate = '#register-form-role-customer';
    var registerFormRoleExecutorTemplate = '#register-form-role-executor';

    var registerFormSelectors = '';
    // var registerFormRoleSelectors = '';
    var registerFormRoleCustomerSelectors = '';
    var registerFormRoleExecutorSelectors = '';
    for (var i = 1; i <= 10; i++) {
        registerFormSelectors += registerFormSelectorsTemplate + '-' + i + ',';
        // registerFormRoleSelectors += registerFormRoleSelectorTemplate.replace('register-form', 'register-form-' + i) + ',';
        registerFormRoleCustomerSelectors += registerFormRoleCustomerTemplate.replace('register-form', 'register-form-' + i) + ',';
        registerFormRoleExecutorSelectors += registerFormRoleExecutorTemplate.replace('register-form', 'register-form-' + i) + ',';
    }
    registerFormSelectors = registerFormSelectors.substr(0, registerFormSelectors.length -1);
    // registerFormRoleSelectors = registerFormRoleSelectors.substr(0, registerFormRoleSelectors.length - 1);
    registerFormRoleCustomerSelectors = registerFormRoleCustomerSelectors.substr(0, registerFormRoleCustomerSelectors.length - 1);
    registerFormRoleExecutorSelectors = registerFormRoleExecutorSelectors.substr(0, registerFormRoleExecutorSelectors.length - 1);

    //@todo move to global js functions scope (separate module)
    // Find position of last occurrence of a case-insensitive string in a string
    var strripos = function ( haystack, needle, offset){
        var i = haystack.toLowerCase().lastIndexOf( needle.toLowerCase(), offset );
        return i >= 0 ? i : false;
    }
    
    //@todo move to global js functions scope (separate module)
    /**
     * Generates url encoded string (function behavior are equal to php http_build_query)
     * now param query supports only this representation:
     * [
     *  {
     *      name: string,
     *      value: string
     *  }
     * ]
     * @param mixed query
     */
    var http_build_query = function (query) {
        var urlEncodedString = '';
        var paramCount = 0;
        for (var i in query) {
            if ((parseInt(i) == i) && (query[i].name != undefined) && (query[i].value != undefined)) {
                //query is object
                urlEncodedString += encodeURIComponent(query[i].name) + '=' + encodeURIComponent(query[i].value) + '&';
                paramCount++;
            }
        }
        if (paramCount) {
            urlEncodedString = urlEncodedString.substr(0, urlEncodedString.length - 1);
        } else {
            console.log('empty http_build_query query param');
        }
        return urlEncodedString;
    }

    //@todo move to global js functions scope (separate module)
    /**
     * Replaces param names
     * now param query supports only this representation:
     * [
     *  {
     *      name: string,
     *      value: string
     *  }
     * ]
     * @param mixed query
     */
    var replaceFormName = function (query, search, needle) {
        var replacedQuery = [];
        var name;
        var value;
        for (var i in query) {
            name = query[i].name;
            value = query[i].value;
            if ((parseInt(i) == i) && (name != undefined) && (value != undefined)) {
                if (strripos(name, search) !== false) {
                    name = name.replace(search, needle);
                }
            }
            replacedQuery[i] = {
                name: name,
                value: value,
            };
        }
        return replacedQuery;
    }

    var findFormId = function (form) {
        var formId = form.prop('id');
        var formIdNum = parseInt(formId.substr(strripos(formId, '-') + 1));
        return formIdNum;
    }

    var findRoleInputs = function (form) {
        var formIdNum = findFormId(form);

        var customer = $('#register-form-' + formIdNum + '-role-customer');
        var executor = $('#register-form-' + formIdNum + '-role-executor');
        return {
            customer: customer,
            executor: executor,
        };
    }

    var container = [];
    var removeError = function () {
        if (container.length) {
            for (var i in container) {
                $(container[i]).removeClass('input-err-valid');
            }
        }
        //$('#register-form .checkeds-block-modal').removeClass('input-err-valid');
        $(registerFormSelectors).find('.checkeds-block-modal').removeClass('input-err-valid');
    }

    //Удаляет ранее добавленные фреймворком скрытые поля для полей - чекбоксов
    var removeHiddenFields = function () {
        $('input[name="register-form[role]"]').each(function () {
            //console.log($(this).get(0).tagName, $(this).prop('name'), $(this).prop().val());
            if (!$(this).prop("id")) {
                $(this).remove();
            }
        })
    }
    /*var customer = $('#register-form-role-customer');
    var executor = $('#register-form-role-executor');
    customer.on('click', function () {
        checked = $(this).prop('checked');
        if (checked) {
            removeHiddenFields();
            executor.prop('checked', false);
        }
    });

    executor.on('click', function () {
        checked = $(this).prop('checked');
        if (checked) {
            removeHiddenFields();
            customer.prop('checked', false);
        }
    });*/

    var customers = $(registerFormRoleCustomerSelectors);
    var executors = $(registerFormRoleExecutorSelectors);
    customers.on('click', function () {
        checked = $(this).prop('checked');
        var customerIdNum = $(this).prop('id').substr('register-form-'.length);
        customerIdNum = parseInt(customerIdNum.substr(0, customerIdNum.indexOf('-')));
        var bExecutor = $(registerFormRoleExecutorTemplate.replace('register-form', 'register-form-' + customerIdNum));
        var pParent = $(this);
        /*while (true) {
            pParent = pParent.parent();
            if (pParent.get(0).tagName == 'P') {
                break;
            }
        }*/
        if (checked) {
            removeHiddenFields();
            bExecutor.prop('checked', false);
        } else {
            bExecutor.prop('checked', true);
            /*pParent.removeClass('active');*/
        }
    });

    executors.on('click', function () {
        checked = $(this).prop('checked');
        var executorIdNum = $(this).prop('id').substr('register-form-'.length);
        executorIdNum = parseInt(executorIdNum.substr(0, executorIdNum.indexOf('-')));
        var bCustomer = $(registerFormRoleCustomerTemplate.replace('register-form', 'register-form-' + executorIdNum));
        /*var pParent = $(this);
        while (true) {
            pParent = pParent.parent();
            if (pParent.get(0).tagName == 'P') {
                break;
            }
        }*/
        if (checked) {
            removeHiddenFields();
            bCustomer.prop('checked', false);
        } else {
            bCustomer.prop('checked', true);
            /*pParent.removeClass('active');*/
        }
    });

    //$('#register-form').on('beforeSubmit', function (e) {
    $(registerFormSelectors).on('beforeSubmit', function (e) {
        var form = $(this);

        var formIdNum = findFormId(form);

        var roleInputs = findRoleInputs(form);
        var customer = roleInputs.customer;
        var executor = roleInputs.executor;

        if (customer.prop('checked')) {
            executor.prop('checked', false);
            removeHiddenFields();
        }
        if (executor.prop('checked')) {
            customer.prop('checked', false);
            removeHiddenFields();
        }
        /*if (!executor.prop('checked') && !customer.prop('checked')) {
            return false;
        }*/
        var error = false;
        $.ajax({
            url: form.attr("action"),
            type: "POST",
            //data: form.serialize(),
            data: http_build_query(replaceFormName(form.serializeArray(), 'register-form-' + formIdNum, 'register-form')),
            success:function(data) {
                if (data.success != undefined && data.success === true) {
                    $.gritter.add({
                        title: 'Успех',
                        text: 'Вы успешно зарегистрированны, письмо с дальнейшими инструкциями выслано на Ваш емайл',
                        class_name: 'gritter_success',
                        sticky: true,
                    });
                } else {
                    error = true;
                    return false;
                    /*$.gritter.add({
                        title: 'Ошибка',
                        text: 'Произошла внутренняя ошибка сервера',
                        class_name: 'gritter_error',
                        sticky: true,
                    });
                    console.log('Произошла внутренняя ошибка сервера');*/
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            },
            complete:function () {
                if (!error) {
                    $('#sigin-account').modal('hide');
                }
            }
        });
        return false;
    });

    //$('#register-form').on('afterValidate', function (evt, messages) {
    $(registerFormSelectors).on('afterValidate', function (evt, messages) {
        var form = $(this);

        var formIdNum = findFormId(form);

        var roleInputs = findRoleInputs(form);
        var customer = roleInputs.customer;
        var executor = roleInputs.executor;

        for (var i in messages) {
            //Если это не поле - выбор роли (заказчик | маркетолог) и есть ошибки
            console.log(i, messages[i]);
            if (!strripos(i, 'role')) {
                if (messages[i].length) {
                    $('#' + i).addClass('input-err-valid');
                    container.push('#' + i);
                    /*form.yiiActiveForm('updateAttribute', i, 'error');*/
                    for (var m in messages[i]) {
                        $.gritter.add({
                            title: '',
                            text: messages[i][m],
                            class_name: 'gritter_error',
                            time:4000,
                        });
                    }
                }
            } else if(strripos(i, 'role') && !customer.prop('checked') && !executor.prop('checked')) {
                if (messages[i].length) {
                    //container.push('#' + i);
                    //$('#register-form .checkeds-block-modal').addClass('input-err-valid');
                    var roleCheckBox = $('#register-form-' + formIdNum + ' .checkeds-block-modal input[name="register-form[role]"]').first();
                    var commonContainer = roleCheckBox;
                    while (true) {
                        commonContainer = commonContainer.parent();
                        if (commonContainer.hasClass('checkeds-block-modal')) {
                            break;
                        }
                    }
                    commonContainer.addClass('input-err-valid');
                    var d = new Date();
                    var gId = d.getUTCHours() + d.getUTCMinutes() + d.getUTCSeconds() + d.getUTCMilliseconds();
                    commonContainer.prop('id', gId);
                    container.push('#' + gId);
                    $.gritter.add({
                        title: '',
                        text: 'Необходимо выбрать роль',
                        class_name: 'gritter_error',
                        time:4000,
                    });
                }
            }
        }
        window.setTimeout(removeError, 3000);
    });

    $('#recovery-form').on('beforeSubmit', function (e) {
        var form = $(this);

        var error = false;
        $.ajax({
            url: form.attr("action"),
            type: "POST",
            data: form.serialize(),
            success:function(data) {
                showMessages(data);
                if (data.success != undefined && data.success === true) {
                    $.gritter.add({
                        title: 'Успех',
                        text: 'Вам отправлено письмо с инструкциями по смене пароля.',
                        class_name: 'gritter_success',
                        sticky: true
                    });
                } else {
                    error = true;
                    return false;
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                error = true;
            },
            complete:function () {
                if (!error) {
                    $('#reset-account').modal('hide');
                }
            }
        });
        return false;

    });

    $('#reset-form').on('beforeSubmit', function (e) {
        var form = $(this);

        var passwordFirst = form.find('.password-first').find('#recovery-form-password').val();
        var passwordSecond = form.find('.password-second').find('#recovery-form-password').val();
        if (passwordFirst != passwordSecond) {
            $.gritter.add({
                title: '',
                text: 'Пароли не совпадают',
                class_name: 'gritter_error',
                time:4000,
            });
            return false;
        }

        var error = false;
        $.ajax({
            url: form.attr("action"),
            type: "POST",
            data: form.serialize(),
            success:function(data) {
                showMessages(data);
                if (data.success != undefined && data.success === true) {
                    $.gritter.add({
                        title: 'Успех',
                        text: 'Пароль успешно изменен',
                        class_name: 'gritter_success',
                        sticky: true
                    });
                } else {
                    error = true;
                    return false;
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                error = true;
            },
            complete:function () {
                if (!error) {
                    $('#reset-account-complete').modal('hide');
                    $('#login-account').modal('show');
                }
            }
        });
        return false;

    });

    $('#login-form').on('afterValidate', function (evt, messages, errorAttributes) {
        showMessages(messages);
    });

    var showMessages = function(messages) {
        for (var i in messages) {
            if (messages[i].length) {
                $('#' + i).addClass('input-err-valid');
                container.push('#' + i);
                for (var m in messages[i]) {
                    $.gritter.add({
                        title: '',
                        text: messages[i][m],
                        class_name: 'gritter_error',
                        time:4000,
                    });
                }
            }
        }
        window.setTimeout(removeError, 3000);
    };

    $('body').on('click', '.bottom-modal-sign-link-left', function () {
        $('#login-account').modal('hide');
        $('#reset-account').modal('show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        },600);
    });

    $('body').on('click', '.bottom-modal-sign-link-right', function () {
        $('#login-account').modal('hide');
        $('#sigin-account').modal('show');
        setTimeout(function () {
            $('body').addClass('modal-open');
        },600);
    });
});
