/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 06.06.17
 * @time: 18:15
 */
$(document).ready(function() {
    $('#input-search-users').keypress(function(e) {
        if(e.which == 13) {
            var val = $(this).val();
            if (val) {
                window.location.href = '/user-list/index?q=' + decodeURIComponent(val.replace(/\+/g, " "));
            } else {
                window.location.href = '/user-list/index';
            }
        }
    });
})
