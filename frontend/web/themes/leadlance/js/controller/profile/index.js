/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 09.04.17
 * @time: 22:11
 *
 * This piece of code reads the url, find the anchor (symbols set after #) and open appropriate tab
 */
$(document).ready(function(){

    var hashes = {
        cases: 'tabs-content-cases',
        my_projects: 'tabs-content-projects',
        my_orders: 'tabs-content-orders',
        positive_reviews: 'tabs-content-testimonials',
        negative_reviews: 'tabs-content-testimonials',
        balance: 'tabs-content-balance',
        services: 'tabs-content-services',
        settings: 'tabs-content-settings'
    };

    var navTabsHeaders = $('.tabs-nav-list').find('li');
    var navsTabsContent = $('.tabs-content');

    var testimonialTabContent = $('#tabs-content-testimonials').get(0);
    var testimonialTabContentIndex = $('.tabs-content-all .tabs-content').index(testimonialTabContent);
    var orderReviewsLoaded = false;
    navTabsHeaders.eq(testimonialTabContentIndex).on('click', function() {
        if(!orderReviewsLoaded) {
            getOrderReviews(1);
        }
    });

    var appendOrderReviews = function (data) {
        var userAvatarUploadDir = data.userAvatarUploadDir;
        var orderReviews = data.orderReviews;
        var str = '';
        var orderReview;
        var user = null;
        var profilePrivate = null;
        var userFullName = '';
        var proAccount = false;
        var d;
        var dt = '';
        var order;
        var orderDt = '';
        var emotionSign;
        var negativeRevOrderInfo = '';
        var userAvatarImage;

        for (var i = 0; i < orderReviews.length; i++) {
            orderReview = orderReviews[i];
            user = orderReview.user;
            profilePrivate = user.profilePrivate ? user.profilePrivate : null;
            userFullName = (profilePrivate && (profilePrivate.last_name || profilePrivate.first_name));
            d = new Date(orderReview.updated_at);
            dt = leadZero(d.getHours()) + ':' + leadZero(d.getMinutes()) + ' | ' + leadZero(d.getDate()) + '.' + leadZero(d.getMonth() + 1) + '.' + d.getFullYear();
            order = orderReview.order;
            userAvatarImage = (profilePrivate && profilePrivate.userAvatar && profilePrivate.userAvatar.image) ? (userAvatarUploadDir + profilePrivate.userAvatar.image) : '';

            if (orderReview.emotion == 1) {
                emotionSign = '+';
                negativeRevOrderInfo = '';
            } else {
                emotionSign = '-';
                negativeRevOrderInfo = ' negative-rev-order-info';
            }

            d = new Date(order.updated_at);
            orderDt = leadZero(d.getDate()) + '.' + leadZero(d.getMonth() + 1) + '.' + d.getFullYear();

            if (userFullName) {
                if (profilePrivate.last_name) {
                    userFullName = profilePrivate.last_name;
                }
                if (profilePrivate.first_name) {
                    if (profilePrivate.last_name) {
                        userFullName += ' '
                    }
                    userFullName += profilePrivate.first_name;
                }
            }
            proAccount = user.proAccount ? true : false;
            /*console.log(orderReview);*/
            str += '<li class="disput">';
            if (userAvatarImage) {
                str += '<div class="pers-avatar div-inline">' +
                    '<img src="' + userAvatarImage + '" style="width:50px; height:50px"/>' +
                '</div>';
            } else {
                str += '<div class="pers-avatar div-inline">' +
                    '</div>';
            }

            str += '<div class="agree-order_name div-inline">' +
                '<p>';
            if (userFullName) {
                str += '<span class="name"><a href="#" class="hover-text-decoration">' + userFullName + '</a> </span>';
            }

            str += '<span class="nickname">(' + user.username + ')</span>&nbsp;';
            if (proAccount) {
                //@todo replace verified jpg (person-test.jpg) with suitable check ($userWasVerified)
                str += '<img src="/themes/leadlance/img/index/person-status.jpg" alt="" class="person-status">&nbsp;' +
                    '<img src="/themes/leadlance/img/index/person-test.jpg" alt="" class="person-test">';
            }
            str += '<span class="date">(' + dt + ')</span>' +
                '</p>' +
                '</div>' +
                '<h3 class="title-reviews-style' + negativeRevOrderInfo + '">' + emotionSign + ' ' +  orderDt + ' ' + order.title + '</h3>' +
                '<p class="some-text-reivews">' + orderReview.review +
                '</p>' +
                '</li>';
        }
        if (str) {
            //load the content in the appropriate tab
            var orderReviewsListContainer = $('#tabs-content-testimonials .tabs-reviews-all .tabs-rev-content .list-reviews-tabs');
            orderReviewsListContainer.find('li').remove();
            orderReviewsListContainer.append(str);
        }
    }

    var getOrderReviews = function (emotion) {
        var orderReviews = [];
        var allOrderReviewsCount;
        var positiveOrderReviewsCount;
        var negativeOrderReviewsCount;
        var action = collection.get('controller').get('action');
        var profileOwnerId = action.get('profileOwnerId');
        var data = {
            emotion: emotion,
        };
        if (profileOwnerId) {
            data.userId = profileOwnerId;
        }
        $.post({
            url: '/profile/get-order-reviews',
            data: data,
            dataType: 'json',
            success: function (data) {
                appendOrderReviews(data);

                // prevent for loading data from server many times
                orderReviewsLoaded = true;

                // update counters
                allOrderReviewsCount = data.allOrderReviewsCount;
                positiveOrderReviewsCount = data.positiveOrderReviewsCount;
                negativeOrderReviewsCount = data.negativeOrderReviewsCount;
                $('li.positively-rev a span.count-rev-true').text(positiveOrderReviewsCount);
                $('li.negative-rev a span.count-rev-fal').text(negativeOrderReviewsCount);
            }
        });
    }

    // @todo to cache orderReviews positive & negative in var and load them in future if needed
    $('li.positively-rev a').on('click', function (e) {
        orderReviewsLoaded = false;
        getOrderReviews(1);
    }).trigger('click');

    $('li.negative-rev a').on('click', function (e) {
        orderReviewsLoaded = false;
        getOrderReviews(2);
    }).trigger('click');

    //show the tab content defined by the hash in url
    var hash = window.location.hash;
    if (hash) {
        hash = hash.substr(1);//cut the `#`
        navsTabsContent.css({'display':'none'});
        var tabId = hashes[hash];
        navsTabsContentItem = $('#' + tabId) !== undefined ? $('#' + tabId) : false;
        $('.tabs-content').each(function (index, obj) {
            if ($(this).prop('id') == tabId) {
                navTabsHeaders.removeClass('active-link-tabs');
                navTabsHeaders.eq(index).addClass('active-link-tabs');
            }
        })
        if (navsTabsContentItem) {
            navsTabsContentItem.show();//.fadeIn(500).addClass('active-tabs')
        }
        if (hash == 'positive_reviews') {
            setTimeout(function () {
                    $('li.positively-rev a').trigger('click');
                },
                1000
            );
        }
        if (hash == 'negative_reviews') {
            setTimeout(function () {
                    $('li.negative-rev a').trigger('click');
                },
                1000
            );
        }
    } else {
        navsTabsContent.eq(0).show();
        navTabsHeaders.eq(0).addClass('active-link-tabs')
    }



        var formName = collection.get('action').get('formName');
        var csrfParam = collection.get('action').get('csrfParam');
        /*var deleteUrl = collection.get('action').get('deleteUrl');*/
        var imageFormName = collection.get('action').get('imageFormName');

        var mainForm = $('#' + formName);
        var csrf = mainForm.find("input[name='" + csrfParam + "']").val();
        var imageContainerId = (imageFormName + '-image').toLowerCase();
        var batch_id;
        var batchHiddenField = false;
        var files = [];


        $('#' + imageContainerId).bind('fileuploadsubmit', function (e, data) {
            data.formData = {
                _csrf_frontend: csrf
            };
            if (batch_id) {
                data.formData.batch_id = batch_id;
            }
        });

        $('#' + imageContainerId).bind('fileuploaddone', function (e, data) {
            var imageContainer = $('#' + imageContainerId.toLowerCase());
            var parent = imageContainer;
            while(true) {
                parent = parent.parent();
                if (parent.get(0).tagName == 'DIV' && parent.hasClass('first-tabs-right')) {
                    break;
                }
            }
            var avatarContainer = parent.find('div').first().find('div').first();

            // <div class='dropbox right-input-file-download'>
            var result = JSON.parse(data.result);
            var f = result.files[0];

            avatarPhoto = avatarContainer.find('img.avatar-photo');
            if (typeof avatarPhoto.get(0) !== "undefined") {
                avatarPhoto.prop('src', f.url);
                avatarPhoto.css({
                    width: '90px',
                    height: '90px',
                });
            } else {
                var avatar = '<img src="' + f.url + '" alt="" style="width: 90px; height: 90px;">';
                $(avatar).appendTo(avatarContainer);
            }

            var personContainer = $('div.person div.person-photo-wrap');
            var personPhoto = personContainer.find('img.person-photo');
            if (typeof personPhoto.get(0) !== "undefined") {
                personPhoto.css({
                    width: '55px',
                    height: '55px',
                });
                personPhoto.prop('src', f.url);
            } else {
                var avatarPersonPhoto = '<img src="' + f.url + '" alt="" style="width: 55px; height: 55px;" class="person-photo">';
                $(avatarPersonPhoto).prependTo(personContainer);
            }

            var avatarContainer2 = $('div.logo-profile a').first();
            var avatarPhoto2 = avatarContainer2.find('img.avatar-photo2');
            if (avatarPhoto2.get(0) !== "undefined") {
                avatarPhoto2.css({
                    width: '90px',
                    height: '90px',
                });
                avatarPhoto2.prop('src', f.url);
            } else {
                var avatarPersonPhoto2 = '<img src="' + f.url + '" alt=""  style="width:90px; height:90px" class="avatar-photo2">';
                $(avatarPersonPhoto2).prependTo(avatarContainer2);
            }

            var loadedImageId = result.info.id;

            files[loadedImageId] = f;
        });


    $('.button-send-message').on('click', function (e) {
        var data = $(this).get(0).dataset;
        var toId = data.toId;
        window.location.href = '/message/create/' + toId;
    })
});
