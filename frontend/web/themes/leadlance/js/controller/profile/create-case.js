/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 15.06.17
 * @time: 10:30
 */
$(document).ready(function(){
    var formName = collection.get('action').get('formName');
    var csrfParam = collection.get('action').get('csrfParam');
    var deleteUrl = collection.get('action').get('deleteUrl');
    var imageFormName = collection.get('action').get('imageFormName');

    var mainForm = $('#' + formName);
    var csrf = mainForm.find("input[name='" + csrfParam + "']").val();
    var imageContainerId = (imageFormName + '-preview_image').toLowerCase();
    var id;
    var modelId = $('#' + formName.toLowerCase() + '-modelid');

    $('#' + imageContainerId).bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            _csrf_frontend: csrf
        };
        if (typeof modelId.get(0) !== "undefined") {
            data.formData.modelId = modelId.val();
        }
    });

    $('#' + imageContainerId).bind('fileuploaddone', function (e, data) {
        var imageContainer = $('#' + imageContainerId.toLowerCase());
        var parent = imageContainer;
        while(true) {
            parent = parent.parent();
            if (parent.get(0).tagName == 'DIV' && parent.hasClass('right-block-form-modal')) {
                break;
            }
        }
        // <div class='dropbox right-input-file-download'>
        var result = JSON.parse(data.result);
        var f = result.files[0];
        id = result.info.id;

        var previewImageContainer = parent.find('div.img-added-modal-block');
        var previewImage = $('#profile_case_preview_image');
        if (typeof previewImage.get(0) !== "undefined") {
            previewImage.prop('src', f.url);
            previewImage.css({
                width: '342px',
                height: '120px',
                display: 'inline-block',
                paddingLeft: '40px',
            });
        } else {
            $('<img src="' + f.url + '" style="width: 342px; height: 120px; display: inline-block; padding-left:40px;" id="profile_case_preview_image">').appendTo(previewImageContainer);
        }



        var fForm = imageContainer;
        while(true) {
            fForm = fForm.parent();
            if (fForm.get(0).tagName == 'FORM') {
                break;
            }
        }

        if (typeof modelId.get(0) === "undefined") {
            $('<input type="hidden" name="' + formName + '[modelId]" value="' + id + '" id="' + formName + '-modelid">').appendTo(fForm);
        }

        if (typeof $('i.fDel').get(0) === "undefined") {
            $('<i class="glyphicon glyphicon-remove float-right right20 fDel" id="fDel_' + id + '">').appendTo($('.img-added-modal-block'));
        }
        $('.fDel').on('click', function (e) {
            var fId = $(this).prop('id');
            var fId = parseInt(fId.substr(fId.indexOf('_') + 1));

            $.ajax({
                url: deleteUrl + fId,
                method: 'POST',
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        pParent.remove();
                    }
                }
            });

            previewImageContainer.find('img, i').remove();
        });
    });

    $('i.fDel').on('click', function (e) {
        alert('b');
        var closeLink = $('i.fDel');
        var closest = closeLink.siblings('img#profile_case_preview_image');
        closeLink.remove();
        closest.remove();

        var fId = $(this).prop('id');
        var fId = parseInt(fId.substr(fId.indexOf('_') + 1));

        $.ajax({
            url: deleteUrl + fId,
            method: 'POST',
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    pParent.remove();
                }
            }
        });
    });

});
