$(document).ready(function() {
    updateImageSize();
    $(window).resize(function() {
        updateImageSize();
    });


    function updateImageSize() {
        $(".presentation-container").each(function(){
            var s_width = screen.width;
            var s_height = screen.height;
            var w_ratio = s_width / s_height;
            var $img = $(this).find("img");
            if (typeof $img.get(0) !== "undefined") {
                var src = $img.prop('src');
                var new_width, new_height;

                var img = new Image();
                img.src = src;
                img.onload = function() {

                    img_width = img.width;
                    img_height = img.height;

                    if (s_width < img_width) {
                        w_ratio = s_width / img_width;
                        new_width = s_width;
                        new_height = img_height * w_ratio;
                        $img.css({"width": new_width + 'px', "height": new_height + 'px'});
                    }
                }
            }
        });
    };

    $('body').on('click', '.answer-comment', function (e) {
        var item = $(this);
        var commentId = item.get(0).dataset.commentId;
        $('#comment-parent_id').val(commentId);
        $(window).scrollTop($('#comment-message').scrollTop());
        $('#comment-message').focus();
        e.returnValue = false;
        return false;
    });

    $('body').on('click', '#send-case-comment', function () {
        var self = $(this);
        self.prop("disabled", true);
        var form = $('#Comment');
        $.ajax({
            url: '/profile/case-add-comment',
            type: "POST",
            data: form.serialize(),
            success:function(data) {
                if (data.html != undefined && data.message == undefined) {
                    $('#case-comments').html(data.html);
                    $('#comment-count').html(data.commentCount);
                    $('#comment-message').val('');
                }
            }
        });
        setTimeout(function () {
                self.prop("disabled", false);
            },
            4000
        );

    });
});
