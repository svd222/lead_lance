/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 12.03.17
 * @time: 14:30
 */
$(document).ready(function(){
    var formName = collection.get('action').get('formName');
    var csrfParam = collection.get('action').get('csrfParam');
    var deleteUrl = collection.get('action').get('deleteUrl');
    var imageFormName = collection.get('action').get('imageFormName');

    var mainForm = $('#' + formName);
    var csrf = mainForm.find("input[name='" + csrfParam + "']").val();
    var imageContainerId = (imageFormName + '-image').toLowerCase();
    var batch_id;
    var batchHiddenField = false;
    var files = [];

    var batchHiddenInput = $('#project-batch_id');
    if (batchHiddenInput.get(0)) {
        batch_id = parseInt(batchHiddenInput.val());
        batchHiddenField = true;
    }


    $('#' + imageContainerId).bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            _csrf_frontend: csrf
        };
        if (batch_id) {
            data.formData.batch_id = batch_id;
        }
    });

    $('#' + imageContainerId).bind('fileuploaddone', function (e, data) {
        var imageContainer = $('#' + imageContainerId.toLowerCase());
        var parent = imageContainer;
        while(true) {
            parent = parent.parent();
            if (parent.get(0).tagName == 'DIV' && (parent.hasClass('dropbox') || parent.hasClass('dropbox-mini'))) {
                break;
            }
        }
        // <div class='dropbox right-input-file-download'>
        var result = JSON.parse(data.result);
        var f = result.files[0];
        batch_id = result.info.batch_id;

        if (!batchHiddenField) {
            var fForm = imageContainer;
            while(true) {
                fForm = fForm.parent();
                if (fForm.get(0).tagName == 'FORM') {
                    break;
                }
            }
            $('<input type="hidden" id="project-batch_id" name="' + formName + '[batch_id]" value="' + batch_id + '">').appendTo(fForm);
            batchHiddenField = true;
        }

        $('.added-mess-file i').on('click', function(e) {
            var fId = $(this).prop('id');
            var id = parseInt(fId.substr(fId.indexOf('_') + 1));

            var pParent = $(this);
            while(true) {
                pParent = pParent.parent();
                if (pParent.get(0).tagName == 'P' && pParent.hasClass('added-mess-file')) {
                    break;
                }
            }

            $.ajax({
                url: deleteUrl + id,
                method: 'POST',
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if (data.success) {
                        pParent.remove();
                    }
                }
            });
        });

        var loadedImageId = result.info.id;

        var filesPresent = $('.added-mess-file').length || files.length;
        if (!filesPresent) {
            $('<br />').appendTo(parent);
        }

        var fLink = '<p class="added-mess-file">';
        fLink += '<a href="#" style="font-size:130%;" class=\"hover-text-decoration">';
        fLink += '<img src="/themes/leadlance/img/profile/add-file-mess.png\" alt="">' + f.originalName;
        fLink += '</a>';
        fLink += '<i class="glyphicon glyphicon-remove float-right right20" id="fDel_' + loadedImageId + '">';
        fLink += '</p>';
        $(fLink).appendTo(parent);

        files[loadedImageId] = f;
    });

    $('.added-mess-file i').on('click', function(e) {
        var fId = $(this).prop('id');
        var id = parseInt(fId.substr(fId.indexOf('_') + 1));

        var pParent = $(this);
        while(true) {
            pParent = pParent.parent();
            if (pParent.get(0).tagName == 'P' && pParent.hasClass('added-mess-file')) {
                break;
            }
        }

        $.ajax({
            url: deleteUrl + id,
            method: 'POST',
            dataType: 'json',
            success: function(data) {
                console.log(data);
                if (data.success) {
                    pParent.remove();
                }
            }
        });
    });

    // hide `lead cost` field if project is `auction`
    var leadCost = $('#project-is_auction').on('click', function () {
        var projectLeadCost = $('#project-lead_cost')
        if($(this).prop("checked")) {
            var oldProjectLeadCostValue = projectLeadCost.val();
            projectLeadCost.get(0).dataset.oldValue =  oldProjectLeadCostValue;
            projectLeadCost.val("");
            projectLeadCost.prop('disabled', true);
            projectLeadCost.closest('label').siblings('#project-currency').prop('disabled', true);
        } else {
            projectLeadCost.prop('disabled', false);
            if (projectLeadCost.get(0).dataset && projectLeadCost.get(0).dataset.oldValue) {
                projectLeadCost.val(projectLeadCost.get(0).dataset.oldValue);
            }
            projectLeadCost.closest('label').siblings('#project-currency').prop('disabled', false);
        }
    });

});
