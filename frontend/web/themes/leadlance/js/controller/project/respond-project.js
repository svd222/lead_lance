$(document).ready(function(){
    var formName = collection.get('action').get('formName');
    var csrfParam = collection.get('action').get('csrfParam');
    var deleteUrl = collection.get('action').get('deleteUrl');
    var imageFormName = collection.get('action').get('imageFormName');
    var mainForm = $('#' + formName);
    var csrf = mainForm.find("input[name='" + csrfParam + "']").val();
    var imageContainerId = (imageFormName + '-image').toLowerCase();
    var batch_id;
    var files = [];
    var image = $('#' + imageContainerId);

    image.off('fileuploadsubmit');
    image.on('fileuploadsubmit', function (e, data) {
        data.formData = {
            _csrf_frontend: csrf
        };
        if (batch_id) {
            data.formData.batch_id = batch_id;
        }
    });

    image.off('fileuploaddone');
    image.on('fileuploaddone', function (e, data) {
        var imageContainer = $('#' + imageContainerId.toLowerCase());
        var parent = imageContainer;
        while(true) {
            parent = parent.parent();
            if (parent.get(0).tagName == 'DIV' && (parent.hasClass('dropbox') || parent.hasClass('dropbox-mini'))) {
                break;
            }
        }
        // <div class='dropbox right-input-file-download'>
        var result = JSON.parse(data.result);
        var f = result.files[0];
        batch_id = result.info.batch_id;

        var form = image.parents('form');
        var batchInput = form.find('.batch-id-info');
        if (batchInput == undefined || !batchInput.val()) {
            var fForm = imageContainer;
            while(true) {
                fForm = fForm.parent();
                if (fForm.get(0).tagName == 'FORM') {
                    break;
                }
            }
            $('<input class="batch-id-info" type="hidden" name="' + formName + '[batch_id]" value="' + batch_id + '">').appendTo(fForm);
        }

        var loadedImageId = result.info.id;

        if (!files.length) {
            $('<br />').appendTo(parent);
        }

        var fLink = '<p class="added-mess-file">';
        fLink += '<a href="#" style="font-size:130%;" class=\"hover-text-decoration">';
        fLink += '<img src="/themes/leadlance/img/profile/add-file-mess.png\" alt="">' + f.originalName;
        fLink += '</a>';
        fLink += '<i class="glyphicon glyphicon-remove float-right right20" id="fDel_' + loadedImageId + '">';
        fLink += '</p>';
        $(fLink).appendTo(parent);

        $('body').on('click', '.added-mess-file i', function(e) {
            var self = $(e.target);
            var fId = self.prop('id');
            var id = parseInt(fId.substr(fId.indexOf('_') + 1));

            var pParent = self;
            while(true) {
                pParent = pParent.parent();
                if (pParent.get(0).tagName == 'P' && pParent.hasClass('added-mess-file')) {
                    break;
                }
            }

            $.ajax({
                url: deleteUrl + id,
                method: 'POST',
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        pParent.remove();
                        $('input[name="' + formName + '[batch_id]"]').remove();
                    }
                }
            });
        });

        files[loadedImageId] = f;
    });

    // slide down active conversation and scroll to first message in conversation
    var linkReviewsContainer = $('.answer-block-all .wrapper-slide-reviews');

    if (typeof linkReviewsContainer.attr('active') == "string") {
        var firstElm =  linkReviewsContainer.find('.list-view > div:eq(0)');

        // var isFirefox = typeof InstallTrigger !== 'undefined';
        // if (isFirefox) {
        //     $('html').css({
        //         overflow: 'hidden',
        //         height: '100%'
        //     });
        //
        //     $('body').css({
        //         overflow: 'auto',
        //         height: '100%'
        //     });
        // }
        // if (firstElm != undefined) {
        //     var diff = firstElm.offset().top - 185;
        //     $('html, body')
        //         .animate({scrollTop: diff})
        // }
    }

    var checkedPossibleMaxCount = collection.get('maxPossibleSelectedCasesCount');
    var checkedCount = 0;

    $('div#create-keys .block-answer-dop-img').on('click', function (e) {
        var input = $(this).find('input[type=checkbox]');
        var checked = input.prop('checked');
        if(checked || (checkedCount < checkedPossibleMaxCount && !checked)) {
            if (!checked) {
                checkedCount++;
            } else {
                checkedCount--;
            }
            if (checkedCount <= 3) {
                input.prop('checked', !checked);
            }
        }
    });

    $('#create-keys #append_profile_cases').on('click', function () {
        // clears early added input`s
        $('.profile-case-item').remove();

        var itemCount = 0;
        var imgs = [];
        // pass trhough each checked input in modal window and create appropriate form input to send to server
        $('#create-keys .block-answer-dop-img input[type=checkbox]').each(function (index, elm) {
            if ($(this).prop('checked')) {
                var val = $(this).prop('id');
                val = val.substr(val.lastIndexOf('_') + 1);
                var profileCaseModelFormName = collection.get('action').get('profileCaseModelFormName');
                var name = profileCaseModelFormName + '[id][]';
                var id = profileCaseModelFormName.toLowerCase() + '_id_' + val;
                $('<input type="hidden" name="' + name + '" id="' + id + '" value="' + val + '" class="profile-case-item">').appendTo($('#ProjectMessage'));

                var parent = $(this);
                while(true) {
                    parent = parent.parent();
                    if (parent.hasClass('block-answer-dop-img')) {
                        break;
                    }
                }
                var width = parent.width() - 15;
                var height = parent.height() - 20;

                var img = parent.find('span img:eq(0)');
                if (img.attr('src')) {
                   imgs.push('<img src="' + img.attr('src') +'" style="width:' + width + 'px; height:' + height + 'px">');
                } else {
                    imgs.push('<div class="case-img-empty div-inline"><p class="photo-bold">LeadLance</p><p class="photo-light">(no photo)</p></div>');
                }
                itemCount++;
            }
        });
        // fill the appropriate div containers
        var defaultContent = '<p class="block-answer-dop-img-text">добавить кейс</p>\
            <span class="btn btn-default btn-file keys-input-file"> </span>';

        var container = $('#ProjectMessage .block-mess-textarea .block-answer-dop-img');
        for(var i = 0; i < checkedPossibleMaxCount; i++) {
            container.eq(i).empty();
            container.append($(defaultContent));
        }
        for(var i = 0; i < imgs.length; i++) {
            container.eq(i).empty();
            if (imgs[i]) {
                $(imgs[i]).appendTo(container.eq(i));
            }
        }

        itemCount = 0;

        $('#create-keys').modal('hide');
    });

    var documentBounds = {
        w: document.documentElement.clientWidth,
        h: document.documentElement.clientHeight,
    };

    var respondProject = {
        options: {
            sent: [],
            readMessages: []
        },

        init: function () {
            this.setAllNewMessagesRead();
            this.sendNewMessageEvent();
            this.scrollEvent();

            var pmTimer = window.setInterval(
                respondProject.setMessageStatus(),
                parseInt(collection.get('conversationUpdateStatusInterval'))
            );
        },

        setRead: function(item) {
            var self = this;
            var itemBounds = item.getBoundingClientRect();
            if (itemBounds.top < documentBounds.h) {
                var itemId = item.dataset.id;
                if (!inArray(itemId, self.options.readMessages) && !inArray(itemId, self.options.sent)) {
                    self.options.readMessages.push(itemId);
                }
                $('> div:nth-last-child(1)', $(item)).stop().fadeTo(2500, 0, function() {
                    $(item).removeClass('new-message');
                    $(this).remove();
                });
            }
        },

        setMessageStatus: function () {
            var self = this;
            if (self.options.readMessages.length) {
                $.post(
                    '/project/set-project-messages-status-received',
                    {
                        ids: self.options.readMessages
                    },
                    function (data) {
                        self.options.sent = self.options.sent.concat(self.options.readMessages);
                        self.options.readMessages = [];
                        if (data.required_to_update == data.updated) {

                            // refresh projects messages counter in left menu by all projects
                            var allProjectMessageCounterContainer = $('.left-menu-toogle > li > a.menu-projects');
                            var allProjectMessageCounter = $('span.menu-counter:eq(0)', allProjectMessageCounterContainer);
                            if (typeof allProjectMessageCounter.get(0) != "undefined") {
                                var allProjectMessageCounterVal = parseInt(allProjectMessageCounter.text()) - data.updated;
                                allProjectMessageCounter.text(allProjectMessageCounterVal);
                                if (allProjectMessageCounterVal === 0 || allProjectMessageCounterVal < 0) {
                                    allProjectMessageCounter.remove();
                                }
                            }

                        }
                    },
                    'json'
                );
            }
        },

        sendNewMessageEvent: function() {
            $('body').on('click', '.button-send-project-message', function (e) {
                var self = $(e.target);
                self.prop("disabled", true);

                var form = self.parents('form');

                $.ajax({
                    type: 'POST',
                    url: form.action,
                    data: form.serialize(),
                    success: function (data) {
                        if (data.html != undefined) {
                            $('#wrapper-slide-reviews').html(data.html);
                            form.find('#projectmessage-message').val('');
                            form.find('.batch-id-info').remove();
                            form.find('.added-mess-file').remove();
                            batch_id = 0;
                        }
                    }
                });

                setTimeout(function () {
                        self.prop("disabled", false);
                    },
                    4000
                );
            });
        },

        setAllNewMessagesRead: function () {
            var areNewMessages = false;
            $('.wrapper-slide-reviews .list-view:eq(0) > div.new-message').each(function (index, item) {
                respondProject.setRead(item);
                areNewMessages = true;
            });
            if (areNewMessages) {
                respondProject.setMessageStatus();
            }
        },

        scrollEvent: function () {
            var self = this;
            $(document).scroll(function() {
                self.setAllNewMessagesRead();
            });
        }
    };
    respondProject.init();

});
