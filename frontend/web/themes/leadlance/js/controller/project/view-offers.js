
$(document).ready(function (e) {
    var currentRoute = collection.get('currentRoute');

    // slide down active conversation and scroll to first message in conversation
    $('.answer-block-all > .list-view > div').each(function (index, elm) {
        if (typeof elm.getAttribute('active') == "string") {
            var linkReviews = $('.answer-content .block-rewievs-print .print-reviews li a.link-reviews-drop-down', $(this));
            linkReviews.click();
            var conversationContainer = linkReviews.closest('.answer-content');
            var firstElm = $('.wrapper-slide-reviews .list-view > div:eq(0)', conversationContainer);
            var b = $('body');

            var isFirefox = typeof InstallTrigger !== 'undefined';
            if (isFirefox) {
                $('html').css({
                    overflow: 'hidden',
                    height: '100%'
                });

                $('body').css({
                    overflow: 'auto',
                    height: '100%'
                });
                b.animate({ scrollTop: firstElm.offset().top })
            } else {
                b.animate({ scrollTop: firstElm.offset().top });
            }
        }
    });

    $(document).on('wsProjectMessage', function (e) {

        var wsTransferObject = e.detail;

        var fromUserId = wsTransferObject.attributes.from_user_id;
        var toUserId = wsTransferObject.attributes.to_user_id;
        var conversationId = getConversationId(fromUserId, toUserId);
        var conversation;
        $('.block-rewievs-print').each(function(index, elm) {
            if (!conversation) {
                var ds = $(this).get(0).dataset;
                if (ds.conversationId == conversationId) {
                    conversation = $(this);
                }
            }
        });

        if (conversation) {
            var counterContainer = $('ul.print-reviews > li:nth-last-child(1)', conversation);
            var counterContainerInner = $('span:eq(0)', counterContainer);
            if (counterContainerInner.get(0) != undefined) {
                if (!counterContainerInner.hasClass('not-blue-count')) {
                    var counter = $('a:eq(0)', counterContainerInner);
                    counter.text(parseInt(counter.text().trim()) + 1);
                }
            } else {
                $('<span class="blue-number reviews-blue-count">\
                <a href="#" class="hover-text-decoration">1</a>\
                    </span>').prependTo(counterContainer);
            }
        }
    });

    var documentBounds = {
        w: document.documentElement.clientWidth,
        h: document.documentElement.clientHeight,
    };

    // custom forms validation
    // check that message is not empty when sending the form
    // this replace yii standard validation because form was renamed to have `conversation index` i.e. ProjectMessage_1_3
    // and therefore yiiActiveForm object don`t work as expected...

    $('form').each(function (index, elm) {
        var id = elm.getAttribute('id');
        var test = /[a-zA-Z]+_\d{1,2}_\d{1,2}/m.test(id);
        if (test) {
            var that = $(this);

            that.on('beforeSubmit', function (e) {
                var val = that.find('#projectmessage-message').val();
                var validated = false;
                if (typeof val != "undefined" && typeof val == "string" && val.length) {
                    validated = true;
                }
                if (!validated) {
                    /*$.gritter.add({
                        title: '',
                        text: 'Сообщение не может быть пустым',
                        class_name: 'gritter-error',
                        time: 4000,
                    });*/

                    e.returnValue = false;
                    return false;
                }
            })
        }
    });

    $('.create-order-btn').on('click', function () {
        var currentRoute = collection.get('currentRoute');
        var projectId = parseInt(currentRoute.params[0]);
        var parent = $(this);

        var executorId;
        var cId = $(this).closest('.answer-content').parent('div').get(0).dataset.key.split('_');
        var userId = parseInt(collection.get('userId'));
        if (parseInt(cId[0]) == userId) {
            executorId = parseInt(cId[1]);
        } else {
            executorId = parseInt(cId[0]);
        }

        $('#create-order .button-create-order').on('click', function (e) {
            window.location.href = '/order/create/' + projectId + '/' + executorId;
        });
        $('#create-order').modal();
        return false;
    });

    var viewOffers = {
        options: {
            sent: [],
            readMessages: []
        },

        init: function () {
            this.setAllNewMessagesRead();
            this.sendNewMessageEvent();
            this.scrollEvent();
            this.messageBlockShowEvent();

            var pmTimer = window.setInterval(
                viewOffers.setMessageStatus(),
                parseInt(collection.get('conversationUpdateStatusInterval'))
            );
        },

        setRead: function(item) {
            var self = this;
            var itemBounds = item.getBoundingClientRect();
            if (itemBounds.top < documentBounds.h) {
                var itemId = item.dataset.id;
                if (!inArray(itemId, self.options.readMessages) && !inArray(itemId, self.options.sent)) {
                    self.options.readMessages.push(itemId);
                }
                $('> div:nth-last-child(1)', $(item)).stop().fadeTo(2500, 0, function() {
                    $(item).removeClass('new-message');
                    $(this).remove();
                });
            }
        },

        setMessageStatus: function () {
            var self = this;
            if (self.options.readMessages.length) {
                $.post(
                    '/project/set-project-messages-status-received',
                    {
                        ids: self.options.readMessages
                    },
                    function (data) {
                        self.options.sent = self.options.sent.concat(self.options.readMessages);
                        self.options.readMessages = [];

                        if (data.required_to_update == data.updated) {
                            var hideShowMessagesButton = $('.wrapper-slide-reviews:visible').parents('.answer-content').find('.link-reviews-drop-down');
                            var blueContainer = hideShowMessagesButton.siblings('span.blue-number');
                            var counter = ('a', blueContainer);
                            var newCounterVal = parseInt(counter.text()) - data.updated;
                            counter.text(newCounterVal);
                            if (!newCounterVal) {
                                blueContainer.remove();
                            }

                            // refresh main offers page counter
                            var allProjectMessageCounterTopContainer = $('#view-offers-project-control > span.blue-number');
                            var allProjectMessageCounterTop = ('> a', allProjectMessageCounterTopContainer);
                            if (typeof allProjectMessageCounterTop.get(0) != "undefined") {
                                var allProjectMessageCounterTopVal = parseInt(allProjectMessageCounterTop.text()) - data.updated;
                                allProjectMessageCounterTop.text(allProjectMessageCounterTopVal);
                                if (allProjectMessageCounterTopVal === 0 || allProjectMessageCounterTopVal < 0) {
                                    allProjectMessageCounterTop.remove();
                                }
                            }

                            // refresh projects messages counter in left menu by all projects
                            var allProjectMessageCounterContainer = $('.left-menu-toogle > li > a.menu-projects');
                            var allProjectMessageCounter = $('span.menu-counter:eq(0)', allProjectMessageCounterContainer);
                            if (typeof allProjectMessageCounter.get(0) != "undefined") {
                                var allProjectMessageCounterVal = parseInt(allProjectMessageCounter.text()) - data.updated;
                                allProjectMessageCounter.text(allProjectMessageCounterVal);
                                if (allProjectMessageCounterVal === 0 || allProjectMessageCounterVal < 0) {
                                    allProjectMessageCounter.remove();
                                }
                            }

                            // refresh project messages counter in left project slide menu
                            $('.left-menu-toogle > li > ul#person-project-list li').each(function (index, elm) {
                                var projectLink = $('a', $(this));
                                var projectId = parseInt(projectLink.get(0).dataset.projectId);
                                if (projectId == parseInt(currentRoute.params[0])) {
                                    var projectMessageCounter = $('span.submenu-counter', projectLink);
                                    if (projectMessageCounter.get(0)) {
                                        var projectMessageCounterVal = parseInt(projectMessageCounter.text());
                                        projectMessageCounterVal -= data.updated;
                                        projectMessageCounter.text(projectMessageCounterVal);
                                        if (projectMessageCounterVal == 0 || projectMessageCounterVal < 0) {
                                            projectMessageCounter.remove();
                                        }
                                    }
                                }
                            });
                        }
                    },
                    'json'
                );
            }
        },

        sendNewMessageEvent: function() {
            $('body').on('click', '.button-send-project-message', function (e) {
                var self = $(e.target);
                self.prop("disabled", true);

                var form = self.parents('form');

                $('.conversation_ids').remove();
                var parent = form;

                while(true) {
                    parent = parent.parent();
                    if (parent.get(0).tagName == 'DIV' && parent.hasClass('print-answer-block-all') && parent.hasClass('hide-main')) {
                        break;
                    }
                }
                var conversationId = parent.get(0).dataset.conversationId;
                $('<input type="hidden" class="conversation_ids" name="conversation_id" value="' + conversationId + '">').appendTo(form);

                $.ajax({
                    type: 'POST',
                    url: form.action,
                    data: form.serialize(),
                    success: function (data) {
                        if (data.html != undefined) {
                            $('#wrapper-conversation-' + conversationId).html(data.html);
                            form.find('#projectmessage-message').val('');
                            form.find('.batch-id-info').remove();
                            form.find('.added-mess-file').remove();
                            batch_id = 0;
                        }
                    }
                });

                var messagesBlock = self.parents('.answer-content').find('.wrapper-slide-reviews');
                if (!messagesBlock.is(":visible")) {
                    var toggleButton = self.parents('.answer-content').find('.link-reviews-drop-down');
                    toggleButton.trigger('click');
                }

                setTimeout(function () {
                        self.prop("disabled", false);
                    },
                    4000
                );
            });
        },

        setAllNewMessagesRead: function () {console.log('setALl');
            var areNewMessages = false;
            $('.wrapper-slide-reviews .list-view:eq(0) > div.new-message').each(function (index, item) {
                if (!$(item).is(':visible')) {
                    return;
                }
                viewOffers.setRead(item);
                areNewMessages = true;
            });
            if (areNewMessages) {
                viewOffers.setMessageStatus();
            }
        },

        scrollEvent: function () {
            var self = this;
            $(document).scroll(function() {
                self.setAllNewMessagesRead();
            });
        },

        messageBlockShowEvent: function () {
            var self = this;
            $('body').on('click', '.link-reviews-drop-down', function (event) {
                var that = $(event.target);
                var answerContent = that.parents('.answer-content');

                var messagesBlock = that.closest('.block-rewievs-print').siblings('.wrapper-slide-reviews');
                if(that.hasClass('up-rev-act')) {
                    that.text('Развернуть').prev().removeClass('not-blue-count');
                    answerContent.find('.answer-block').slideUp();
                    messagesBlock.slideUp('slow',function () {
                        that.removeClass('up-rev-act');
                    });
                } else {
                    that.text('Свернуть').prev().addClass('not-blue-count');
                    answerContent.find('.answer-block').slideDown();
                    messagesBlock.slideDown('slow',function () {
                        that.addClass('up-rev-act');
                        if (!that.hasClass('answer-initialized')) {
                            var conversationId = that.closest('.block-rewievs-print').get(0).dataset.conversationId;
                            self.initAnswerBlockEvents(conversationId);
                            that.addClass('answer-initialized');
                        }
                    });
                }
            });
        },

        initAnswerBlockEvents: function (conversationId) {
            var answerBlock = $('#' + conversationId);

            ctx = $('div#' + conversationId + ':eq(0)');

            var dataIndex = answerBlock.get(0).dataset.index;

            var formName = collection.get('action').get('formName')[dataIndex];
            var imageFormName = collection.get('action').get('imageFormName')[dataIndex];

            var deleteUrl = collection.get('action').get('deleteUrl');

            var imageContainerId = '#' + imageFormName;
            var image = ctx.find(imageContainerId);

            batch_id = 0;
            files = [];

            image.on('fileuploadsubmit', function (e, data) {
                var csrfParam = collection.get('action').get('csrfParam');
                var csrf = $("input[name='_csrf-frontend']").val();

                data.formData = {
                    _csrf_frontend: csrf
                };
                if (batch_id) {
                    data.formData.batch_id = batch_id;
                }
            });

            image.on('fileuploaddone', function (e, data) {
                var parent = ctx.find(imageContainerId);
                while (parent) {
                    parent = parent.parent();
                    if (parent.hasClass('dropbox') || parent.hasClass('dropbox-mini')) {
                        break;
                    }
                }
                var result = JSON.parse(data.result);
                var f = result.files[0];
                batch_id = result.info.batch_id;

                var form = $(imageContainerId).parents('form');
                var batchInput = form.find('.batch-id-info');
                if (batchInput == undefined || !batchInput.val()) {

                    var fForm = ctx.find(imageContainerId);
                    while (true) {
                        fForm = fForm.parent();
                        if (fForm.get(0).tagName == 'FORM') {
                            break;
                        }
                    }
                    $('<input class="batch-id-info" type="hidden" name="' + formName + '[batch_id]" value="' + batch_id + '">').appendTo(fForm);
                }

                var loadedImageId = result.info.id;

                if (!files.length) {
                    $('<br />').appendTo(parent);
                }

                var fLink = '<p class="added-mess-file">';
                fLink += '<a href="#" style="font-size:130%;" class=\"hover-text-decoration">';
                fLink += '<img src="/themes/leadlance/img/profile/add-file-mess.png\" alt="">' + f.originalName;
                fLink += '</a>';
                fLink += '<i class="glyphicon glyphicon-remove float-right right20" id="fDel_' + loadedImageId + '">';
                fLink += '</p>';
                $(fLink).appendTo(parent);

                $('body').on('click', '.added-mess-file i', function (e) {
                    var self = $(e.target);
                    var fId = self.prop('id');
                    var id = parseInt(fId.substr(fId.indexOf('_') + 1));

                    var pParent = self;
                    while (true) {
                        pParent = pParent.parent();
                        if (pParent.get(0).tagName == 'P' && pParent.hasClass('added-mess-file')) {
                            break;
                        }
                    }

                    $.ajax({
                        url: deleteUrl + id,
                        method: 'POST',
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                pParent.remove();
                                $('input[name="' + formName + '[batch_id]"]').remove();
                            }
                        }
                    });
                });

                files[loadedImageId] = f;
            });
        }
    };
    viewOffers.init();
});
