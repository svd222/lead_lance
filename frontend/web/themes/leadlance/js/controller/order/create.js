/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 15.03.17
 * @time: 16:41
 */
$(document).ready(function(){
    var formName = collection.get('action').get('formName');
    var csrfParam = collection.get('action').get('csrfParam');
    var deleteUrl = collection.get('action').get('deleteUrl');
    var imageFormName = collection.get('action').get('imageFormName');

    var mainForm = $('#' + formName);
    var csrf = mainForm.find("input[name='" + csrfParam + "']").val();
    var imageContainerId = (imageFormName + '-image').toLowerCase();
    var batch_id;
    var batchHiddenField = false;
    var files = [];


    $('#' + imageContainerId).bind('fileuploadsubmit', function (e, data) {
        data.formData = {
            _csrf_frontend: csrf
        };
        if (batch_id) {
            data.formData.batch_id = batch_id;
        }
    });

    $('#' + imageContainerId).bind('fileuploaddone', function (e, data) {
        var imageContainer = $('#' + imageContainerId.toLowerCase());
        var parent = imageContainer;
        while(true) {
            parent = parent.parent();
            if (parent.get(0).tagName == 'DIV' && (parent.hasClass('dropbox') || parent.hasClass('dropbox-mini'))) {
                break;
            }
        }
        // <div class='dropbox right-input-file-download'>
        var result = JSON.parse(data.result);
        var f = result.files[0];
        batch_id = result.info.batch_id;
        console.log(result);

        if (!batchHiddenField) {
            var fForm = imageContainer;
            while(true) {
                fForm = fForm.parent();
                if (fForm.get(0).tagName == 'FORM') {
                    break;
                }
            }
            $('<input type="hidden" name="' + formName + '[batch_id]" value="' + batch_id + '">').appendTo(fForm);
            batchHiddenField = true;
        }

        var loadedImageId = result.info.id;

        if (!files.length) {
            $('<br />').appendTo(parent);
        }

        var fLink = '<p class="added-mess-file">';
        fLink += '<a href="#" style="font-size:130%;" class=\"hover-text-decoration">';
        fLink += '<img src="/themes/leadlance/img/profile/add-file-mess.png\" alt="">' + f.originalName;
        fLink += '</a>';
        fLink += '<i class="glyphicon glyphicon-remove float-right right20" id="fDel_' + loadedImageId + '">';
        fLink += '</p>';
        $(fLink).appendTo(parent);

        $('.added-mess-file i').on('click', function(e) {
            var fId = $(this).prop('id');
            var id = parseInt(fId.substr(fId.indexOf('_') + 1));

            var pParent = $(this);
            while(true) {
                pParent = pParent.parent();
                if (pParent.get(0).tagName == 'P' && pParent.hasClass('added-mess-file')) {
                    break;
                }
            }

            $.ajax({
                url: deleteUrl + id,
                method: 'POST',
                dataType: 'json',
                success: function(data) {
                    if (data.success) {
                        pParent.remove();
                    }
                }
            });
        });

        files[loadedImageId] = f;
    });

    $('.button-return-order').on('click', function () {
        var currentRoute = collection.get('currentRoute');
        var projectId = parseInt(currentRoute.params[0]);
        window.location.href = '/project/view-offers/' + projectId;
        return false;
    });
});
