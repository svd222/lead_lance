$(document).ready(
    function () {
        var formName = collection.get('action').get('formName');
        var csrfParam = collection.get('action').get('csrfParam');
        var deleteUrl = collection.get('action').get('deleteUrl');
        var imageFormName = collection.get('action').get('imageFormName');

        var mainForm = $('#' + formName);
        var csrf = mainForm.find("input[name='" + csrfParam + "']").val();
        var imageContainerId = (imageFormName + '-image').toLowerCase();
        var batch_id;
        var batchHiddenField = false;
        var files = [];


        $('#' + imageContainerId).bind('fileuploadsubmit', function (e, data) {
            data.formData = {
                _csrf_frontend: csrf
            };
            if (batch_id) {
                data.formData.batch_id = batch_id;
            }
        });

        $('#' + imageContainerId).bind('fileuploaddone', function (e, data) {
            var imageContainer = $('#' + imageContainerId.toLowerCase());
            var parent = imageContainer;
            while(true) {
                parent = parent.parent();
                if (parent.get(0).tagName == 'DIV' && (parent.hasClass('dropbox') || parent.hasClass('dropbox-mini'))) {
                    break;
                }
            }
            // <div class='dropbox right-input-file-download'>
            var result = JSON.parse(data.result);
            var f = result.files[0];
            batch_id = result.info.batch_id;

            if (!batchHiddenField) {
                var fForm = imageContainer;
                while(true) {
                    fForm = fForm.parent();
                    if (fForm.get(0).tagName == 'FORM') {
                        break;
                    }
                }
                $('<input type="hidden" name="' + formName + '[batch_id]" value="' + batch_id + '">').appendTo(fForm);
                batchHiddenField = true;
            }

            var loadedImageId = result.info.id;

            if (!files.length) {
                $('<br />').appendTo(parent);
            }

            var fLink = '<p class="added-mess-file">';
            fLink += '<a href="#" style="font-size:130%;" class=\"hover-text-decoration">';
            fLink += '<img src="/themes/leadlance/img/profile/add-file-mess.png\" alt="">' + f.originalName;
            fLink += '</a>';
            fLink += '<i class="glyphicon glyphicon-remove float-right right20" id="fDel_' + loadedImageId + '">';
            fLink += '</p>';
            $(fLink).appendTo(parent);

            $('.added-mess-file i').on('click', function(e) {
                var fId = $(this).prop('id');
                var id = parseInt(fId.substr(fId.indexOf('_') + 1));

                var pParent = $(this);
                while(true) {
                    pParent = pParent.parent();
                    if (pParent.get(0).tagName == 'P' && pParent.hasClass('added-mess-file')) {
                        break;
                    }
                }

                $.ajax({
                    url: deleteUrl + id,
                    method: 'POST',
                    dataType: 'json',
                    success: function(data) {
                        if (data.success) {
                            pParent.remove();
                        }
                    }
                });
            });

            files[loadedImageId] = f;
        });

        var form = $('#OrderMessage');
        form.on('beforeSubmit', function () {
            /*var sendedData = {
                message: form.find('#ordermessage-message').val(),
                fromUserId: form.find('#ordermessage-from_user_id').val(),
                toUserId: form.find('#ordermessage-to_user_id').val(),
                orderId: form.find('#ordermessage-order_id').val(),
            }*/
            $.ajax({
                url : '/order/order-message-send',
                data: form.serialize(),
                method: 'POST',
                dataType: 'json',
                success: function (data) {
                    if ($.isPlainObject(data) && typeof data.id != "undefined" && data.id) {
                        //success
                        var msgBlock = $(getOrderMessage(data));
                        var formContainer = $('#order-message-form-container');
                        formContainer.after(msgBlock);

                        //clear form & batch images
                        $('#ordermessage-message').val('');
                        $('p.added-mess-file').remove();
                        form.find('input[type=hidden]').each(function (index, elm) {
                            if ($(this).attr('name') == 'OrderMessage[batch_id]') {
                                $(this).remove();
                            }
                        });
                        batchHiddenField = false;
                        files = [];
                        batch_id = null;
                    }

                }
            });
            return false;
        });
    }
);