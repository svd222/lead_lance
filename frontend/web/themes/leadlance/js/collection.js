/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 14.03.17
 * @time: 14:41
 */
function Collection() {
    this.varCollection = [];
}

Collection.prototype.set = function (name, value, replace) {
    if (replace == undefined) {
        relplace = false;
    }
    if (!this.get(name) || (this.get(name) !== undefined && replace)) {
        this.varCollection[name] = value;
    }
}

Collection.prototype.get = function (name, defaultValue) {
    return this.varCollection[name] ? this.varCollection[name] : defaultValue;
}

var collection = new Collection();

function Action(id) {
    this.id = id;
}

Action.prototype = collection;

function Controller(id) {
    this.id = id;
}

Controller.prototype = collection;
