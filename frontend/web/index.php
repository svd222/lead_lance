<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
require(__DIR__ . '/../../common/config/bootstrap.php');
require(__DIR__ . '/../config/bootstrap.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . '/../../common/config/main-local.php'),
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . '/../config/main-local.php')
);

\yii\base\Event::on(\yii\web\Application::className(), \yii\web\Application::EVENT_BEFORE_REQUEST, function ($e) {
    /*var_dump($_SERVER);
    var_dump(Yii::$app->user->isGuest);
    var_dump(preg_match("|/landing|mis", $_SERVER['REQUEST_URI']));
    var_dump(Yii::$app->user->isGuest && !preg_match("|/landing|mis", $_SERVER['REQUEST_URI']));*/

    /*if (Yii::$app->user->isGuest && !preg_match("|/landing|mis", $_SERVER['REQUEST_URI'])) {
        Yii::$app->response->redirect('/landing');
        Yii::$app->end();
    }*/
});

(new yii\web\Application($config))->run();
