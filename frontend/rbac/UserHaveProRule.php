<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 10.04.17
 * @time: 20:50
 */
namespace frontend\rbac;

use common\models\User;
use common\models\ProAccountQuery;
use yii\rbac\Rule;

/**
 * Checks if user have PRO account
 */
class UserHaveProRule extends Rule
{
    public $name = 'haveProAccount';

    /**
     * @var boolean $result Store the result of request permission
     */
    public static $result;

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        return User::hasPro($user);
    }
}