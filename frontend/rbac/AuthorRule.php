<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 14.06.17
 * @time: 8:08
 */
namespace frontend\rbac;

use yii\base\InvalidCallException;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\rbac\Rule;
use Yii;

/**
 * Class AuthorRule
 *
 * Check if user is author of model
 *
 * Usage:
 * ```php
 * Yii::$app->user->can('somePermissionOrRoleAssociatedWithThisRule', [
 *     'model' => $model,
 *     'property' => 'user_id'
 * ]);
 * ```
 *
 * @package frontend\rbac
 */
class AuthorRule extends Rule
{
    public $name = 'userIsAuthor';

    /**
     * @param string|int $user the user ID.
     * @param Item $item the role or permission that this rule is associated with
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        $model = ArrayHelper::getValue($params, 'model');
        $property = ArrayHelper::getValue($params, 'property');
        if (!$model) {
            throw new InvalidCallException();
        }
        if (!$property) {
            throw new InvalidCallException();
        }
        return $user == $model->$property;
    }
}