<?php

return [
    [
        'username' => 'okirlin',
        'auth_key' => 'iwTNae9t34OmnK6l4vT4IeaTk-YWI2Rv',
        'password_hash' => '$2y$13$CXT0Rkle1EMJ/c1l5bylL.EylfmQ39O5JlHJVFpNn618OUS1HwaIi',
//        'password_reset_token' => 't5GU9NwpuGYSfb7FEZMAxqtuz2PkEvv_' . time(),
        'created_at' => '1391885313',
        'updated_at' => '1391885313',
        'email' => 'brady.renner@rutherford.com',
    ],
    [
        'username' => 'troy.becker',
        'auth_key' => 'EdKfXrx88weFMV0vIxuTMWKgfK2tS3Lp',
        'password_hash' => '$2y$13$g5nv41Px7VBqhS3hVsVN2.MKfgT3jFdkXEsMC4rQJLfaMa7VaJqL2',
        //'password_reset_token' => '4BSNyiZNAuxjs5Mty990c47sVrgllIi_' . time(),
        'created_at' => '1391885313',
        'updated_at' => '1391885313',
        'email' => 'nicolas.dianna@hotmail.com',
        'status' => '0',
    ],

    [
        'username' => 'svd222',
        'auth_key' => '7UpGcH1rZgCVjdFHjLqmwc0ZkD_xoezq',
        // svd222
        'password_hash' => '$2y$10$ntvcTfxkwBtVmGK.XqGBPuDa5l43UdH2z1ETBPqs.gBqQz2nZYF1u',
        'created_at' => '1481612778',
        'updated_at' => '1481612778',
        'confirmed_at' => '1481612778',
        'registration_ip' => '127.0.0.1',
        'email' => 'svd22286@gmail.com',
    ],
];
