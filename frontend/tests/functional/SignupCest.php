<?php

namespace frontend\tests\functional;

use frontend\tests\FunctionalTester;

class SignupCest
{
    protected $formId = '#registration-form';


    public function _before(FunctionalTester $I)
    {
        $I->amOnRoute('user/register');
    }

    public function signupWithEmptyFields(FunctionalTester $I)
    {
        $I->see('Sign up', 'h3');
        $I->submitForm($this->formId, []);
        $I->seeValidationError('Username cannot be blank.');
        $I->seeValidationError('Email cannot be blank.');
        $I->seeValidationError('Password cannot be blank.');

    }

    public function signupWithWrongEmail(FunctionalTester $I)
    {
        $I->submitForm(
            $this->formId, [
            'register-form[username]'  => 'tester',
            'register-form[email]'     => 'ttttt',
            'register-form[password]'  => 'tester_password',
        ]
        );
        $I->dontSee('Username cannot be blank.', '.help-block');
        $I->dontSee('Password cannot be blank.', '.help-block');
        $I->see('Email is not a valid email address.', '.help-block');
    }

    public function signupSuccessfully(FunctionalTester $I)
    {
        $I->submitForm($this->formId, [
            'register-form[username]' => 'tester',
            'register-form[email]' => 'tester.email@example.com',
            'register-form[password]' => 'tester_password',
        ]);

        $I->seeRecord('common\models\User', [
            'username' => 'tester',
            'email' => 'tester.email@example.com',
        ]);

        $I->see('Your account has been created');
    }
}
