<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 23.02.17
 * @time: 18:41
 */
namespace frontend\models;

use common\models\ProfilePrivate;
use Yii;
use common\interfaces\IList;
use dektrium\user\models\RegistrationForm as BaseForm;
use yii\helpers\ArrayHelper;
use common\models\User;

class RegistrationForm extends BaseForm implements IList
{
    public $role;

    const ROLE_NONE = 'none';

    const ROLE_CUSTOMER = 'customer';

    const ROLE_EXECUTOR = 'executor';

    public $first_name;

    public $recaptcha;

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            'roleString' => ['role', 'string'],
            'roleRequired' => ['role', 'required'],
            'roleValid' => ['role', 'validateRole'],
            'nameRequired' => ['first_name', 'required'],
        ]);
    }

    /**
     * Validate user role attribute
     *
     * @param $attribute
     * @param $params
     */
    public function validateRole($attribute, $params)
    {
        $list = self::getList();
        if ($this->$attribute == self::ROLE_NONE) {
            $this->addError('role', Yii::t('app/registration', 'You must set user role'));
        }
        if (!isset($list[$this->$attribute])) {
            $this->addError('role', Yii::t('app/registration', 'Incorrect user role') . '`'.$this->$attribute.'`');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'role'    => Yii::t('app', 'Role'),
            'first_name' => Yii::t('app', 'First name'),
        ]);
    }

    /**
     * Overrides parent implementation, add assign role to user
     *
     * @return bool
     */
    public function register()
    {
        if (!$this->validate()) {
            return false;
        }

        /** @var User $user */
        $user = Yii::createObject(User::className());
        $user->setScenario('register');
        $this->loadAttributes($user);

        if (!$user->register()) {
            return false;
        }

        $profilePrivate = new ProfilePrivate();
        $profilePrivate->user_id = $user->id;
        $profilePrivate->first_name = $this->first_name;
        $profilePrivate->save();

        $user->ensureUploadDirExists();

        $authManager = Yii::$app->authManager;
        $role = $authManager->getRole($this->role);
        $authManager->assign($role, $user->id);

        Yii::$app->session->setFlash(
            'info',
            Yii::t(
                'app/user',
                'Your account has been created and confirmed'
            )
        );

        return true;
    }

    /**
     * @inheritdoc
     */
    public static function getList()
    {
        return [
            self::ROLE_CUSTOMER => Yii::t('app', 'Customer'),
            self::ROLE_EXECUTOR => Yii::t('app', 'Executor'),
        ];
    }

    /**
     * @inheritdoc
     */
    protected function loadAttributes(\dektrium\user\models\User $user)
    {
        if ($user->scenario == 'register') {
            $user->setAttribute('email', $this->email);
            $user->setAttribute('username', $this->username);
        } else {
            parent::loadAttributes($user);
        }
    }
}
