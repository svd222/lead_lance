## 1) Принципы ООП:

###Наследование:
Позволяет одному классу приобретать свойства и методы другого (при условии что они не обьявлены приватными). Например есть класс yii\web\Controller он унаследован от \yii\base\Controller и соотвественно в нем также как и в базовом есть такие свойства и методы как $enableCsrfValidation, $actionParams, beforeAction() и т.д. (Прилагаю NewsController.php)```
	
##Инкапсуляция: 
Позволяет сокрыть внутреннюю реализацию, а наружу предоставлять лишь необходимый клиенту интерфейс. В прилагаемом CurrentRoute.php есть закрытые свойства $moduleId, $controllerId, $actionId, а также соответствующие методы - внешний интерфейс для конечного клиента. getModuleId(), getControllerId(), getActionId().
	
##Полиморфизм: 
Полиморфизм я не использовал в данном проекте, но поясню. Позволяет обращаться к объектам унаследованным от одного базового полиморфно, например у всех фигур унаследованных от класса 
```php
class Figure
{
	abstract public function getSquare();
} 

class Square extends Figure {...}
class Circle extends Figure {...}
```
также будет метод getSquare (со своей конкретной реализацией т. к. метод в базовом классе объявлен абстрактным и его тело должно быть определено в наследуемых).
Тогда пусть у нас есть массив объектов типа Figure: $figures. И если нам нужно посчитать площадь всех фигур, мы можем написать:
```php
$square = 0;
foreach ($figures as $figure) {
	$square += $figire->getSquare();
}
```

## 2) MVC
###модель 
предоставляет и управляет данными
###вид 
предоставляет пользовательский интерфейс, в случае веб разработки это html css и вывод переменных
###контроллер 
делает запрос к модели и отдают виду см NewsController.php

## 3) Использование паттернов
В данном проекте я не использовал шаблоны проектирования (использовал на других проектах)
использовал на других проектах.

## 4) тестирование
тестируемость проекта реализована согласно стандартам фреймворка Yii2
тесты лежат в папке tests, я реализовал unit тесты (см. в прилагаемом файле ProbabilityTest.php)  
