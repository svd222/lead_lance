<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'controllerMap' => [
        'migrate'=>[
            'class'=>'console\commands\MigrateController',
            'migrationLookup'=>[
                /*'@app/console/migrations',*/
                '@vendor/dektrium/yii2-user/migrations',
                '@yii/rbac/migrations',
            ]
        ]
    ],
    'modules' => [
        'rbac' => 'dektrium\rbac\RbacConsoleModule'
    ],
    'params' => $params,
];
