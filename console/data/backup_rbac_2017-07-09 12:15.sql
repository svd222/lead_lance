/* backup of table `{{%auth_item}}` */
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('admin', 1, 'admin', NULL, NULL, 1482328642, 1485619516);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('backend_project-type_index', 2, 'View list project types', NULL, NULL, 1482329731, 1482329731);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('backend_traffic-type_index', 2, 'View traffic types list', NULL, NULL, 1482329760, 1482329760);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('common', 1, 'Grant access common permissions for customers and executors', NULL, NULL, 1491567595, 1499270437);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('customer', 1, 'customer role', NULL, NULL, 1484753283, 1497965601);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('DeleteOwnEntity', 2, 'Allow user to delete own early created model', 'AuthorRule', NULL, 1497418273, 1497418273);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('executor', 1, 'executor role', NULL, NULL, 1488229107, 1498148163);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_case_index', 2, 'Allow view list cases', NULL, NULL, 1497965569, 1497965569);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_message_create', 2, 'Allow start or continue conversation with other user', 'UserHaveProRule', NULL, 1491569013, 1492971162);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_message_get-user-info', 2, 'Provide user info through ajax', NULL, NULL, 1499270416, 1499270416);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_message_index', 2, 'Allow view all messages associated with this user', NULL, NULL, 1491457331, 1491457331);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_order_create', 2, 'allows customers to create orders', NULL, NULL, 1484753329, 1484753329);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_case-view', 2, 'Allow view case', NULL, NULL, 1497654171, 1497654171);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_cases', 2, 'View profile cases', NULL, NULL, 1486379005, 1486379005);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_change-password', 2, 'Allow change password through ajax', NULL, NULL, 1496302717, 1496302717);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_check-email', 2, 'Allow to send confirmation email', NULL, NULL, 1496060322, 1496060322);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_confirm-email', 2, 'Allow email confirmation on profile page', NULL, NULL, 1496077900, 1496077900);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_create-case', 2, 'Allow create case', NULL, NULL, 1486131950, 1497413162);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_delete-case', 2, 'Allow delete case', NULL, NULL, 1497415661, 1497415661);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_delete-profile', 2, 'Allow delete profile through ajax', NULL, NULL, 1496581363, 1496581363);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_get-city-list', 2, 'Allow get city list through ajax', NULL, NULL, 1496229576, 1496229576);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_image-upload', 2, 'Allow upload image (for imperavi editor ext)', NULL, NULL, 1497175303, 1497175303);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_images-get', 2, 'Allow get image (for imperavi editor ext)', NULL, NULL, 1497175212, 1497175212);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_index', 2, 'allow view profile', NULL, NULL, 1485607492, 1485607492);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_phone-e164', 2, 'Return correct phone through ajax', NULL, NULL, 1486814180, 1486814180);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_preview-image-delete', 2, 'Allow delete profile case preview image', NULL, NULL, 1497543918, 1497543918);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_save-private', 2, 'Allow ajax based saving personal info', NULL, NULL, 1487023664, 1487023664);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_upload-case-preview-image', 2, 'Allow upload case preview image', NULL, NULL, 1497515085, 1497515085);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_profile_validate-phone', 2, 'Allow phone validation through ajax', NULL, NULL, 1486811637, 1486811637);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_project_index', 2, 'Allow view list of projects', NULL, NULL, 1495286807, 1495286807);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_registration_validate-form', 2, 'Allow validate registration form through ajax', NULL, NULL, 1488209482, 1488209482);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('frontend_user-list_index', 2, 'Allow view users list', NULL, NULL, 1496690746, 1496691782);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('HasPro', 2, 'User has pro account', 'UserHaveProRule', NULL, 1494761566, 1494761628);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('moderator', 1, 'moderator', NULL, NULL, 1482328681, 1485607562);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('OfferOrder', 2, 'Allow offer to do order', 'UserHaveProRule', NULL, 1491928507, 1491932388);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('SeePersonalInfo', 2, 'Allow view personal info of other user', 'UserHaveProRule', NULL, 1491930696, 1491930696);
INSERT INTO `ll_auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('SendPrivateMessage', 2, 'Allow send private message to other user', 'UserHaveProRule', NULL, 1491925247, 1491925247);



/* backup of table `{{%auth_item_child}}` */
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('moderator', 'backend_project-type_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('moderator', 'backend_traffic-type_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'common');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'common');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('admin', 'customer');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'DeleteOwnEntity');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_case_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_case_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_message_create');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_message_get-user-info');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_message_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_order_create');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_profile_case-view');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_profile_cases');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_cases');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_profile_change-password');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_profile_check-email');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_profile_confirm-email');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_profile_create-case');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_create-case');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_delete-case');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_profile_delete-profile');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_profile_get-city-list');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_image-upload');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_images-get');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_profile_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_profile_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('moderator', 'frontend_profile_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_profile_phone-e164');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_profile_phone-e164');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_preview-image-delete');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_profile_save-private');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_save-private');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('executor', 'frontend_profile_upload-case-preview-image');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_profile_validate-phone');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_project_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'frontend_registration_validate-form');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'frontend_user-list_index');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('admin', 'moderator');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('customer', 'OfferOrder');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'SeePersonalInfo');
INSERT INTO `ll_auth_item_child` (`parent`, `child`) VALUES ('common', 'SendPrivateMessage');



/* backup of table `{{%auth_assignment}}` */
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '1', 1484756678);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '19', 1488231412);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '20', 1488231910);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '22', 1490170880);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '26', 1490211563);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '27', 1490211615);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '28', 1490220589);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '29', 1490222686);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '34', 1490224443);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('customer', '35', 1490224458);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '18', 1488229151);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '2', 1491929678);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '21', 1490106741);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '23', 1490210743);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '24', 1490210782);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '25', 1490211337);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '30', 1490222770);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '31', 1490222868);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '32', 1490222973);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '33', 1490223005);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '36', 1491907001);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '37', 1491907164);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '38', 1491907325);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '39', 1491907413);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '40', 1491908439);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '41', 1491908754);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '42', 1491908801);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '43', 1491908859);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '44', 1491911012);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '45', 1491911054);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '46', 1491911210);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '47', 1491911455);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '48', 1491912384);
INSERT INTO `ll_auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('executor', '53', 1491914262);



/* backup of table `{{%auth_rule}}` */
INSERT INTO `ll_auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES ('AuthorRule', 'O:24:\"frontend\\rbac\\AuthorRule\":3:{s:4:\"name\";s:10:\"AuthorRule\";s:9:\"createdAt\";i:1497417167;s:9:\"updatedAt\";i:1497417167;}', 1497417167, 1497417167);
INSERT INTO `ll_auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES ('UserHaveProRule', 'O:29:\"frontend\\rbac\\UserHaveProRule\":3:{s:4:\"name\";s:15:\"UserHaveProRule\";s:9:\"createdAt\";i:1491920621;s:9:\"updatedAt\";i:1491920621;}', 1491920621, 1491920621);



