<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 02.07.17
 * @time: 16:27
 */
namespace console\controllers;

use conquer\helpers\Json;
use ZMQ;
use Yii;
use console\components\WsHandler;
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use React\ZMQ\Context;
use ZMQContext;
use yii\console\Controller;

/**
 * Class WsServerController
 *
 * Usage in console
 * ```php
 * $ cd /path/to/project
 * $ php yii ws-server/run $
 * ```
 * @package console\controllers
 */
class WsServerController extends Controller
{
    public function actionRun()
    {
        Yii::$app->db->createCommand('SET SESSION wait_timeout = 28800;')->execute();
        set_time_limit(0);
        ini_set("default_socket_timeout", 86400);
        $handler = new WsHandler();

        //@todo change 127.0.0.1 to appropriate value from config
        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    $handler
                )
            ), Yii::$app->params['ws']['port']
        );

        $context = new Context($server->loop);

        $pull = $context->getSocket(ZMQ::SOCKET_PULL);

        $dsn = Yii::$app->params['mq']['protocol']."://".Yii::$app->params['mq']['serverIp'].":".Yii::$app->params['mq']['port'];
        $pull->bind($dsn);
        $pull->on('message', function($data) use ($handler) {
            $handler->onZMQMessage($data);
        });

        $server->run();
    }
}
