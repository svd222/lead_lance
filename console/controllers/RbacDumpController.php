<?php
/**
 * Created by PhpStorm.
 *
 * @author svd
 * @date 28.10.2016
 * @time 20:42
 *
 * Выполняет операции backup/restore над Rbac данными сохраняя в/извлекая из json
 *
 * Usage: php yii rbac-dump/backup "@app/runtime/temp.json" to backup. Will generate @app/runtime/temp.json
 *        php yii rbac-dump/restore "@app/runtime/temp.json" to restore from backup. Will restore from @app/runtime/temp.json
 * Copy to server: scp /var/www/ll/console/runtime/temp.json user@ip:/path/to/console/runtime/temp.json
 *
 */
namespace console\controllers;

use Yii;
use yii\console\Controller;

/**
 * This command execute backup/restore of RBAC to/from json file
 *
 * @author svd <svd22286@gmail.com>
 * @since 2.0
 */
class RbacDumpController extends Controller
{
    /** @var $itemTable string Таблица Rbac сущностей */
    private $itemTable;

    /** @var $itemChildTable string Таблица Rbac отношений между сущностями */
    private $itemChildTable;

    /** @var $assignmentTable string Таблица Rbac назначений */
    private $assignmentTable;

    /** @var $ruleTable string Таблица Rbac правил */
    private $ruleTable;

    /** @var $preparedRawSql string Сырой sql (аварийный бекап) */
    private $preparedRawSql;

    /** Таблица Rbac сущностей */
    const ITEM_TABLE = 'itemTable';

    /** Таблица отношений между Rbacсущностями */
    const ITEM_CHILD_TABLE = 'itemChildTable';

    /** Таблица Rbac назначений */
    const ASSIGNMENT_TABLE = 'assignmentTable';

    /** Таблица Rbac правил */
    const RULE_TABLE = 'ruleTable';

    /** @inheritdoc */
    public function init() {
        $this->itemTable = Yii::$app->authManager->itemTable;
        $this->itemChildTable = Yii::$app->authManager->itemChildTable;
        $this->assignmentTable = Yii::$app->authManager->assignmentTable;
        $this->ruleTable = Yii::$app->authManager->ruleTable;
        $this->dataDir();
    }

    /**
     * Создает Db Rbac json дамп
     *
     * @param $filePath string путь к генерируемому json дампу
     */
    public function actionBackup($filePath) {
        $filePath = Yii::getAlias($filePath);
        if (Yii::$app->db->getDriverName() == 'mysql') {

            $authItems = $this->getData(self::ITEM_TABLE);
            $authItemsChilds = $this->getData(self::ITEM_CHILD_TABLE);
            $authItemsAssignment  = $this->getData(self::ASSIGNMENT_TABLE);
            $authRules = $this->getData(self::RULE_TABLE);

            $result = json_encode([
                'itemTable' => $authItems,
                'itemChildTable' => $authItemsChilds,
                'assignmentTable' => $authItemsAssignment,
                'ruleTable' => $authRules
            ]);

            $fp = fopen($filePath, 'wb');
            if($fp !== false) {
                fwrite($fp, $result, strlen($result));
                fclose($fp);
                echo 'Rbac backup was created successfull! Path: ' . $filePath ."\n";
                echo "For copy use this command:\n";
                echo "scp ".$filePath." user@ip:/path/to/console/runtime/temp.json\n";
            }
        } else {
            echo 'Unsupported driver type: `'.Yii::$app->db->getDriverName().'`'. "\n";
        }
    }

    /**
     * Возвращает массив данных из таблицы
     * смотри константы в [[app\commands\RbacDumpControler]]
     *
     * @param $table
     * @return array
     */
    private function getData($table)
    {
        switch($table) {
            case self::ITEM_TABLE : {
                $table = $this->itemTable;
                break;
            }
            case self::ITEM_CHILD_TABLE : {
                $table = $this->itemChildTable;
                break;
            }
            case self::ASSIGNMENT_TABLE : {
                $table = $this->assignmentTable;
                break;
            }
            case self::RULE_TABLE : {
                $table = $this->ruleTable;
                break;
            }
        }
        $command = Yii::$app->db->createCommand('SELECT * FROM '.$table);
        $command->prepare();
        return $command->queryAll();
    }

    /**
     * Восстанавливает Db Rbac из json дампа, созданного ранее @see [[\app\commands\RbacDumpController::actionBackup()]]
     *
     * @param $filePath string Путь к json дампу
     */
    public function actionRestore($filePath) {
        $filePath = Yii::getAlias($filePath);
        $fp = fopen($filePath, 'rb');
        $content = '';
        if ($fp !== false) {
            while(!feof($fp)) {
                $content .= fread($fp, 4096);
            }
            fclose($fp);
        } else {
            echo 'Unreadable file: ' . $filePath . "\n";
            return;
        }
        $obj = json_decode($content);

        $this->restore($obj);

        echo 'Restored from `'.$filePath."`\n";
    }

    private function prepareBackupSql(array $data, $table) {
        $sql = '';
        switch($table) {
            case self::ITEM_TABLE : {
                $table = $this->itemTable;
                break;
            }
            case self::ITEM_CHILD_TABLE : {
                $table = $this->itemChildTable;
                break;
            }
            case self::ASSIGNMENT_TABLE : {
                $table = $this->assignmentTable;
                break;
            }
            case self::RULE_TABLE : {
                $table = $this->ruleTable;
                break;
            }
        }
        $sql .= '/* backup of table `'.$table.'` ' . "*/\n";
        $connection = Yii::$app->db;
        foreach ($data as $k => $v) {
            $command = $connection->createCommand()->insert($table, $v);
            $command->prepare();
            $sql .= $command->rawSql. ";\n";
        }
        $this->preparedRawSql .= $sql . "\n\n\n";
    }

    /**
     * Check for exists data dir, and create it if not exeists.
     *
     * @return bool|string
     */
    private function dataDir() {
        $dirPath = Yii::getAlias('@app/data');
        if(!file_exists($dirPath)) {
            mkdir($dirPath, 0755);
        }
        return $dirPath;
    }

    private function backupSql() {
        $dirPath = $this->dataDir();
        $filePath = $dirPath . '/backup_rbac_'.gmdate('Y-m-d H:i').'.sql';
        $fp = fopen($filePath, 'wb');
        if($fp !== false) {
            fwrite($fp, $this->preparedRawSql, strlen($this->preparedRawSql));
            fclose($fp);
            echo "Old backup generated, file path: `".$filePath."`\n";
            return true;
        } else {
            echo 'can`t open file `'.$filePath.'`. Check permissions.';
        }
        return false;
    }

    /**
     * Восстанавливает Db RBAC из Json объекта
     *
     * @param $obj
     */
    private function restore($obj) {
        $connection = Yii::$app->db;

        echo "Genrating old backup.\n";
        $authItems = $this->getData(self::ITEM_TABLE);
        $this->prepareBackupSql($authItems, self::ITEM_TABLE);

        $authItemsChilds = $this->getData(self::ITEM_CHILD_TABLE);
        $this->prepareBackupSql($authItemsChilds, self::ITEM_CHILD_TABLE);

        $authAssignments = $this->getData(self::ASSIGNMENT_TABLE);
        $this->prepareBackupSql($authAssignments, self::ASSIGNMENT_TABLE);

        $authRules = $this->getData(self::RULE_TABLE);
        $this->prepareBackupSql($authRules, self::RULE_TABLE);

        $backupGenerated = $this->backupSql();
        if($backupGenerated) {
            $connection->createCommand()->truncateTable($this->itemChildTable)->execute();
            $connection->createCommand()->truncateTable($this->assignmentTable)->execute();

            try {
                $connection->createCommand()->dropForeignKey($connection->tablePrefix . 'auth_assignment_ibfk_1', $this->assignmentTable)->execute();
            } catch (\Exception $e) {
                echo "\n" . 'msg: ' . $e->getMessage() . ' line ' . __LINE__ ."\n";
            }
            try {
                $connection->createCommand()->dropForeignKey($connection->tablePrefix . 'auth_item_child_ibfk_1', $this->itemChildTable)->execute();
            } catch (\Exception $e) {
                echo "\n" . 'msg: ' . $e->getMessage() . ' line ' . __LINE__ ."\n";
            }
            try {
                $connection->createCommand()->dropForeignKey($connection->tablePrefix . 'auth_item_child_ibfk_2', $this->itemChildTable)->execute();
            } catch (\Exception $e) {
                echo "\n" . 'msg: ' . $e->getMessage() . ' line ' . __LINE__ ."\n";
            }
            $connection->createCommand()->truncateTable($this->itemTable)->execute();
            try {
                $connection->createCommand()->dropForeignKey($connection->tablePrefix . 'auth_item_ibfk_1', $this->itemTable)->execute();
            } catch (\Exception $e) {
                echo "\n" . 'msg: ' . $e->getMessage() . ' line ' . __LINE__ ."\n";
            }
            $connection->createCommand()->truncateTable($this->ruleTable)->execute();


            $order = ['itemChildTable', 'assignmentTable', 'itemTable', 'ruleTable'];
            $tableNames = [$this->itemChildTable, $this->assignmentTable, $this->itemTable, $this->ruleTable];
            for($i = 0; $i < count($order); $i++) {
                $dataKey = $order[$i];
                $data = $obj->$dataKey;
                foreach ($data as $d) {
                    $connection->createCommand()->insert($tableNames[$i], (array)$d)->execute();
                }
            }

            //Repare the foreign keys
            $connection->createCommand()->addForeignKey($connection->tablePrefix . 'auth_item_ibfk_1', $this->itemTable, 'rule_name', $this->ruleTable, 'name')->execute();
            $connection->createCommand()->addForeignKey($connection->tablePrefix . 'auth_item_child_ibfk_1', $this->itemChildTable, 'parent', $this->itemTable, 'name')->execute();
            $connection->createCommand()->addForeignKey($connection->tablePrefix . 'auth_item_child_ibfk_2', $this->itemChildTable, 'child', $this->itemTable, 'name')->execute();
            $connection->createCommand()->addForeignKey($connection->tablePrefix . 'auth_assignment_ibfk_1', $this->assignmentTable, 'item_name', $this->itemTable, 'name')->execute();
            //$this->createIndex('idx-auth_item-type', $authManager->itemTable, 'type');
        }
    }
}
