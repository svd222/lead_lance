<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 02.07.17
 * @time: 14:18
 */
namespace console\components;

use common\models\WsCommandList;
use common\models\WsSession;
use common\models\WsTransferObject;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use yii\base\InvalidCallException;
use yii\db\ActiveRecord;
use yii\helpers\Json;
use yii\helpers\VarDumper;
use yii\base\InvalidParamException;
use Yii;

class WsHandler implements MessageComponentInterface
{
    protected $clients;
    private $subscriptions;
    private $users;

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
        $this->subscriptions = [];
        $this->users = [];
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        $this->users[$conn->resourceId] = $conn;
    }

    public function onMessage(ConnectionInterface $conn, $msg)
    {
        $data = json_decode($msg);
        if (!empty($data) && !empty($data->command)) {
            switch ($data->command) {
                case "registerSession": {
                    $this->subscriptions[$conn->resourceId] = $data->userId;
                    $obj = new \stdClass();
                    $obj->resourceId = $conn->resourceId;
                    $obj->userId = $data->userId;
                    $obj->command = 'registerSession';
                    $wsSession = new WsSession([
                        'user_id' => $data->userId,
                        'resource_id' => $conn->resourceId
                    ]);
                    if (!$wsSession->save()) {
                        $obj = new \stdClass();
                        $obj->errorMessage = VarDumper::dumpAsString($wsSession->errors);
                    }
                    $conn->send(Json::encode($obj));
                    break;
                }
                /*case "message": {
                    if (isset($this->subscriptions[$conn->resourceId])) {
                        $target = $this->subscriptions[$conn->resourceId];
                        foreach ($this->subscriptions as $id => $channel) {
                            if ($channel == $target && $id != $conn->resourceId) {
                                $this->users[$id]->send($data->message);
                            }
                        }
                    }
                    break;
                }*/
            }
        }
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        unset($this->users[$conn->resourceId]);
        unset($this->subscriptions[$conn->resourceId]);
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

    /**
     * @param string $msg Represent json encoded [[common\models\WsTransferObject]]
     * @throws InvalidCallException
     */
    public function onZMQMessage($msg)
    {
        $obj = Json::decode($msg);
        $command = $obj['command'];
        WsCommandList::getList();
        if (!in_array($command, WsCommandList::getList())) {
            throw new InvalidCallException('Unknown command ' . $command);
        }

        $wsTransferObject = new WsTransferObject();
        $wsTransferObject->id = $obj['id'];
        $wsTransferObject->userId = $obj['userId'];
        $wsTransferObject->recipientId = $obj['recipientId'];
        $wsTransferObject->class = $obj['class'];
        $wsTransferObject->command = $obj['command'];
        $wsTransferObject->attributes = $obj['attributes'];

        $className = $wsTransferObject->class;
        $entityId = $wsTransferObject->id;
        $userId = $wsTransferObject->userId;

        $methods = get_class_methods($className);

        $hasFindOneMethod = in_array('findOne', $methods);
        $entityFound = $hasFindOneMethod && ($entity = $className::findOne($entityId));
        $entityIsDescendantOfAR = $entityFound && ($entity instanceof ActiveRecord);

        if ($entityIsDescendantOfAR) {
            /**
             * @var WsSession $wsSession
             */
            $wsSession = WsSession::find()
                ->last()
                ->byUserId($wsTransferObject->recipientId)//$entity->getWsRecipientId()
                ->registered()
                // @todo add active method to WsSessionQuery
                ->one();
            if ($wsSession) {
                $resourceId = $wsSession->resource_id;
                $conn = isset($this->users[$resourceId]) ? $this->users[$resourceId] : null;
                if ($conn) {
                    $obj = new \stdClass();
                    $obj->wsTransferObject = $wsTransferObject;
                    $conn->send(Json::encode($obj));
                }
            }
        } else {
            throw new InvalidParamException($className . ' ' . Yii::t('app/ws', 'must be descendant of') . ' \yii\db\ActiveRecord');
        }
    }
}