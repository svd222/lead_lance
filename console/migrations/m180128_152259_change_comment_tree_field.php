<?php

use yii\db\Migration;

/**
 * Class m180128_152259_change_comment_tree_field
 */
class m180128_152259_change_comment_tree_field extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%comment}}', 'tree', 'int(11) NULL');
    }

    public function down()
    {
        $this->alterColumn('{{%comment}}', 'tree', 'int(11) NOT NULL');
    }
}
