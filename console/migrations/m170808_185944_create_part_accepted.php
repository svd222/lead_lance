<?php

use yii\db\Migration;

class m170808_185944_create_part_accepted extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%part_accepted}}', [
            'id' => $this->primaryKey(),
            'lead_count_accepted' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_part_accepted_order', '{{%part_accepted}}', 'order_id', '{{%order}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_part_accepted_order', '{{%part_accepted}}');
        $this->dropTable('{{%part_accepted}}');
    }
}
