<?php

use yii\db\Migration;

/**
 * Class m180124_163129_news_add_tags
 */
class m180124_163129_news_add_tags extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%news}}', 'tags', $this->string()->notNull());

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%news}}', 'tags');

    }

}
