<?php

use yii\db\Migration;

class m170927_144806_alter_order_review_part_accepted_disput_tables_add_timestamp_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order_review}}', 'created_at', $this->dateTime());
        $this->addColumn('{{%order_review}}', 'updated_at', $this->dateTime());

        $this->addColumn('{{%part_accepted}}', 'created_at', $this->dateTime());

        $this->addColumn('{{%disput}}', 'created_at', $this->dateTime());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%disput}}', 'created_at');

        $this->dropColumn('{{%part_accepted}}', 'created_at');

        $this->dropColumn('{{%order_review}}', 'updated_at');
        $this->dropColumn('{{%order_review}}', 'created_at');
    }
}
