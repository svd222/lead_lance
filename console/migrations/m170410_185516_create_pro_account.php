<?php

use yii\db\Migration;

class m170410_185516_create_pro_account extends Migration
{
    public function up()
    {
        $this->createTable('{{%pro_certificate}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->string(),
            'discount' => $this->float(5,2),
            
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->createTable('{{%pro_account}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->dateTime()->notNull(),
            'suspended_at' => $this->dateTime(),
            'resumed_at' => $this->dateTime(),
            'life_time' => $this->integer()->notNull(),//in days
            'user_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1),//1 - active 2 - suspended
            'by_certificate_id' => $this->integer()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('fk_pro_account_user', '{{%pro_account}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_pro_account_pro_certificate', '{{%pro_account}}', 'by_certificate_id', '{{%pro_certificate}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%pro_account}}');
        $this->dropTable('{{%pro_certificate}}');
    }
}
