<?php

use yii\db\Migration;

class m170808_190147_create_disput extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%disput}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'reason' => $this->text(),
            'order_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_disput_order', '{{%disput}}', 'order_id', '{{%order}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_disput_order', '{{%disput}}');
        $this->dropTable('{{%disput}}');
    }
}
