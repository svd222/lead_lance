<?php

use yii\db\Migration;

class m170927_103448_alter_part_accepted_add_user_id_field extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%part_accepted}}', 'user_id', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%part_accepted}}', 'user_id');
    }
}
