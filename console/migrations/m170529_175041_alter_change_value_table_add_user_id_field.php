<?php

use yii\db\Migration;

class m170529_175041_alter_change_value_table_add_user_id_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%change_value}}', 'user_id', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('{{%change_value}}', 'user_id');
    }
}
