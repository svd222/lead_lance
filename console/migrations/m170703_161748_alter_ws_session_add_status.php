<?php

use yii\db\Migration;

class m170703_161748_alter_ws_session_add_status extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%ws_session}}', 'status', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%ws_session}}', 'status');
    }
}
