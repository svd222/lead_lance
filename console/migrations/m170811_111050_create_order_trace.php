<?php

use yii\db\Migration;

class m170811_111050_create_order_trace extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%order_trace}}', [
            'id' => $this->primaryKey(),
            'initiator_id' => $this->integer()->defaultValue(0), // 0 means cron for actions like `php yii .../change-work-is-done-to-order-closed`
            'initiator_role' => $this->string(),
            'from_state' => $this->integer(),
            'to_state' => $this->integer(),
            'description' => $this->string(),
            'order_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('K_order_trace_initiator_role', '{{%order_trace}}', 'initiator_role');
        $this->createIndex('K_order_trace_description', '{{%order_trace}}', 'description');
    }

    public function safeDown()
    {
        $this->dropIndex('K_order_trace_initiator_role', '{{%order_trace}}');
        $this->dropIndex('K_order_trace_description', '{{%order_trace}}');
        $this->dropTable('{{%order_trace}}');
    }
}
