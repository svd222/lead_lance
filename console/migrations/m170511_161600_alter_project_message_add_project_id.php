<?php

use yii\db\Migration;

class m170511_161600_alter_project_message_add_project_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%project_message}}', 'project_id', $this->integer());
        $this->addForeignKey('fk_project_message_project', '{{%project_message}}', 'project_id', '{{%project}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('fk_project_message_project', '{{%project_message}}');
        $this->dropColumn('{{%project_message}}', 'project_id');
    }
}
