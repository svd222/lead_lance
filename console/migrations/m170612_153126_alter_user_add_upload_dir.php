<?php

use yii\db\Migration;

class m170612_153126_alter_user_add_upload_dir extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'upload_dir', $this->string());
        $this->createIndex('K_user_upload_dir', '{{%user}}', 'upload_dir');
    }

    public function safeDown()
    {
        $this->dropIndex('K_user_upload_dir', '{{%user}}');
        $this->dropColumn('{{%user}}', 'upload_dir');
    }
}
