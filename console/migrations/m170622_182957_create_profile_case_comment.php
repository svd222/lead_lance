<?php

use yii\db\Migration;

class m170622_182957_create_profile_case_comment extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%profile_case_comment}}', [
            'id' => $this->primaryKey(),
            'message' => $this->text()->notNull(),
            'profile_case_id' => $this->integer()->notNull(),
            'from_user_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'parent_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            // fields below are represent nested sets
            // @see https://github.com/creocoder/yii2-nested-sets
            'tree' => $this->integer()->notNull(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('fk_profile_case_comment_user_from', '{{%profile_case_comment}}', 'from_user_id', '{{%user}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_profile_case_comment_user_from', '{{%profile_case_comment}}');
        $this->dropTable('{{%profile_case_comment}}');
    }
}
