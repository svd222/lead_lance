<?php

use yii\db\Migration;

class m170312_102820_create_image extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%image}}', [
            'id' => $this->primaryKey(),
            'batch_id' => $this->integer(),
            'image' => $this->string()->notNull(),
            'original_image' => $this->string(),
            'owner_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->createIndex('K_image_image', '{{%image}}', 'image');
        $this->createIndex('K_image_original_image', '{{%image}}', 'original_image');
    }

    public function down()
    {
        $this->dropIndex('K_image_original_image', '{{%image}}');
        $this->dropIndex('K_image_image', '{{%image}}');
        $this->dropTable('{{%image}}');
    }

    /*

     */
}
