<?php

use yii\db\Migration;

class m170814_070820_create_order_message extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%order_message}}', [
            'id' => $this->primaryKey(),
            'message' => $this->text()->notNull(),
            'from_user_id' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->dateTime(),
            'order_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('fk_order_message_user_from', '{{%order_message}}', 'from_user_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_order_message_user_to', '{{%order_message}}', 'to_user_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_order_message_order_id', '{{%order_message}}', 'order_id', '{{%order}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_message_order_id', '{{%order_message}}');
        $this->dropForeignKey('fk_order_message_user_to', '{{%order_message}}');
        $this->dropForeignKey('fk_order_message_user_from', '{{%order_message}}');
        $this->dropTable('{{%order_message}}');
    }
}
