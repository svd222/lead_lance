<?php

use yii\db\Migration;

class m170721_122618_alter_project_table_add_executor_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%project}}', 'executor_id', $this->integer(). ' AFTER user_id');
    }

    public function down()
    {
        $this->dropColumn('{{%project}}', 'executor_id');
    }
}
