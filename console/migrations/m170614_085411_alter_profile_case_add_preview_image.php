<?php

use yii\db\Migration;

class m170614_085411_alter_profile_case_add_preview_image extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%profile_case}}', 'preview_image', $this->string());
        $this->createIndex('K_profile_case_preview_image', '{{%profile_case}}', 'preview_image');
    }

    public function safeDown()
    {
        $this->dropIndex('K_profile_case_preview_image', '{{%profile_case}}');
        $this->dropColumn('{{%profile_case}}', 'preview_image');
    }
}
