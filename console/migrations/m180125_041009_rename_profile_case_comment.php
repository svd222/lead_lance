<?php

use yii\db\Migration;

/**
 * Class m180125_041009_rename_profile_case_comment
 */
class m180125_041009_rename_profile_case_comment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('{{%profile_case_comment}}', 'profile_case_id', 'item_id');
        $this->addColumn('{{%profile_case_comment}}', 'type', 'enum("case", "news") NOT NULL DEFAULT "news" AFTER item_id');
        $this->renameTable('{{%profile_case_comment}}', '{{%comment}}');
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->renameColumn('{{%comment}}', 'item_id', 'profile_case_id');
        $this->dropColumn('{{%comment}}', 'type');
        $this->renameTable('{{%comment}}', '{{%profile_case_comment}}');
    }

}
