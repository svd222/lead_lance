<?php

use yii\db\Migration;

class m170721_151549_create_project_user_refuse extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%project_user_refuse}}', [
            'id' => $this->primaryKey(),
            'initiator_id' => $this->integer(),
            'project_id' => $this->integer()->notNull(),
            'refused_user_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function safeDown()
    {
        $this->dropTable('{{%project_user_refuse}}');
    }
}
