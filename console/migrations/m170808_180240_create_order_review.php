<?php

use yii\db\Migration;

class m170808_180240_create_order_review extends Migration
{
    public function up()
    {
        $this->createTable('{{%order_review}}', [
            'id' => $this->primaryKey(),
            'emotion' => $this->integer(), // 0 - netral, 1 - positve, 2 - negative
            'review' => $this->text(),
            'user_id' => $this->integer(), // review author
            'order_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_order_review_order', '{{%order_review}}', 'order_id', '{{%order}}', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('FK_order_review_order', '{{%order_review}}');
        $this->dropTable('{{%order_review}}');
    }
}


