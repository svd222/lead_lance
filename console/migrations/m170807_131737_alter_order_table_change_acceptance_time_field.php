<?php

use yii\db\Migration;

class m170807_131737_alter_order_table_change_acceptance_time_field extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%order}}', 'acceptance_time', $this->integer());
    }

    public function down()
    {
        $this->alterColumn('{{%order}}', 'acceptance_time', $this->dateTime());
    }
}
