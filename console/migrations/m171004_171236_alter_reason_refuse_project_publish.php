<?php

use yii\db\Migration;

class m171004_171236_alter_reason_refuse_project_publish extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%reason_refuse_project_publish}}', 'type', $this->integer()->notNull());
        $this->renameTable('{{%reason_refuse_project_publish}}', '{{%reason_refuse_project}}');
    }

    public function safeDown()
    {
        $this->renameTable('{{%reason_refuse_project}}', '{{%reason_refuse_project_publish}}');
        $this->dropColumn('{{%reason_refuse_project_publish}}', 'type');
    }
}
