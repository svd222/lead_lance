<?php

use yii\db\Migration;

/**
 * Class m180118_180140_create_news
 */
class m180118_180140_create_news extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id'         => $this->primaryKey(),
            'title'        => $this->string()->notNull(),
            'text'         => $this->text(4096)->notNull(),
            'user_id'      => $this->integer()->notNull(),
            'status'       => $this->integer()->notNull()->defaultValue(1),
            'image'        => $this->string(),
            'created_at'   => $this->dateTime(),
            'updated_at'   => $this->dateTime(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        $this->addForeignKey('FK_news_user', '{{%news}}', 'user_id', '{{%user}}', 'id');
        $this->createIndex('K_title', '{{%news}}', 'title');

    }

    public function safeDown()
    {
        $this->dropTable('{{%news}}');
    }
}
