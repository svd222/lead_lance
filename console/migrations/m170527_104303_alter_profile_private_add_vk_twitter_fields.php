<?php

use yii\db\Migration;

class m170527_104303_alter_profile_private_add_vk_twitter_fields extends Migration
{
    public function up()
    {
        $this->addColumn('{{%profile_private}}', 'vk', $this->string() . ' AFTER `viber`');
        $this->createIndex('K_profile_private_vk', '{{%profile_private}}', 'vk');

        $this->addColumn('{{%profile_private}}', 'twitter', $this->string() . ' AFTER `vk`');
        $this->createIndex('K_profile_private_twitter', '{{%profile_private}}', 'twitter');
    }

    public function down()
    {
        $this->dropColumn('{{%profile_private}}', 'twitter');
        $this->dropColumn('{{%profile_private}}', 'vk');
    }
}
