<?php

use yii\db\Migration;

class m170528_112809_alter_tables_project_order_add_column_status extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%project}}', 'status', $this->integer()->notNull()->defaultValue(1));
        $this->addColumn('{{%order}}', 'status', $this->integer()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'status');
        $this->dropColumn('{{%project}}', 'status');
    }
}
