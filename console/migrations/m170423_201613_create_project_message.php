<?php

use yii\db\Migration;

class m170423_201613_create_project_message extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%project_message}}', [
            'id' => $this->primaryKey(),
            'message' => $this->text()->notNull(),
            'from_user_id' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->dateTime(),
            
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('fk_project_message_user_from', '{{%project_message}}', 'from_user_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_project_message_user_to', '{{%project_message}}', 'to_user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%project_message}}');
    }

}
