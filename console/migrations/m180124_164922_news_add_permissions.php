<?php

use yii\db\Migration;

/**
 * Class m180124_164922_news_add_permissions
 */
class m180124_164922_news_add_permissions extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->batchInsert(
            '{{%auth_item}}',
            [
                'name',
                'type',
                'description',
            ],
            [
                [
                    'frontend_news_index',
                    2,
                    'Allow view list news'
                ],
                [
                    'frontend_news_view',
                    2,
                    'Allow view news'
                ]
            ]
        );

        $this->batchInsert(
            '{{%auth_item_child}}',
            [
                'parent',
                'child',
            ],
            [
                [
                    'common',
                    'frontend_news_index',
                ],
                [
                    'customer',
                    'frontend_news_index',
                ],
                [
                    'executor',
                    'frontend_news_index',
                ],
                [
                    'moderator',
                    'frontend_news_index',
                ],
            ]
        );

        $this->batchInsert(
            '{{%auth_item_child}}',
            [
                'parent',
                'child',
            ],
            [
                [
                    'common',
                    'frontend_news_view',
                ],
                [
                    'customer',
                    'frontend_news_view',
                ],
                [
                    'executor',
                    'frontend_news_view',
                ],
                [
                    'moderator',
                    'frontend_news_view',
                ],
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->delete(
            '{{%auth_item_child}}',
            [
                'child' => 'frontend_news_index',
            ]
        );
        $this->delete(
            '{{%auth_item_child}}',
            [
                'child' => 'frontend_news_view',
            ]
        );

        $this->delete(
            '{{%auth_item}}',
            [
                'name' => 'frontend_news_index',
            ]
        );

        $this->delete(
            '{{%auth_item}}',
            [
                'name' => 'frontend_news_view',
            ]
        );

    }

}
