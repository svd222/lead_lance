<?php

use yii\db\Migration;

class m170711_185825_create_project_message_profile_case extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%project_message_profile_case}}', [
                'project_message_id' => $this->integer()->notNull(),
                'profile_case_id' => $this->integer()->notNull(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_unicode_ci'
        );
        $this->createIndex('UK_project_message_profile_case', '{{%project_message_profile_case}}', ['project_message_id', 'profile_case_id'], true);
    }

    public function safeDown()
    {
        $this->dropIndex('UK_project_message_profile_case', '{{%project_message_profile_case}}');
        $this->dropTable('{{%project_message_profile_case}}');
    }
}
