<?php

use yii\db\Migration;

class m170921_120216_drop_order_project_type extends Migration
{
    public function safeUp()
    {
        $this->dropTable('{{%order_project_type}}');
    }

    public function safeDown()
    {
        //связующая таблица заказ - тип проекта
        $this->createTable('{{%order_project_type}}', [
            'order_id' => $this->integer()->notNull(),
            'project_type_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }
}
