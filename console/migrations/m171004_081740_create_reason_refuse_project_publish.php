<?php

use yii\db\Migration;

class m171004_081740_create_reason_refuse_project_publish extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%reason_refuse_project_publish}}',
            [
                'id' => $this->primaryKey(),
                'project_id' => $this->integer()->notNull(),
                'reason' => $this->text(),
                'created_at' => $this->dateTime(),
            ],
            'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function safeDown()
    {
        $this->dropTable('{{%reason_refuse_project_publish}}');
    }
}
