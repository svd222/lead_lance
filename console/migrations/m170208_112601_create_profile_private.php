<?php

use yii\db\Migration;

class m170208_112601_create_profile_private extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%country}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(40)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->createIndex('K_country_name', '{{%country}}', 'name');

        $this->createTable('{{%region}}', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer()->notNull(),
            'name' => $this->string(40)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_region_country', '{{%region}}', 'country_id', '{{%country}}', 'id');
        $this->createIndex('K_region_name', '{{%country}}', 'name');

        $this->createTable('{{%city}}', [
            'id' => $this->primaryKey(),
            'region_id' => $this->integer()->notNull(),
            'name' => $this->string(40)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_city_regioin', '{{%city}}', 'region_id', '{{%region}}', 'id');
        $this->createIndex('K_city_name', '{{%city}}', 'name');

        $this->createTable('{{%profile_private}}', [
            /*'id' => $this->primaryKey(),*/
            'user_id' => $this->primaryKey(),
            'first_name' => $this->string(20),
            'last_name' => $this->string(20),
            'experience' => $this->integer(),
            'city_id' => $this->integer(),
            'birth_date' => $this->dateTime(),
            'phone' => $this->string(20),
            'skype' => $this->string(30),
            'telegram' => $this->string(30),
            'whatsapp' => $this->string(30),
            'viber' => $this->string(30),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime()
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_profile_private_user', '{{%profile_private}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('FK_profile_private_city', '{{%profile_private}}', 'city_id', '{{%city}}', 'id');

        $this->createIndex('K_profile_private_first_name', '{{%profile_private}}', 'first_name');
        $this->createIndex('K_profile_private_last_name', '{{%profile_private}}', 'last_name');
        $this->createIndex('K_profile_private_skype', '{{%profile_private}}', 'skype');
        $this->createIndex('K_profile_private_telegram', '{{%profile_private}}', 'telegram');
        $this->createIndex('K_profile_private_whatsapp', '{{%profile_private}}', 'whatsapp');
        $this->createIndex('K_profile_private_viber', '{{%profile_private}}', 'viber');
    }

    public function down()
    {
        $this->dropTable('{{%profile_private}}');
        $this->dropTable('{{%city}}');
        $this->dropTable('{{%region}}');
        $this->dropTable('{{%country}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
