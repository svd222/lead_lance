<?php

use yii\db\Migration;

class m170117_131116_create_order extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(1024)->notNull(),
            'lead_count_required' => $this->integer(),
            'execution_time' => $this->integer(),//только для обычных проектов (is_urgent = false)
            'is_urgent' => $this->boolean(),
            'lead_cost' => $this->decimal(11,3),//только если проект не аукцион (is_auction = false)
            'user_id' => $this->integer()->notNull(),//id заказчика
            'executor_id' => $this->integer()->notNull(),//id исполнителя
            'deadline_dt' => $this->dateTime(),//только для срочных проектов (is_urgent = true)
            'acceptance_time' => $this->dateTime(),//срок принятия выполненного задания
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'project_id' => $this->integer()->notNull(),//id проекта
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_project_customer', '{{%order}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('FK_project_executor', '{{%order}}', 'executor_id', '{{%user}}', 'id');
        $this->createIndex('K_title', '{{%order}}', 'title');

        //связующая таблица заказ - тип проекта
        $this->createTable('{{%order_project_type}}', [
            'order_id' => $this->integer()->notNull(),
            'project_type_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        //$this->createIndex('UK_order_project_type', '{{%order_project_type}}', ['order_id', 'project_type_id'], true);

        //связующая таблица заказ - вид траффика
        $this->createTable('{{%order_traffic_type}}', [
            'order_id' => $this->integer()->notNull(),
            'traffic_type_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        //$this->createIndex('UK_order_traffic_type', '{{%order_traffic_type}}', ['order_id', 'traffic_type_id'], true);
    }

    public function safeDown()
    {
        $this->dropTable('{{%order_traffic_type}}');
        $this->dropTable('{{%order_project_type}}');
        $this->dropTable('{{%order}}');
    }
}
