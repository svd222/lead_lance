<?php

use yii\db\Migration;

class m170405_141357_create_message extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%message}}', [
            'id' => $this->primaryKey(),
            'message' => $this->text()->notNull(),
            'from_user_id' => $this->integer()->notNull(),
            'to_user_id' => $this->integer()->notNull(),
            'is_system' => $this->integer(),
            'is_from_admin' => $this->integer(),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('fk_message_user_from', '{{%message}}', 'from_user_id', '{{%user}}', 'id');
        $this->addForeignKey('fk_message_user_to', '{{%message}}', 'to_user_id', '{{%user}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%message}}');
    }
}
