<?php

use yii\db\Migration;

class m170128_132736_create_profile_case extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%profile_case}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text()->notNull(),
            'presentation' => $this->text()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        //@todo fix bug
        // create index K_profile_case_tdp on {{%profile_case}} (title,description,presentation) ...Exception: SQLSTATE[42000]: Syntax error or access violation:
        // 1170 BLOB/TEXT column 'description' used in key specification without a key length
        /*$this->createIndex('K_profile_case_tdp', '{{%profile_case}}', [
            'title',
            'description',
            'presentation'
        ]);*/
    }

    public function down()
    {
        $this->dropTable('{{%profile_case}}');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
