<?php

use yii\db\Migration;

class m170706_143121_create_notification extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%notification}}', [
            'id' => $this->primaryKey(),
            'initiator_id' => $this->integer(),
            'recipient_id' => $this->integer()->notNull(),
            'type' => $this->integer(),
            'status' => $this->integer()->notNull(),
            'entity_type' => $this->smallInteger()->notNull()->defaultValue(0),
            'entity_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function safeDown()
    {
        $this->dropTable('{{%notification}}');
    }
}
