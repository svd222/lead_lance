<?php

use yii\db\Migration;

class m170510_163740_alter_project_message_add_lead_cost extends Migration
{
    public function up()
    {
        $this->addColumn('{{%project_message}}', 'lead_cost', $this->double(3));
    }

    public function down()
    {
        $this->dropColumn('{{%project_message}}', 'lead_cost');
    }
}
