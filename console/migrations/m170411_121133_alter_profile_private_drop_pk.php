<?php

use yii\db\Migration;

class m170411_121133_alter_profile_private_drop_pk extends Migration
{
    public function up()
    {
        $this->dropForeignKey('FK_profile_private_city', '{{%profile_private}}');
        $this->dropForeignKey('FK_profile_private_user', '{{%profile_private}}');
        $command = 'ALTER TABLE ' . \common\models\ProfilePrivate::tableName() . ' CHANGE COLUMN `user_id` `user_id` INT(11) NOT NULL, DROP PRIMARY KEY;';
        $this->db->createCommand($command)->execute();

        $this->addForeignKey('FK_profile_private_user', '{{%profile_private}}', 'user_id', '{{%user}}', 'id');
        $this->addForeignKey('FK_profile_private_city', '{{%profile_private}}', 'city_id', '{{%city}}', 'id');
        $this->addColumn('{{%profile_private}}', 'id', $this->primaryKey());
    }

    public function down()
    {
        /*echo "m170411_121133_alter_profile_private_drop_pk cannot be reverted.\n";

        return false;*/
    }
}
