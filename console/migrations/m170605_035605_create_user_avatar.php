<?php

use yii\db\Migration;

class m170605_035605_create_user_avatar extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%user_avatar}}', [
            'id' => $this->primaryKey(),
            'image' => $this->string()->notNull(),
            'original_image' => $this->string(),
            'user_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->createIndex('K_user_avatar_image', '{{%user_avatar}}', 'image');
        $this->createIndex('K_user_avatar_original_image', '{{%user_avatar}}', 'original_image');
    }

    public function down()
    {
        $this->dropIndex('K_user_avatar_original_image', '{{%user_avatar}}');
        $this->dropIndex('K_user_avatar_image', '{{%user_avatar}}');
        $this->dropTable('{{%user_avatar}}');
    }
}
