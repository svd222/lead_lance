<?php

use yii\db\Migration;

class m170614_112518_alter_profile_case_set_nullable_fields_add_created_at_field extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%profile_case}}', 'title', $this->string());
        $this->alterColumn('{{%profile_case}}', 'description', $this->text());
        $this->alterColumn('{{%profile_case}}', 'presentation', $this->text());
        $this->alterColumn('{{%profile_case}}', 'user_id', $this->integer());
        $this->addColumn('{{%profile_case}}', 'created_at', $this->dateTime());
        $this->addColumn('{{%profile_case}}', 'updated_at', $this->dateTime());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%profile_case}}', 'updated_at');
        $this->dropColumn('{{%profile_case}}', 'created_at');
        $this->alterColumn('{{%profile_case}}', 'title', $this->string()->notNull());
        $this->alterColumn('{{%profile_case}}', 'description', $this->text()->notNull());
        $this->alterColumn('{{%profile_case}}', 'presentation', $this->text()->notNull());
        $this->alterColumn('{{%profile_case}}', 'user_id', $this->integer()->notNull());
    }
}
