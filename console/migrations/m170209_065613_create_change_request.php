<?php

use yii\db\Migration;

class m170209_065613_create_change_request extends Migration
{
    public function up()
    {
        $this->createTable('{{%change_request}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string()->notNull(),
            'attribute' => $this->string(),
            'value' => $this->string(),
            'lifetime' => $this->integer()->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'status' => $this->boolean()->notNull()->defaultValue(false),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function down()
    {
        $this->dropTable('{{%change_request}}');
    }
}
