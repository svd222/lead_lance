<?php

use yii\db\Migration;

class m161213_060909_create_projects extends Migration
{
    public function safeUp()
    {
        //Типы проектов (ниша проекта)
        $this->createTable('{{%project_type}}',[
            'id' => $this->primaryKey(),
            'type' => $this->string(32)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        //$this->addPrimaryKey('PK_project_type', '{{%project_type}}', 'id');
        $this->createIndex('UK_project_type', '{{%project_type}}', 'type', true);

        //Виды трафика
        $this->createTable('{{%traffic_type}}',[
            'id' => $this->primaryKey(),
            'type' => $this->string(32)->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        //$this->addPrimaryKey('PK_traffic_type', '{{%traffic_type}}', 'id');
        $this->createIndex('UK_traffic_type', '{{%traffic_type}}', 'type', true);

        //проекты
        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull(),
            'description' => $this->text(1024)->notNull(),
            'lead_count_required' => $this->integer(),
            'execution_time' => $this->integer(),//только для обычных проектов (is_urgent = false)
            'is_urgent' => $this->boolean(),
            'is_auction' => $this->boolean(),//если = true стоимость лида lead_cost не устанавливается (имеет дефолтное значение = 0)
            'lead_cost' => $this->decimal(11,3),//только если проект не аукцион (is_auction = false)
            'user_id' => $this->integer()->notNull(),
            'deadline_dt' => $this->dateTime(),//только для срочных проектов (is_urgent = true)
            'is_multiproject' => $this->boolean(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        //$this->addPrimaryKey('PK_project', '{{%project}}', 'id');
        $this->addForeignKey('FK_project_user', '{{%project}}', 'user_id', '{{%user}}', 'id');
        $this->createIndex('K_title', '{{%project}}', 'title');
        //$this->createIndex('K_description', '{{%project}}', 'description');
        //$this->createIndex('K_title_description', '{{%project}}', ['title', 'description']);

        //связующая таблица проект - тип проекта
        $this->createTable('{{%project_project_type}}', [
            'project_id' => $this->integer()->notNull(),
            'project_type_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        $this->createIndex('UK_project_project_type', '{{%project_project_type}}', ['project_id', 'project_type_id'], true);

        //связующая таблица проект - вид траффика
        $this->createTable('{{%project_traffic_type}}', [
            'project_id' => $this->integer()->notNull(),
            'traffic_type_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
        $this->createIndex('UK_project_traffic_type', '{{%project_traffic_type}}', ['project_id', 'traffic_type_id'], true);
    }

    public function safeDown()
    {
        $this->dropTable('{{%project_traffic_type}}');
        $this->dropTable('{{%project_project_type}}');
        $this->dropTable('{{%project}}');
        $this->dropTable('{{%traffic_type}}');
        $this->dropTable('{{%project_type}}');
    }
}
