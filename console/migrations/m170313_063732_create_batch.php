<?php

use yii\db\Migration;

class m170313_063732_create_batch extends Migration
{
    public function up()
    {
        $this->createTable('{{%batch}}', [
            'id' => $this->primaryKey(),
            'entity_id' => $this->integer(),
            'entity_type' => $this->smallInteger()->notNull()->defaultValue(0),
            'owner_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function down()
    {
        $this->dropTable('{{%batch}}');
    }
}
