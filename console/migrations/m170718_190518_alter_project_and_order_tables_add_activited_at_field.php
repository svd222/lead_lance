<?php

use yii\db\Migration;

class m170718_190518_alter_project_and_order_tables_add_activited_at_field extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%project}}', 'activited_at', $this->dateTime(). ' AFTER updated_at');
        $this->addColumn('{{%order}}', 'activited_at', $this->dateTime().' AFTER updated_at');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'activited_at');
        $this->dropColumn('{{%project}}', 'activited_at');
    }
}
