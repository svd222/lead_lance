<?php

use yii\db\Migration;

class m170808_190202_create_disput_decision extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%disput_decision}}', [
            'id' => $this->primaryKey(),
            'can_leave_review' => $this->integer()->notNull()->defaultValue(1), //1 - both can leave review, 2 - only customer, 3 - only executor
            'lead_to_pay_count' => $this->integer()->notNull(),
            'disput_id' => $this->integer()->notNull(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->addForeignKey('FK_disput_decision_disput', '{{%disput_decision}}', 'disput_id', '{{%disput}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_disput_decision_disput', '{{%disput_decision}}');
        $this->dropTable('{{%disput_decision}}');
    }
}
