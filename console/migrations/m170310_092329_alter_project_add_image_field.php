<?php

use yii\db\Migration;

class m170310_092329_alter_project_add_image_field extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%project}}', 'image', $this->string());
        $this->createIndex('K_image', '{{%project}}', 'image');
    }

    public function down()
    {
        $this->dropIndex('K_image', '{{%project}}');
        $this->dropColumn('{{%project}}', 'image');
    }
}
