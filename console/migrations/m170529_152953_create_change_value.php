<?php

use yii\db\Migration;

class m170529_152953_create_change_value extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%change_value}}', [
            'id' => $this->primaryKey(),
            'guid' => $this->string(),
            'one_time' => $this->boolean(),
            'lifetime' => $this->integer(),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->timestamp(),
            'new_value' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');

        $this->createIndex('K_chanhe_value_guid', '{{%change_value}}', 'guid');
    }

    public function safeDown()
    {
        $this->dropIndex('K_chanhe_value_guid', '{{%change_value}}');
        $this->dropTable('{{%change_value}}');
    }
}
