<?php

use yii\db\Migration;

class m170703_134819_create_ws_session extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%ws_session}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'resource_id' => $this->integer(),
            'created_at' => $this->dateTime(),
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function safeDown()
    {
        $this->dropTable('{{%ws_session}}');
    }
}
