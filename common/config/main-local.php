<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=lead_lance',
            'username' => 'root',
            'password' => 'svd',
            'charset' => 'utf8',
            'tablePrefix' => 'll_',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,


            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'noreply@leadlance.ru',
                'password' => '8hwbcQpc19ck',
                'port' => '465',
                'encryption' => 'ssl',
            ],

            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.mail.ru', //smtp.mail.ru
                'username' => 'svd22286@mail.ru',
                'password' => 'svdrulez!',
                'port' => '465',
                'encryption' => 'ssl',
            ],*/
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend and frontend
            'class' => '\yii\web\Session',
            'name' => 'leadlance',
            'cookieParams' => [
                'lifetime' => 0,
                'path' => '/',
                'domain' => '.ll.local',
                'secure' => false,
                'httponly' => true,
            ]
        ]
    ],
];
