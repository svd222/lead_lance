<?php
return [
    'frontendDomain' => 'http://ll.local',
    'backendDomain' => 'http://backend.ll.local',

    'ws' => [
        //websocket
        'serverIp' => '127.0.0.1', //leadlancer.ru
        'port' => 8123,  // 443
        'location' => '', //  /websocket
    ],
    'mq' => [
        //message queue
        'protocol' => 'tcp',
        'serverIp' => '127.0.0.1',
        'port' => 4444
    ],
    'fms' => [
        'Order' => [
            /**
             * the key for array below represent the date in format YmdHi
             * Warning: DON`T CHANGE THE CONFIG BELOW on production!!!!. If need reconfigure, then create a new configuration with a new key equal to creation date
             */
            '201708041907' => [
                'stateSwitchTable' => [
                    ['command' => 'executor-refuse-execute-order',                     'state' => 'INIT', 'nextState' => 'ORDER_CLOSED'],
                    ['command' => 'executor-accept-order-to-execute',                   'state' => 'INIT', 'nextState' => 'IN_WORK'],
                    ['command' => 'customer-refuse-order',                              'state' => 'INIT', 'nextState' => 'ORDER_CLOSED'],//

                    ['command' => 'change-in-work-to-date-1-expired',                     'state' => 'IN_WORK', 'nextState' => 'DATE_1_EXPIRED'],
                    ['command' => 'work-is-done-by-executor',                           'state' => 'IN_WORK', 'nextState' => 'WORK_IS_DONE'],
                    ['command' => 'executor-refuse-execute-order',                     'state' => 'IN_WORK', 'nextState' => 'ORDER_CLOSED'],

                    ['command' => 'customer-accept-work-and-leave-review',               'state' => 'DATE_1_EXPIRED', 'nextState' => 'ORDER_CLOSED'],
                    ['command' => 'customer-open-disput',                             'state' => 'DATE_1_EXPIRED', 'nextState' => 'DISPUT_RESOLUTION'],
                    ['command' => 'work-is-done-by-executor',                           'state' => 'DATE_1_EXPIRED', 'nextState' => 'WORK_IS_DONE'],
                    ['command' => 'executor-refuse-execute-order',                     'state' => 'DATE_1_EXPIRED', 'nextState' => 'ORDER_CLOSED'],

                    ['command' => 'change-work-is-done-to-order-closed',                  'state' => 'WORK_IS_DONE', 'nextState' => 'ORDER_CLOSED'],
                    ['command' => 'customer-accept-work-and-leave-review',               'state' => 'WORK_IS_DONE', 'nextState' => 'ORDER_CLOSED'],
                    ['command' => 'customer-accept-part-work',                         'state' => 'WORK_IS_DONE', 'nextState' => 'PART_ACCEPTED'],
                    ['command' => 'customer-decline-accept-work',                      'state' => 'WORK_IS_DONE', 'nextState' => 'RESULT_OF_WORK_DECLINED'],

                    ['command' => 'decide-disput',                                   'state' => 'DISPUT_RESOLUTION', 'nextState' => 'ORDER_CLOSED_BY_DISPUT'],

                    ['command' => 'executor-agreed-and-leave-review',                   'state' => 'PART_ACCEPTED', 'nextState' => 'ORDER_CLOSED'],
                    ['command' => 'executor-open-dispute',                             'state' => 'PART_ACCEPTED', 'nextState' => 'DISPUT_RESOLUTION'],
                    ['command' => 'change-part-accepted-to-order-closed',                'state' => 'PART_ACCEPTED', 'nextState' => 'ORDER_CLOSED'],

                    ['command' => 'executor-agreed-and-leave-review',                   'state' => 'RESULT_OF_WORK_DECLINED', 'nextState' => 'ORDER_CLOSED'],
                    ['command' => 'executor-open-dispute',                             'state' => 'RESULT_OF_WORK_DECLINED', 'nextState' => 'DISPUT_RESOLUTION'],
                    ['command' => 'change-result-of-work-declined-to-order-closed',        'state' => 'RESULT_OF_WORK_DECLINED', 'nextState' => 'ORDER_CLOSED'],

                    ['command' => 'winner-leave-response',                            'state' => 'ORDER_CLOSED_BY_DISPUT', 'nextState' => 'ORDER_CLOSED_BY_DISPUT'],
                    ['command' => 'change-order-closed-by-disput-to-order-closed-finaly',   'state' => 'ORDER_CLOSED_BY_DISPUT', 'nextState' => 'ORDER_CLOSED_FINALY'],

                    ['command' => 'collocutors-leave-response',                       'state' => 'ORDER_CLOSED', 'nextState' => 'ORDER_CLOSED'],
                    ['command' => 'change-order-closed-to-order-closed-finaly',           'state' => 'ORDER_CLOSED', 'nextState' => 'ORDER_CLOSED_FINALY'],
                ],
                'statusStateTable' =>
                    [
                        'INIT' => 1,
                        'IN_WORK' => 2,
                        'DATE_1_EXPIRED' => 3,
                        'WORK_IS_DONE' => 4,
                        'DISPUT_RESOLUTION' => 5,
                        'PART_ACCEPTED' => 6,
                        'RESULT_OF_WORK_DECLINED' => 7,
                        'ORDER_CLOSED_BY_DISPUT' => 8,
                        'ORDER_CLOSED' => 9,
                        'ORDER_CLOSED_FINALY' => 10
                    ],
                'notifyTable' => [
                    'INIT' => [
                        'customer' => 'Заказ ожидает подтверждения исполнителем',
                        'executor' => 'Вас выбрали исполнителем. Подтвердите заказ.',
                        'admin' => 'Заказ создан'
                    ],
                    'IN_WORK' => [
                        'customer' => 'Заказ в работе.',
                        'executor' => 'Заказ в работе.',
                        'admin' => 'Исполнитель приступил к выполнению заказа'
                    ],
                    'DATE_1_EXPIRED' => [
                        'customer' => 'Заказ в работе.',
                        'executor' => 'Заказ в работе.',
                        'admin' => 'В работе. прошло {%date_1_expired%} дней',
                    ],
                    'WORK_IS_DONE' => [
                        'customer' => 'Исполнитель уведомил о выполнении заказа.',
                        'executor' => 'Заказ ожидает подтверждения рекламодателя.',
                        'admin' => 'Исполнитель уведомил о выполнении заказа'
                    ],
                    'DISPUT_RESOLUTION' => [
                        'customer' => 'По заказу открыт спор {%disput}',
                        'executor' => 'По заказу открыт спор {%disput}',
                        'admin' => 'По заказу открыт спор {%disput}'
                    ],
                    'PART_ACCEPTED' => [
                        'customer' => 'Вы приняли заказ частично ({%acceptedLeadCount} лидов)',
                        'executor' => 'Рекламодатель принял {%acceptedLeadCount} лидов',
                        'admin' => 'Рекламодатель принял {%acceptedLeadCount} лидов'
                    ],
                    'RESULT_OF_WORK_DECLINED' => [
                        'customer' => 'Вы отказались принимать заказ',
                        'executor' => 'Рекламодатель отказался принимать заказ',
                        'admin' => 'Рекламодатель отказался принимать заказ'
                    ],
                    'ORDER_CLOSED_BY_DISPUT' => [
                        'customer' => 'Заказ закрыт в результате спора {%disput}',
                        'executor' => 'Заказ закрыт в результате спора {%disput}',
                        'admin' => 'Заказ закрыт в результате спора {%disput}'
                    ],
                    'ORDER_CLOSED' => [
                        'customer' => 'Заказ закрыт',
                        'executor' => 'Заказ закрыт',
                        'admin' => 'Заказ закрыт'
                    ],
                    'ORDER_CLOSED_FINALY' => [
                        'customer' => 'Заказ закрыт.',
                        'executor' => 'Заказ закрыт.',
                        'admin' => 'Заказ закрыт окончательно'
                    ],
                ],
                'actionsConfig' => [
                    /*'customer' => [

                    ],*/
                    'executor' => [
                        'INIT' => [
                            'executor-accept-order-to-execute' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Принять к исполнению',
                            ],
                            'executor-refuse-execute-order' => [
                                'class' => 'order-executive-reject',
                                'title' => 'Отказаться от исполнения',
                            ],
                        ],
                        'IN_WORK' => [
                            'work-is-done-by-executor' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Работа завершена',
                            ],
                            'executor-refuse-execute-order' => [
                                'class' => 'order-executive-reject',
                                'title' => 'Отказаться от исполнения',
                            ],
                        ],
                        'DATE_1_EXPIRED' => [
                            'work-is-done-by-executor' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Работа завершена',
                            ],
                            'executor-refuse-execute-order' => [
                                'class' => 'order-executive-reject',
                                'title' => 'Отказаться от исполнения',
                            ]
                        ],
                        'PART_ACCEPTED' => [
                            'executor-agreed-and-leave-review' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Работа завершена',
                                'target' => 'executor-agreed-and-leave-review',
                                'footer' => [
                                    'dependencyClass' => 'common\models\OrderReview',
                                ],
                            ],
                            'executor-open-dispute' => [
                                'class' => 'order-executive-reject',
                                'title' => 'Открыть спор',
                                'target' => 'executor-open-dispute',
                                'footer' => [
                                    'dependencyClass' => 'common\models\Disput',
                                ],
                            ]
                        ],
                        'RESULT_OF_WORK_DECLINED' => [
                            'executor-agreed-and-leave-review' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Согласен',
                                'target' => 'executor-agreed-and-leave-review',
                                'footer' => [
                                    'dependencyClass' => 'common\models\OrderReview',
                                ],
                            ],
                            'executor-open-dispute' => [
                                'class' => 'order-executive-reject',
                                'title' => 'Открыть спор',
                                'target' => 'executor-open-dispute',
                                'footer' => [
                                    'dependencyClass' => 'common\models\Disput',
                                ],
                            ]
                        ],
                        'ORDER_CLOSED_BY_DISPUT' => [
                            'winner-leave-response' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Оставить отзыв',
                                'target' => 'winner-leave-response',
                                'footer' => [
                                    'dependencyClass' => 'common\models\OrderReview',
                                ],
                            ]
                        ],
                        'ORDER_CLOSED' => [
                            'collocutors-leave-response' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Оставить отзыв',
                                'target' => 'collocutors-leave-response',
                                'footer' => [
                                    'dependencyClass' => 'common\models\OrderReview',
                                ],
                            ]
                        ]
                    ],
                    'customer' => [
                        'INIT' => [
                            'customer-refuse-order' => [
                                'class' => 'order-executive-reject',
                                'title' => 'Отказаться от заказа',
                            ]
                        ],
                        'DATE_1_EXPIRED' => [
                            'customer-accept-work-and-leave-review' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Оставить отзыв',
                                'target' => 'customer-accept-work-and-leave-review',
                                'footer' => [
                                    'dependencyClass' => 'common\models\OrderReview',
                                ],
                            ],
                            'customer-open-disput' => [
                                'class' => 'order-executive-reject',
                                'title' => 'Открыть спор',
                                'target' => 'customer-open-disput',
                                'footer' => [
                                    'dependencyClass' => 'common\models\Disput',
                                ],
                            ],
                        ],
                        'WORK_IS_DONE' => [
                            'customer-accept-work-and-leave-review' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Оставить отзыв',
                                'target' => 'customer-accept-work-and-leave-review',
                                'footer' => [
                                    'dependencyClass' => 'common\models\OrderReview',
                                ],
                            ],
                            'customer-accept-part-work' => [
                                'class' => 'order-executive-part',
                                'title' => 'Принять частично',
                                'target' => 'customer-accept-part-work',
                                'footer' => [
                                    'dependencyClass' => 'common\models\PartAccepted',
                                ],
                            ],
                            'customer-decline-accept-work' => [
                                'class' => 'order-executive-reject',
                                'title' => 'Отказывюсь принимать работу',
                            ],
                        ],
                        'ORDER_CLOSED_BY_DISPUT' => [
                            'winner-leave-response' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Оставить отзыв',
                                'target' => 'winner-leave-response',
                                'footer' => [
                                    'dependencyClass' => 'common\models\OrderReview',
                                ],
                            ]
                        ],
                        'ORDER_CLOSED' => [
                            'collocutors-leave-response' => [
                                'class' => 'order-executive-agree',
                                'title' => 'Оставить отзыв',
                                'target' => 'collocutors-leave-response',
                                'footer' => [
                                    'dependencyClass' => 'common\models\OrderReview',
                                ],
                            ]
                        ]
                    ],

                    /*
                     * <button type="button" class="order-executive-agree" data-toggle="modal" data-target="#end-order">Подтвердить выполнение заказа</button>
                        <button type="button" class="order-executive-part" data-toggle="modal" data-target="#pay-lead">Выполнен частично</button>
                        <button type="button" class="order-executive-reject" data-toggle="modal" data-target="#open-dispute">Отклонить (открыть спор)</button>
                     */
                ],
            ]
        ]
    ]
];
