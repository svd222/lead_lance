<?php
return [
    'adminEmail' => 'noreply@leadlance.ru',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'noreplyEmail' => 'noreply@leadlance.ru',

    'ws' => [
        //websocket
        'serverIp' => '127.0.0.1',
        'port' => 8123
    ],
    'mq' => [
        //message queue
        'protocol' => 'tcp',
        'serverIp' => '127.0.0.1',
        'port' => 4444
    ],
    'maxPossibleSelectedCasesCount' => 3, //Задает число максимально возможных выбранных кейсов при ответе исполнителем на проект
    'pagination' => [
        'pageSize' => 10,
    ],
];
