<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::$container->set(\common\interfaces\fms\ConfigFMSLoaderInterface::class, \common\components\fms\FsFMSConfigLoader::class);

Yii::$container->set(\common\interfaces\fms\FMSInterface::class, \common\components\fms\FMS::class);
