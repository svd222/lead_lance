<?php
return [
    'name' => 'Leadlance',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend and frontend
            'name' => 'leadlance',
            'class' => '\yii\web\Session',
            'cookieParams' => [
                'lifetime' => 0,
                'path' => '/',
                //change the domain in main-local.php if needed
                'domain' => '.leadlancer.ru',
                'secure' => false,
                'httponly' => true,
            ]
        ],
    ],
];
