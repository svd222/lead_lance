<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 31.12.16
 * @time: 3:49
 */
namespace common\interfaces;

interface IList
{
    /**
     * Return list of enities index by integer
     *
     * @return array [
     *  int => mixed
     * ]
     */
    public static function getList();
}