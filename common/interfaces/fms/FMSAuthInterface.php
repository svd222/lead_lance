<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 05.08.17
 * Time: 17:35
 */
namespace common\interfaces\fms;

interface FMSAuthInterface
{
    public function checkAccess($command);
}