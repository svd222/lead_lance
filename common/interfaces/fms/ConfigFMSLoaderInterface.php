<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 04.08.17
 * Time: 18:05
 */
namespace common\interfaces\fms;

interface ConfigFMSLoaderInterface
{
    /**
     * load config from source, parse and returns it.
     * @return mixed
     */
    public function getStateSwitchTable();

    /**
     * @param string $key
     * return array
     */
    public function get($key);
}