<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 05.08.17
 * Time: 17:52
 */
namespace common\interfaces\fms;

interface FMSInterface extends FMSAuthInterface
{
    public function getCurrentState();

    public function checkAccess($command);

    public function switchState($state);
}