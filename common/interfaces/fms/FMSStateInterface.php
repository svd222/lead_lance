<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 04.08.17
 * @time: 17:50
 */
namespace common\interfaces\fms;

interface FMSStateInterface
{
    /**
     * @return string | null
     */
    public function getFMSCurrentState();

    /**
     * @param string $state
     * @return bool
     */
    public function switchFMSState($state);
}