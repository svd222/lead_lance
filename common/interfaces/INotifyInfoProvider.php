<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 06.07.17
 * @time: 19:19
 */
namespace common\interfaces;

interface INotifyInfoProvider
{
    /**
     * Provide info for creating [[\common\models\Notification]]
     *
     * Should return
     * return [
     *   'initiator_id' => Yii::$app->user->id, // If of user initiator
     *   'recipient_id' => $this->to_user_id, // Id of user that must be notified
     *   'entity_type' => Notification::ENTITY_TYPE_*, // notification entity type @see [[\common\models\Notification]]
     *   'entity_id' => $this->id, // id of entity
     *  ];
     *
     * @return []
     */
    public function provideNotifyInfo();
}