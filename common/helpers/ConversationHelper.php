<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 13.07.17
 * @time: 21:43
 */

namespace common\helpers;

use yii\base\InvalidParamException;

class ConversationHelper
{
    /**
     * Returns conversation Id
     * Example of usage:
     * ```php
     *  \common\helpers\ConversationHelper::getConversationId(1, 3)  // should return 1_3
     *  \common\helpers\ConversationHelper::getConversationId(3, 1)  // should return 1_3
     *  \common\helpers\ConversationHelper::getConversationId('3_1') // should return 1_3
     * ```
     * @param int $collocutor1Id
     * @param int $collocutor2Id
     * @return string
     */
    public static function getConversationId($collocutor1Id, $collocutor2Id = 0)
    {
        if ($collocutor1Id && $collocutor2Id) {
            $conversationId = $collocutor1Id < $collocutor2Id ? $collocutor1Id . '_' . $collocutor2Id : $collocutor2Id . '_' . $collocutor1Id;
        } else {
            if ($collocutor1Id && !$collocutor2Id) {
                $ids = explode('_', $collocutor1Id);
                $conversationId = self::getConversationId($ids[0], $ids[1]);
            } else {
                throw new InvalidParamException('Invalid parameters passed');
            }
        }
        return $conversationId;
    }

    /**
     * Pasrses conversationId into [
     *  0 => string id of first collocutor
     *  1 => string id of second collocutor
     * ]
     *
     * @param $conversationId
     * @return array
     */
    public static function parseConversationId($conversationId)
    {
        return explode('_', $conversationId);
    }
}