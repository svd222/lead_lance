<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 25.12.16
 * @time: 14:55
 */
namespace common\helpers;

use Yii;
use common\components\Probability;

/**
 * Class CommonHelper Предоставляет общие вспомогательные функции
 *
 * @package common\helpers
 */
class CommonHelper
{
    /**
     * Возвращает случайный элемент из заданного набора с заданной вероятностью
     *
     * @param $items [ Набор элементов
     *  int => mixed
     * ]
     * @param $probability [ Массив вероятностей
     *  int => float
     * ]
     * @return mixed
     */
    public static function getRandomItems($items, $probability = []) {
        if (empty($probability)) {
            $probability = array_fill(0, count($items), 1/count($items));
        }
        return $items[Probability::getCase($probability)];
    }

    /**
     * Возвращает список укороченных названий месяцев
     *
     * @return array
     * [
     *  int => string
     * ]
     */
    public static function getShortMonthList($index = false)
    {
        $list = [
            'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec',
        ];
        for ($i = 0; $i < count($list); $i++) {
            $list[$i] = Yii::t('app', $list[$i]);
        }
        return $index ? $list[$index] : $list;
    }

    /**
     * Cuts text up to $length characters
     * Appends '...' to cutted text if $appendEllipsis = true
     *
     * @param string $text
     * @param int $length
     * @param bool $appendEllipsis
     * @return string
     */
    public static function truncText($text, $length, $appendEllipsis = true) {
        $cText = mb_substr($text, 0, $length);
        if (mb_strlen($cText) < mb_strlen($text) && $appendEllipsis) {
            $cText .= '...';
        }
        return $cText;
    }

    public static function humanReadableCost($cost)
    {
        return number_format($cost, 2);
    }
}