<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 28.12.16
 * @time: 03:11
 */
namespace common\helpers;

class DateDiffHelper
{
    /**
     * Возвращает человеко понятную разницу во времени
     * примеры:
     * 2 часа 46 минут назад
     * 32 секунды назад
     * 1 год назад
     *
     * @param $dt int | string
     * @param null $now int | string
     * @return string
     */
    public static function getHumanReadableDiff($dt, $now = null) {
        if(is_null($now)) {
            $now = time();
        } else {
            //try to convert to unix timestamp
            $try = strtotime($now);
            $now = $try ? $try : $now;
        }
        //try to convert unix timestamp
        $try = strtotime($dt);
        $dt = $try ? $try : $dt;
        $diff = $dt - $now;
        if ($diff < 0) {
            $diff = 0 - $diff;
        }

        $period = ['year', 'month', 'day', 'hour', 'minute'];
        $minute = 60;
        $hour = 60 * $minute;
        $day = 24 * $hour;
        $month = 30 * $day;
        $year = 365 * $day;

        $pc = $c = null;
        foreach ($period as $k => $p) {
            $tp = $$p;
            $c = $diff/$tp;
            if ($c >= 1) {
                $pc = $period[$k];
                break;
            }
        }
        array_pop($period);
        array_pop($period);
        if (in_array($pc, $period)) {
            return intval($c) . ' ' . $pc . 's ago';
        } elseif ($pc == 'hour') {
            $remDiv = $diff%$hour;
            if ($remDiv == 0) {
                return $c . ' hours ago';
            } else {
                $hours = intval($diff/$hour);
                $remDiv = $diff%$hour;
                $minutes = intval($remDiv/$minute);
                return  $hours . ' hours ' . $minutes . ' minutes ago';
            }
        } elseif ($pc == $minute) {
            if ($c > 0) {
                return $c . ' minutes ago';
            }
        }
        return $diff . ' seconds ago';
    }

    public static function getHumanReadableDiffInt($dt, $now = null)
    {
        $diff = self::getHumanReadableDiff($dt, $now = null);
        $diff = (int)$diff;
        return $diff;
    }
}