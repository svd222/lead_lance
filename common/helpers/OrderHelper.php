<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 09.08.17
 * @time: 18:44
 */
namespace common\helpers;

use common\models\Disput;
use common\models\Order;
use common\models\PartAccepted;
use Guzzle\Http\Url;
use Yii;
use common\interfaces\fms\FMSInterface;
use common\models\User;
use yii\helpers\Html;

/**
 * FMS helper
 *
 * Class FmsHelper
 * @package common\helpers
 */
class OrderHelper
{
    /**
     * @param FMSInterface $fms
     * @return array
     */
    public static function getOrderActionsConfig(FMSInterface $fms)
    {
        $fmsConfig = Yii::$container->get('common\interfaces\fms\ConfigFMSLoaderInterface', [], [
            'entityName' => 'Order',
        ]);

        $rawConfig = $fmsConfig->get('actionsConfig');

        $role = User::isExecutor(Yii::$app->user->id) ? 'executor' : ( User::isCustomer(Yii::$app->user->id) ? 'customer' : '' );
        $actionsConfig = [];
        if (isset($rawConfig[$role])) {
            $actionsConfig = $rawConfig[$role];
            $currentState = $fms->getCurrentState();
            $actionsConfig = isset($actionsConfig[$currentState]) ? $actionsConfig[$currentState] : null;
        }
        return $actionsConfig;
    }

    /**
     * @param Order $order
     * @param string $state
     * @param string $userRole
     */
    public static function getNotify($order, $state, $userRole)
    {
        $fmsConfig = Yii::$container->get('common\interfaces\fms\ConfigFMSLoaderInterface', [], [
            'entityName' => 'Order',
        ]);
        $notify = '';
        $notifyTable = $fmsConfig->get('notifyTable');
        if (isset($notifyTable[$state])) {
            if (isset($notifyTable[$state][$userRole])) {
                $notify = $notifyTable[$state][$userRole];
            }
        }
        $pattern = '/{%([a-zA-Z][a-zA-Z0-9_]+)}/mis';
        preg_match_all($pattern, $notify, $matches);
        foreach ($matches[1] as $k=> $v) {
            $replacement = self::getReplacement($order, $v);
            $notify = preg_replace($pattern, $replacement, $notify);
        }
        return $notify;
    }

    public static function getReplacement($order, $key)
    {
        $ret = '';
        if ($key == 'disput') {
            /**
             * @var Disput $disput
             */
            $disput = $order->disput;
            if ($disput) {
                $ret = Html::a($disput->id, '/order/disput/' . $disput->id);
            }
        }
        if ($key == 'acceptedLeadCount') {
            /**
             * @var PartAccepted $partAccepted
             */
            $partAccepted = $order->partAccepted;
            $ret = $partAccepted->lead_count_accepted;
        }
        return $ret;
    }

    public static function getCurrentNotify($order)
    {
        $notify = '';
        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->id;
            $userRole = User::isCustomer($userId) ? 'customer' : (User::isExecutor($userId) ? 'executor' : 'admin');
        }
        $state = $order->getFMSCurrentState();
        if ($userRole) {
            $notify = self::getNotify($order, $state, $userRole);
        }
        return $notify;
    }

    public static function getFilterName()
    {
        return 'orderStatus';
    }

    /**
     * @see params['fms']['Order'][ver]['statusStateTable']
     * @return array
     */
    public static function getActiveStatusList()
    {
        return [1, 2, 3, 4, 6, 7];
    }

    /**
     * @see params['fms']['Order'][ver]['statusStateTable']
     * @return array
     */
    public static function getDisputStatusList()
    {
        return [5];
    }

    /**
     * @see params['fms']['Order'][ver]['statusStateTable']
     * @return array
     */
    public static function getClosedStatusList()
    {
        return [8,9,10];
    }

    protected function getStatusStateTable()
    {
        /**
         * @var ConfigFMSLoaderInterface $config
         */
        $fmsConfig = Yii::$container->get(ConfigFMSLoaderInterface::class, [], [
            'entityName' => 'Order',
        ]);
        return $fmsConfig->get('statusStateTable');
    }
}