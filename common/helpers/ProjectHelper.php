<?php

namespace common\helpers;

use common\models\GuideProjectStatus;
use common\models\Project;
use Yii;
use common\models\User;

/**
 * FMS helper
 *
 * Class FmsHelper
 * @package common\helpers
 */
class ProjectHelper
{
    /**
     * @param Project $project
     * @param string $state
     * @param string $userRole
     */
    public static function getNotification($state, $userRole)
    {
        $fmsConfig = Yii::$container->get('common\interfaces\fms\ConfigFMSLoaderInterface', [], [
            'entityName' => 'Project',
        ]);
        $notification = [
            'green' => '',
            'red'   => '',
        ];
        $goodNotificationStates = [
            GuideProjectStatus::STATUS_NEW,
            GuideProjectStatus::STATUS_PUBLISHED,
            GuideProjectStatus::STATUS_EXECUTOR_CHOSEN,
        ];
        $notifyTable = $fmsConfig->get('notifyTable');
        if (in_array($state, $goodNotificationStates)) {
            if (isset($notifyTable[$state]) && isset($notifyTable[$state][$userRole])) {
                $notification['green'] = $notifyTable[$state][$userRole];
            }
        } else {
            if (isset($notifyTable[$state]) && isset($notifyTable[$state][$userRole])) {
                $notification['red'] = $notifyTable[$state][$userRole];
            }
        }

        return $notification;
    }

    /**
     * @param $project
     * @return string
     */
    public static function getCurrentNotification($project)
    {
        $notification = '';
        if (!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->id;
            $userRole = User::isCustomer($userId)
                ? 'customer'
                : (User::isExecutor($userId)
                    ? 'executor'
                    : 'admin');
            if ($project->isUserExecutor($userId)) {
                $userRole = 'chosenAsExecutor';
            }
            $state = $project->getFMSCurrentState();

            if (in_array($state, [GuideProjectStatus::STATUS_NEW, GuideProjectStatus::STATUS_PUBLISHED])
                && $project->isExecutorChosen()
            ) {
                $state = GuideProjectStatus::STATUS_EXECUTOR_CHOSEN;
            }
            if (!empty($userRole)) {
                $notification = self::getNotification($state, $userRole);
            }
        }

        return $notification;
    }

}
