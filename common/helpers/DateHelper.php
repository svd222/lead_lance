<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 23.04.17
 * @time: 18:16
 */
namespace common\helpers;

/**
 * Class DateHelper Хелпер содержит вспомогательные функции для работы с датой/временем
 * @package common\helpers
 */
class DateHelper {
    /**
     * Возвращает русское название месяца
     *
     * @param int $month Месяц от 1 до 12
     * @param bool $short Возвращать ли в короткой форме
     * @return mixed
     */
    public static function translateRuMonth($month, $short = false, $multiple = false) {
        $months = [
            1 => ['янв', 'январь', 'января'],
            2 => ['фев', 'февраль', 'февраля'],
            3 => ['мар', 'март', 'марта'],
            4 => ['апр', 'апрель', 'апреля'],
            5 => ['май', 'май', 'мая'],
            6 => ['июнь', 'июнь', 'июня'],
            7 => ['июль', 'июль', 'июля'],
            8 => ['авг', 'август', 'августа'],
            9 => ['сен', 'сентябрь', 'сентября'],
            10 => ['окт', 'октябрь', 'октября'],
            11 => ['ноя', 'ноябрь', 'ноября'],
            12 => ['дек', 'декабрь', 'декабря'],
        ];
        return $short ? $months[$month][0] : $multiple ? $months[$month][2] : $months[$month][1];
    }
}