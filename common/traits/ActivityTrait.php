<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 18.07.17
 * @time: 21:56
 */

namespace common\traits;

trait ActivityTrait
{
    /**
     * Update activity of the entity
     */
    public function updateActivity()
    {
        $activityAttribute = 'activited_at';
        if (!empty($this->activtyAttribute)) {
            $activityAttribute = $this->activityAttribute;
        }
        $this->$activityAttribute = gmdate('Y-m-d H:i:s');
        return $this->update(true, [$activityAttribute]);
    }
}