<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 22.09.17
 * Time: 16:18
 */
namespace common\traits;

use Yii;
use yii\base\Model;

trait IsOwnerTrait
{
    /**
     * Checks whether user is owner of model (Order or project)
     *
     * @param Model $model
     * @return bool
     */
    public function checkIsOwner($model, $property)
    {
        return Yii::$app->user->can('IsOwnThisEntity', [
            'model' => $model,
            'property' => 'user_id',
        ]);
    }
}