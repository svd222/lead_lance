<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 05.06.17
 * @time: 9:52
 */
namespace common\traits;

use Yii;
use yii\base\InvalidConfigException;

trait DeleteFileTrait
{
    /**
     * Delete file
     *
     * @throws InvalidConfigException
     * @return bool
     */
    public function deleteFile()
    {
        $className = get_class($this);
        if (!defined($className.'::UPLOAD_DIRECTORY')) {
            throw new InvalidConfigException('UPLOAD_DIRECTORY must be defined in the called class');
        }
        if (!isset($this->fileAttribute)) {
            throw new InvalidConfigException('`$fileAttribute` property must be defined in the called class');
        }
        $fileAttribute = $this->fileAttribute;
        $file = Yii::getAlias(self::UPLOAD_DIRECTORY . DIRECTORY_SEPARATOR . $this->$fileAttribute);
        return unlink($file);
    }
}