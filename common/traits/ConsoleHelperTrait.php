<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 17.12.16
 * @time: 18:43
 */
namespace common\traits;

trait ConsoleHelperTrait
{
    private function debug($message, $key = 'debug var: ') {
        if (!is_string($message)) {
            if (is_array($message) || is_object($message)) {
                $message = var_export($message, true);
            }
        }
        fwrite(STDOUT, "\n". ($key ? $key." ":"") . $message . "\n");
    }
}