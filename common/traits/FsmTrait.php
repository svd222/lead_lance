<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 01.08.17
 * @time: 16:14
 */

/**
 * Class Fsm represent finite state machine
 */
trait FsmTrait
{
    protected $states;

    protected $currentState;

    public function loadStatesTable()
    {

    }

    /**
     * Load and returns current state
     *
     * @return mixed
     */
    public function getCurrentState()
    {
        if (!$this->currentState) {
            //load current state
        }
        return $this->currentState;
    }

    public function process($command)
    {
        $currentState = $this->getCurrentState();
        foreach ($this->states as $k => $state) {
            if ($k == $currentState->state) {
                $methods = $state->methods;
                if (in_array($command, $methods)) {

                }
            }
        }
    }

    public function can()
    {

    }
}