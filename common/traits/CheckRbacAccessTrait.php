<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 25.02.17
 * @time: 19:26
 */
namespace common\traits;

use Yii;
use yii\web\ForbiddenHttpException;

trait CheckRbacAccessTrait
{
    /**
     * @var [] $exceptActions массив действий для которых не нужно проверять RBAC доступ
     */
    protected $exceptActions = [];

    private function check($action)
    {
        $actionId = $action->id;
        $moduleId = $this->module->id;
        $GUID = $moduleId . '_' . $this->id . '_' . $actionId;
        if (!in_array($GUID, $this->exceptActions)) {
            if (!Yii::$app->user->can($GUID)) {
                throw new ForbiddenHttpException('Access denied');
            }
        }
        return true;
    }
}