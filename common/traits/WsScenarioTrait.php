<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 05.07.17
 * Time: 13:38
 */
namespace common\traits;

trait WsScenarioTrait
{
    /**
     * @var [] $wsScenarios define map of event => commands
     * [
     *  mixed EVENT => [
     *      int => string
     *  ]
     * ]
     * example:
     * ```php
     * $wsScenarios = [
     *  \yii\db\ActiveRecord::EVENT_AFTER_INSERT => [
     *      'message', 'notify'
     *  ]
     * ]
     * ```
     * This defination means: on firing of event EVENT_AFTER_INSERT of object that trait belongs, websocket commands 'message' & 'notify' would be called
     */
    //protected $wsScenarios;

    public function wsScenarios()
    {
        return $this->wsScenarios;
    }
}