<?php
namespace common\tests;

use common\helpers\DateDiffHelper;

/**
 * Class DateDiffHelperTest Тестирует поведение [[common\helpers\DateDiffHelper]] хелпер компонента
 * @package common\tests
 */
class DateDiffHelperTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Tests human readable diff in hours
     */
    public function testHourDiff()
    {
        $startDate = '2016-12-25 17:00:00';
        $now = '2016-12-25 19:00:00';
        $diff = DateDiffHelper::getHumanReadableDiff($startDate, $now);
        $this->assertEquals('2 hours ago', $diff);
    }

    /**
     * Tests human readable diff in year
     */
    public function testYearDiff() {
        $startDate = '2015-12-25 17:00:00';
        $now = '2016-12-25 19:00:00';
        $diff = DateDiffHelper::getHumanReadableDiff($startDate, $now);
        $this->assertEquals('1 years ago', $diff);
    }

    /**
     * Tests human readable diff in hours and minutes
     */
    public function testHoursAndMinutes() {
        $startDate = '2016-12-25 17:00:00';
        $now = '2016-12-25 19:32:00';
        $diff = DateDiffHelper::getHumanReadableDiff($startDate, $now);
        $this->assertEquals('2 hours 32 minutes ago', $diff);
    }

    /**
     * Tests human readable diff in seconds
     */
    public function testSeconds() {
        $startDate = '2016-12-25 17:00:00';
        $now = '2016-12-25 17:00:27';
        $diff = DateDiffHelper::getHumanReadableDiff($startDate, $now);
        $this->assertEquals('27 seconds ago', $diff);
    }
}