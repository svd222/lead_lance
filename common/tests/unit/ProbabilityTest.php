<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 25.12.16
 * @time: 12:04
 */
namespace common\tests;


use common\components\Probability;

class ProbabilityTest extends \Codeception\Test\Unit
{
    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * Тестирует классический случай с монеткой (наступление одного из двух событий: орел / решка)
     * Количество экспериментов 10000
     * Погрешность 3%
     */
    public function testProbability1Case()
    {
        $countCases = 10000;
        $error = 0.03;// 3%
        $border = $countCases/2;
        $leftBound = $border - $error*$countCases;
        $rigtBound = $border + $error*$countCases;
        $cases = [0, 0];

        for ($i = 0; $i < $countCases; $i++) {
            $case = Probability::getCase(0.5);
            if($case) {
                $cases[0]++;
                continue;
            }
            $cases[1]++;
        }
        $firstCaseInBound = $cases[0] >= $leftBound && $cases[0] <= $rigtBound;
        $secondCaseInBound = $cases[1] >= $leftBound && $cases[1] <= $rigtBound;
        $this->assertTrue($firstCaseInBound);
        $this->assertTrue($secondCaseInBound);
    }

    /**
     * Тестирует наступление события из заданного множества
     */
    public function testProbabilityMoreCases() {
        $countCases = 10000;
        $error = 0.03;// 3%
        $probability = [0.15, 0.25, 0.35, 0.25];
        $cases = [0, 0, 0, 0];

        for ($i = 0; $i < $countCases; $i++) {
            $case = Probability::getCase($probability);
            $cases[$case]++;
        }

        for ($i = 0; $i < count($probability); $i++) {
            $border = $probability[$i] * $countCases;
            $leftBound = $border - $error * $countCases;
            $rightBound = $border + $error * $countCases;
            $caseInBound = $cases[$i] >= $leftBound && $cases[$i] <= $rightBound;
            $this->assertTrue($caseInBound);
        }
    }
}