<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 13.12.16
 * @time: 11:06
 *
 */
namespace common\tests\unit;

use common\components\TimeStampUTC;
use common\tests\_data\timeStampUTC\models\TestModel1;
use common\tests\_data\timeStampUTC\models\TestModel2;
use common\traits\ConsoleHelperTrait;
use Yii;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class TimeStampUTCTest
 *
 * Before execution of this, you should migrate required data:
 * php yii_test migrate --migrationPath=@common/tests/_data/timeStampUTC/migrations
 *
 * @package common\tests
 */
class TimeStampUTCTest extends \Codeception\Test\Unit
{
    use ConsoleHelperTrait;

    /**
     * @var \common\tests\UnitTester
     */
    protected $tester;

    /**
     * @var TimeStampUTC
     */
    protected $tsUTC;

    /**
     * Задает начальную конфигурацию, создает TimeStampUTC компонент
     */
    protected function _before()
    {
        $this->tsUTC = Yii::createObject([
            'class' => TimeStampUTC::className(),
            'propertiesMap' => [
                TestModel1::className() => [
                    'createdAtAttribute' => [
                        'enableUTCBehavior' => true,
                    ],
                    'updatedAtAttribute' => [
                        'enableUTCBehavior' => false,
                    ]
                ],

                TestModel2::className() => [
                    'createdAtAttribute' => [
                        'enableUTCBehavior' => true,
                    ],
                    'updatedAtAttribute' => [
                        'attributeName' => 'updated',
                        'value' => function($event) {
                            return time() - 10800;
                        }
                    ]
                ],
            ]
        ]);
    }

    /**
     * Удаляет записи в БД после завершения теста
     */
    protected function _after()
    {
        TestModel1::deleteAll(true);
        TestModel2::deleteAll(true);
    }

    /**
     * Тестирует дефолтное поведение - установку значения соотв. поля в UTC для datetime значений в БД
     */
    public function testDefaultBehavior()
    {
        $nowUTC = strtotime(gmdate('Y-m-d H:i:s'));
        $tsm1 = new TestModel1;
        $tsm1->created_at = date('Y-m-d H:i:s', $nowUTC + mt_rand(100, 1000));
        $tsm1->save();
        $this->assertEquals($nowUTC, strtotime($tsm1->created_at));
    }

    /**
     * Тестирует дефолтное поведение - установку значения соотв. поля в UTC для datetime значений в БД
     */
    public function testDefaultBehavior2()
    {
        $nowUTCStr = gmdate('Y-m-d H:i:s');
        $tsm1 = new TestModel1;
        $tsm1->created_at = date('Y-m-d H:i:s',strtotime($nowUTCStr) + mt_rand(100, 1000)); //date('Y-m-d H:i:s', $nowUTC + mt_rand(100, 1000));
        $tsm1->save();
        $this->assertEquals($nowUTCStr, $tsm1->created_at);
    }

    /**
     * Тестирует дефолтное поведение - установку значения соотв. поля в UTC для integer значений в БД
     */
    public function testDefaultBehavior3()
    {
        $nowUTC = strtotime(gmdate('Y-m-d H:i:s'));
        $tsm1 = new TestModel2;
        $tsm1->created_at = $nowUTC + mt_rand(100, 1000);
        $tsm1->save();
        $this->assertEquals($nowUTC, $tsm1->created_at);
    }

    /**
     * Тестирует выброс исключения при задании неверной конфигурации
     */
    public function testNotExistAtrributeConfig() {
        $conf = $this->tsUTC->propertiesMap;
        $backConf = $conf;
        $conf[TestModel1::className()] = [
            'createdAtAttribute' => [
                'attributeName' => 'notExistAttr',
                'enableUTCBehavior' => true,
            ],
        ];
        $conf = ArrayHelper::merge([
            'class' => TimeStampUTC::className(),
        ], [
            'propertiesMap' => $conf
        ]);
        $newTsUTC = Yii::createObject($conf);

        $this->tester->expectException('yii\base\InvalidConfigException', function() {
            $tsm = new TestModel1;
            $tsm->created_at = date('Y-m-d H:i:s');
            $tsm->save();
        });

        //detach all events
        $newTsUTC->detachAll();
        unset($newTsUTC);
        //reestablish TimeStampUTC for further usage
        $config = [
            'class' => TimeStampUTC::className(),
            'propertiesMap' => $backConf
        ];
        Yii::createObject($config);
    }

    /**
     * Тестирует запрет применения компонента к определенному аттрибуту
     */
    public function testDisableUTCBehavior() {
        $tsm1 = new TestModel1;
        $tsm1->created_at = date('Y-m-d H:i:s');
        $tsm1->save();

        $value = gmdate('Y-m-d H:i:s', time() - mt_rand(1, 100));
        $tsm1->updated_at = $value;
        $tsm1->save();
        $this->assertEquals($value, $tsm1->updated_at);
    }

    /**
     * Тестирует функционал переопределения имени атррибута
     * Тестирует использование неименованных ф-ий в качестве значений
     */
    public function testCustomAttrNameAndClosure() {
        $tsm2 = new TestModel2;
        $initValue = time();
        $tsm2->created_at = $initValue;
        $tsm2->save();

        $tsm2->updated = $initValue;
        $tsm2->save();
        $this->assertEquals($initValue - 10800, $tsm2->updated);
    }

    /**
     * Тестирует функционал останова распространения события
     */
    public function testStopPropagation() {
        $conf = $this->tsUTC->propertiesMap;
        $backConf = $conf;
        $conf[TestModel1::className()] = [
            'updatedAtAttribute' => [
                'enableUTCBehavior' => true,
                'stopPropagation' => true,
            ]
        ];
        $conf = ArrayHelper::merge([
            'class' => TimeStampUTC::className(),
        ], [
            'propertiesMap' => $conf
        ]);
        $newTsUTC = Yii::createObject($conf);

        $nowUTCStr = gmdate('Y-m-d H:i:s');
        $tsm1 = new TestModel1;

        $customHandler = $this->getCustomeHandler($tsm1);
        Event::on(TestModel1::className(), ActiveRecord::EVENT_BEFORE_UPDATE, $customHandler);

        $tsm1->created_at = date('Y-m-d H:i:s',strtotime($nowUTCStr) + mt_rand(100, 1000));
        $tsm1->save();
        $this->assertEquals($nowUTCStr, $tsm1->created_at);

        $tsm1->updated_at = date('Y-m-d');
        $tsm1->save();
        /*
         * assert that a value
         * 1) changed by first attached hadler (in TimeStampUTC component)
         * 2) not changed by handler attached in this function (event is not propagained)
         */
        $this->assertEquals($nowUTCStr, $tsm1->updated_at);

        //detach custom handler
        Event::off(TestModel1::className(), ActiveRecord::EVENT_BEFORE_UPDATE, $customHandler);

        //detach all events
        $newTsUTC->detachAll();
        unset($newTsUTC);
        //reestablish TimeStampUTC for further usage
        $config = [
            'class' => TimeStampUTC::className(),
            'propertiesMap' => $backConf
        ];
        Yii::createObject($config);
    }

    /**
     * Возвращает некоторый обработчик
     * Предполагается что он никогда не будет вызван
     *
     * @param $model
     * @return \Closure
     */
    public function getCustomeHandler($model) {
        return function($event) use ($model) {
            //this handler should not never called
            $oldValue = strtotime($model->updated_at);
            $model->updated_at = date('Y-m-d H:i:s', $oldValue + 3600);
        };
    }
}