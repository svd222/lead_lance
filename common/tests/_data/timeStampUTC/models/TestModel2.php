<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 17.12.16
 * @time: 0:10
 */
namespace common\tests\_data\timeStampUTC\models;

use yii\db\ActiveRecord;

/**
 * TestModel2 - Model for [[common\components\TimeStampUTC]] component testing
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated
 *
 * @package common\tests\_data\timeStampUTC\models
 */
class TestModel2 extends ActiveRecord
{
    public static function tableName()
    {
        return 'test_model_2';
    }
}