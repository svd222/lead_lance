<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 15.12.16
 * @time: 20:44
 */
namespace common\tests\_data\timeStampUTC\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * TestModel1 - Model for [[common\components\TimeStampUTC]] component testing
 *
 * @property int $id
 * @property datetime $created_at
 * @property datetime $updated_at
 *
 * @package common\tests\_data\timeStampUTC\models
 */
class TestModel1 extends ActiveRecord
{
    public static function tableName()
    {
        return 'test_model_1';
    }
}