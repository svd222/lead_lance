<?php

use yii\db\Migration;

/**
 * Handles the creation for table `test_model_2`.
 */
class m161216_211608_create_test_model_2 extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('test_model_2', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(),
            'updated' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('test_model_2');
    }
}
