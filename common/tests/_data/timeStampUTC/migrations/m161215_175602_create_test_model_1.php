<?php

use yii\db\Migration;

/**
 * Handles the creation for table `test_model_1`.
 */
class m161215_175602_create_test_model_1 extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('test_model_1', [
            'id' => $this->primaryKey(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('test_model_1');
    }
}
