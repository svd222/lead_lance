<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 28.12.16
 * @time: 02:49
 */
return [
    [
        'title' => 'some title',
        'description' => 'some description',
        'lead_count_required' => 100,
        'execution_time' => 10,
        'is_urgent' => 0,
        'is_auction' => 0,
        'lead_cost' => 10.0,
        'user_id' => 1,
        'created_at' => '2016-12-25 17:00:00',
        'updated_at' => '2016-12-25 17:00:00',
        'is_multiproject' => 0,
        'deadline_dt' => '2016-12-25 17:00:00',
    ],
];