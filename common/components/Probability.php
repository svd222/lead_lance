<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 25.12.16
 * @time: 11:22
 */
namespace common\components;

use yii\base\InvalidConfigException;

class Probability
{
    /**
     * Возвращает событие по заданной/ым вероятности/ям.
     * $probability = 0.3;//30%
     * $case = Probability::getCase($probability);
     * $case is true of false
     *
     * $probability = [0.3, 0.2, 0.5];//set of probabilities
     * $case = Probability::getCase($probability);
     * $case is one of possible $probability keys [0,1,2]
     *
     * @param $probability float possible value from 0 to 1
     * @return bool|int
     * @throws InvalidConfigException
     */
    public static function getCase($probability)
    {
        $gp = mt_rand(0, 100);
        if (is_array($probability)) {
            $lB = 0;
            $res = false;
            for($i = 0; $i < count($probability); $i++) {
                $rB = $probability[$i]*100;
                $rB = (int)$rB;
                $rB += $lB;
                if($gp == $rB || $gp == $lB) {
                    $res = $i;
                }
                if($gp > $lB && $gp < $rB) {
                    $res = $i;
                }
                $lB = $rB;
            }
            //Крайний случай, малая вероятность. Не противоречит нормальному распределению
            if($res === false) {
                $res = mt_rand(0, count($probability) - 1);
            }
        } elseif (is_float($probability)) {
            $probability *= 100;
            $res = $gp <= $probability;
        } else {
            throw new InvalidConfigException('Value $probability must be float or array');
        }
        return $res;
    }
}