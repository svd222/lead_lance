<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 11.08.17
 * @time: 1:40
 */
namespace common\components;

use yii\base\Event;

class StateEvent extends Event
{
    /**
     * @var string $fromState Represent initial state
     */
    public $fromState;

    /**
     * @var string $toState Represent final state
     */
    public $toState;
}