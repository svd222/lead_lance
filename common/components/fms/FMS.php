<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 05.08.17
 * @time: 2:02
 */
namespace common\components\fms;

use common\helpers\FmsHelper;
use common\interfaces\fms\FMSInterface;
use common\models\User;
use Yii;
use yii\base\InvalidCallException;
use yii\web\ForbiddenHttpException;
use common\interfaces\fms\ConfigFMSLoaderInterface;

class FMS implements FMSInterface
{
    /**
     * @var string $currentState The current state ('INIT', 'COMPEITED', e.t.c)
     */
    protected $currentState;

    /**
     * @var array $stateSwitchTable The config that represent possible states and commands
     */
    protected $stateSwitchTable;

    /**
     * @var $model
     */
    protected $model;

    /**
     * @var ConfigFMSLoaderInterface $fmsConfig
     */
    protected $fmsConfig;

    public function __construct($entityName, $model)
    {
        /**
         * @var ConfigFMSLoaderInterface $fmsConfig
         */
        $this->fmsConfig = Yii::$container->get('common\interfaces\fms\ConfigFMSLoaderInterface', [], [
            'entityName' => $entityName,
        ]);
        $this->stateSwitchTable = $this->fmsConfig->getStateSwitchTable();
        $this->model = $model;
        if (!$this->model instanceof \common\interfaces\fms\FMSAuthInterface) {
            throw new InvalidCallException(get_class($this->model) . ' must implement \common\interfaces\fms\FMSAuthInterface');
        }
        if (!$this->model instanceof \common\interfaces\fms\FMSStateInterface) {
            throw new InvalidCallException(get_class($this->model) . ' must implement \common\interfaces\fms\FMSStateInterface');
        }
        $this->model = $model;
    }

    /**
     * Returns current state ('INIT', 'COMPLETED', e.t.c)
     *
     * @return string | null
     */
    public function getCurrentState()
    {
        if (!$this->currentState) {
            $this->currentState = $this->model->getFMSCurrentState();
        }
        return $this->currentState;
    }

    /**
     * Check the command for present in possible state & check entity access
     *
     * @param $command
     * @return bool
     */
    public function checkAccess($command)
    {
        $currentConfig = $this->getConfigForCurrentState();
        $commandAccess = false;
        if ($currentConfig) {
            foreach ($currentConfig as $c) {
                if ($c['command'] == $command) {
                    $commandAccess = true;
                }
            }
        }
        $commandAccess = $commandAccess && $this->model->checkAccess($command);
        return $commandAccess;
    }

    /**
     * @return [
     *  ['command' => string, 'nextState' => string]
     * ] | null
     */
    protected function getConfigForCurrentState()
    {
        $currentState = $this->getCurrentState();
        return isset($this->stateSwitchTable[$currentState]) ? $this->stateSwitchTable[$currentState] : null;
    }

    /**
     * Switches entity state
     *
     * @param string $command (action id like my-action)
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function switchState($command)
    {
        $currentConfig = $this->getConfigForCurrentState();
        $currentState = $this->getCurrentState();
        $nextState = '';
        if ($currentConfig) {
            foreach ($currentConfig as $k => $pair) {
                if ($pair['command'] == $command) {
                    $nextState = $pair['nextState'];
                }
            }
        }
        if (!$nextState) {
            throw new ForbiddenHttpException();
        }
        //if ($nextState != $currentState) {
            return $this->model->switchFMSState($nextState);
        //}
        return false;
    }
}