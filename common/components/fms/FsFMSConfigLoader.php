<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 04.08.17
 * @time: 18:29
 */
namespace common\components\fms;

use common\interfaces\fms\ConfigFMSLoaderInterface;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;

class FsFMSConfigLoader extends Component implements ConfigFMSLoaderInterface {

    /**
     * @var string $entityName The name of the entity of which represent current FMS state and for which config required
     */
    public $entityName;

    /**
     * @var int $version
     */
    public $version;

    /**
     * @var array $config
     */
    protected $config;

    protected function getConfig()
    {
        if (!$this->entityName) {
            throw new InvalidConfigException('Entity name must be set');
        }

        $rawConfig = Yii::$app->params['fms'][$this->entityName];

        $versions = [];
        foreach ($rawConfig as $key => $data) {
            $versions[] = $key;
        }
        if (!$this->version) {
            sort($versions);
            $data = $rawConfig[$versions[count($versions) - 1]];
        } else {
            if (isset($rawConfig[$this->version])) {
                $data = $rawConfig[$this->version];
            } else {
                throw new InvalidConfigException();
            }
        }
        $this->config = $data;
    }

    public function getStateSwitchTable()
    {
        $data = $this->get('stateSwitchTable');
        $config = [];
        foreach ($data as $row) {
            if (!isset($config[$row['state']])) {
                $config[$row['state']] = [];
            }
            $config[$row['state']][] = ['command' => $row['command'], 'nextState' => $row['nextState']];
        }
        return $config;
    }

    /**
     * @param string $key
     * @return array
     */
    public function get($key)
    {
        if (!$this->config) {
            $this->getConfig();
        }
        return isset($this->config[$key]) ? $this->config[$key] : [];
    }
}