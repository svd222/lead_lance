<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 21.12.16
 * @time: 16:37
 */
namespace common\components;

use common\models\Order;
use common\models\OrderMessage;
use common\models\Project;
use common\models\User;
use Yii;
use common\traits\CheckRbacAccessTrait;
use common\models\UserAvatar;
use common\models\Notification;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;

class Controller extends \yii\web\Controller
{
    use CheckRbacAccessTrait;

    /**
     * @todo Вынести в компонент Request (тоже самое и в классе frontend\controllers\RegistrationController)
     */
    const AJAX_PARAM = 'ajax';

    /**
     * Проверяет доступ на основе RBAC
     *
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest && $action->id != 'landing') {
            Yii::$app->response->redirect('/landing');
            Yii::$app->end();
        }
        if (parent::beforeAction($action)) {
            // @todo move code below to something like CurrentRouteHelper
            $actionId = $action->id;
            $moduleId = $this->module->id;
            $mcID = $moduleId . '_' . $this->id;

            if (!empty(Yii::$app->user->identity) && Yii::$app->user->identity->id) {
                /**
                 * @var User $user
                 */
                $user = Yii::$app->user->identity;
                if ($user->profilePrivate && $user->profilePrivate->userAvatar) {
                    $this->view->params['userAvatar'] = $user->profilePrivate->userAvatar;
                } else {
                    $this->view->params['userAvatar'] = new UserAvatar();
                }

                // notifications
                // set notifications status
                if ($mcID == 'frontend_message' && $actionId == 'create') {
                    $collocutorId = Yii::$app->request->get('id');
                    Notification::setStatusReceived(Yii::$app->user->id, Notification::ENTITY_TYPE_MESSAGE, $collocutorId);
                }

                if ($mcID == 'frontend_project' && in_array($actionId, ['view-offers', 'respond-project'])) {
                    $projectId = Yii::$app->request->get('id');
                    /**
                     * @var Project $project
                     */
                    if (($project = Project::findOne($projectId)) === null) {
                        throw new NotFoundHttpException('The requested page does not exist.');
                    }
                    if ($actionId == 'respond-project') {
                        $collocutorId = $project->user_id;
                        Notification::setStatusReceived(Yii::$app->user->id, Notification::ENTITY_TYPE_PROJECT_USER_REFUSE, $collocutorId);
                    } else {
                        if ($actionId == 'view-offers') {
                            $notifications = Notification::find()
                                ->byRecipient(Yii::$app->user->id)
                                ->byStatus(Notification::STATUS_NEW)
                                ->byEntityType(Notification::ENTITY_TYPE_PROJECT_USER_REFUSE)
                                ->all();
                            if (!empty($notifications)) {
                                $collocutorId = [];
                                foreach ($notifications as $notification) {
                                    /**
                                     * @var Notification $notification
                                     */
                                    $collocutorId[] = $notification->initiator_id;
                                }
                                Notification::setStatusReceived(Yii::$app->user->id, Notification::ENTITY_TYPE_PROJECT_USER_REFUSE, $collocutorId);
                            }
                        }
                    }
                }

                if ($mcID == 'frontend_order' && $actionId == 'view') {
                    /**
                     * @var Order $order
                     */
                    $order = Order::findOne(Yii::$app->request->get('id'));
                    if ($order) {
                        if ($order->user_id == Yii::$app->user->id) {
                            $collocutorId = $order->executor_id;
                        }
                        if ($order->executor_id == Yii::$app->user->id) {
                            $collocutorId = $order->user_id;
                        }
                        if (isset($collocutorId)) {
                            Notification::setStatusReceived(Yii::$app->user->id, Notification::ENTITY_TYPE_ORDER_STATE_CHANGED, $collocutorId);

                            // @todo cut this code, and realize 'new' notify on page by AJAX (see realization of project messages)
                            Notification::setStatusReceived(Yii::$app->user->id, Notification::ENTITY_TYPE_ORDER_MESSAGE, $collocutorId);
                        }
                    }
                }

                //@todo move code of calculate of projects and orders to separate place
                // get all notifications for further usage
                $notifications = Notification::find()
                    ->byRecipient(Yii::$app->user->id)
                    ->byStatus(Notification::STATUS_NEW)
                    ->addOrderBy(['created_at' => SORT_DESC])
                    ->all();
                $this->view->params['notifications'] = $notifications;

                // get projects and orders for left drop down menu
                $projectsCount = Yii::$app->params['project']['left_menu']['count'];
                $ordersCount = Yii::$app->params['order']['left_menu']['count'];

                $projects = [];
                $orders = [];
                $userId = Yii::$app->user->id;
                if (User::isExecutor($userId) || User::isCustomer($userId)) {
                    $projects = Project::find()
                        ->my()
                        ->active()
                        ->addOrderBy(['activited_at' => SORT_DESC])
                        ->addGroupBy('id')
                        ->limit($projectsCount)
                        ->with([
                            'projectMessages' => function ($query) {
                                /**
                                 * @var ActiveQuery $query
                                 */
                                $query->with('newNotifications');
                            }
                        ])
                        ->all();
                    $orders = Order::find()
                        ->my()
                        /*->active()*/
                        ->addOrderBy(['activited_at' => SORT_DESC])
                        ->limit($ordersCount)
                        ->with([
                            'orderMessages' => function ($query) {
                                /**
                                 * @var ActiveQuery $query
                                 */
                                $query->with('newNotifications');
                            },
                            'newNotifications',
                        ])
                        ->all();


                }
                $this->view->params['left_menu_projects'] = $projects;
                $this->view->params['left_menu_orders'] = $orders;

                $ordersNewActionNotifications = $user->getNewNotifications()->byEntityType(Notification::ENTITY_TYPE_ORDER_STATE_CHANGED)->count();
                $ordersNewMessageNotifications = $user->getNewNotifications()->byEntityType(Notification::ENTITY_TYPE_ORDER_MESSAGE)->count();
                $this->view->params['ordersNewActionNotifications'] = $ordersNewActionNotifications;
                $this->view->params['ordersNewMessageNotifications'] = $ordersNewMessageNotifications;

                $projectsNewMessageNotifications = $user->getNewNotifications()->byEntityType(Notification::ENTITY_TYPE_PROJECT_MESSAGE)->count();
                $this->view->params['projectsNewMessageNotifications'] = $projectsNewMessageNotifications;
            }

            return true; //$this->check($action);
        }
        return false;
    }
}
