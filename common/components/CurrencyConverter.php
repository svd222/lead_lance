<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 31.12.16
 * @time: 6:02
 */
namespace common\components;

use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;

class CurrencyConverter extends Component
{
    public $baseCurrency = 'RUB';

    public function init()
    {
        parent::init();
        if (is_null($this->baseCurrency)) {
            throw new InvalidConfigException('baseCurrency param must be set');
        }
    }

    protected function grabCurrencyRates()
    {
        return [
            'RUB' => [
                'USD' => 61.59
            ],
        ];
    }

    public function convert($value, $from)
    {
        $factor = 1;
        $currencyRates = $this->grabCurrencyRates();
        if ($from != $this->baseCurrency) {
            if (!isset($currencyRates[$this->baseCurrency][$from])) {
                throw new InvalidParamException('The rates for the currency is not exists');
            }
            $factor = $currencyRates[$this->baseCurrency][$from];
        }
        return $value * $factor;
    }
}