<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 28.12.16
 * @time: 02:47
 */
namespace common\fixtures;

use yii\test\ActiveFixture;

class Project extends ActiveFixture
{
    public $modelClass = 'common\models\Project';
}