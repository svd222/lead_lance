<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 06.07.17
 * @time: 11:57
 */
namespace common\widgets;

use Yii;
use yii\base\Widget;

class WsEventHandlerRegistrator extends Widget
{
    /**
     * @var [] $exceptActions массив действий для которых не нужно регистрировать ws AssetBundles
     */
    public $exceptActions = [];

    /**
     * @var [] $includeActions массив действий для которых нужно регистрировать ws AssetBundles
     */
    public $includeActions = [];

    /**
     * @var [] $assetBundles массив ассетов для регистрации
     */
    public $assetBundles = [];

    /**
     * Register assets contained in [[\common\widgets\WsEventHandlerRegistrator::$assetBundles]] if route of current page
     * does not match [[\common\widgets\WsEventHandlerRegistrator::$exceptActions]]
     * or route of current page match in [[\common\widgets\WsEventHandlerRegistrator::$includeActions]]
     */
    public function run()
    {
        $controller = Yii::$app->controller;
        $actionId = $controller->action->id;
        $moduleId = $controller->module->id;
        $controllerId = $controller->id;
        $GUID = $moduleId . '_' . $controllerId . '_' . $actionId;

        $emptyIncludeActions = empty($this->includeActions);

        if (!in_array($GUID, $this->exceptActions) && $emptyIncludeActions) {
            foreach ($this->assetBundles as $assetBundle) {
                /**
                 * @var \yii\web\AssetBundle $assetBundle
                 */
                $assetBundle::register(Yii::$app->view);
            }
        } else {
            if (in_array($GUID, $this->includeActions)) {
                foreach ($this->assetBundles as $assetBundle) {
                    /**
                     * @var \yii\web\AssetBundle $assetBundle
                     */
                    $assetBundle::register(Yii::$app->view);
                }
            }
        }
    }
}