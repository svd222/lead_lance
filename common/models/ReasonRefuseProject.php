<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%reason_refuse_project_publish}}".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $reason
 * @property string $created_at
 */
class ReasonRefuseProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%reason_refuse_project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'type'], 'required'],
            [['project_id', 'type'], 'integer'],
            [['reason'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/reason_refuse_project', 'ID'),
            'project_id' => Yii::t('app/reason_refuse_project', 'Project ID'),
            'reason' => Yii::t('app/reason_refuse_project', 'Reason'),
            'created_at' => Yii::t('app/reason_refuse_project', 'Created At'),
            'type' => Yii::t('app/reason_refuse_project', 'Type'),
        ];
    }
}
