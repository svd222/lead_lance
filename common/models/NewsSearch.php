<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/19/2018
 * Time: 1:27 PM
 */

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * NewsSearch represents the model behind the search form about `common\models\News`.
 */
class NewsSearch extends News
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['title', 'text', 'tags'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchBackendNews($params)
    {
        $query = News::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'         => $this->id,
            'user_id'    => $this->user_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'status'     => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'tags', $this->tags]);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public static function searchFrontendNews($params = [])
    {
        if (!empty($params['id'])) {
            $id = (int)$params['id'];
        }
        $query = self::find()
            ->where("title IS NOT NULL AND text IS NOT NULL")
            ->published()
            ->orderBy(['id' => SORT_DESC]);
// todo find a way to get comment count in sql for a batch of news, in one query, not a separate query for each news
//            ->joinwith('comment')->addSelect('COUNT({{%comment}}.id) AS commentCount')
//            ->groupBy('{{%news}}.id');

        if (!empty($id)) {
            $query = $query->andWhere(['=', 'user_id', $id]);
        } else {
            $query = $query->with('user');
        }

        if (!empty($params['searchQuery'])) {
            $query
                ->andFilterWhere(['like', 'title', $params['searchQuery']])
                ->orFilterWhere(['like', 'text', $params['searchQuery']]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

}
