<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%disput}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $reason
 * @property integer $order_id
 * @property string $created_at
 *
 * @property Order $order
 * @property DisputDecision[] $disputDecisions
 */
class Disput extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%disput}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'order_id'], 'required'],
            [['user_id', 'order_id'], 'integer'],
            [['reason'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/disput', 'ID'),
            'user_id' => Yii::t('app/disput', 'User ID'),
            'reason' => Yii::t('app/disput', 'Reason'),
            'order_id' => Yii::t('app/disput', 'Order ID'),
            'created_at' => Yii::t('app/disput', 'Created at'),
        ];
    }

    /**     *
     * @param bool $insert
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (!Yii::$app->user->isGuest && !$this->user_id) {
                    $this->user_id = Yii::$app->user->id;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisputDecisions()
    {
        return $this->hasMany(DisputDecision::className(), ['disput_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return DisputQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DisputQuery(get_called_class());
    }
}
