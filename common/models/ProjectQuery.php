<?php

namespace common\models;

use Yii;
use yii\base\InvalidCallException;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[Project]].
 *
 * @see Project
 */
class ProjectQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Project[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Project|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Возвращает [[\common\models\ProjectQuery]], для проектов отсортированных по убыванию
     *
     * @return $this
     */
    public function orderDesc()
    {
        return $this->addOrderBy(['id' => SORT_DESC]);
    }

    /**
     * Возвращает [[\common\models\ProjectQuery]], для проектов автором которых есть данный пользователь,
     * либо проекты исполнителя (те проекты по которым есть переписка)
     *
     * @throws InvalidCallException
     * @return $this
     */
    public function my()
    {
        $userId = Yii::$app->user->id;

        if (User::isExecutor($userId)) {
            $query = $this->joinWith([
                'projectMessages' => function ($query2) use ($userId) {
                    /**
                     * @var ActiveQuery $query2
                     */
                    $query2->andWhere('([[to_user_id]] = :to_user_id OR [[from_user_id]] = :from_user_id)', [
                        'to_user_id' => $userId,
                        'from_user_id' => $userId,
                    ]);
                }
            ], true, 'INNER JOIN');
        } else {
            if (User::isCustomer($userId)) {
                $query = $this->andWhere('user_id = :user_id', [
                    ':user_id' => $userId,
                ]);
            } else {
                throw new InvalidCallException();
            }
        }
        return $query;
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere('{{%project}}.[[status]] = :status_new OR {{%project}}.[[status]] =:status_published', [
            'status_new' => GuideProjectStatus::STATUS_NEW,
            'status_published' => GuideProjectStatus::STATUS_PUBLISHED,
        ]);
    }

    /**
     * @return $this
     */
    public function activeOrMine()
    {
        return $this->andWhere('{{%project}}.[[status]] = :status_new OR {{%project}}.[[status]] = :status_published OR {{%project}}.[[user_id]] = :user_id', [
            'status_new'       => GuideProjectStatus::STATUS_NEW,
            'status_published' => GuideProjectStatus::STATUS_PUBLISHED,
            'user_id'          => Yii::$app->user->id,
        ]);
    }

    /**
     * @param $userId
     * @return $this
     */
    public function notRefused($userId)
    {
        $projectIds = ProjectUserRefuse::find()
            ->byUser($userId)
            ->select(['project_id'])
            ->all();
        return $this->andWhere([
            'not in', '{{%project}}.id', ArrayHelper::map($projectIds, 'project_id', 'project_id')
        ]);
    }

    /**
     * @param $id
     * @return $this
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }
}
