<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%pro_certificate}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property double $discount
 *
 * @property ProAccount[] $proAccounts
 */
class ProCertificate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pro_certificate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['discount'], 'number'],
            [['title', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/certificate', 'ID'),
            'title' => Yii::t('app/certificate', 'Title'),
            'description' => Yii::t('app/certificate', 'Description'),
            'discount' => Yii::t('app/certificate', 'Discount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProAccounts()
    {
        return $this->hasMany(ProAccount::className(), ['by_certificate_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ProCertificateQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProCertificateQuery(get_called_class());
    }
}
