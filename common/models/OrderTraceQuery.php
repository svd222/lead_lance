<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[OrderTrace]].
 *
 * @see OrderTrace
 */
class OrderTraceQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return OrderTrace[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrderTrace|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
