<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%part_accepted}}".
 *
 * @property integer $id
 * @property integer $lead_count_accepted
 * @property integer $order_id
 * @property integer $user_id
 * @property string $created_at
 *
 * @property Order $order
 */
class PartAccepted extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%part_accepted}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lead_count_accepted', 'order_id', 'user_id'], 'required'],
            [['lead_count_accepted', 'order_id', 'user_id'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/part_accepted', 'ID'),
            'lead_count_accepted' => Yii::t('app/part_accepted', 'Lead Count Accepted'),
            'order_id' => Yii::t('app/part_accepted', 'Order ID'),
            'user_id' => Yii::t('app/part_accepted', 'User ID'),
            'created-at' => Yii::t('app/part_accepted', 'Created at'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return PartAcceptedQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PartAcceptedQuery(get_called_class());
    }
}
