<?php

namespace common\models;

use common\traits\WsScenarioTrait;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "{{%project_user_refuse}}".
 *
 * @property integer $id
 * @property integer $initiator_id
 * @property integer $project_id
 * @property integer $refused_user_id
 */
class ProjectUserRefuse extends \yii\db\ActiveRecord
{
    use WsScenarioTrait;

    protected $wsScenarios = [
        ActiveRecord::EVENT_AFTER_INSERT => [
            WsCommandList::COMMAND_PROJECT_USER_REFUSE,
        ],
    ];

    protected $wsRecipientId;

    /**
     * Caclulate and returns ws recipient id
     *
     * @return int
     */
    public function getWsRecipientId()
    {
        if (!$this->wsRecipientId) {
            if ($this->initiator_id == $this->refused_user_id) {
                /**
                 * @var Project $project
                 */
                $project = $this->project;
                $this->wsRecipientId = $project->user_id;
            } else {
                $this->wsRecipientId = $this->refused_user_id;
            }
        }

        return $this->wsRecipientId;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_user_refuse}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initiator_id', 'project_id', 'refused_user_id'], 'integer'],
            [['project_id', 'refused_user_id'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/project_user_refuse', 'ID'),
            'initiator_id' => Yii::t('app/project_user_refuse', 'Initiator ID'),
            'project_id' => Yii::t('app/project_user_refuse', 'Project ID'),
            'refused_user_id' => Yii::t('app/project_user_refuse', 'User ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return ProjectUserRefuseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectUserRefuseQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefusedUser()
    {
        return $this->hasMany(User::className(), ['id' => 'refused_user_id']);
    }

    /**
     * @inheritdoc
     */
    public function provideNotifyInfo()
    {
        $initiatorId = Yii::$app->user->id;
        $recipientId = $this->getWsRecipientId();
        return [
            'initiator_id' => $initiatorId,
            'recipient_id' => $recipientId,
            'entity_type' => Notification::ENTITY_TYPE_PROJECT_USER_REFUSE,
            'entity_id' => $this->id,
        ];
    }
}
