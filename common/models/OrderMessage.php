<?php

namespace common\models;

use common\traits\WsScenarioTrait;
use Yii;

/**
 * This is the model class for table "{{%order_message}}".
 *
 * @property integer $id
 * @property string $message
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property integer $status
 * @property string $created_at
 * @property integer $order_id
 *
 * @property Order $order
 * @property User $fromUser
 * @property User $toUser
 */
class OrderMessage extends \yii\db\ActiveRecord
{
    use WsScenarioTrait;

    protected $wsScenarios = [
        self::EVENT_AFTER_INSERT => [
            WsCommandList::COMMAND_ORDER_MESSAGE,
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_message}}';
    }

    /**
     * Переменная не в camelCase т к это "виртуальное поле" таблицы БД
     * @var int $batch_id
     */
    protected $batch_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id', 'order_id'], 'required'],
            [['message'], 'string'],
            [['message'], 'validateMessage'],
            [['from_user_id', 'to_user_id', 'status', 'order_id'], 'integer'],
            [['created_at'], 'safe'],
            [['batch_id'], 'integer'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user_id' => 'id']],
            [['to_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_user_id' => 'id']],
        ];
    }

    public function validateMessage($attribute, $params, $validator)
    {
        if (empty($this->message) && empty($this->batch_id)) {
            $this->addError($attribute, Yii::t('app/order_message', 'At least one of attrs: message or attached files must be set'));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/order_message', 'ID'),
            'message' => Yii::t('app/order_message', 'Message'),
            'from_user_id' => Yii::t('app/order_message', 'From User ID'),
            'to_user_id' => Yii::t('app/order_message', 'To User ID'),
            'status' => Yii::t('app/order_message', 'Status'),
            'created_at' => Yii::t('app/order_message', 'Created At'),
            'order_id' => Yii::t('app/order_message', 'Order ID'),
            'batch_id' => Yii::t('app/order_message', 'Batch Id'),
        ];
    }

    /**
     * @return \yii\db\Query
     */
    public function getNotifications()
    {

        return $this->hasMany(Notification::className(), ['entity_id' => 'id'])
            ->andWhere('entity_type = :entity_type', [
                'entity_type' => Notification::ENTITY_TYPE_ORDER_MESSAGE
            ]);
    }

    /**
     * Return \common\models\NotificationQuery (with status = \common\models\Notification::STATUS_NEW) for current user
     *
     * @return \common\models\NotificationQuery
     */
    public function getNewNotifications()
    {
        return $this->getNotifications()
            ->andWhere('[[status]] = :status', [
                'status' => Notification::STATUS_NEW
            ])
            ->andWhere(['recipient_id' => Yii::$app->user->id]);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * Get the created early batch id
     *
     * @return int
     */
    public function getBatch_id()
    {
        return $this->batch_id;
    }

    /**
     * Set batch id
     *
     * @param $value
     */
    public function setBatch_id($value)
    {
        $this->batch_id = $value;
    }

    /**
     * Associates the previously created package of documents with the project
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->batch_id) {
            $batch = Batch::find()
                ->where([
                    'id' => $this->batch_id,
                    'entity_id' => null,
                ])->one();
            if ($batch) {
                $batch->entity_id = $this->id;
                $batch->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Set the linked package of documents
     */
    public function afterFind()
    {
        $batch = Batch::find()->where('entity_id = :entity_id AND entity_type = :entity_type', [
            'entity_id' => $this->id,
            'entity_type' => Batch::ENTITY_TYPE_ORDER_MESSAGE,
        ])->one();
        if ($batch) {
            $this->setBatch_id($batch->id);
        }
        parent::afterFind();
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] связанный пакет документов
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getBatch()
    {
        return $this->hasOne(Batch::className(), ['entity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    /**
     * @inheritdoc
     * @return OrderMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderMessageQuery(get_called_class());
    }
}
