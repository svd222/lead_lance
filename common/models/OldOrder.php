<?php

namespace common\models;

use common\interfaces\fms\FMSAuthInterface;
use common\interfaces\fms\FMSStateInterface;
use common\traits\ActivityTrait;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $lead_count_required
 * @property integer $execution_time
 * @property integer $is_urgent
 * @property string $lead_cost
 * @property integer $user_id
 * @property integer $executor_id
 * @property string $deadline_dt
 * @property string $acceptance_time
 * @property string $created_at
 * @property string $updated_at
 * @property integer $project_id
 * @property integer $status
 * @property integer $batch_id
 *
 * @property User $user
 * @property User $executor
 */
class Order extends \yii\db\ActiveRecord implements FMSStateInterface, FMSAuthInterface
{
    use ActivityTrait;

    /**
     * @var int $deadlineDtDay
     */
    /*protected $deadlineDtDay;*/

    /**
     * @var int $deadlineDtMonth
     */
    /*protected $deadlineDtMonth;*/

    /**
     * @var int $deadlineDtYear
     */
    /*protected $deadlineDtYear;*/

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * Переменная не в camelCase т к это "виртуальное поле" таблицы БД
     * @var int $batch_id
     */
    protected $batch_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'user_id', 'executor_id', 'project_id', 'acceptance_time', 'lead_count_required'], 'required'],
            [['description'], 'string'],
            [['lead_count_required', 'execution_time', 'is_urgent', 'user_id', 'executor_id', 'project_id', 'status'], 'integer'],
            ['status', 'default', 'value' => function($model, $attribute) {
                return GuideOrderStatus::STATUS_INIT;
            }],
            [['lead_cost'], 'number'],
            [['acceptance_time', 'created_at', 'updated_at'], 'safe'],//'deadline_dt',
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['executor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['executor_id' => 'id']],
            [['batch_id'], 'integer'],
            /*[['deadlineDtDay'], 'integer', 'min' => 1, 'max' => 31],
            [['deadlineDtMonth'], 'integer', 'min' => 1, 'max' => 12],
            [['deadlineDtYear'], 'integer', 'min' => 2017],*/
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/order', 'ID'),
            'title' => Yii::t('app/order', 'Title'),
            'description' => Yii::t('app/order', 'Description'),
            'lead_count_required' => Yii::t('app/order', 'Lead Count Required'),
            'execution_time' => Yii::t('app/order', 'Execution Time'),
            /*'is_urgent' => Yii::t('app/order', 'Is Urgent'),*/
            'lead_cost' => Yii::t('app/order', 'Lead Cost'),
            'user_id' => Yii::t('app/order', 'User ID'),
            'executor_id' => Yii::t('app/order', 'Executor ID'),
            //'deadline_dt' => Yii::t('app/order', 'Deadline Dt'),
            'acceptance_time' => Yii::t('app/order', 'Acceptance Time'),
            'created_at' => Yii::t('app/order', 'Created At'),
            'updated_at' => Yii::t('app/order', 'Updated At'),
            'project_id' => Yii::t('app/order', 'Project ID'),
            /*'deadlineDtYear' => Yii::t('app/order', 'Deadline Year'),
            'deadlineDtMonth' => Yii::t('app/order', 'Deadline Month'),
            'deadlineDtDay' => Yii::t('app/order', 'Deadline Day'),*/
            'batch_id' => Yii::t('app/order', 'Batch Id'),
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            $this->user_id = Yii::$app->user->id;
            $this->created_at = $this->updated_at = gmdate('Y-m-d H:i:s');
            /*if (!$this->is_urgent) {
                if ($this->execution_time && $this->dateType) {
                    $this->execution_time *= GuideDateType::getMultiplier($this->dateType);
                }
            }*/
        }
        if (parent::beforeSave($insert)) {
            /*if($this->is_urgent) {
                $this->deadline_dt = $this->deadlineDtYear.'-'.$this->deadlineDtMonth.'-'.$this->deadlineDtDay.' 23:59:59';
            }*/
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the created early batch id
     *
     * @return int
     */
    public function getBatch_id()
    {
        return $this->batch_id;
    }

    /**
     * Set batch id
     *
     * @param $value
     */
    public function setBatch_id($value)
    {
        $this->batch_id = $value;
    }

    /**
     * Associates the previously created package of documents with the project
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->batch_id) {
            $batch = Batch::findOne($this->batch_id);
            $batch->entity_id = $this->id;
            $batch->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Set the linked package of documents
     */
    public function afterFind()
    {
        $batch = Batch::find()->where('entity_id = :entity_id AND entity_type = :entity_type', [
            'entity_id' => $this->id,
            'entity_type' => Batch::ENTITY_TYPE_ORDER,
        ])->one();
        if ($batch) {
            $this->setBatch_id($batch->id);
        }
        parent::afterFind();
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] связанный пакет документов
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getBatch()
    {
        return $this->hasOne(Batch::className(), ['entity_id' => 'id']);
    }

    /**
     * Get deadaline day
     *
     * @param int $value
     */
    /*public function getDeadlineDtDay()
    {
        return $this->deadlineDtDay;
    }*/

    /**
     * Set deadaline day
     *
     * @param $value
     */
    /*public function setDeadlineDtDay($value)
    {
        $this->deadlineDtDay = $value;
    }*/

    /**
     * Get deadaline month
     *
     * @return int
     */
    /*public function getDeadlineDtMonth()
    {
        return $this->deadlineDtMonth;
    }*/


    /**
     * Set deadaline month
     *
     * @param int $value
     */
    /*public function setDeadlineDtMonth($value)
    {
        $this->deadlineDtMonth = $value;
    }*/

    /**
     * Get deadaline year
     *
     * @return int
     */
    /*public function getDeadlineDtYear()
    {
        return $this->deadlineDtYear;
    }*/

    /**
     * Set deadaline year
     *
     * @param int $value
     */
    /*public function setDeadlineDtYear($value)
    {
        $this->deadlineDtYear = $value;
    }*/

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor()
    {
        return $this->hasOne(User::className(), ['id' => 'executor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }


    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] для связанных типов проектов
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getProjectTypes() {
        return $this->hasMany(ProjectType::className(), ['id' => 'project_type_id'])->viaTable('{{%order_project_type}}', ['order_id' => 'id']);
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] для связанных типов трафика
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getTrafficTypes() {
        return $this->hasMany(TrafficType::className(), ['id' => 'traffic_type_id'])->viaTable('{{%order_traffic_type}}', ['order_id' => 'id']);
    }

    /**
     * @return ActiveDataProvider
     */
    public static function search()
    {
        $query = self::find()
            ->my()
            ->orderDesc()
            ->with([
                'projectTypes',
                'trafficTypes'
            ]);

        $paramFilterName = GuideOrderStatus::getFilterName();
        $status = (int)Yii::$app->request->getQueryParam($paramFilterName);
        if ($status) {
            $query->andWhere('status = :status', [
                'status' => $status,
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        return $dataProvider;
    }

    /**
     * Check that this command can be executed in this state
     *
     * @param $command
     * @return bool
     */
    public function checkAccess($command)
    {
        return true;
    }

    /**
     * Returns string representing current FMS state
     *
     * @return string|null
     */
    public function getFMSCurrentState()
    {
        $statesTable = array_flip(GuideOrderStatus::$statesTable);
        return isset($statesTable[$this->status]) ? $statesTable[$this->status] : null;
    }

    /**
     * Switches FMS state
     *
     * @param string $state
     * @return bool
     */
    public function switchFMSState($state)
    {

        $status = isset(GuideOrderStatus::$statesTable[$state]) ? GuideOrderStatus::$statesTable[$state] : null;
        if ($status) {
            $this->status = $status;
            if ($this->save()) {
                return true;
            }
        }
        return false;
    }
}
