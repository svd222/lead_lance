<?php

namespace common\models;

use common\interfaces\IList;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "{{%traffic_type}}".
 *
 * @property integer $id
 * @property string $type
 */
class TrafficType extends \yii\db\ActiveRecord implements IList
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%traffic_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 32],
            [['type'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @inheritdoc
     * @return TrafficTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TrafficTypeQuery(get_called_class());
    }

    /**
     * Возвращает список всех типов трафика
     */
    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'type');
    }
}
