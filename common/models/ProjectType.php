<?php

namespace common\models;

use common\interfaces\IList;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%project_type}}".
 *
 * @property integer $id
 * @property string $type
 */
class ProjectType extends \yii\db\ActiveRecord implements IList
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['type'], 'string', 'max' => 32],
            [['type'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
        ];
    }

    /**
     * @inheritdoc
     * @return ProjectTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectTypeQuery(get_called_class());
    }

    /**
     * Возвращает список всех типов проектов
     */
    public static function getList()
    {
        return ArrayHelper::map(static::find()->all(), 'id', 'type');
    }
}
