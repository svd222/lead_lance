<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Project;

/**
 * ProjectSearch represents the model behind the search form about `common\models\Project`.
 */
class ProjectSearch extends Project
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lead_count_required', 'execution_time', 'is_urgent', 'is_auction', 'user_id', 'executor_id', 'is_multiproject', 'status'], 'integer'],
            [['title', 'description', 'deadline_dt', 'created_at', 'updated_at', 'activited_at', 'image'], 'safe'],
            [['lead_cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Project::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_count_required' => $this->lead_count_required,
            'execution_time' => $this->execution_time,
            'is_urgent' => $this->is_urgent,
            'is_auction' => $this->is_auction,
            'lead_cost' => $this->lead_cost,
            'user_id' => $this->user_id,
            'executor_id' => $this->executor_id,
            'deadline_dt' => $this->deadline_dt,
            'is_multiproject' => $this->is_multiproject,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'activited_at' => $this->activited_at,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image]);

        return $dataProvider;
    }
}
