<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * This is the model class for table "{{%profile_case}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $presentation
 * @property integer $user_id
 * @property string $preview_image
 * @property string $created_at
 * @property string $updated_at
 *
 * @property integer $modelId
 */
class ProfileCase extends \yii\db\ActiveRecord
{
    /**
     * scenario for create/update case
     */
    const SCENARIO_CREATE_CASE = 'create_case';

    /**
     * scenario for upload preview image (before create case)
     */
    const SCENARIO_UPLOAD_PREVIEW_IMAGE = 'upload_preview_image';

    /**
     * @var int $modelId Used for creating model on upload preview_image
     */
    public $modelId;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_case}}';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = $this->updated_at = gmdate('Y-m-d H:i:s');
            } else {
                $this->updated_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'presentation', 'user_id'], 'required', 'on' => self::SCENARIO_CREATE_CASE],
            [['description', 'presentation', 'title'], 'string', 'skipOnEmpty' => false, 'on' => self::SCENARIO_CREATE_CASE],
            [['user_id'], 'integer', 'skipOnEmpty' => false, 'on' => self::SCENARIO_CREATE_CASE],
            [['title', 'preview_image'], 'string', 'max' => 255, 'on' => self::SCENARIO_CREATE_CASE],
            //[['preview_image'], 'safe', 'on' => self::SCENARIO_CREATE_CASE],

            [['preview_image'], 'required', 'on' => self::SCENARIO_UPLOAD_PREVIEW_IMAGE],
            [['preview_image'], 'string', 'max' => 255, 'on' => self::SCENARIO_UPLOAD_PREVIEW_IMAGE],
            [['title', 'description', 'presentation', 'user_id'], 'safe', 'on' => self::SCENARIO_UPLOAD_PREVIEW_IMAGE],
            ['modelId', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/profile_case', 'ID'),
            'title' => Yii::t('app/profile_case', 'Title'),
            'description' => Yii::t('app/profile_case', 'Description'),
            'presentation' => Yii::t('app/profile_case', 'Presentation'),
            'preview_image' => Yii::t('app/profile_case', 'Preview image'),
            'user_id' => Yii::t('app/profile_case', 'User ID'),
            'created_at' => Yii::t('app/profile_case', 'Created At'),
            'updated_at' => Yii::t('app/profile_case', 'Updated At'),
        ];
    }

    public static function search($params = [])
    {
        if (!empty($params['id'])) {
            $id = (int)$params['id'];
        }
        $query = self::find()
            ->where("title IS NOT NULL AND presentation IS NOT NULL")
            ->orderBy(['id' => SORT_DESC]);
        if (!empty($id)) {
            $query = $query->andWhere(['=', 'user_id', $id]);
        } else {
            $query = $query->with('user');
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        return $dataProvider;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function deletePreviewImage()
    {
        $path = $this->user->ensureUploadDirExists();
        $fullPath = $path . DIRECTORY_SEPARATOR . $this->preview_image;
        unlink($fullPath);
        $this->preview_image = null;
        $this->update(false);
        return true;
    }

    /**
     * @inheritdoc
     * @return ProfileCaseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProfileCaseQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasMany(Comment::className(), ['item_id' => 'id'])
            ->andWhere([
                'type' => Comment::COMMENT_TYPE_CASE,
            ]);
    }
}
