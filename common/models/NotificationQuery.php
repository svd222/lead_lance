<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Notification]].
 *
 * @see Notification
 */
class NotificationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Notification[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Notification|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Returns notifications by recepient
     *
     * @param $userId
     * @return $this
     */
    public function byRecipient($userId)
    {
        return $this->andWhere('recipient_id = :recipient_id', [
            'recipient_id' => $userId,
        ]);
    }

    /**
     * Returns notifications by status
     *
     * @param int $status
     * @return $this
     */
    public function byStatus($status)
    {
        return $this->andWhere('status = :status', [
            'status' => $status,
        ]);
    }

    public function received()
    {

    }

    /**
     * Returns notifications by entity type
     *
     * @param $entityType
     * @return $this
     */
    public function byEntityType($entityType)
    {
        return $this->andWhere('entity_type = :entity_type', [
            'entity_type' => $entityType
        ]);
    }
}
