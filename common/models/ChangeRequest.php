<?php

namespace common\models;

use Yii;
use yii\base\InvalidParamException;
use yii\db\Query;

/**
 * This is the model class for table "{{%change_request}}".
 *
 * @property integer $id
 * @property string $code maybe string or numeric string
 * @property string $attribute attribute name
 * @property string $value
 * @property integer $lifetime in minutes
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 *
 * @property array $format write only
 */
class ChangeRequest extends \yii\db\ActiveRecord
{
    /**
     * @var array $format
     * [
     *  type => string
     *  length => int
     * ]
     */
    private $format;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%change_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'lifetime'], 'required'],
            [['lifetime', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['code', 'attribute', 'value'], 'string', 'max' => 255],
            [['format'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/change_request', 'ID'),
            'code' => Yii::t('app/change_request', 'Code'),
            'attribute' => Yii::t('app/change_request', 'Attribute'),
            'value' => Yii::t('app/change_request', 'Value'),
            'lifetime' => Yii::t('app/change_request', 'Lifetime'),//lifetime in minutes
            'created_at' => Yii::t('app/change_request', 'Created At'),
            'updated_at' => Yii::t('app/change_request', 'Updated At'),
            'status' => Yii::t('app/change_request', 'Status'),
        ];
    }

    /**
     * On creating model set the status, created_at, update_at and code attributes
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->status = GuideChangeRequestStatus::STATUS_NEW;
            $this->created_at = $this->updated_at = gmdate('Y-m-d H:i:s');

            if (!$this->format) {
                return false;
            }
            $type = $this->format['type'];
            $length = $this->format['length'];
            $code = '';
            if ($type == 'string') {
                $ret = Yii::$app->security->generateRandomString($length);
            }
            if ($type == 'numeric') {
                for ($i = 0; $i < $length; $i++) {
                    $ret .= mt_rand(0,9);
                }
            }
            if (!$code) {
                return false;
            }
            $this->code = $code;
        }
        if (parent::beforeSave($insert)) {
            return true;
        }
        return false;
    }

    /**
     * @param array $format
     * [
     *  type => string
     *  length => int
     * ]
     *
     * @throws InvalidParamException
     * @return
     */
    public function setFormat($format)
    {
        $type = $format['type'];
        $length = isset($format['length']) ? $format['length'] : 8;
        $this->format = ['type' => $type, 'length' => $length];
    }

    /**
     * Set model to used state
     */
    public function used()
    {
        $this->status = GuideChangeRequestStatus::STATUS_USED;
        $this->save();
    }

    /**
     * Returns [[common\models\ChangeRequest]] by code
     *
     * @param string $code
     * @return null | \yii\db\ActiveRecord
     */
    public static function findByCode($code)
    {
        return self::find()->where('code = :code')->params([
            ':code' => $code,
        ])->one();
    }

    /**
     * Returns [[common\models\ChangeRequest]] by code if model is valid (status = 0)
     *
     * @param string $code
     * @return null | \yii\db\ActiveRecord
     */
    public static function findValidByCode($code)
    {
        return self::find()->where('code = :code AND [[status]] = :status')->params([
            ':code' => $code,
            ':status' => GuideChangeRequestStatus::STATUS_NEW,
        ])->one();
    }

    /**
     * Set outdated (not used before) rows
     *
     * @return void
     */
    public static function setOutdated()
    {
        Yii::$app->db->createCommand()->update('{{%change_request}}',
            ['status' => GuideChangeRequestStatus::STATUS_OUTDATED],
            '(UNIX_TIMESTAMP(created_at) + lifetime*60) > (UTC_TIMESTAMP() + 0)'
        )->execute();
    }
}
