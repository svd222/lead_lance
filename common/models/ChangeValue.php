<?php

namespace common\models;

use phpDocumentor\Reflection\Types\Callable_;
use Yii;

/**
 * This is the model class for table "{{%change_value}}".
 *
 * @property integer $id
 * @property string $guid
 * @property integer $one_time
 * @property integer $lifetime
 * @property integer $status
 * @property string $created_at
 * @property string $new_value
 * @property integer $user_id
 */
class ChangeValue extends \yii\db\ActiveRecord
{
    /**
     * new value
     */
    const STATUS_NEW = 1;

    /**
     * readed
     */
    const STATUS_READED = 2;

    /**
     * deleted
     */
    const STATUS_DELETED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%change_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['one_time', 'lifetime', 'status', 'user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['guid', 'new_value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = gmdate('Y-m-d H:i:s');
                $this->status = self::STATUS_NEW;
                if (!$this->guid) {
                    $this->guid = uniqid($this->id.time(), true);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        if ($this->lifetime == 0 && $this->one_time) {
            $this->status = self::STATUS_DELETED;
        } else {
            $this->status = self::STATUS_READED;
            if ( ($this->lifetime + strtotime($this->created_at)) < time() ) {
                $this->status = self::STATUS_DELETED;
            }
        }
        $this->update();
        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/change_value', 'ID'),
            'guid' => Yii::t('app/change_value', 'Guid'),
            'one_time' => Yii::t('app/change_value', 'One Time'),
            'lifetime' => Yii::t('app/change_value', 'Lifetime'),
            'status' => Yii::t('app/change_value', 'Status'),
            'created_at' => Yii::t('app/change_value', 'Created At'),
            'new_value' => Yii::t('app/change_value', 'New Value'),
            'user_id' => Yii::t('app/change_value', 'User'),
        ];
    }

    /**
     * @inheritdoc
     * @return ChangeValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ChangeValueQuery(get_called_class());
    }

    /**
     * @param $value
     * @param int $lifetime
     * @param callable $generator
     * @return ChangeValue
     */
    public static function request($value, $lifetime = 0, callable $generator = null)
    {
        $model = new self();
        $model->new_value = $value;
        if ($lifetime == 0) {
            $model->one_time = 1;
        } else {
            $model->one_time = 0;
        }
        $model->lifetime = $lifetime;
        if ($generator) {
            $model->guid = $generator();
        }
        $model->user_id = Yii::$app->user->id;
        $model->save();
        return $model;
    }
}
