<?php

namespace common\models;

use common\components\StateEvent;
use common\helpers\OrderHelper;
use common\interfaces\fms\FMSAuthInterface;
use common\interfaces\fms\FMSStateInterface;
use common\interfaces\INotifyInfoProvider;
use common\traits\ActivityTrait;
use common\traits\WsScenarioTrait;
use Yii;
use yii\base\Event;
use yii\data\ActiveDataProvider;
use common\interfaces\fms\ConfigFMSLoaderInterface;
use common\models\TrafficType;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $lead_count_required
 * @property integer $execution_time
 * @property integer $is_urgent
 * @property string $lead_cost
 * @property integer $user_id
 * @property integer $executor_id
 * @property string $deadline_dt
 * @property string $acceptance_time
 * @property string $created_at
 * @property string $updated_at
 * @property integer $project_id
 * @property integer $status
 * @property integer $batch_id
 *
 * @property User $user
 * @property User $executor
 */
class Order extends \yii\db\ActiveRecord implements FMSStateInterface, FMSAuthInterface
{
    use ActivityTrait;

    use WsScenarioTrait;

    protected $wsScenarios = [
        self::EVENT_CHANGED_STATE => [
            WsCommandList::COMMAND_ORDER_CHANGED_STATE,
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @event common\components\StateEvent an event that is triggered after state has been changed
     */
    const EVENT_CHANGED_STATE = 'event_changed_state';

    /**
     * Переменная не в camelCase т к это "виртуальное поле" таблицы БД
     * @var int $batch_id
     */
    protected $batch_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'user_id', 'executor_id', 'project_id', 'acceptance_time', 'lead_count_required'], 'required'],
            [['description'], 'string'],
            [['lead_count_required', 'execution_time', 'is_urgent', 'user_id', 'executor_id', 'project_id', 'status'], 'integer'],
            ['status', 'default', 'value' => function($model, $attribute) {
                /**
                 * @var ConfigFMSLoaderInterface $config
                 */
                $config = Yii::$container->get(ConfigFMSLoaderInterface::class, [], [
                    'entityName' => 'Order',
                ]);
                $statusStateTable = $config->get('statusStateTable');
                $statuses = array_keys($statusStateTable);
                return $statusStateTable[$statuses[0]];
            }],
            [['lead_cost'], 'number'],
            [['acceptance_time', 'created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['executor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['executor_id' => 'id']],
            [['batch_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/order', 'ID'),
            'title' => Yii::t('app/order', 'Title'),
            'description' => Yii::t('app/order', 'Description'),
            'lead_count_required' => Yii::t('app/order', 'Lead Count Required'),
            'execution_time' => Yii::t('app/order', 'Execution Time'),
            'lead_cost' => Yii::t('app/order', 'Lead Cost'),
            'user_id' => Yii::t('app/order', 'User ID'),
            'executor_id' => Yii::t('app/order', 'Executor ID'),
            'acceptance_time' => Yii::t('app/order', 'Acceptance Time'),
            'created_at' => Yii::t('app/order', 'Created At'),
            'updated_at' => Yii::t('app/order', 'Updated At'),
            'project_id' => Yii::t('app/order', 'Project ID'),
            'batch_id' => Yii::t('app/order', 'Batch Id'),
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (!Yii::$app->user->isGuest && $insert) {
                if (!$this->user_id) {
                    $this->user_id = Yii::$app->user->id;
                }
                if (!$this->created_at) {
                    $this->created_at = $this->updated_at = gmdate('Y-m-d H:i:s');
                }
            }
            if (!$insert) {
                $this->updated_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * Get the created early batch id
     *
     * @return int
     */
    public function getBatch_id()
    {
        return $this->batch_id;
    }

    /**
     * Set batch id
     *
     * @param $value
     */
    public function setBatch_id($value)
    {
        $this->batch_id = $value;
    }

    /**
     * Associates the previously created package of documents with the project
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->batch_id) {
            $batch = Batch::findOne($this->batch_id);
            $batch->entity_id = $this->id;
            $batch->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Set the linked package of documents
     */
    public function afterFind()
    {
        $batch = Batch::find()->where('entity_id = :entity_id AND entity_type = :entity_type', [
            'entity_id' => $this->id,
            'entity_type' => Batch::ENTITY_TYPE_ORDER,
        ])->one();
        if ($batch) {
            $this->setBatch_id($batch->id);
        }
        parent::afterFind();
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] связанный пакет документов
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getBatch()
    {
        return $this->hasOne(Batch::className(), ['entity_id' => 'id']);
    }

    /**
     * @return OrderReviewQuery
     */
    public function getOrderReviews()
    {
        return $this->hasMany(OrderReview::className(), ['order_id' => 'id']);
    }

    /**
     * @return DisputQuery
     */
    public function getDisput()
    {
        return $this->hasOne(Disput::className(), ['order_id' => 'id']);
    }

    /**
     * @return PartAcceptedQuery
     */
    public function getPartAccepted()
    {
        return $this->hasOne(PartAccepted::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor()
    {
        return $this->hasOne(User::className(), ['id' => 'executor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @inheritdoc
     * @return OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] для связанных типов трафика
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getTrafficTypes() {
        return $this->hasMany(TrafficType::className(), ['id' => 'traffic_type_id'])->viaTable('{{%order_traffic_type}}', ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\Query
     */
    public function getNotifications()
    {

        return $this->hasMany(Notification::className(), ['entity_id' => 'id'])
            ->andWhere('entity_type = :entity_type', [
                'entity_type' => Notification::ENTITY_TYPE_ORDER_STATE_CHANGED,
            ]);
    }

    /**
     * Return \common\models\NotificationQuery (with status = \common\models\Notification::STATUS_NEW) for current user
     *
     * @return \common\models\NotificationQuery
     */
    public function getNewNotifications()
    {
        return $this->getNotifications()
            ->andWhere('[[status]] = :status', [
                'status' => Notification::STATUS_NEW
            ])
            ->andWhere(['recipient_id' => Yii::$app->user->id]);
    }

    /**
     * @return ActiveDataProvider
     */
    public static function search()
    {
        $query = self::find()
            ->my()
            ->orderDesc()
            ->with([
                'trafficTypes'
            ]);

        $paramFilterName = OrderHelper::getFilterName();
        $status = Yii::$app->request->getQueryParam($paramFilterName);
        $statuses = [];
        switch ($status) {
            case 'new' : {
                $statuses = OrderHelper::getActiveStatusList();
                break;
            }
            case 'disput' : {
                $statuses = OrderHelper::getDisputStatusList();
                break;
            }
            case 'closed' : {
                $statuses = OrderHelper::getClosedStatusList();
                break;
            }
            default : {
                break;
            }
        }
        if (!empty($statuses)) {
            $query->andWhere(['status' => $statuses]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);
        return $dataProvider;
    }

    /**
     * Check that this command can be executed in this state
     *
     * @param $command
     * @return bool
     */
    public function checkAccess($command)
    {
        return true;
    }

    /**
     * @return ConfigFMSLoaderInterface
     */
    protected function getFmsConfig()
    {
        /**
         * @var ConfigFMSLoaderInterface $fmsConfig
         */
        $fmsConfig = Yii::$container->get('common\interfaces\fms\ConfigFMSLoaderInterface', [], [
            'entityName' => 'Order',
        ]);
        return $fmsConfig;
    }

    /**
     * Returns string representing current FMS state
     *
     * @return string|null
     */
    public function getFMSCurrentState()
    {
        /**
         * @var ConfigFMSLoaderInterface $fmsConfig
         */
        $fmsConfig = $this->getFmsConfig();
        $statesTable = array_flip($fmsConfig->get('statusStateTable'));
        return isset($statesTable[$this->status]) ? $statesTable[$this->status] : null;
    }

    /**
     * Switches FMS state
     *
     * @param string $state
     * @return bool
     */
    public function switchFMSState($state)
    {
        /**
         * @var ConfigFMSLoaderInterface $fmsConfig
         */
        $fmsConfig = $this->getFmsConfig();
        $statesTable = $fmsConfig->get('statusStateTable');
        $oldState = $this->getFMSCurrentState();
        $status = $statesTable[$state];
        if ($status) {
            $this->status = $status;
            if ($this->save(false)) {
                $event = Yii::createObject([
                    'class' => StateEvent::className(),
                    'fromState' => $oldState,
                    'toState' => $state,
                    'sender' => $this,
                ]);
                $this->trigger(self::EVENT_CHANGED_STATE, $event);
                return true;
            }
        }
        return false;
    }

    /**
     * Возвращает [[\common\models\OrderMessageQuery]]
     *
     * @return OrderMessageQuery
     */
    public function getOrderMessages()
    {
        return $this->hasMany(OrderMessage::className(), ['order_id' => 'id']);
    }
}
