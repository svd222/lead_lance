<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%project_project_type}}".
 *
 * @property integer $project_id
 * @property integer $project_type_id
 */
class ProjectProjectType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_project_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'project_type_id'], 'required'],
            [['project_id', 'project_type_id'], 'integer'],
            [['project_id', 'project_type_id'], 'unique', 'targetAttribute' => ['project_id', 'project_type_id'], 'message' => 'The combination of Project ID and Project Type ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('app', 'Project ID'),
            'project_type_id' => Yii::t('app', 'Project Type ID'),
        ];
    }
}
