<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 27.05.17
 * @time: 18:39
 */
namespace common\models;

use Yii;
use common\interfaces\IList;

class GuideProjectStatus implements IList {

    /**
     * newly created
     */
    const STATUS_NEW = 1;

    /**
     * repaired by owner
     */
    const STATUS_REPAIRED = 2;

    /**
     * published
     */
    const STATUS_PUBLISHED = 3;

    /**
     * unpublished
     */
    const STATUS_UNPUBLISHED = 4;

    /**
     * unpublished by admin
     */
    const STATUS_UNPUBLISHED_BY_ADMIN = 5;

    /**
     * accomplished
     */
    const STATUS_ACCOMPLISHED = 6;

    /**
     * deleted by admin
     */
    const STATUS_REJECTED = 7;

    /**
     * executor is chosen
     */
    const STATUS_EXECUTOR_CHOSEN = 8;

    public static function getList()
    {
        return [
            self::STATUS_NEW => Yii::t('app/project', 'New'),
            self::STATUS_PUBLISHED => Yii::t('app/project', 'Published'),
            self::STATUS_UNPUBLISHED => Yii::t('app/project', 'Unpublished'),
            self::STATUS_UNPUBLISHED_BY_ADMIN => Yii::t('app/project', 'Unpublished by admin'),
            self::STATUS_REPAIRED => Yii::t('app/project', 'Repaired'),
            self::STATUS_ACCOMPLISHED => Yii::t('app/project', 'Accomplished'),
            self::STATUS_REJECTED => Yii::t('app/project', 'Rejected by admin'),
        ] ;
    }

    public static function getFilterName()
    {
        return 'projectStatus';
    }
}
