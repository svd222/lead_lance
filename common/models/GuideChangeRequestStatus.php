<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 09.02.17
 * @time: 20:49
 */
namespace common\models;

use Yii;
use common\interfaces\IList;

class GuideChangeRequestStatus implements IList
{
    const STATUS_NEW = 0;

    const STATUS_USED = 1;

    const STATUS_OUTDATED = 2;
    /**
     * @inheritdoc
     */
    public static function getList()
    {
        return [
            self::STATUS_NEW => Yii::t('app', 'New'),
            self::STATUS_USED => Yii::t('app', 'Used'),
            self::STATUS_OUTDATED => Yii::t('app', 'Outdated'),
        ];
    }
}