<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 03.07.17
 * @time: 18:10
 */
namespace common\models;

use Yii;
use yii\base\InvalidParamException;
use yii\db\ActiveRecord;
use yii\helpers\VarDumper;

/**
 * Class WsTransferObject Represent packed \yii\db\ActiveRecord object
 *
 * Usage:
 * ```php
 *  $wsTransferObject = \common\models\WsTransferObject::pack($obj, $command, $userId, $recipientId)
 * ```
 * WsTra
 * @package common\models
 */
class WsTransferObject
{
    /**
     * @var string class name of packed object
     */
    public $class;

    /**
     * @var int $userId
     */
    public $userId;

    /**
     * @var int $recipientId
     */
    public $recipientId;

    /**
     * @var int $id Id of packed object
     */
    public $id;

    /**
     * @var string $command
     */
    public $command;

    /**
     * @var [] $attributes
     */
    public $attributes;

    /**
     * Pack object (object extended from [[\yii\db\ActiveRecord]]) into new object (by cloning its public properties)
     * Used for save traffic
     *
     * @todo add id to obj (now id received from object, but need to check for main field)
     *
     * @param mixed $object
     * @param int $userId
     *
     * @return WsTransferObject
     *
     * @throws InvalidParamException
     */
    public static function pack($object, $command, $userId, $recipientId = 0, $exceptAttributes = [])
    {
        if (!$object instanceof ActiveRecord) {
            throw new InvalidParamException(get_class($object) . ' ' . Yii::t('app/ws', 'must be descendant of') . ' \yii\db\ActiveRecord');
        }
        $wsTransferObject = null;
        if (!empty($attrs = $object->attributes)) {
            $wsTransferObject = new self();
            $wsTransferObject->userId = $userId;
            $wsTransferObject->recipientId = $recipientId;
            $wsTransferObject->class = get_class($object);
            $wsTransferObject->id = $object->id;
            $wsTransferObject->command = $command;
            $wsTransferObject->attributes = [];

            $exceptAttributes = array_flip($exceptAttributes);
            foreach ($attrs as $k => $v) {
                if (!isset($exceptAttributes[$k])) {
                    $wsTransferObject->attributes[$k] = $v;
                }
            }
        }
        return $wsTransferObject;
    }
}