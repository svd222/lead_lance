<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Disput]].
 *
 * @see Disput
 */
class DisputQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Disput[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Disput|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
