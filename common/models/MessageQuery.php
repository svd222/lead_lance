<?php

namespace common\models;

use Yii;
use yii\web\ForbiddenHttpException;

/**
 * This is the ActiveQuery class for [[Message]].
 *
 * @see Message
 */
class MessageQuery extends \yii\db\ActiveQuery
{
    /**
     * @inheritdoc
     * @return Message[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Message|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Возвращает [[\common\models\MessageQuery]], для сообщений автором которых есть данный пользователь, с указанием получателя
     *
     * @param int $to_user_id id получателя сообщения
     * @param int $from_user_id id отправителя
     * @return $this
     */
    public function meTo($to_user_id, $from_user_id = 0, $sort = SORT_DESC)
    {
        $from_user_id = $from_user_id ? $from_user_id : Yii::$app->user->id;
        return $this->andWhere('[[to_user_id]] = :to_user_id AND [[from_user_id]] = :from_user_id', [
            'to_user_id' => $to_user_id,
            'from_user_id' => $from_user_id,
        ])->addOrderBy(['created_at' => $sort]);
    }

    /**
     * Возвращает [[\common\models\MessageQuery]], для сообщений автором которых есть данный пользователь, с указанием получателя
     *
     * @param int $from_user_id id отправителя сообщения
     * @param int $to_user_id id получателя сообщения
     * @return $this
     */
    public function toMeFrom($from_user_id, $to_user_id = 0, $sort = SORT_DESC)
    {
        $to_user_id = $to_user_id ? $to_user_id : Yii::$app->user->id;
        return $this->andWhere('[[from_user_id]] = :from_user_id AND [[to_user_id]] = :to_user_id', [
            'from_user_id' => $from_user_id,
            'to_user_id' => $to_user_id,
        ])->addOrderBy(['created_at' => $sort]);
    }

    /**
     * Возвращает [[\common\models\MessageQuery]], для сообщений автором которых есть данный пользователь или отправитель указанный в параметре $to_user_id
     * с указанием получателя или отправителя
     *
     * @param int $from_user_id id отправителя сообщения
     * @param int $to_user_id id получателя сообщения
     * @return $this
     */
    public function meToOrToMeFrom($to_user_id, $from_user_id = 0, $sort = SORT_DESC)
    {
        $from_user_id = $from_user_id ? $from_user_id : Yii::$app->user->id;
        return $this->andWhere('([[to_user_id]] = :to_user_id AND [[from_user_id]] = :from_user_id) OR ([[to_user_id]] = :from_user_id AND [[from_user_id]] = :to_user_id)', [
            'to_user_id' => $to_user_id,
            'from_user_id' => $from_user_id,
        ])->addOrderBy(['created_at' => $sort]);
    }

    /**
     * Возвращает [[\common\models\MessageQuery]] список всех сообщений по указанному пользователю, сгруппированный по этому пользователю и по получателю
     *
     * @param $userId
     * @param int $sort SORT_ASC | SORT_DESC
     * @return $this
     */
    public function listConversation($userId, $sort = SORT_DESC)
    {
        return $this
            ->groupBy(['from_user_id', 'to_user_id'])
            ->andWhere('[[from_user_id]] = :from_user_id OR [[to_user_id]] = :to_user_id', [
                'from_user_id' => $userId,
                'to_user_id' => $userId,
            ])
            ->addOrderBy(['created_at' => $sort]);
    }

    /**
     * Возвращает [[\common\models\MessageQuery]] для системных сообщений
     *
     * @return $this
     */
    public function isSystem()
    {
        return $this->andWhere('is_system = ' . Message::IS_SYSTEM);
    }

    /**
     * Возвращает [[\common\models\MessageQuery]] для сообщений посланных админом
     *
     * @return $this
     */
    public function isFromAdmin()
    {
        return $this->andWhere('is_from_admin = ' . Message::IS_FROM_ADMIN);
    }

    /**
     * Возвращает [[\common\models\MessageQuery]] для сообщений отфильтрованных по статусу (новое | прочитанное)
     */
    public function status($status = Message::STATUS_NEW)
    {
        return $this->andWhere('[[status]] = '. $status);
    }
}
