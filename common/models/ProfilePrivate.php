<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%profile_private}}".
 *
 * @property integer $user_id
 * @property string $first_name
 * @property string $last_name
 * @property integer $experience
 * @property integer $city_id
 * @property string $birth_date
 * @property string $phone
 * @property string $skype
 * @property string $telegram
 * @property string $whatsapp
 * @property string $viber
 * @property string $vk
 * @property string $twitter
 * @property string $created_at
 * @property string $updated_at
 *
 * @property int $birthdateDay
 * @property int $birthdateMonth
 * @property int $birthdateYear
 * @property string $emailChange
 * @property string $fullName
 *
 * @property City $city
 * @property User $user
 */
class ProfilePrivate extends \yii\db\ActiveRecord
{
    /**
     * @var int $birthdateMonth
     */
    protected $birthdateMonth;

    /**
     * @var int $birthdateYear
     */
    protected $birthdateYear;

    /**
     * @var int $birthdateDay
     */
    protected $birthdateDay;

    /**
     * @var string $name
     */
    protected $fullName;

    /**
     * @var string $email;
     */
    public $email;

    /**
     * @var int $country_id
     */
    protected $country_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile_private}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['experience', 'city_id'], 'integer'],
            [['birth_date', 'created_at', 'updated_at'], 'safe'],
            [['first_name', 'last_name', 'phone'], 'string', 'max' => 20],
            [['skype', 'telegram', 'whatsapp', 'viber', 'vk', 'twitter'], 'string', 'max' => 30],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['birthdateDay'], 'integer', 'min' => 1, 'max' => 31],
            [['birthdateMonth'], 'integer', 'min' => 1, 'max' => 12],
            [['birthdateYear'], 'integer'],
            ['birth_date', 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['email', 'country_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app/profile_private', 'User ID'),
            'first_name' => Yii::t('app/profile_private', 'First Name'),
            'last_name' => Yii::t('app/profile_private', 'Last Name'),
            'experience' => Yii::t('app/profile_private', 'Experience'),
            'city_id' => Yii::t('app/profile_private', 'City ID'),
            'birth_date' => Yii::t('app/profile_private', 'Birth Date'),
            'phone' => Yii::t('app/profile_private', 'Phone'),
            'skype' => Yii::t('app/profile_private', 'Skype'),
            'telegram' => Yii::t('app/profile_private', 'Telegram'),
            'whatsapp' => Yii::t('app/profile_private', 'Whatsapp'),
            'viber' => Yii::t('app/profile_private', 'Viber'),
            'vk' => Yii::t('app/profile_private', 'VK'),
            'twitter' => Yii::t('app/profile_private', 'twitter'),
            'created_at' => Yii::t('app/profile_private', 'Created At'),
            'updated_at' => Yii::t('app/profile_private', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    /**
     * Returns full name (virtual field)
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Parse birth_date to Y m d
     */
    public function afterFind()
    {
        if ($this->birth_date) {
            $birthDate = new \DateTime($this->birth_date);
            $this->birthdateYear = $birthDate->format('Y');
            $this->birthdateMonth = $birthDate->format('m');
            $this->birthdateDay = $birthDate->format('d');
        }
        if ($this->first_name || $this->last_name) {
            $first_name = $this->first_name ? $this->first_name : '';
            $last_name = $this->last_name ? $this->last_name : '';
            $this->fullName =  $first_name . ($last_name ? ' ' . $last_name : '');
        }
        if (!$this->email) {
            $this->email = $this->user->email;
        }
        if ($this->city_id) {
            $this->country_id = $this->city->region->country->id;
        }
        parent::afterFind();
    }

    /**
     * Before saving set the correct birth_date
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            if ($this->birthdateYear && $this->birthdateMonth && $this->birthdateDay) {
                $this->birth_date = $this->birthdateYear.'-'.$this->birthdateMonth.'-'.$this->birthdateDay.' 23:59:59';
            }
            $this->created_at = $this->updated_at = gmdate('Y-m-d H:i:s');
        } /*else {
            if ($this->birthdateYear && $this->birthdateMonth && $this->birthdateDay) {
                $this->birth_date = $this->birthdateYear.'-'.$this->birthdateMonth.'-'.$this->birthdateDay.' 23:59:59';
            }
            $this->updated_at = gmdate('Y-m-d H:i:s');
        }*/
        if (parent::beforeSave($insert)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Set Birthday month
     *
     * @param int $value
     */
    public function setBirthdateMonth($value)
    {
        $this->birthdateMonth = $value;
    }

    /**
     * Get Birthday month
     *
     * @return int
     */
    public function getBirthdateMonth()
    {
        return $this->birthdateMonth;
    }

    /**
     * Set Birthday day
     *
     * @param int $value
     */
    public function setBirthdateDay($value)
    {
        $this->birthdateDay = $value;
    }

    /**
     * Get Birthday day
     *
     * @return int
     */
    public function getBirthdateDay()
    {
        return $this->birthdateDay;
    }

    /**
     * Set Birthday year
     *
     * @param int $value
     */
    public function setBirthdateYear($value)
    {
        $this->birthdateYear = $value;
    }

    /**
     * Get Birthday year
     *
     * @return int
     */
    public function getBirthdateYear()
    {
        return $this->birthdateYear;
    }

    /**
     * Get country_id
     *
     * @return mixed
     */
    public function getCountry_id()
    {
        return $this->country_id;
    }

    /**
     * Set country_id
     *
     * @param $countryId
     */
    public function setCountry_id($countryId)
    {
        $this->country_id = $countryId;
    }

    public function getUserAvatar()
    {
        return $this->hasOne(UserAvatar::className(), ['user_id' => 'user_id']);
    }
}
