<?php

namespace common\models;

use common\interfaces\INotifyInfoProvider;
use Yii;
use common\traits\WsScenarioTrait;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%message}}".
 *
 * @property integer $id
 * @property string $message
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property integer $is_system
 * @property integer $is_from_admin
 * @property integer $status
 * @property string $created_at
 *
 * @property User $fromUser
 * @property User $toUser
 * @property int $batch_id
 */
class Message extends \yii\db\ActiveRecord implements INotifyInfoProvider
{
    use WsScenarioTrait;

    protected $wsScenarios = [
        ActiveRecord::EVENT_AFTER_INSERT => [
            WsCommandList::COMMAND_MESSAGE
        ]
    ];

    /**
     * @return int
     */
    public function getWsRecipientId()
    {
        return $this->to_user_id;
    }

    /**
     * new message
     */
    const STATUS_NEW = 1;

    /**
     * readed message
     */
    const STATUS_READED = 2;

    /**
     * message was sended by system
     */
    const IS_SYSTEM = 1;

    /**
     * message was sended by admin
     */
    const IS_FROM_ADMIN = 1;

    /**
     * Переменная не в camelCase т к это "виртуальное поле" таблицы БД
     * @var int $batch_id
     */
    protected $batch_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id'], 'required'],
            [['message'], 'string'],
            [['from_user_id', 'to_user_id', 'is_system', 'is_from_admin', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['batch_id'], 'integer'],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user_id' => 'id']],
            [['to_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/message', 'ID'),
            'message' => Yii::t('app/message', 'Message'),
            'from_user_id' => Yii::t('app/message', 'From User ID'),
            'to_user_id' => Yii::t('app/message', 'To User ID'),
            'is_system' => Yii::t('app/message', 'Is System'),
            'is_from_admin' => Yii::t('app/message', 'Is From Admin'),
            'status' => Yii::t('app/message', 'Status'),
            'created_at' => Yii::t('app/message', 'Created At'),
            'batch_id' => Yii::t('app/message', 'Batch Id'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function provideNotifyInfo()
    {
        return [
            'initiator_id' => Yii::$app->user->id,
            'recipient_id' => $this->to_user_id,
            'entity_type' => Notification::ENTITY_TYPE_MESSAGE,
            'entity_id' => $this->id,
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->status = static::STATUS_NEW;
                $this->created_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * Get the created early batch id
     *
     * @return int
     */
    public function getBatch_id()
    {
        return $this->batch_id;
    }

    /**
     * Set batch id
     *
     * @param $value
     */
    public function setBatch_id($value)
    {
        $this->batch_id = $value;
    }

    /**
     * Associates the previously created package of documents with the project
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->batch_id) {
            $batch = Batch::findOne($this->batch_id);
            $batch->entity_id = $this->id;
            $batch->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Set the linked package of documents
     */
    public function afterFind()
    {
        $batch = Batch::find()->where('entity_id = :entity_id AND entity_type = :entity_type', [
            'entity_id' => $this->id,
            'entity_type' => Batch::ENTITY_TYPE_MESSAGE,
        ])->one();
        if ($batch) {
            $this->setBatch_id($batch->id);
        }
        parent::afterFind();
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] связанный пакет документов
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getBatch()
    {
        return $this->hasOne(Batch::className(), ['entity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    /**
     * @inheritdoc
     * @return MessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageQuery(get_called_class());
    }

    /**
     * Set the status to [[\common\models\Message::STATUS_READED]]
     */
    public function readed()
    {
        $this->status = static::STATUS_READED;
        $this->update();
    }
}
