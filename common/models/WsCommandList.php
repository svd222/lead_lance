<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 17.07.17
 * @time: 14:23
 */

namespace common\models;

use common\interfaces\IList;

class WsCommandList implements IList
{
    /**
     * register user ws session
     */
    const COMMAND_REGISTER_SESSION = 'registerSession';

    /**
     * private message
     */
    const COMMAND_MESSAGE = 'message';

    /**
     * project_message
     */
    const COMMAND_PROJECT_MESSAGE = 'project_message';

    /**
     * action
     */
    const COMMAND_ACTION = 'action';

    /**
     * user refuse from project / user is denied project execution
     */
    const COMMAND_PROJECT_USER_REFUSE = 'project_user_refuse';

    /**
     * state of order has been changed
     */
    const COMMAND_ORDER_CHANGED_STATE = 'order_changed_state';

    /**
     * order message
     */
    const COMMAND_ORDER_MESSAGE = 'order_message';

    /**
     * order review
     */
    const COMMAND_ORDER_REVIEW = 'order_review';

    /**
     * Returns command available list
     *
     * @return array
     */
    public static function getList()
    {
        return [
            self::COMMAND_REGISTER_SESSION,
            self::COMMAND_MESSAGE,
            self::COMMAND_PROJECT_MESSAGE,
            self::COMMAND_ACTION,
            self::COMMAND_PROJECT_USER_REFUSE,
            self::COMMAND_ORDER_CHANGED_STATE,
            self::COMMAND_ORDER_MESSAGE,
            self::COMMAND_ORDER_REVIEW,
        ];
    }
}