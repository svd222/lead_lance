<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[WsSession]].
 *
 * @see WsSession
 */
class WsSessionQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ProAccount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProAccount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @return $this
     */
    public function last()
    {
        return $this->orderBy(['created_at' => SORT_DESC]);
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function byUserId($userId) {
        return $this
            ->where('user_id = :user_id', [
                'user_id' => $userId
            ]);
    }

    /**
     * @param int $resourceId
     * @return $this
     */
    public function byResourceId($resourceId) {
        return $this
            ->where('resource_id = :resource_id', [
                'resource_id' => $resourceId
            ]);
    }

    /**
     * @return $this
     */
    public function registered()
    {
        return $this->andWhere('[[status]] = :status', [
            'status' => WsSession::STATUS_REGISTERED
        ]);
    }

    /**
     * @return $this
     */
    public function notRegistered()
    {
        return $this->andWhere('[[status]] = :status', [
            'status' => WsSession::STATUS_NEW
        ]);
    }
}