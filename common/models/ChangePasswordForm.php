<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 01.06.17
 * @time: 1:11
 */

namespace common\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class ChangePasswordForm extends Model
{
    public $oldPassword;
    public $newPassword;
    public $retypePassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['newPassword', 'oldPassword', 'retypePassword'], 'required'],
            // password is validated by validatePassword()
            [['oldPassword', 'newPassword', 'retypePassword'], 'string', 'min' => 6],
            ['newPassword', 'compare', 'compareAttribute' => 'retypePassword'],
            [['oldPassword'], 'validateOldPassword'],
        ];
    }

    /**
     * Validates the old password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validateOldPassword($attribute, $params)
    {
        /**
         * @var User $user
         */
        $user = Yii::$app->user->identity;
        if (!Yii::$app->security->validatePassword($this->oldPassword, $user->password_hash))
        {
            $this->addError('oldPassword', Yii::t('app', 'Old password is incorrect'));
        }
    }
}
