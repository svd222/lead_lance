<?php

namespace common\models;

use common\helpers\OrderHelper;
use Yii;

/**
 * This is the ActiveQuery class for [[Order]].
 *
 * @see Order
 */
class OrderQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Order[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Order|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Returns orders initiated by (current or $userId) user or accepted by (current or $userId) user
     *
     * @return $this
     */
    public function my($userId = 0)
    {
        $userId = $userId ? $userId : Yii::$app->user->id;
        $whereCondition = '';
        if (User::isExecutor($userId)) {
            $whereCondition = 'executor_id = :user_id';
        } elseif (User::isCustomer($userId)) {
            $whereCondition = 'user_id = :user_id';
        }
        return $this->andWhere($whereCondition, [
            'user_id' => $userId
        ]);
    }

    public function active()
    {
        return $this->andWhere(['[[status]]' => OrderHelper::getActiveStatusList()]);
    }

    /**
     * @return $this
     */
    public function orderDesc()
    {
        return $this->addOrderBy(['id' => SORT_DESC]);
    }

    public function orderNotClosed()
    {
        return $this->andWhere(['!=', '{{%order}}.status', 9]);
    }
}
