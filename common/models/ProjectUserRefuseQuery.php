<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ProjectUserRefuse]].
 *
 * @see ProjectUserRefuse
 */
class ProjectUserRefuseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ProjectUserRefuse[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProjectUserRefuse|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param $userId
     * @return $this
     */
    public function byUser($userId)
    {
        return $this->andWhere([
            'refused_user_id' => $userId,
        ]);
    }

    /**     *
     * @param $projectId
     * @return $this
     */
    public function byProject($projectId)
    {
        return $this->andWhere([
            'project_id' => $projectId
        ]);
    }

    /**
     * @param $initiatorId
     * @return $this
     */
    public function byInitiator($initiatorId)
    {
        return $this->andWhere([
            'initiator_id' => $initiatorId
        ]);
    }
}
