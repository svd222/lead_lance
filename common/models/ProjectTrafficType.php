<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%project_traffic_type}}".
 *
 * @property integer $project_id
 * @property integer $traffic_type_id
 */
class ProjectTrafficType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_traffic_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'traffic_type_id'], 'required'],
            [['project_id', 'traffic_type_id'], 'integer'],
            [['project_id', 'traffic_type_id'], 'unique', 'targetAttribute' => ['project_id', 'traffic_type_id'], 'message' => 'The combination of Project ID and Traffic Type ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('app', 'Project ID'),
            'traffic_type_id' => Yii::t('app', 'Traffic Type ID'),
        ];
    }
}
