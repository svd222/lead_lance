<?php
/**
 * Created by PhpStorm.
 * @author: svd
 * @date: 13.12.16
 * @time: 9:22
 */
namespace common\models;

use Yii;
use dektrium\user\models\User as BaseUser;
use yii\base\Event;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * Class User
 *
 * @property string $upload_dir
 *
 * @package common\models
 */
class User extends BaseUser {

    /**
     * @var \yii\rbac\Assignment[] $userRoles
     */
    protected static $userRoles;

    public static $proAccounts = [];

    const ROLE_CUSTOMER = 'customer';

    const ROLE_EXECUTOR = 'executor';

    const UPLOAD_DIRECTORY = '@frontend/web/upload/user_content';

    const UPLOAD_WEB = '/upload/user_content';

    protected $userUploadDir;

    /**
     * return Active (not blocked) user
     *
     * @param int $user_id
     * @return AR instance | null
     */
    public static function getActiveUser($user_id) {
        return self::find()->where('id = :id AND blocked_at IS NULL')->params([
            ':id' => $user_id
        ])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['upload_dir'], 'string', 'max' => 50],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'upload_dir' => Yii::t('app/user', 'Upload directory'),
        ]);
    }

    public function getProAccount()
    {
        return $this->hasOne(ProAccount::className(), ['user_id' => 'id']);
    }

    public static function hasPro($userId)
    {
        //@todo wrap piece of code that requests of permission to cache
        if (!isset(self::$proAccounts[$userId])) {
            $userModel = static::find()
                ->where(['id' => $userId])
                ->active()
                ->with([
                        'proAccount' => function($query) {
                            /**
                             * @var ProAccountQuery $query
                             */
                            $query->active();
                            /*
                            $query->andWhere([
                                'status' => ProAccount::STATUS_ACTIVE,
                            ])
                            ->andWhere('(UNIX_TIMESTAMP(resumed_at) + (life_time * 60 * 60 * 24)) > UNIX_TIMESTAMP()');
                            */
                        }
                    ]
                )->one();
            self::$proAccounts[$userId] = $userModel && !empty($userModel->proAccount);
        }
        return self::$proAccounts[$userId];
    }


    /*public static function findByUsername($username)
    {
        return self::find()->where('username = :username')->params([
            ':username' => $username
        ])->one();
    }*/

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * @param $userId
     * @return \yii\rbac\Assignment[]
     */
    public static function getUserRoles($userId)
    {
        if (static::$userRoles === null || !isset(static::$userRoles[$userId])) {
            $authManager = Yii::$app->authManager;
            static::$userRoles[$userId] = $authManager->getAssignments($userId);
        }
        return static::$userRoles[$userId];
    }

    /**
     * @param $userId
     * @return bool
     */
    public static function isCustomer($userId)
    {
        $userRoles = self::getUserRoles($userId);
        return !empty($userRoles) && isset($userRoles['customer']);
    }

    /**
     * @param $userId
     * @return bool
     */
    public static function isExecutor($userId)
    {
        $userRoles = self::getUserRoles($userId);
        return !empty($userRoles) && isset($userRoles['executor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfilePrivate()
    {
        return $this->hasOne(ProfilePrivate::className(), ['user_id' => 'id']);
    }

    public static function genUniqDirName($userId)
    {
        return uniqid(substr($userId, 0, 16) . time(), true);
    }

    /**
     * Ensures that user upload dir (mainly used for profile cases) exists
     *
     * @param $userId
     */
    public function ensureUploadDirExists()
    {
        if (!$this->userUploadDir) {
            if (!$this->upload_dir) {
                $this->upload_dir = self::genUniqDirName($this->id);
                $this->update(true, ['upload_dir']);
            }

            $uploadDirName = $this->upload_dir;
            $this->userUploadDir = Yii::getAlias(self::UPLOAD_DIRECTORY . DIRECTORY_SEPARATOR . $uploadDirName);
            if (!file_exists($this->userUploadDir)) {
                FileHelper::createDirectory($this->userUploadDir, 0777);
            }
        }
        return $this->userUploadDir;
    }

    /**
     * @return OrderReviewQuery
     */
    public function getOrderReview()
    {
        return $this->hasMany(OrderReview::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\Query
     */
    public function getNotifications()
    {
        return $this->hasMany(Notification::className(), ['recipient_id' => 'id']);

    }

    /**
     * Return \common\models\NotificationQuery (with status = \common\models\Notification::STATUS_NEW) for current user
     *
     * @return \common\models\NotificationQuery
     */
    public function getNewNotifications()
    {
        return $this->getNotifications()
            ->andWhere('[[status]] = :status', [
                'status' => Notification::STATUS_NEW,
            ])
            ->andWhere(['recipient_id' => Yii::$app->user->id]);
    }
}
