<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 11.02.17
 * @time: 10:02
 */
namespace common\models;

use Yii;
use yii\base\Model;

class ProfileChangeEmail extends Model
{
    public $email;

    public function rules()
    {
        return [
            [['email'], 'validateEmail'],
        ];
    }

    public function validateEmail($attribute, $params)
    {
        $email = $this->email;
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->addError('phone', Yii::t('app/profile_change_email', 'Invalid email'));
        }
    }
}