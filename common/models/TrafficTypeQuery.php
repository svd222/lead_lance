<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[TrafficType]].
 *
 * @see TrafficType
 */
class TrafficTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TrafficType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TrafficType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
