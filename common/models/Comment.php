<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/25/2018
 * Time: 7:09 PM
 */

namespace common\models;

use Yii;
use yii\db\ActiveQuery;
use creocoder\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "{{%comment}}".
 *
 * @property integer $id
 * @property string $message
 * @property integer $item_id
 * @property string $type
 * @property integer $from_user_id
 * @property integer $status
 * @property integer $parent_id
 * @property string $created_at
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 *
 * @property User $fromUser
 */
class Comment extends \yii\db\ActiveRecord
{
    const COMMENT_TYPE_CASE = 'case';
    const COMMENT_TYPE_NEWS = 'news';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%comment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message', 'item_id', 'from_user_id', /*'tree', 'lft', 'rgt', 'depth'*/], 'required'],
            [['message'], 'string'],
            [['item_id', 'from_user_id', 'status', 'parent_id', 'tree', 'lft', 'rgt', 'depth'], 'integer'],
            [['created_at'], 'safe'],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app/comment', 'ID'),
            'message'      => Yii::t('app/comment', 'Message'),
            'item_id'      => Yii::t('app/comment', 'Profile Case ID'),
            'from_user_id' => Yii::t('app/comment', 'From User ID'),
            'status'       => Yii::t('app/comment', 'Status'),
            'parent_id'    => Yii::t('app/comment', 'Parent ID'),
            'created_at'   => Yii::t('app/comment', 'Created At'),
            'tree'         => Yii::t('app/comment', 'Tree'),
            'lft'          => Yii::t('app/comment', 'Lft'),
            'rgt'          => Yii::t('app/comment', 'Rgt'),
            'depth'        => Yii::t('app/comment', 'Depth'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    public function behaviors() {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::className(),
                'treeAttribute' => 'tree',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'depth',
            ],
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     * @return CommentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new CommentQuery(get_called_class());
    }

    /**
     * Returns comment count by item id
     *
     * @param $itemId
     */
    public function getCommentCountByItemId($itemId, $type)
    {
        return $this->find()->byItemId($itemId)->byType($type)->count();
    }

    public function getRootComments($id, $type)
    {
        $rootComments = Comment::find()
            ->roots()
            ->byItemId($id)
            ->byType($type)
            ->joinWith([
                'fromUser' => function ($query) {
                    /**
                     * @var ActiveQuery $query
                     */
                    $query->joinWith([
                        'profilePrivate' => function ($query2) {
                            /**
                             * @var ActiveQuery $query2
                             */
                            $query2->with([
                                /**
                                 * @var ActiveQuery $query3
                                 */
                                'userAvatar' => function ($query3) {
                                    $query3->andWhere('[[image]] IS NOT NULL and [[image]] <> \'\'');
                                }
                            ]);
                        }
                    ]);
                    $query->with([
                        'proAccount' => function($query) {
                            /**
                             * @var ProAccountQuery $query
                             */
                            $query->active();
                            /*
                            $query->andWhere([
                                'status' => ProAccount::STATUS_ACTIVE,
                            ])
                            ->andWhere('(UNIX_TIMESTAMP(resumed_at) + (life_time * 60 * 60 * 24)) > UNIX_TIMESTAMP()');
                            */
                        },
                    ]);
                }
            ])
            ->all();

        return $rootComments;
    }
}
