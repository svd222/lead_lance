<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_trace}}".
 *
 * @property integer $id
 * @property integer $initiator_id
 * @property string $initiator_role
 * @property integer $from_state
 * @property integer $to_state
 * @property string $description
 * @property integer $order_id
 */
class OrderTrace extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_trace}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initiator_id', 'from_state', 'to_state', 'order_id'], 'integer'],
            [['order_id'], 'required'],
            [['initiator_role', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/order_trace', 'ID'),
            'initiator_id' => Yii::t('app/order_trace', 'Initiator ID'),
            'initiator_role' => Yii::t('app/order_trace', 'Initiator Role'),
            'from_state' => Yii::t('app/order_trace', 'From State'),
            'to_state' => Yii::t('app/order_trace', 'To State'),
            'description' => Yii::t('app/order_trace', 'Description'),
            'order_id' => Yii::t('app/order_trace', 'Order ID'),
        ];
    }

    /**
     * @inheritdoc
     * @return OrderTraceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderTraceQuery(get_called_class());
    }
}
