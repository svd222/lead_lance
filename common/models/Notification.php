<?php

namespace common\models;

use InvalidArgumentException;
use Yii;
use common\helpers\ConversationHelper;

/**
 * This is the model class for table "{{%notification}}".
 *
 * @property integer $id
 * @property integer $initiator_id
 * @property integer $recipient_id
 * @property integer $type
 * @property integer $status
 * @property integer $entity_type
 * @property integer $entity_id
 * @property string $created_at
 * @property string $updated_at
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * new
     */
    const STATUS_NEW = 1;

    /**
     * received
     */
    const STATUS_RECEIVED = 2;

    /**
     * private message
     */
    const ENTITY_TYPE_MESSAGE = 1;

    /**
     * project message
     */
    const ENTITY_TYPE_PROJECT_MESSAGE = 2;

    /**
     * order
     */
    const ENTITY_TYPE_ORDER = 3;

    /**
     * user refuse from project / user is denied project execution
     */
    const ENTITY_TYPE_PROJECT_USER_REFUSE = 4;

    /**
     * order state changed
     */
    const ENTITY_TYPE_ORDER_STATE_CHANGED = 5;

    /**
     * order message
     */
    const ENTITY_TYPE_ORDER_MESSAGE = 6;

    /**
     * order review added by result of work by order
     */
    const ENTITY_TYPE_ORDER_REVIEW_ADDED = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['initiator_id', 'recipient_id', 'type', 'status', 'entity_type', 'entity_id'], 'integer'],
            [['recipient_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/ws_session', 'ID'),
            'initiator_id' => Yii::t('app/ws_session', 'Initiator ID'),
            'recipient_id' => Yii::t('app/ws_session', 'Recipient ID'),
            'type' => Yii::t('app/ws_session', 'Type'),
            'status' => Yii::t('app/ws_session', 'Status'),
            'entity_type' => Yii::t('app/ws_session', 'Entity Type'),
            'entity_id' => Yii::t('app/ws_session', 'Entity ID'),
            'created_at' => Yii::t('app/ws_session', 'Created At'),
            'updated_at' => Yii::t('app/ws_session', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = $this->updated_at = gmdate('Y-m-d H:i:s');
                $this->status = self::STATUS_NEW;
            } else {
                $this->updated_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     * @return NotificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NotificationQuery(get_called_class());
    }

    /**
     * Returns notifications by entity type
     *
     * @param Notification[] $notifications
     * @param int | array $entityType @see [[common\models\Notification::ENTITY_TYPE_*]]
     *
     * @return Notification[]
     */
    public static function selectNotificationsByEntityType(&$notifications, $entityType)
    {
        $ns = [];
        for ($i = 0; $i < count($notifications); $i++) {
            $notification = $notifications[$i];
            if (is_int($entityType)) {
                if ($notification->entity_type == $entityType) {
                    $ns[] = $notification;
                }
            } else {
                if (is_array($entityType)) {
                    foreach ($entityType as $eT) {
                        if ($notification->entity_type == $eT) {
                            $ns[] = $notification;
                        }
                    }
                }
            }
        }
        return $ns;
    }

    /**
     * Returns notifications by sender
     *
     * @param Notification[] $notifications
     * @param int|array $sender
     *
     * @return Notification[]
     */
    public static function selectNotificationsByInitiator(&$notifications, $initiatorId)
    {
        $ns = [];
        if (is_array($initiatorId)) {

            $notificationCount = count($notifications);
            for ($i = 0; $i < $notificationCount; $i++) {
                /**
                 * @var Notification $notification
                 */
                $notification = $notifications[$i];
                if (in_array($notification->initiator_id, $initiatorId)) {
                    $currentInitiatorId = $notification->initiator_id;
                    if (!isset($ns[$currentInitiatorId])) {
                        $ns[$currentInitiatorId] = [];
                    }
                    $ns[$currentInitiatorId][] = $notification;
                }
            }
        } else {
            if (intval($initiatorId)) {
                for ($i = 0; $i < count($notifications); $i++) {
                    /**
                     * @var Notification $notification
                     */
                    $notification = $notifications[$i];

                    if ($notification->initiator_id == $initiatorId) {
                        $ns[] = $notification;
                    }
                }
            } else {
                throw new InvalidArgumentException('Initiator id must be integer of array');
            }
        }
        return $ns;
    }

    /**
     * Update status to [[\common\models\Notification::STATUS_RECEIVED]]
     *
     * @param int $recipientId
     * @param int $entityType
     * @param int|array $initiatorId
     */
    public static function setStatusReceived($recipientId, $entityType, $initiatorId = 0)
    {
        $condition = [
            '[[status]]' => self::STATUS_NEW,
            '[[recipient_id]]' => $recipientId,
            '[[entity_type]]' => $entityType,
        ];
        if (!empty($initiatorId)) {
            $condition['[[initiator_id]]'] = $initiatorId;
        }
        return self::updateAll(
            ['status' => self::STATUS_RECEIVED],
            $condition
        );
    }
}
