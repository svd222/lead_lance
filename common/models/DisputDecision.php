<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%disput_decision}}".
 *
 * @property integer $id
 * @property integer $can_leave_review
 * @property integer $lead_to_pay_count
 * @property integer $disput_id
 *
 * @property Disput $disput
 */
class DisputDecision extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%disput_decision}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['can_leave_review', 'lead_to_pay_count', 'disput_id'], 'integer'],
            [['lead_to_pay_count', 'disput_id'], 'required'],
            [['disput_id'], 'exist', 'skipOnError' => true, 'targetClass' => Disput::className(), 'targetAttribute' => ['disput_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/disput_decision', 'ID'),
            'can_leave_review' => Yii::t('app/disput_decision', 'Can Leave Review'),
            'lead_to_pay_count' => Yii::t('app/disput_decision', 'Lead To Pay Count'),
            'disput_id' => Yii::t('app/disput_decision', 'Disput ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisput()
    {
        return $this->hasOne(Disput::className(), ['id' => 'disput_id']);
    }

    /**
     * @inheritdoc
     * @return DisputDecisionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DisputDecisionQuery(get_called_class());
    }
}
