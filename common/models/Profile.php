<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 27.01.17
 * @time: 11:44
 */
namespace common\models;

use dektrium\user\models\Profile as BaseProfile;

class Profile extends BaseProfile
{
    public function getProfileCases()
    {
        return $this->hasMany(ProfileCase::className(), ['user_id' => 'id']);
    }

    public function getProfilePrivate()
    {
        return $this->hasOne(ProfilePrivate::className(), ['user_id' => 'user_id']);
    }
}