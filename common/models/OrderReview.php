<?php

namespace common\models;

use common\traits\WsScenarioTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_review}}".
 *
 * @property integer $id
 * @property integer $emotion
 * @property string $review
 * @property integer $user_id
 * @property integer $order_id
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order $order
 */
class OrderReview extends \yii\db\ActiveRecord
{
    use WsScenarioTrait;

    const EMOTION_POSITIVE = 1;
    const EMOTION_NEGATIVE = 2;

    protected $wsScenarios = [
        ActiveRecord::EVENT_AFTER_INSERT => [
            WsCommandList::COMMAND_ORDER_REVIEW,
        ]
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_review}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['emotion', 'user_id', 'order_id'], 'integer'],
            [['review'], 'string'],
            [['order_id', 'emotion', 'review', 'user_id'], 'required'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/order_review', 'ID'),
            'emotion' => Yii::t('app/order_review', 'Emotion'),
            'review' => Yii::t('app/order_review', 'Review'),
            'user_id' => Yii::t('app/order_review', 'User ID'),
            'order_id' => Yii::t('app/order_review', 'Order ID'),
            'created_at' => Yii::t('app/order_review', 'Created at'),
            'updated_at' => Yii::t('app/order_review', 'Updated at'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     * @return OrderReviewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderReviewQuery(get_called_class());
    }
}
