<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%ws_session}}".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $resource_id
 * @property string $created_at
 * @property integer $status
 */
class WsSession extends \yii\db\ActiveRecord
{
    /**
     * session was created
     */
    const STATUS_NEW = 1;

    /**
     * session was registered
     */
    const STATUS_REGISTERED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ws_session}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'resource_id', 'status'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/ws_session', 'ID'),
            'user_id' => Yii::t('app/ws_session', 'User ID'),
            'resource_id' => Yii::t('app/ws_session', 'Resource ID'),
            'created_at' => Yii::t('app/ws_session', 'Created At'),
            'status' => Yii::t('app/ws_session', 'Status'),
        ];
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)) {
            if ($insert) {
                $this->status = self::STATUS_NEW;
                $this->created_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    public static function find()
    {
        return new WsSessionQuery(get_called_class());
    }
}
