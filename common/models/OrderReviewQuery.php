<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[OrderReview]].
 *
 * @see OrderReview
 */
class OrderReviewQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return OrderReview[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrderReview|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param int $forUserId
     * @return $this the query for order reviews left by collocutors
     */
    public function byCollocutor($forUserId = 0)
    {
        /**
         * @var OrderQuery $orderQuery
         */
        $orderQuery = Order::find();
        /**
         * @var Order[] $orderList
         */
        $orderList = $orderQuery->my($forUserId)->select('id');
        return $this->andWhere(['order_id' => $orderList]);
    }

    /**
     * @return $this
     */
    public function positive()
    {
        return $this->andWhere('emotion = :emotion', [
            'emotion' => OrderReview::EMOTION_POSITIVE
        ]);
    }

    /**
     * @return $this
     */
    public function negative()
    {
        return $this->andWhere('emotion = :emotion', [
            'emotion' => OrderReview::EMOTION_NEGATIVE
        ]);
    }
}
