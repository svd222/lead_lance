<?php

namespace common\models;

use common\traits\DeleteFileTrait;
use Yii;

/**
 * This is the model class for table "{{%image}}".
 *
 * @property integer $id
 * @property integer $batch_id
 * @property string $image
 * @property string $original_image
 * @property integer $owner_id
 * @property string $created_at
 * @property string $updated_at
 */
class Image extends \yii\db\ActiveRecord
{
    use DeleteFileTrait;

    /**
     * @var string $fileAttribute indicates on attribute that consist file name
     * to delete @see [[common\traits\DeleteFileTrait]]
     */
    public $fileAttribute = 'image';

    const UPLOAD_DIRECTORY = '@frontend/web/upload/images';

    const UPLOAD_WEB = '/upload/images';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['batch_id', 'owner_id'], 'integer'],
            [['image'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['image', 'original_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/image', 'ID'),
            'batch_id' => Yii::t('app/image', 'Batch ID'),
            'image' => Yii::t('app/image', 'Image'),
            'original_image' => Yii::t('app/image', 'Original Image'),
            'owner_id' => Yii::t('app/image', 'Owner ID'),
            'created_at' => Yii::t('app/image', 'Created At'),
            'updated_at' => Yii::t('app/image', 'Updated At'),
        ];
    }
}
