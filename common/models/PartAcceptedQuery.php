<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[PartAccepted]].
 *
 * @see PartAccepted
 */
class PartAcceptedQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PartAccepted[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PartAccepted|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
