<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[OrderMessage]].
 *
 * @see OrderMessage
 */
class OrderMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return OrderMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return OrderMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function sortDesc()
    {
        return $this->addOrderBy(['id' => SORT_DESC]);
    }
}
