<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 22.12.16
 * Time: 18:05
 */
namespace common\models;

use yii\base\Model;
use Yii;
use yii\helpers\ArrayHelper;

class ProjectFilterForm extends Model
{
    /**
     * @var int $range Возможные значения 0, 1, 2
     * 0 - все
     * 1 - мои
     * 2 - на модерации
     */
    public $range = self::RANGE_ALL;

    const RANGE_ALL = 0;

    const RANGE_MY = 1;

    /**
     * @var []int $project_type
     */
    public $project_type;

    /**
     * @var []int $traffic_type
     */
    public $traffic_type;

    /**
     * @var boolean $excludeMulti Исключать ли мульти проекты
     */
    public $excludeUrgent = false;

    /**
     * @var boolean $excludeMulti Исключать ли мульти проекты
     */
    public $excludeMulti = false;

    public $keyWords;

    public function rules()
    {
        return [
            [['range', 'project_type', 'traffic_type'], 'integer'],
            [['keyWords'], 'string'],
            ['range', 'validateRange'],
            [['excludeUrgent', 'excludeMulti'], 'boolean'],
            [['project_type', 'traffic_type'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * Фильтр по проектам
     * Проверяет  значение аттрибута range на соотвествие одному из двух значений: все / мои
     *
     * @param $attribute
     * @param $params
     */
    public function validateRange($attribute, $params) {
        if (!in_array($this->range, [self::RANGE_ALL, self::RANGE_MY])) {
            $this->addError('range', Yii::t('app', 'Range must be on of `all` or `my` values.'));
        }
    }

    public function afterValidate()
    {
        $keyWords = $this->keyWords;
        if(!empty($keyWords) && is_string($keyWords)) {
            $keyWords = preg_replace('/\s,/mis', '[---]', $keyWords);
            $keyWords = explode('[---]', $keyWords);
            $this->keyWords = join(' ', $keyWords);
        }
        parent::afterValidate();
    }
}