<?php

namespace common\models;

use common\traits\ActivityTrait;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use common\models\ProjectMessage;
use yii\helpers\StringHelper;
use common\helpers\ConversationHelper;
use common\interfaces\fms\ConfigFMSLoaderInterface;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $lead_count_required
 * @property integer $execution_time
 * @property integer $is_urgent
 * @property integer $is_auction
 * @property string $lead_cost
 * @property integer $user_id
 * @property integer $executor_id
 * @property string $deadline_dt
 * @property integer $is_multiproject
 * @property string $created_at
 * @property string $updated_at
 * @property string $currency
 * @property int $dateType
 * @property int $deadlineDtDay
 * @property int $deadlineDtMonth
 * @property int $deadlineDtYear
 * @property int $status
 * @property string $image
 * @property int $batch_id
 *
 *
 * @property User $user
 */
class Project extends \yii\db\ActiveRecord
{
    use ActivityTrait;

    /**
     * @var string $currency
     */
    protected $currency;

    /**
     * @var string $dateType
     */
    protected $dateType;

    /**
     * @var int $deadlineDtDay
     */
    protected $deadlineDtDay;

    /**
     * @var int $deadlineDtMonth
     */
    protected $deadlineDtMonth;

    /**
     * @var int $deadlineDtYear
     */
    protected $deadlineDtYear;

    /**
     * Переменная не в camelCase т к это "виртуальное поле" таблицы БД
     * @var int $batch_id
     */
    protected $batch_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'lead_count_required'], 'required'],
            [['description'], 'string'],
            [['lead_count_required', 'execution_time', 'is_urgent', 'is_auction', 'user_id', 'executor_id', 'is_multiproject', 'status'], 'integer'],
            /*['status', 'default', 'value' => function($model, $attribute) {
                return GuideProjectStatus::STATUS_NEW;
            }],*/
            [['lead_cost'], 'number'],
            [['lead_cost'], 'checkCost', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['deadline_dt', 'created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['image'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['currency'], 'string', 'length' => [2, 4]],
            [['currency'], 'default', 'value' => 'RUB'],
            [['dateType'], 'integer'],
            [['batch_id'], 'integer'],
            [['deadlineDtDay'], 'integer', 'min' => 1, 'max' => 31],
            [['deadlineDtMonth'], 'integer', 'min' => 1, 'max' => 12],
            [['deadlineDtYear'], 'integer', 'min' => 2017],
            [['dateType'], 'checkDateType'],
            ['deadline_dt', 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            ['execution_time', 'integer'],
            ['execution_time', 'checkExecutionTime', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                  => Yii::t('app/project', 'ID'),
            'title'               => Yii::t('app/project', 'Title'),
            'image'               => Yii::t('app/project', 'Image'),
            'description'         => Yii::t('app/project', 'Description'),
            'lead_count_required' => Yii::t('app/project', 'Lead Count Required'),
            'execution_time'      => Yii::t('app/project', 'Execution Time'),
            'is_urgent'           => Yii::t('app/project', 'Is Urgent'),
            'is_auction'          => Yii::t('app/project', 'Is Auction'),
            'lead_cost'           => Yii::t('app/project', 'Lead Cost'),
            'user_id'             => Yii::t('app/project', 'Owner ID'),
            'executor_id'         => Yii::t('app/project', 'Executor ID'),
            'deadline_dt'         => Yii::t('app/project', 'Deadline Dt'),
            'is_multiproject'     => Yii::t('app/project', 'Is Multiproject'),
            'created_at'          => Yii::t('app/project', 'Created At'),
            'updated_at'          => Yii::t('app/project', 'Updated At'),
            'currency'            => Yii::t('app/project', 'Currency'),
            'dateType'            => Yii::t('app/project', 'Date type'),
            'batch_id'            => Yii::t('app/project', 'Batch Id'),
            'status'              => Yii::t('app/project', 'Status'),
        ];
    }

    public function checkCost($attribute, $params)
    {
        if (!$this->is_auction) {
            if (!(isset($this->lead_cost) && is_numeric($this->lead_cost))) {
                $this->addError('lead_cost', Yii::t('app/project', 'Lead cost must be set'));
            }
        }
    }

    /**
     * Check execution time attribute if project is not urgent
     * else, if project is urgent check deadline_dt attribute
     */
    public function checkExecutionTime($attribute, $params)
    {
        if (!$this->is_urgent) {
            if (!(isset($this->execution_time) && is_numeric($this->execution_time))) {
                $this->addError('exexution_time', Yii::t('app/project', 'Execution time must be set'));
            }
        } else {
            if (!(isset($this->deadline_dt) && is_int($this->deadline_dt))) {
                $this->addError('deadline_dt', Yii::t('app/project', 'Deadline must be set'));
            }
        }
    }

    /**
     * Перед сохранением устанавливает:
     *      дату дедлайна если проект срочный
     *      id пользователя если запись новая
     * @param bool $insert Whether is new record
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->user_id = Yii::$app->user->id;
            $this->created_at = $this->updated_at = gmdate('Y-m-d H:i:s');
            $this->status = GuideProjectStatus::STATUS_NEW;
            if (!$this->is_urgent) {
                if ($this->execution_time && $this->dateType) {
                    $this->execution_time *= GuideDateType::getMultiplier($this->dateType);
                }
            }
        }
        if (parent::beforeSave($insert)) {
            if ($this->is_urgent) {
                $this->deadline_dt = $this->deadlineDtYear . '-' . $this->deadlineDtMonth . '-' . $this->deadlineDtDay . ' 23:59:59';
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the created early batch id
     *
     * @return int
     */
    public function getBatch_id()
    {
        return $this->batch_id;
    }

    /**
     * Set batch id
     *
     * @param $value
     */
    public function setBatch_id($value)
    {
        $this->batch_id = $value;
    }

    /**
     * Associates the previously created package of documents with the project
     *
     * @param bool  $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->batch_id) {
            $batch = Batch::find()
                ->where([
                    'id'        => $this->batch_id,
                    'entity_id' => null,
                ])->one();
            if ($batch) {
                $batch->entity_id = $this->id;
                $batch->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Set the linked package of documents
     */
    public function afterFind()
    {
        $batch = Batch::find()->where('entity_id = :entity_id AND entity_type = :entity_type', [
            'entity_id'   => $this->id,
            'entity_type' => Batch::ENTITY_TYPE_PROJECT,
        ])->one();
        if ($batch) {
            $this->setBatch_id($batch->id);
        }
        parent::afterFind();
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] связанный пакет документов
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getBatch()
    {
        return $this->hasOne(Batch::className(), ['entity_id' => 'id']);
    }

    /**
     * @param $userId
     * @return ActiveQuery
     */
    public function getProjectsUserRefuse()
    {
        return $this->hasMany(ProjectUserRefuse::className(), ['project_id' => 'id']);
    }

    /**
     * Get the currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set the currency
     *
     * @param $value
     */
    public function setCurrency($value)
    {
        $this->currency = $value;
    }

    /**
     * Get the date type
     *
     * @return string
     */
    public function getDateType()
    {
        return $this->dateType;
    }

    /**
     * Set the date type
     *
     * @param $value
     */
    public function setDateType($value)
    {
        $this->dateType = $value;
    }

    /**
     * Get deadaline day
     *
     * @param int $value
     */
    public function getDeadlineDtDay()
    {
        return $this->deadlineDtDay;
    }

    /**
     * Set deadaline day
     *
     * @param $value
     */
    public function setDeadlineDtDay($value)
    {
        $this->deadlineDtDay = $value;
    }

    /**
     * Get deadaline month
     *
     * @return int
     */
    public function getDeadlineDtMonth()
    {
        return $this->deadlineDtMonth;
    }


    /**
     * Set deadaline month
     *
     * @param int $value
     */
    public function setDeadlineDtMonth($value)
    {
        $this->deadlineDtMonth = $value;
    }

    /**
     * Get deadaline year
     *
     * @return int
     */
    public function getDeadlineDtYear()
    {
        return $this->deadlineDtYear;
    }

    /**
     * Set deadaline year
     *
     * @param int $value
     */
    public function setDeadlineDtYear($value)
    {
        $this->deadlineDtYear = $value;
    }

    /**
     * Checks the data type
     * Data type must be one of 0 | 1
     *
     * @param $attribute
     * @param $params
     */
    public function checkDateType($attribute, $params)
    {
        if (!GuideDateType::isDateType($this->dateType)) {
            $this->addError(Yii::t('app/project', 'Date type is invalid'));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExecutor()
    {
        return $this->hasOne(User::className(), ['id' => 'executor_id']);
    }

    /**
     * @inheritdoc
     * @return ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectMessages()
    {
        return $this->hasMany(ProjectMessage::className(), ['project_id' => 'id']);
    }


    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] для связанных типов проектов
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getProjectTypes()
    {
        return $this->hasMany(ProjectType::className(), ['id' => 'project_type_id'])->viaTable('{{%project_project_type}}', ['project_id' => 'id']);
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] для связанных типов трафика
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getTrafficTypes()
    {
        return $this->hasMany(TrafficType::className(), ['id' => 'traffic_type_id'])->viaTable('{{%project_traffic_type}}', ['project_id' => 'id']);
    }

    public static function filterSearch()
    {
        //$query = self::find()->where(['=', 'user_id', Yii::$app->user->id])->orderBy(['id' => SORT_DESC]);
        $query = self::find()->orderDesc()->my()->with([
            'trafficTypes',
            'projectTypes',
        ]);

        $paramFilterName = GuideProjectStatus::getFilterName();
        $status = Yii::$app->request->getQueryParam($paramFilterName);
        if (!empty($status)) {
            if (is_array($status)) {
                $query->andWhere(['in', '{{%project}}.status', $status]);
            } else {
                $query->andWhere('{{%project}}.status = :status', [
                    'status' => $status,
                ]);
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        return $dataProvider;
    }

    /**
     * Return true if project is active (published or new), otherwise false
     *
     * @return bool
     */
    public function isActive()
    {
        return $this->status == GuideProjectStatus::STATUS_PUBLISHED || $this->status == GuideProjectStatus::STATUS_NEW;
    }

    /**
     * Return true if project is published, otherwise false
     *
     * @return bool
     */
    public function isPublished()
    {
        return $this->status == GuideProjectStatus::STATUS_PUBLISHED;
    }

    /**
     * Return true if project is unpublished, otherwise false
     *
     * @return bool
     */
    public function isUnpublished()
    {
        return $this->status == GuideProjectStatus::STATUS_UNPUBLISHED;
    }

    /**
     * @return bool
     */
    public function canUnpublish()
    {
        return in_array($this->status, [
            GuideProjectStatus::STATUS_NEW,
            GuideProjectStatus::STATUS_PUBLISHED,
        ]);
    }

    /**
     * Return true if project is unpublished by admin, otherwise false
     *
     * @return bool
     */
    public function isUnpublishedByAdmin()
    {
        return $this->status == GuideProjectStatus::STATUS_UNPUBLISHED_BY_ADMIN;
    }

    /**
     * Return true if project is repaired, otherwise false
     *
     * @return bool
     */
    public function isRepaired()
    {
        return $this->status == GuideProjectStatus::STATUS_REPAIRED;
    }

    /**
     * Returns if project visible for owner
     *
     * @return bool
     */
    public function isVisibleForOwner()
    {
        return $this->isUnpublished() || $this->isActive() || $this->isRepaired() || $this->isUnpublishedByAdmin();
    }

    /**
     * divide $this->projectMessages into 2 arrays:
     * $projectMessages should consist root project messages and
     * $projectMessagesBranchNodes contains the remaining nodes of the branch
     *
     * @return array
     */
    public function getMessages()
    {
        $projectMessagesBranchNodes = [];
        $projectMessages = $this->getProjectAnswers();

        $pass = [];
        foreach ($projectMessages as $projectMessage) {
            $pass[] = $projectMessage->id;
        }

        foreach ($this->projectMessages as $pM) {
            if (!in_array($pM->id, $pass)) {
                /**
                 * @var ProjectMessage $pM
                 */
                $conversationId = ConversationHelper::getConversationId($pM->from_user_id, $pM->to_user_id);
                /*if (!isset($projectMessages[$conversationId])) {
                    $projectMessages[$conversationId] = $pM;
                    continue;
                }*/
                if (!isset($projectMessagesBranchNodes[$conversationId])) {
                    $projectMessagesBranchNodes[$conversationId] = [];
                }
                $projectMessagesBranchNodes[$conversationId][] = $pM;
            }
        }

        return [
            'projectMessages'            => $projectMessages,
            'projectMessagesBranchNodes' => $projectMessagesBranchNodes,
        ];
    }

    public function getProjectAnswers()
    {
        $projectMessages = [];
        $temp = [];
        foreach ($this->projectMessages as $pM) {
            $conversationId = ConversationHelper::getConversationId($pM->from_user_id, $pM->to_user_id);
            if (!isset($temp[$conversationId])) {
                $temp[$conversationId] = [];
            }
            $temp[$conversationId][] = $pM;
        }

        foreach ($temp as $k => $v) {
            $index = count($v) - 1;
            $projectMessages[$k] = $v[$index];
        }

        return $projectMessages;
    }

    /**
     * @param $projectId
     * @return mixed
     */
    public static function canEdit($projectId)
    {
        return self::find()->byId($projectId)->activeOrMine()->count();
    }

    /**
     * Returns string representing current FMS state
     *
     * @return string|null
     */
    public function getFMSCurrentState()
    {
        /**
         * @var ConfigFMSLoaderInterface $fmsConfig
         */
        $fmsConfig = $this->getFmsConfig();
        $statesTable = array_flip($fmsConfig->get('statusStateTable'));
        return isset($statesTable[$this->status]) ? $statesTable[$this->status] : null;
    }

    /**
     * @return ConfigFMSLoaderInterface
     */
    protected function getFmsConfig()
    {
        /**
         * @var ConfigFMSLoaderInterface $fmsConfig
         */
        $fmsConfig = Yii::$container->get('common\interfaces\fms\ConfigFMSLoaderInterface', [], [
            'entityName' => 'Project',
        ]);
        return $fmsConfig;
    }

    /**
     * Check if the user is chosen as an executor of the current project
     *
     * @param $userId
     * @return array|\yii\db\ActiveRecord[]
     */
    public function isUserExecutor($userId)
    {
        return $this->getOrder()
            ->orderNotClosed()
            ->andWhere([
                '{{%order}}.executor_id' => $userId,
            ])->all();
    }

    /**
     * Check if the an executor chosen for the current project
     *
     * @return mixed
     */
    public function isExecutorChosen()
    {
        return $this->getOrder()
            ->orderNotClosed()
            ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasMany(Order::className(), ['project_id' => 'id']);
    }
}
