<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%pro_account}}".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $suspended_at
 * @property string $resumed_at
 * @property integer $life_time
 * @property integer $user_id
 * @property integer $status
 * @property integer $by_certificate_id
 *
 * @property ProCertificate $byCertificate
 * @property User $user
 */
class ProAccount extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;

    const STATUS_SUSPENDED = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pro_account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'life_time', 'user_id'], 'required'],
            [['created_at', 'suspended_at', 'resumed_at'], 'safe'],
            [['life_time', 'user_id', 'status', 'by_certificate_id'], 'integer'],
            [['by_certificate_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProCertificate::className(), 'targetAttribute' => ['by_certificate_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/certificate', 'ID'),
            'created_at' => Yii::t('app/certificate', 'Created At'),
            'suspended_at' => Yii::t('app/certificate', 'Suspended At'),
            'resumed_at' => Yii::t('app/certificate', 'Resumed At'),
            'life_time' => Yii::t('app/certificate', 'Life Time'),
            'user_id' => Yii::t('app/certificate', 'User ID'),
            'status' => Yii::t('app/certificate', 'Status'),
            'by_certificate_id' => Yii::t('app/certificate', 'By Certificate ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getByCertificate()
    {
        return $this->hasOne(ProCertificate::className(), ['id' => 'by_certificate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return ProAccountQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProAccountQuery(get_called_class());
    }
}
