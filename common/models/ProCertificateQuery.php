<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ProCertificate]].
 *
 * @see ProCertificate
 */
class ProCertificateQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ProCertificate[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProCertificate|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
