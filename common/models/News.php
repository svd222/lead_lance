<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/19/2018
 * Time: 12:54 AM
 */

namespace common\models;

use common\traits\ActivityTrait;
use Ratchet\Wamp\Exception;
use Yii;

/**
 * This is the model class for table "{{%news}}".
 *
 * @property integer $id
 * @property string  $title
 * @property string  $text
 * @property integer $user_id
 * @property integer $status
 * @property string  $image
 * @property string  $tags
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @property User    $user
 */
class News extends \yii\db\ActiveRecord
{
    use ActivityTrait;

    const NEWS_STATUS_UNPUBLISHED = 1;
    const NEWS_STATUS_PUBLISHED = 2;

    const UPLOAD_DIRECTORY = '@frontend/web/upload/news_content';
    const UPLOAD_WEB = '/upload/news_content';

    public static function getStatusList()
    {
        return [
            self::NEWS_STATUS_UNPUBLISHED => Yii::t('app/news', 'Unpublished'),
            self::NEWS_STATUS_PUBLISHED   => Yii::t('app/news', 'Published'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text',], 'required'],
            [['text'], 'string'],
            [['user_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['image'], 'file'],
            [['tags'], 'string', 'max' => 255],
            [['status'], 'integer', 'max' => 2],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app/news', 'ID'),
            'title'      => Yii::t('app/news', 'Title'),
            'text'       => Yii::t('app/news', 'Text'),
            'user_id'    => Yii::t('app/news', 'User ID'),
            'image'      => Yii::t('app/news', 'Image'),
            'tags'       => Yii::t('app/news', 'Tags'),
            'created_at' => Yii::t('app/news', 'Created At'),
            'updated_at' => Yii::t('app/news', 'Updated At'),
            'status'     => Yii::t('app/news', 'Status'),
        ];
    }

    /**
     * @param bool $isNewItem Whether is new record
     * @return bool
     */
    public function beforeSave($isNewItem)
    {
        if ($isNewItem) {
            $this->created_at = gmdate('Y-m-d H:i:s');
            $currentUser = Yii::$app->getUser();
            if (!empty($currentUser)) {
                $this->user_id = $currentUser->id;
            }
        }
        $this->updated_at = gmdate('Y-m-d H:i:s');

        return parent::beforeSave($isNewItem);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     * @return NewsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    /**
     * @param mixed $id
     * @return null|static
     * @throws Exception
     */
    public static function findOne($id)
    {
        /** @var News $news */
        $news = parent::findOne($id);
        if (empty($news)) {
            throw new Exception('News was not found');
        }
        return $news;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasMany(Comment::className(), ['item_id' => 'id']);
//            ->andWhere([
//                'type' => Comment::COMMENT_TYPE_NEWS,
//            ]);
    }

}
