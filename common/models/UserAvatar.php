<?php

namespace common\models;

use common\traits\DeleteFileTrait;
use Yii;

/**
 * This is the model class for table "{{%user_avatar}}".
 *
 * @property integer $id
 * @property string $image
 * @property string $original_image
 * @property integer $user_id
 * @property string $created_at
 * @property string $updated_at
 */
class UserAvatar extends \yii\db\ActiveRecord
{
    use DeleteFileTrait;

    /**
     * @var string $fileAttribute indicates on attribute that consist file name
     * to delete @see [[common\traits\DeleteFileTrait]]
     */
    public $fileAttribute = 'image';

    const UPLOAD_DIRECTORY = '@frontend/web/upload/user_avatars';

    const UPLOAD_WEB = '/upload/user_avatars';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_avatar}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'required'],
            [['user_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['image', 'original_image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/user_avatar', 'ID'),
            'image' => Yii::t('app/user_avatar', 'Image'),
            'original_image' => Yii::t('app/user_avatar', 'Original Image'),
            'user_id' => Yii::t('app/user_avatar', 'User ID'),
            'created_at' => Yii::t('app/user_avatar', 'Created At'),
            'updated_at' => Yii::t('app/user_avatar', 'Updated At'),
        ];
    }

    public function getProfilePrivate()
    {
        return $this->hasOne(ProfilePrivate::className(), ['user_id' => 'user_id']);
    }
}
