<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[DisputDecision]].
 *
 * @see DisputDecision
 */
class DisputDecisionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DisputDecision[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DisputDecision|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
