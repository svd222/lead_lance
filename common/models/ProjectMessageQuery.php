<?php

namespace common\models;

use yii\helpers\ArrayHelper;

/**
 * This is the ActiveQuery class for [[ProjectMessage]].
 *
 * @see ProjectMessage
 */
class ProjectMessageQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ProjectMessage[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProjectMessage|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Возвращает [[\common\models\ProjectMessageQuery]], для сообщений автором которых есть данный пользователь или отправитель
     *
     * @param int $from_user_id id отправителя сообщения
     * @param int $to_user_id id получателя сообщения
     * @return $this
     */
    public function meToOrToMeFrom($to_user_id, $from_user_id)
    {
        return $this->andWhere('([[to_user_id]] = :to_user_id AND [[from_user_id]] = :from_user_id) OR ([[to_user_id]] = :from_user_id AND [[from_user_id]] = :to_user_id)', [
            'to_user_id' => $to_user_id,
            'from_user_id' => $from_user_id,
        ]);
    }

    public function byProject($pId)
    {
        return $this->andWhere('project_id = :project_id', [
            'project_id' => $pId
        ]);
    }

    public function refused($projectId)
    {
        $userIds = ProjectUserRefuse::find()
            ->byProject($projectId)
            ->select(['refused_user_id'])
            ->all();
        return $this->orWhere(
            [
                'or',
                ['in', 'from_user_id', ArrayHelper::map($userIds, 'refused_user_id', 'refused_user_id')],
                ['in', 'to_user_id', ArrayHelper::map($userIds, 'refused_user_id', 'refused_user_id')]
            ]
        );
    }

    public function notRefused($projectId)
    {
        $userIds = ProjectUserRefuse::find()
            ->byProject($projectId)
            ->select(['refused_user_id'])
            ->all();
        return $this->andWhere(
            [
                'and',
                ['not in', 'from_user_id', ArrayHelper::map($userIds, 'refused_user_id', 'refused_user_id')],
                ['not in', 'to_user_id', ArrayHelper::map($userIds, 'refused_user_id', 'refused_user_id')]
            ]
        );
    }
}
