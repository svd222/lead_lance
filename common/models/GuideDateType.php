<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 31.12.16
 * @time: 7:01
 */
namespace common\models;

use Yii;
use common\interfaces\IList;

class GuideDateType implements IList
{
    const TYPE_DAY = 1;

    const TYPE_WEEK = 2;

    const TYPE_MONTH = 4;

    /**
     * Проверяет корректность типа даты
     *
     * @param $dt
     * @return bool
     */
    public static function isDateType($dt)
    {
        $list = self::getList();
        return isset($list[$dt]);
    }

    /**
     * Возвращает множитель для преобразования даты в днях
     *
     * @param $dt
     * @return int
     */
    public static function getMultiplier($dt) {
        switch ($dt) {
            case self::TYPE_DAY: {
                return 1;
                break;
            }
            case self::TYPE_WEEK: {
                return 7;
                break;
            }
            case self::TYPE_MONTH: {
                return 30;
                break;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function getList()
    {
        return [
            self::TYPE_DAY => Yii::t('app', 'Day'),
            self::TYPE_WEEK => Yii::t('app', 'Week'),
            self::TYPE_MONTH => Yii::t('app', 'Month'),
        ];
    }
}