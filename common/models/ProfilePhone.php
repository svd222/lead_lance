<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 11.02.17
 * @time: 10:02
 */
namespace common\models;

use Yii;
use yii\base\Model;
use \libphonenumber\PhoneNumberUtil;
use \libphonenumber\PhoneNumberFormat;

class ProfilePhone extends Model
{
    public $phone;

    const REGION_RU = 'RU';

    const REGION_KZ = 'KZ';

    public function rules()
    {
        return [
            [['phone'], 'validatePhone'],
        ];
    }

    public function validatePhone($attribute, $params)
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $parsedNumber = $phoneUtil->parse($this->$attribute, self::REGION_RU);
        if (!$phoneUtil->isValidNumber($parsedNumber))
        {
            $this->addError('phone', Yii::t('app/profile_change_phone', 'Invalid phone number'));
        }
    }

    /**
     * format to E164
     *
     * @return bool
     */
    public static function format($phone, $format = PhoneNumberFormat::E164)
    {
        $phoneUtil = PhoneNumberUtil::getInstance();
        $phoneNumber = $phoneUtil->parse($phone, self::REGION_RU);
        $phone = $phoneUtil->format($phoneNumber, PhoneNumberFormat::E164);
        return $phone;
    }
}