<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ChangeValue]].
 *
 * @see ChangeValue
 */
class ChangeValueQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ChangeValue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ChangeValue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byGuid($guid)
    {
        return $this->andWhere('guid = :guid', ['guid' => $guid]);
    }

    public function byStatus($status) {
        return $this->andWhere('status = :status', ['status' => $status]);
    }

    public function byStatusNot($status) {
        return $this->andWhere('status <> :status', ['status' => $status]);
    }
}
