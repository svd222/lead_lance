<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/25/2018
 * Time: 6:54 PM
 */

namespace common\models;

use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * This is the ActiveQuery class for [[Comment]].
 *
 * @see Comment
 */
class CommentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Comment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Comment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Filter Comment by item_id
     *
     * @param $id
     * @return $this
     */
    public function byItemId($id)
    {
        return $this->andWhere('item_id = :item_id', [
            'item_id' => $id
        ]);
    }

    /**
     * Filter Comment by type
     *
     * @param $type
     * @return $this
     */
    public function byType($type)
    {
        return $this->andWhere('type = :type', [
            'type' => $type
        ]);
    }

    public function behaviors() {
        return [
            NestedSetsQueryBehavior::className(),
        ];
    }

}
