<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_traffic_type}}".
 *
 * @property integer $order_id
 * @property integer $traffic_type_id
 */
class OrderTrafficType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_traffic_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'traffic_type_id'], 'required'],
            [['order_id', 'traffic_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order ID'),
            'traffic_type_id' => Yii::t('app', 'Traffic Type ID'),
        ];
    }
}
