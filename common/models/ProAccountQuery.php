<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ProAccount]].
 *
 * @see ProAccount
 */
class ProAccountQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ProAccount[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ProAccount|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function active()
    {
        return //$this->addSelect(["(UNIX_TIMESTAMP(resumed_at) + (life_time * 60 * 60 * 24)) as end_date", '*'])
            $this->andWhere([
                'status' => ProAccount::STATUS_ACTIVE,
            ])
            ->andWhere('(UNIX_TIMESTAMP(resumed_at) + (life_time * 60 * 60 * 24)) > UNIX_TIMESTAMP()');
    }
}
