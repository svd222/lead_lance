<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%order_project_type}}".
 *
 * @property integer $order_id
 * @property integer $project_type_id
 */
class OrderProjectType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_project_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'project_type_id'], 'required'],
            [['order_id', 'project_type_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => Yii::t('app', 'Order ID'),
            'project_type_id' => Yii::t('app', 'Project Type ID'),
        ];
    }
}
