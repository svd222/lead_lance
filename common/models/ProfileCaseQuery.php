<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 11.07.17
 * @time: 18:44
 */

namespace common\models;

/**
 * This is the ActiveQuery class for [[ProfileCase]].
 *
 * @see ProfileCase
 */
class ProfileCaseQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Comment[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Comment|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * Filter Comment by profile_case_id
     *
     * @param $id
     * @return $this
     */
    public function byOwner($id)
    {
        return $this->andWhere('user_id = :user_id', [
            'user_id' => $id
        ]);
    }
}
