<?php

namespace common\models;

use common\interfaces\IList;
use Yii;

/**
 * This is the model class for table "{{%batch}}".
 *
 * @property integer $id
 * @property integer $entity_id
 * @property integer $entity_type
 * @property integer $owner_id
 * @property string $created_at
 * @property string $updated_at
 */
class Batch extends \yii\db\ActiveRecord implements IList
{
    const ENTITY_TYPE_PROJECT = 1;

    const ENTITY_TYPE_ORDER = 2;

    const ENTITY_TYPE_MESSAGE = 3;

    const ENTITY_TYPE_PROJECT_MESSAGE = 4;

    const ENTITY_TYPE_ORDER_MESSAGE = 5;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%batch}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entity_id', 'entity_type', 'owner_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = $this->updated_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/batch', 'ID'),
            'entity_id' => Yii::t('app/batch', 'Entity ID'),
            'entity_type' => Yii::t('app/batch', 'Entity Type'),
            'owner_id' => Yii::t('app/batch', 'Owner ID'),
            'created_at' => Yii::t('app/batch', 'Created At'),
            'updated_at' => Yii::t('app/batch', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getList()
    {
        return [
            self::ENTITY_TYPE_PROJECT => Yii::t('app/batch', 'Project'),
            self::ENTITY_TYPE_ORDER => Yii::t('app/batch', 'Order'),
            self::ENTITY_TYPE_MESSAGE => Yii::t('app/batch', 'Message'),
            self::ENTITY_TYPE_PROJECT_MESSAGE => Yii::t('app/batch', 'Project message'),
            self::ENTITY_TYPE_ORDER_MESSAGE => Yii::t('app/batch', 'Order message'),
        ];
    }

    public function getImages()
    {
        return $this->hasMany(Image::className(), ['batch_id' => 'id']);
    }
}
