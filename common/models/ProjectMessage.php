<?php

namespace common\models;

use yii\db\ActiveRecord;
use common\traits\WsScenarioTrait;

use Yii;

/**
 * This is the model class for table "{{%project_message}}".
 *
 * @property integer $id
 * @property string $message
 * @property integer $from_user_id
 * @property integer $to_user_id
 * @property integer $status
 * @property string $created_at
 * @property float $lead_cost
 * @property int $project_id
 *
 * @property int $batch_id
 * @property User $fromUser
 * @property User $toUser
 */
class ProjectMessage extends ActiveRecord
{
    use WsScenarioTrait;

    protected $wsScenarios = [
        ActiveRecord::EVENT_AFTER_INSERT => [
            WsCommandList::COMMAND_PROJECT_MESSAGE,
        ],
    ];

    /**
     * @return int
     */
    public function getWsRecipientId()
    {
        return $this->to_user_id;
    }

    /**
     * Переменная не в camelCase т к это "виртуальное поле" таблицы БД
     * @var int $batch_id
     */
    protected $batch_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_message}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_user_id', 'to_user_id', 'project_id'], 'required'],
            [['message'], 'string'],
            [['lead_cost'], 'double'],
            [['lead_cost'], 'required', 'when' => function ($model) {
                /**
                 * @var ProjectMessage $model
                 */
                /**
                 * @var Project $project
                 */
                $project = $model->project;
                $userId = Yii::$app->user->id;
                $count = ProjectMessage::find()
                    ->where([
                        'project_id' => $project->id,
                    ])
                    ->andWhere(['<', 'created_at', gmdate('Y-m-d H:i:s', time())])
                    ->andWhere('from_user_id = ' . $userId . ' OR to_user_id = ' . $userId)
                    ->count();
                return !$count && $project->is_auction;
            }],
            [['from_user_id', 'to_user_id', 'status', 'project_id'], 'integer'],
            [['created_at', 'project_id'], 'safe'],
            [['batch_id'], 'integer'],
            [['from_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['from_user_id' => 'id']],
            [['to_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['to_user_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/project_message', 'ID'),
            'message' => Yii::t('app/project_message', 'Message'),
            'from_user_id' => Yii::t('app/project_message', 'From User ID'),
            'to_user_id' => Yii::t('app/project_message', 'To User ID'),
            'status' => Yii::t('app/project_message', 'Status'),
            'lead_cost' => Yii::t('app/project_message', 'Lead Cost'),
            'project_id' => Yii::t('app/project_message', 'Project ID'),
            'created_at' => Yii::t('app/project_message', 'Created At'),
            'batch_id' => Yii::t('app/project_message', 'Batch Id'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function provideNotifyInfo()
    {
        return [
            'initiator_id' => Yii::$app->user->id,
            'recipient_id' => $this->to_user_id,
            'entity_type' => Notification::ENTITY_TYPE_PROJECT_MESSAGE,
            'entity_id' => $this->id,
        ];
    }

    /**
     * @return \yii\db\Query
     */
    public function getNotifications()
    {

        return $this->hasMany(Notification::className(), ['entity_id' => 'id'])
            ->andWhere('entity_type = :entity_type', [
                'entity_type' => Notification::ENTITY_TYPE_PROJECT_MESSAGE
            ]);
    }

    /**
     * Return \common\models\NotificationQuery (with status = \common\models\Notification::STATUS_NEW) for current user
     *
     * @return \common\models\NotificationQuery
     */
    public function getNewNotifications()
    {
        return $this->getNotifications()
            ->andWhere('[[status]] = :status', [
                'status' => Notification::STATUS_NEW
            ])
            ->andWhere(['recipient_id' => Yii::$app->user->id]);
    }

    /**
     * Get the created early batch id
     *
     * @return int
     */
    public function getBatch_id()
    {
        return $this->batch_id;
    }

    /**
     * Set batch id
     *
     * @param $value
     */
    public function setBatch_id($value)
    {
        $this->batch_id = $value;
    }

    /**
     * Associates the previously created package of documents with the project
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert && $this->batch_id) {
            $batch = Batch::findOne($this->batch_id);
            $batch->entity_id = $this->id;
            $batch->save();
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * Set the linked package of documents
     */
    public function afterFind()
    {
        $batch = Batch::find()->where('entity_id = :entity_id AND entity_type = :entity_type', [
            'entity_id' => $this->id,
            'entity_type' => Batch::ENTITY_TYPE_PROJECT_MESSAGE,
        ])->one();
        if ($batch) {
            $this->setBatch_id($batch->id);
        }
        parent::afterFind();
    }

    /**
     * Возвращает [[\yii\db\ActiveQueryInterface]] связанный пакет документов
     *
     * @return \yii\db\ActiveQueryInterface
     */
    public function getBatch()
    {
        return $this->hasOne(Batch::className(), ['entity_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromUser()
    {
        return $this->hasOne(User::className(), ['id' => 'from_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToUser()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }

    /**
     * @inheritdoc
     * @return ProjectMessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectMessageQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfileCases()
    {
        return $this->hasMany(ProfileCase::className(), ['id' => 'profile_case_id'])
            ->viaTable('{{%project_message_profile_case}}', ['project_message_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (empty($this->batch_id) && empty($this->message)) {
            return false;
        }

        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = gmdate('Y-m-d H:i:s');
            }
            return true;
        }
        return false;
    }
}
