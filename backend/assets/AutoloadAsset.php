<?php
namespace backend\assets;
use yii\web\AssetBundle;
use Yii;

/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 03.10.17
 * @time: 16:44
 */

class AutoloadAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public function init()
    {
        parent::init();
        //load controller specific js file (if module `frontend` or `backend`)
        $controller = Yii::$app->controller;
        $moduleId = $controller->module->id;
        $controllerId = $controller->id;
        $actionId = $controller->action->id;
        $jsExt = '.js';
        if ($moduleId == 'frontend' || $moduleId == 'backend') {
            if (file_exists($this->basePath . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR . $controllerId . $jsExt)) {
                $this->js[] = 'js/controller/'.$controllerId . $jsExt;
            }
            if (file_exists($this->basePath . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR . 'controller' . DIRECTORY_SEPARATOR . $controllerId . DIRECTORY_SEPARATOR . $actionId . $jsExt)) {
                $this->js[] = 'js/controller/'.$controllerId . DIRECTORY_SEPARATOR . $actionId . $jsExt;
            }
        }
    }

    public $jsOptions = [
        'defer' => true,
    ];
}