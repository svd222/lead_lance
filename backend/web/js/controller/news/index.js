$(document).ready(function (evt) {
    var hash = window.location.hash;
    hash = hash.substr(1);
    if (hash) {
        $('.news-index .nav-tabs li').removeClass('active');
        var tabContent = $('.news-index .tabs-content div.tab-content');
        tabContent.removeClass('hidden');
        tabContent.removeClass('show');
        tabContent.hide();
        var selectedTab = $('#' + hash);
        selectedTab.show();
        var selectedIndex = $('.news-index .tabs-content div.tab-content').index(selectedTab.get(0));
        $('.news-index .nav-tabs li').eq(selectedIndex).addClass('active');

        // remove `active` marker from pagination and mark first elm as `active` in all unselected tab content
        var contentTabs = ['index', 'published', 'unpublished'];
        contentTabs.forEach(function (id, index, array) {
            if (id != hash) {
                var items = $('#' + id + ' div.grid-view ul.pagination li');
                items.removeClass('active');
                var markeredAsActive = false;
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (markeredAsActive) {
                        break;
                    }
                    item = $(item);
                    if (item.hasClass('prev') || item.hasClass('next')) {
                        continue;
                    } else {
                        item.addClass('active');
                        markeredAsActive = true;
                    }
                }
            }
        })
    }

    $('.news-index .nav-tabs li').on('click', function (e) {
        $('.news-index .nav-tabs li').removeClass('active');
        $(this).addClass('active');
        var selectedTabIndex = $('.news-index .nav-tabs li').index(this);
        var tabContent = $('.news-index .tabs-content div.tab-content');
        tabContent.removeClass('hidden');
        tabContent.removeClass('show');
        tabContent.hide();
        $('.news-index .tabs-content div.tab-content').eq(selectedTabIndex).show();
    })
});
