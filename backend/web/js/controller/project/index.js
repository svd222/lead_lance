$(document).ready(function (evt) {
    $('.unpublish-project-by-admin, .reject-project-by-admin').on('click', function (e) {
        var projectId = this.dataset.id;
        var form = $('#ReasonRefuseProject');
        form.find('#reasonrefuseproject-project_id').val(projectId);
        form.find('#reasonrefuseproject-reason').val(null);
        if ($(this).hasClass('unpublish-project-by-admin')) {
            // unpublish-project-by-admin link clicked
            form.find('#reasonrefuseproject-type').val(1);
            form.prop('action', '/project/admin-unpublish?id=' + projectId);
        } else {
            // reject-project-by-admin link clicked
            form.find('#reasonrefuseproject-type').val(2);
            form.prop('action', '/project/admin-reject?id=' + projectId);
        }
        $('#project-unpublish-comment').modal();
        e.returnValue = false;
        return false;
    });

    //uncomment the following if you wanna to show project info when mouse over the title of the project (in project management panel)
    /*$('.project-title').on('mouseover', function (e) {
        var position = $(this).position();
        var content = $(this).find('.project-content');
        content.css({
            position: 'absolute',
            left: position.left + 'px',
            top: position.top + 'px',
            width: $(this).width()*2 + 'px',
            heght: 'auto',
        });
        content.removeClass('hidden');
    });

    $('.project-content').on('mouseout', function (e) {
        $(this).addClass('hidden');
    });*/

    var hash = window.location.hash;
    hash = hash.substr(1);
    if (hash) {
        $('.project-index .nav-tabs li').removeClass('active');
        var tabContent = $('.project-index .tabs-content div.tab-content');
        tabContent.removeClass('hidden');
        tabContent.removeClass('show');
        tabContent.hide();
        var selectedTab = $('#' + hash);
        selectedTab.show();
        var selectedIndex = $('.project-index .tabs-content div.tab-content').index(selectedTab.get(0));
        $('.project-index .nav-tabs li').eq(selectedIndex).addClass('active');

        // remove `active` marker from pagination and mark first elm as `active` in all unselected tab content
        var contentTabs = ['index', 'moderated', 'published', 'unpublished', 'accomplished', 'rejected'];
        contentTabs.forEach(function (id, index, array) {
            if (id != hash) {
                var items = $('#' + id + ' div.grid-view ul.pagination li');
                items.removeClass('active');
                var markeredAsActive = false;
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (markeredAsActive) {
                        break;
                    }
                    item = $(item);
                    if (item.hasClass('prev') || item.hasClass('next')) {
                        continue;
                    } else {
                        item.addClass('active');
                        markeredAsActive = true;
                    }
                }
            }
        })
    }

    $('.project-index .nav-tabs li').on('click', function (e) {
        $('.project-index .nav-tabs li').removeClass('active');
        $(this).addClass('active');
        var selectedTabIndex = $('.project-index .nav-tabs li').index(this);
        var tabContent = $('.project-index .tabs-content div.tab-content');
        tabContent.removeClass('hidden');
        tabContent.removeClass('show');
        tabContent.hide();
        $('.project-index .tabs-content div.tab-content').eq(selectedTabIndex).show();
    })
})