<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/19/2018
 * Time: 12:27 AM
 */

namespace backend\controllers;

use Ratchet\Wamp\Exception;
use Yii;
use common\widgets\GritterAlert;
use common\models\News;
use common\models\NewsSearch;
use yii\data\ArrayDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use vova07\imperavi\actions\GetImagesAction;

/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        $ret = ArrayHelper::merge([
            'images-get'   => [
                'class' => GetImagesAction::class,
                'url'   => Yii::getAlias(News::UPLOAD_DIRECTORY . News::UPLOAD_WEB),
                'path'  => Yii::$app->params['frontendDomain'] . News::UPLOAD_WEB,
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadAction',
                'url'   => Yii::getAlias(News::UPLOAD_DIRECTORY . News::UPLOAD_WEB),
                'path'  => Yii::$app->params['frontendDomain'] . News::UPLOAD_WEB,
            ],
        ], parent::actions());

        return $ret;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->searchBackendNews(Yii::$app->request->queryParams);
        /**
         * @var yii\db\QueryInterface $query
         */
        $query = $dataProvider->query;
        $get = Yii::$app->request->get();

        $pagination = [
            'pageSize' => Yii::$app->params['pagination']['pageSize'],
            'params'   => array_merge($get, ['#' => 'index']),
        ];

        $allModels = $query->all();
        $dataProvider = new ArrayDataProvider([
            'allModels'  => $allModels,
            'pagination' => $pagination,
        ]);

        $publishedModels = [];
        $unpublishedModels = [];
        foreach ($allModels as $m) {
            /**
             * @var News $m
             */
            if ($m->status == News::NEWS_STATUS_PUBLISHED) {
                $publishedModels[] = $m;
            } else if ($m->status == News::NEWS_STATUS_UNPUBLISHED) {
                $unpublishedModels[] = $m;
            }
        }
        $pagination['params'] = array_merge($get, ['#' => 'published']);
        $publishedDataProvider = new ArrayDataProvider([
            'allModels'  => $publishedModels,
            'pagination' => $pagination,
        ]);
        $pagination['params'] = array_merge($get, ['#' => 'unpublished']);
        $unpublishedDataProvider = new ArrayDataProvider([
            'allModels'  => $unpublishedModels,
            'pagination' => $pagination,
        ]);

        return $this->render('index', [
            'searchModel'             => $searchModel,
            'dataProvider'            => $dataProvider,
            'publishedDataProvider'   => $publishedDataProvider,
            'unpublishedDataProvider' => $unpublishedDataProvider,
        ]);
    }

    /**
     * Publishes a news
     *
     * @return \yii\web\Response
     * @throws Exception
     */
    public function actionPublish()
    {
        $id = (int)Yii::$app->request->get('id');
        if (empty($id)) {
            throw new Exception('Id is not found');
        }
        $news = $this->findModel($id);
        $news->status = News::NEWS_STATUS_PUBLISHED;
        $news->save();
        Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/news', 'The news was successfully published'));

        return $this->redirect('/news/index');
    }

    /**
     * Unpublishes a news
     *
     * @return \yii\web\Response
     * @throws Exception
     */
    public function actionUnpublish()
    {
        $id = (int)Yii::$app->request->get('id');
        if (empty($id)) {
            throw new Exception('Id is not found');
        }
        $news = $this->findModel($id);
        $news->status = News::NEWS_STATUS_UNPUBLISHED;
        $news->save();
        Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/news', 'The news was successfully unpublished'));

        return $this->redirect('/news/index');
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model'      => $this->findModel($id),
            'statusList' => News::getStatusList(),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $post = Yii::$app->request->post();

        if ($model->load($post)) {
            $previewImageFile = UploadedFile::getInstance($model, 'image');
            if (!empty($previewImageFile)) {
                $directory = Yii::getAlias(News::UPLOAD_DIRECTORY);

                $uid = uniqid(time(), true);
                $fileName = $uid;
                if (!empty($previewImageFile->extension)) {
                    $fileName .= '.' . $previewImageFile->extension;
                }

                $fullPath = $directory . DIRECTORY_SEPARATOR . $fileName;
                $previewImageFile->saveAs($fullPath);
                $model->image = $fileName;
            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model'      => $model,
                'statusList' => News::getStatusList(),
            ]);
        }
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $previewImageFile = UploadedFile::getInstance($model, 'image');
            if (!empty($previewImageFile)) {
                $directory = Yii::getAlias(News::UPLOAD_DIRECTORY);

                $uid = uniqid(time(), true);
                $fileName = $uid;
                if (!empty($previewImageFile->extension)) {
                    $fileName .= '.' . $previewImageFile->extension;
                }

                $fullPath = $directory . DIRECTORY_SEPARATOR . $fileName;
                $previewImageFile->saveAs($fullPath);
                $model->image = $fileName;
            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model'      => $model,
                'statusList' => News::getStatusList(),
            ]);
        }
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Upload user avatar
     *
     * @return string
     * @throws HttpException
     */
    public function actionUploadNewsPreviewImage()
    {
        $post = Yii::$app->request->post();
        $csrfParam = str_replace('-', '_', Yii::$app->request->csrfParam);

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new News();
        $previewImageFile = UploadedFile::getInstance($model, 'image');
        $directory = Yii::getAlias(News::UPLOAD_DIRECTORY);

        $uid = uniqid(time(), true);
        $fileName = $uid;
        if (!empty($previewImageFile->extension)) {
            $fileName .= '.' . $previewImageFile->extension;
        }

            $fullPath = $directory . DIRECTORY_SEPARATOR . $fileName;
            if ($previewImageFile->saveAs($fullPath)) {
                $path = News::UPLOAD_WEB . DIRECTORY_SEPARATOR . $fileName;
                return Json::encode([
                    'files' => [
                        [
                            'name' => $fileName,
                            'size' => $previewImageFile->size,
                            'url' => Yii::$app->params['frontendDomain'] . $path,
                            'thumbnailUrl' => $path,
                            'deleteUrl' => 'image-delete?name=' . $fileName,
                            'deleteType' => 'POST',
                        ],
                    ],
                ]);
//                if (!empty($post) && !empty($post['modelId'])) {
//                    $news = News::findOne($post['modelId']);
//                } else {
//                    $news = new News();
//                }
//                $news->scenario = News::SCENARIO_UPLOAD_PREVIEW_IMAGE;
//                $news->image = $fileName;
//                $news->user_id = Yii::$app->user->id;
//
//                if ($news->save()) {
//                    return Json::encode([
//                        'info' => [
//                            'id' => $news->id,
//                        ],
//                        'files' => [
//                            [
//                                'name' => $fileName,
//                                'size' => $previewImageFile->size,
//                                'url' => $fullPath,
//                                'deleteType' => 'POST',
//                            ],
//                        ],
//                    ]);
//                }
//                return '';
            }
    }
}
