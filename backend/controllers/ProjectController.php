<?php

namespace backend\controllers;

use common\models\GuideProjectStatus;
use common\models\ReasonRefuseProject;
use Yii;
use common\widgets\GritterAlert;
use common\models\Project;
use common\models\ProjectSearch;
use yii\data\ArrayDataProvider;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        /**
         * @var yii\db\QueryInterface $query
         */
        $query = $dataProvider->query;
        $get = Yii::$app->request->get();

        $pagination = [
            'pageSize' => Yii::$app->params['pagination']['pageSize'],
            'params' => array_merge($get, ['#' => 'index']),
        ];

        $allModels = $query->all();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'pagination' => $pagination,
        ]);

        $moderatedModels = $publishedModels = $unpublishedModels = $rejectedModels = $accomplishedModels = [];
        $moderatedConditionArray = [GuideProjectStatus::STATUS_NEW, GuideProjectStatus::STATUS_REPAIRED];
        $publishedConditionArray = [GuideProjectStatus::STATUS_PUBLISHED];
        $unpublisedConditionArray = [GuideProjectStatus::STATUS_UNPUBLISHED, GuideProjectStatus::STATUS_UNPUBLISHED_BY_ADMIN];
        $rejectedConditionArray = [GuideProjectStatus::STATUS_REJECTED];
        $accomplishedConditionArray = [GuideProjectStatus::STATUS_ACCOMPLISHED];
        foreach ($allModels as $m) {
            /**
             * @var Project $m
             */
            if(in_array($m->status, $moderatedConditionArray)) {
                $moderatedModels[] = $m;
            } else {
                if(in_array($m->status, $publishedConditionArray)) {
                    $publishedModels[] = $m;
                } else {
                    if(in_array($m->status, $unpublisedConditionArray)) {
                        $unpublishedModels[] = $m;
                    } else {
                        if(in_array($m->status, $rejectedConditionArray)) {
                            $rejectedModels[] = $m;
                        } else {
                            if(in_array($m->status, $accomplishedConditionArray)) {
                                $accomplishedModels[] = $m;
                            }
                        }
                    }
                }
            }
        }
        $pagination['params'] = array_merge($get, ['#' => 'moderated']);
        $moderatedDataProvider = new ArrayDataProvider([
            'allModels' => $moderatedModels,
            'pagination' => $pagination,
        ]);
        $pagination['params'] = array_merge($get, ['#' => 'published']);
        $publishedDataProvider = new ArrayDataProvider([
            'allModels' => $publishedModels,
            'pagination' => $pagination,
        ]);
        $pagination['params'] = array_merge($get, ['#' => 'unpublished']);
        $unpublishedDataProvider = new ArrayDataProvider([
            'allModels' => $unpublishedModels,
            'pagination' => $pagination,
        ]);
        $pagination['params'] = array_merge($get, ['#' => 'rejected']);
        $rejectedDataProvider = new ArrayDataProvider([
            'allModels' => $rejectedModels,
            'pagination' => $pagination,
        ]);
        $pagination['params'] = array_merge($get, ['#' => 'accomplished']);
        $accomplishedDataProvider = new ArrayDataProvider([
            'allModels' => $accomplishedModels,
            'pagination' => $pagination,
        ]);

        // split data in a dataProvider by
        $reasonRefuseProjectModel = new ReasonRefuseProject();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'reasonRefuseProjectModel' => $reasonRefuseProjectModel,
            'dataProvider' => $dataProvider,
            'moderatedDataProvider' => $moderatedDataProvider,
            'publishedDataProvider' => $publishedDataProvider,
            'unpublishedDataProvider' => $unpublishedDataProvider,
            'rejectedDataProvider' => $rejectedDataProvider,
            'accomplishedDataProvider' => $accomplishedDataProvider,
        ]);
    }

    public function actionAdminPublish()
    {
        $id = intval(Yii::$app->request->get('id'));
        $project = $this->findModel($id);
        $project->status = GuideProjectStatus::STATUS_PUBLISHED;
        $project->save();
        Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/project', 'The project was successfully published'));
        return $this->redirect('/project/index');
    }

    public function actionAdminUnpublish()
    {
        $id = intval(Yii::$app->request->get('id'));
        $post = Yii::$app->request->post();
        $reasonRefuseProject = new ReasonRefuseProject();
        $reasonRefuseProject->load($post);
        $redirectUrl = '/project/index#data-cell-'.$id;
        if ($reasonRefuseProject->save()) {
            $project = $this->findModel($id);
            $project->status = GuideProjectStatus::STATUS_UNPUBLISHED_BY_ADMIN;
            if ($project->save()) {
                Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/project', 'The project was successfully unpublished'));
                return $this->redirect($redirectUrl);
            } else {
                $message = 'Internal error occured';
                Yii::$app->session->setFlash(GritterAlert::TYPE_ERROR, Yii::t('app', $message));
                return $this->redirect($redirectUrl);
            }
        } else {
            $message = 'Internal error occured';
            Yii::$app->session->setFlash(GritterAlert::TYPE_ERROR, Yii::t('app', $message));
            return $this->redirect($redirectUrl);
        }
    }

    public function actionAdminReject()
    {
        $id = intval(Yii::$app->request->get('id'));
        $post = Yii::$app->request->post();
        $reasonRefuseProject = new ReasonRefuseProject();
        $reasonRefuseProject->load($post);
        $redirectUrl = '/project/index#data-cell-'.$id;
        if ($reasonRefuseProject->save()) {
            $project = $this->findModel($id);
            $project->status = GuideProjectStatus::STATUS_REJECTED;
            if ($project->save()) {
                Yii::$app->session->setFlash(GritterAlert::TYPE_SUCCESS, Yii::t('app/project', 'The project was successfully rejected'));
                return $this->redirect($redirectUrl);
            } else {
                $message = 'Internal error occured';
                Yii::$app->session->setFlash(GritterAlert::TYPE_ERROR, Yii::t('app', $message));
                return $this->redirect($redirectUrl);
            }
        } else {
            $message = 'Internal error occured';
            Yii::$app->session->setFlash(GritterAlert::TYPE_ERROR, Yii::t('app', $message));
            return $this->redirect($redirectUrl);
        }
    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
