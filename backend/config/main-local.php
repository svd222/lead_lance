<?php

$config = [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'KWPPHDbBT0INeCXhr9QZHNA2nr8VfThq',
        ],
    ],
];

if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];

    if(!isset($config['modules'])) {
        $config['modules'] = [];
    }
    /*$config['modules']['rbac'] = [
        'class' => 'dektrium\rbac\RbacWebModule',
    ];*/
}

return $config;
