<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $searchModel common\models\ProjectSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $reasonRefuseProjectModel \common\models\ReasonRefuseProject
 * @var \yii\data\ArrayDataProvider $moderatedDataProvider,
 * @var \yii\data\ArrayDataProvider $publishedDataProvider,
 * @var \yii\data\ArrayDataProvider $unpublishedDataProvider,
 * @var \yii\data\ArrayDataProvider $rejectedDataProvider,
 * @var \yii\data\ArrayDataProvider $accomplishedDataProvider
 */

$this->title = Yii::t('app/project', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--<p>
        <?/*= Html::a(Yii::t('app/project', 'Create Project'), ['create'], ['class' => 'btn btn-success']) */?>
    </p>-->
    <ul class="nav nav-tabs">
        <li class="active"><a href="#">Все</a></li>
        <li><a href="#">на модерации</a></li>
        <li><a href="#">опубликованные</a></li>
        <li><a href="#">отклоненные</a></li>
        <li><a href="#">снятые с публикации</a></li>
        <li><a href="#">завершённые</a></li>
    </ul>
    <div class="tabs-content">
        <div class="tab-content show" id="index">
            <?= $this->render('_tab_content', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
        <div class="tab-content hidden" id="moderated">
            <?= $this->render('_tab_content', [
                'dataProvider' => $moderatedDataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
        <div class="tab-content hidden" id="published">
            <?= $this->render('_tab_content', [
                'dataProvider' => $publishedDataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
        <div class="tab-content hidden" id="rejected">
            <?= $this->render('_tab_content', [
                'dataProvider' => $rejectedDataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
        <div class="tab-content hidden" id="unpublished">
            <?= $this->render('_tab_content', [
                'dataProvider' => $unpublishedDataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
        <div class="tab-content hidden" id="accomplished">
            <?= $this->render('_tab_content', [
                'dataProvider' => $accomplishedDataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
    </div>
</div>

<div id="project-unpublish-comment" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?= Yii::t('app/project', 'Type reason of project reject/unpublish'); ?></h4>
            </div>
            <?php
            $form = ActiveForm::begin([
                'id' => $reasonRefuseProjectModel->formName(),
                'options' => [
                    'class' => 'create-order-form project-crate-form-style'
                ],
                'enableAjaxValidation' => false,
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'template' => '{input}',
                    'options' => [
                        'tag' => false,
                    ],
                ]
            ]);
            ?>
            <div class="modal-body">
                <?= $form->field($reasonRefuseProjectModel, 'reason')->textarea([
                    'rows' => '8',
                    'style' => 'max-width:570px;',
                ]); ?>
            </div>
            <?= $form->field($reasonRefuseProjectModel, 'project_id')->hiddenInput(); ?>
            <?= $form->field($reasonRefuseProjectModel, 'type')->hiddenInput(); ?>
            <div class="modal-footer">
                <?= Html::submitButton(
                    'Submit',
                    [
                        'class' => 'btn btn-default',
                        /*'data-dismiss' => 'modal'*/
                    ])
                ?>
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
            </div>
        </div>
            <?php
                ActiveForm::end();
            ?>
    </div>
</div>
