<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 06.10.17
 * @time: 14:17
 */

use yii\grid\GridView;
use common\models\Project;
use common\models\GuideProjectStatus;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\ProjectSearch $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'tableOptions' => [
        'style' => 'table-layout: fixed;',
        'class' => 'table table-striped table-bordered',
        'id' => 'project-list',
    ],
    'columns' => [
        /*['class' => 'yii\grid\SerialColumn'],*/
        [
            'attribute' => 'id',
            'headerOptions' => [
                'style' => 'width:5%;',
            ],
            'contentOptions' => function($model, $key, $index, $column) {
                return [
                    'id' => 'data-cell-'.$model->id,
                ];
            },
        ],
        [
            'attribute' => 'title',
            'contentOptions' => function($model, $key, $index, $column) {
                return [
                    'data-project-id' => $model->id,
                    'class' => 'project-title',
                ];
            },

            'content' => function($model, $key, $index, $column) {
                /**
                 * @var Project $model
                 */
                /**
                 * @var User $user
                 */
                /*$user = $model->user;
                $userLink = Html::a($user->username, '/user/admin/update?id='.$user->id);
                $projectContent = '<div class="project-content hidden rounded">' .
                    '<h4 class="rounded col-md-offset-1 col-lg-offset-1">' . $model->title . '</h4>' .
                    '<div class="project-intro col-md-offset-1 col-lg-offset-1">' . $userLink . '&nbsp;&nbsp;' . $model->created_at . '</div>' .
                    '<div class="project-content-body col-md-offset-1 col-lg-offset-1">' . $model->description . '</div>' .
                    '</div>';*/

                return $model->title;// . $projectContent;
            }
        ],
        [
            'attribute' => 'description',
            'format' => 'ntext',
            'headerOptions' => [
                'style' => 'width:25%;',
            ],
        ],
        /*'description:ntext',*/
        'lead_count_required',
        [
            'attribute' => 'execution_time',
            'content' => function($model, $key, $index, $column) {
                /**
                 * @var Project $model
                 */
                return $model->execution_time . ' дней';
            }
        ],
        [
            'attribute' => 'status',
            'value' => function($model, $key, $index, $column) {
                /**
                 * @var Project $model
                 */
                $list = GuideProjectStatus::getList();
                return $list[$model->status];
            }
        ],
        // 'is_urgent',
        // 'is_auction',
        // 'lead_cost',
        // 'user_id',
        // 'executor_id',
        // 'deadline_dt',
        // 'is_multiproject',
        // 'created_at',
        // 'updated_at',
        // 'activited_at',
        // 'image',
        // 'status',

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {admin-publish} {admin-unpublish} {admin-reject}',
            'buttons' => [
                'view' => function($url, $model, $key) {
                    /**
                     * @var Project $model
                     */
                    $frontendDomain = Yii::$app->params['frontendDomain'];
                    $viewLink = Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $frontendDomain . '/project/view-offers/'.$model->id, [
                        'title' => Yii::t('yii', 'View'),
                        'aria-label' => Yii::t('yii', 'View'),
                        'data-pjax' => 0,
                    ]);
                    return $viewLink;
                },
                'admin-publish' => function ($url, $model, $key) {
                    $html = '';
                    /**
                     * @var Project $model
                     */
                    $publishLink = Html::a('<span class="glyphicon glyphicon-check"></span>', '/project/admin-publish?id='.$model->id, [
                        'title' => Yii::t('app/project', 'Publish'),
                        'aria-label' => Yii::t('app/project', 'Publish'),
                        'data-pjax' => 0,
                    ]);
                    $publishLinkCondition = $model->status == GuideProjectStatus::STATUS_NEW || $model->status == GuideProjectStatus::STATUS_UNPUBLISHED_BY_ADMIN;

                    if ($publishLinkCondition) {
                        $html = $publishLink;
                    }
                    return $html;
                },
                'admin-unpublish' => function ($url, $model, $key) {
                    $html = '';
                    /**
                     * @var Project $model
                     */

                    $unpublishLink = Html::a('<span class="glyphicon glyphicon-check red-icon"></span>', '/project/admin-unpublish?id='.$model->id, [
                        'title' => Yii::t('app/project', 'Unpublish'),
                        'aria-label' => Yii::t('app/project', 'Unpublish'),
                        'data-pjax' => 0,
                        'data-id' => $model->id,
                        'class' => 'unpublish-project-by-admin'
                    ]);
                    $unpublishLinkCondition = $model->status == GuideProjectStatus::STATUS_PUBLISHED;

                    if ($unpublishLinkCondition) {
                        $html = $unpublishLink;
                    }
                    return $html;
                },
                'admin-reject' => function ($url, $model, $key) {
                    $html = '';
                    /**
                     * @var Project $model
                     */

                    $rejectLink = Html::a('<span class="glyphicon glyphicon-remove red-icon"></span>', '/project/admin-reject?id='.$model->id, [
                        'title' => Yii::t('app/project', 'Reject'),
                        'aria-label' => Yii::t('app/project', 'Reject'),
                        'data-pjax' => 0,
                        'data-id' => $model->id,
                        'class' => 'reject-project-by-admin'
                    ]);
                    $rejectLinkCondition = in_array($model->status , [
                        GuideProjectStatus::STATUS_NEW,
                        GuideProjectStatus::STATUS_REPAIRED,
                    ]);

                    if ($rejectLinkCondition) {
                        $html = $rejectLink;
                    }
                    return $html;
                },
            ]
        ],
    ],
]); ?>