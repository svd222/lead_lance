<?php
/**
 * Created by PhpStorm
 * Author: Mikhail Zheludkov <mzheludkov@gmail.com>
 * Date: 01/19/2018
 * Time: 2:07 AM
 */

use yii\grid\GridView;
use common\models\News;
use yii\helpers\Html;

/**
 * @var yii\web\View                $this
 * @var common\models\NewsSearch    $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'tableOptions' => [
        'style' => 'table-layout: fixed;',
        'class' => 'table table-striped table-bordered',
        'id'    => 'news-list',
    ],
    'columns'      => [
        [
            'attribute'      => 'id',
            'headerOptions'  => [
                'style' => 'width:5%;',
            ],
            'contentOptions' => function ($model, $key, $index, $column) {
                return [
                    'id' => 'data-cell-' . $model->id,
                ];
            },
        ],
        [
            'attribute'      => 'title',
            'contentOptions' => function ($model, $key, $index, $column) {
                return [
                    'data-news-id' => $model->id,
                    'class'        => 'news-title',
                ];
            },

            'content' => function ($model, $key, $index, $column) {
                return $model->title;
            },
        ],
        [
            'attribute'     => 'text',
            'format'        => 'html',
            'headerOptions' => [
                'style' => 'width:25%;',
            ],
        ],
        [
            'attribute' => 'status',
            'value'     => function ($model, $key, $index, $column) {
                /**
                 * @var News $model
                 */
                $list = News::getStatusList();

                return $list[$model->status];
            },
        ],
        [
            'attribute' => 'image',
            'value'     => function ($model, $key, $index, $column) {
                return Yii::$app->params['frontendDomain'] . News::UPLOAD_WEB . DIRECTORY_SEPARATOR . $model->image;
            },
            'format'    => [
                'image',
                [
                    'width' => '200',
                ],
            ],
            'headerOptions' => [
                'style' => 'width: 210px;',
            ],
        ],
        [
            'attribute' => 'tags',
            'value'     => function ($model, $key, $index, $column) {
                return $model->tags;
            },
        ],
        [
            'attribute' => 'created_at',
            'value'     => function ($model, $key, $index, $column) {
                return $model->created_at;
            },
        ],
        [
            'attribute' => 'updated_at',
            'value'     => function ($model, $key, $index, $column) {
                return $model->created_at;
            },
        ],
        [
            'class'    => 'yii\grid\ActionColumn',
            'template' => '{view} {publish} {unpublish} {update} {delete}',
            'buttons'  => [
                'view'      => function ($url, $model, $key) {
                    $frontendDomain = Yii::$app->params['frontendDomain'];
                    $viewLink = Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $frontendDomain . '/news/view/' . $model->id, [
                        'title'      => Yii::t('yii', 'View'),
                        'aria-label' => Yii::t('yii', 'View'),
                        'data-pjax'  => 0,
                    ]);

                    return $viewLink;
                },
                'publish'   => function ($url, $model, $key) {
                    $html = '';
                    $publishLink = Html::a(
                        '<span class="glyphicon glyphicon-check"></span>',
                        '/news/publish?id=' . $model->id,
                        [
                            'title'      => Yii::t('app/news', 'Publish'),
                            'aria-label' => Yii::t('app/news', 'Publish'),
                            'data-pjax'  => 0,
                        ]
                    );
                    $publishLinkCondition = $model->status == News::NEWS_STATUS_UNPUBLISHED;

                    if ($publishLinkCondition) {
                        $html = $publishLink;
                    }

                    return $html;
                },
                'unpublish' => function ($url, $model, $key) {
                    $html = '';
                    $unpublishLink = Html::a(
                        '<span class="glyphicon glyphicon-check red-icon"></span>',
                        '/news/unpublish?id=' . $model->id, [
                            'title'      => Yii::t('app/news', 'Unpublish'),
                            'aria-label' => Yii::t('app/news', 'Unpublish'),
                            'data-pjax'  => 0,
                            'data-id'    => $model->id,
                        ]
                    );
                    $unpublishLinkCondition = $model->status == News::NEWS_STATUS_PUBLISHED;

                    if ($unpublishLinkCondition) {
                        $html = $unpublishLink;
                    }

                    return $html;
                },
                'update'    => function ($url, $model, $key) {
                    $html = Html::a(
                        '<span class="glyphicon glyphicon-pencil"></span>',
                        '/news/update?id=' . $model->id, [
                            'title'      => Yii::t('app/news', 'Update'),
                            'aria-label' => Yii::t('app/news', 'Update'),
                            'data-pjax'  => 0,
                            'data-id'    => $model->id,
                        ]
                    );

                    return $html;
                },
                'delete'    => function ($url, $model, $key) {
                    $html = Html::a(
                        '<span class="glyphicon glyphicon-remove red-icon"></span>',
                        [
                            'delete',
                            'id' => $model->id,
                        ],
                        [
                            'data' => [
                                'confirm' => Yii::t('app/news', 'Are you sure you want to delete this item?'),
                                'method' => 'post',
                            ],
                            'title'      => Yii::t('app/news', 'Delete'),
                            'aria-label' => Yii::t('app/news', 'Delete'),
                            'data-pjax'  => 0,
                            'data-id'    => $model->id,
                        ]);

                    return $html;
                },
            ],
        ],
    ],
]); ?>
