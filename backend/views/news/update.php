<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $statusList array */

?>

<?php
    $this->title = Yii::t('app/news', 'Update News') . ': ' . $model->title;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('app/news', 'News'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('app/news', 'Update');
?>
<div class="news-update">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <?= $this->render('_form', [
        'model'      => $model,
        'statusList' => $statusList,
    ]) ?>

</div>
