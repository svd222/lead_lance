<?php

use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $searchModel common\models\NewsSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var \yii\data\ArrayDataProvider $publishedDataProvider,
 * @var \yii\data\ArrayDataProvider $unpublishedDataProvider,
 */

    $this->title = Yii::t('app/news', 'News');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(
            Yii::t('app/news', 'Create News'),
            [
                    'create',
            ],
            [
                    'class' => 'btn btn-success',
            ]
        ); ?>
    </p>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#">Все</a></li>
        <li><a href="#">опубликованные</a></li>
        <li><a href="#">снятые с публикации</a></li>
    </ul>
    <div class="tabs-content">
        <div class="tab-content show" id="index">
            <?= $this->render('_tab_content', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
        <div class="tab-content hidden" id="published">
            <?= $this->render('_tab_content', [
                'dataProvider' => $publishedDataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
        <div class="tab-content hidden" id="unpublished">
            <?= $this->render('_tab_content', [
                'dataProvider' => $unpublishedDataProvider,
                'searchModel' => $searchModel,
            ]); ?>
        </div>
    </div>
</div>
