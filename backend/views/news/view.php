<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\News;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $statusList array */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app/news', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view">

    <h1>
        <?= Html::encode($this->title) ?>
    </h1>

    <p>
        <?= Html::a(Yii::t('app/news', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app/news', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app/news', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'text:html',
            [
                'attribute' => 'image',
                'value'     => Yii::$app->params['frontendDomain'] . News::UPLOAD_WEB . DIRECTORY_SEPARATOR . $model->image,
                'format'    => [
                    'image',
                    [
                        'width' => '293',
                    ],
                ],
            ],
            'tags',
            [
                'attribute' => 'status',
                'value'     => $statusList[$model->status],
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
