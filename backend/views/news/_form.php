<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget;
use yii\helpers\Url;
use common\models\News;

/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
/* @var $statusList array */


$this->registerCss(
    '
        .photo-news {
            background-color: #e8e8e8;
            color: white;
            font-size: 36px;
            line-height: 36px;
            min-height: 214px;
            overflow: hidden;
            padding-top: 70px;
            position: relative;
            text-align: center;
            width: 293px;
        }
    '
);
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(Widget::className(), [
        'settings' => [
            'lang'             => 'ru',
            'minHeight'        => 400,
            'imageManagerJson' => Url::to(['/news/images-get']),
            'imageUpload'      => Url::to(['/news/image-upload']),
            'plugins'          => [
                'fullscreen',
                'imagemanager',
                'table',
            ],
        ],
    ]); ?>

    <div class="photo-news div-inline">
        <?php if (!empty($model->image)): ?>
            <img src="<?= Yii::$app->params['frontendDomain'] . News::UPLOAD_WEB . DIRECTORY_SEPARATOR . $model->image; ?>" alt="LeadLance" width="293">
        <?php else: ?>
            <p class="photo-bold" >LeadLance</p>
            <p class="photo-light">(no photo)</p>
        <?php endif; ?>
    </div>

    <?= $form->field($model, 'image')->fileInput() ?>
    <input name="News[image]" value="<?= $model->image; ?>" type="hidden">

    <?= $form->field($model, 'tags')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->dropDownList($statusList); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app/news', 'Create') : Yii::t('app/news', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
