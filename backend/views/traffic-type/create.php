<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TrafficType */

$this->title = Yii::t('app', 'Create Traffic Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Traffic Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="traffic-type-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
